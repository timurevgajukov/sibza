<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-body">
            <div class="form-group">
                <input class="form-control" type="text" name="title" id="title" required
                       placeholder="${t.g("Введите название аудио")}"
                       <c:if test="${audio != null}">value="${audio.title}"</c:if>>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="resource">
                    <option value="0" disabled selected>${t.g("Привяжите ресурс")}</option>
                    <c:forEach items="${resources}" var="resource">
                        <option value="${resource.id}"<c:if
                                test="${letter.resources.size() > 0 && letter.resources.get(0).id == resource.id}"> selected</c:if>>
                                ${resource.fileName}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/audio" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>