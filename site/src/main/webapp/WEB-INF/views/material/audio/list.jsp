<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${security.adminOrEditor}">
    <a href="/audio/add" class="btn btn-info">${t.g("Добавить")}</a><br/><br/>
</c:if>

<c:choose>
    <c:when test="${audioList != null && audioList.size() != 0}">
        <t:insertAttribute name="audio-list" defaultValue="/WEB-INF/views/nifty/audio/audio-list.jsp" />
    </c:when>
    <c:otherwise>
        <div class="panel">
            <div class="panel-body">${t.g("Список аудио данных пуст")}</div>
        </div>
    </c:otherwise>
</c:choose>