<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${security.adminOrEditor}">
    <a href="/numerals/add" class="btn btn-info">${t.g("Добавить")}</a><br/><br/>
</c:if>

<c:choose>
    <c:when test="${numerals != null && numerals.size() != 0}">
        <c:forEach items="${numerals}" var="numeral">
            <div class="col-md-4 col-sm-6">
                <div class="panel">
                    <div class="panel-header">
                        <c:if test="${security.adminOrEditor}">
                            <div class="panel-control">
                                <a href="/numerals/edit/${numeral.id}" class="btn">
                                    <i class="fa fa-pencil text-light"></i>
                                </a>
                                <a href="/numerals/delete/request/${numeral.id}" class="btn">
                                    <i class="fa fa-trash text-light"></i>
                                </a>
                            </div>
                        </c:if>
                    </div>
                    <div class="panel-body text-center">
                        <c:if test="${security.adminOrEditor}"><br /></c:if>
                        <span class="text-5x">${numeral.number}</span><br/>
                        <span class="text-2x" style="color: #c9c9c9">${numeral.word.word}</span><br /><br />
                    </div>
                    <div class="panel-footer text-center" style="padding: 0px; height: 38px;">
                        <c:if test="${numeral.word.resources != null && numeral.word.resources.size() != 0}">
                            <audio src="/upload/${numeral.word.resources.get(0).id}/${numeral.word.resources.get(0).fileName}"
                                   id="audio-${numeral.word.resources.get(0).id}" preload="none">
                                <a href="/upload/${numeral.word.resources.get(0).id}/${numeral.word.resources.get(0).fileName}">
                                    <span class="text-2x"><i class="fa fa-volume-up"></i></span>
                                </a>
                            </audio>
                            <button class="audio-control btn btn-primary btn-block btn-icon icon-2x fa fa-play"
                                    data-id="${numeral.word.resources.get(0).id}" data-played="0"></button>
                        </c:if>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        ${t.g("Список чисел пуст")}
    </c:otherwise>
</c:choose>
