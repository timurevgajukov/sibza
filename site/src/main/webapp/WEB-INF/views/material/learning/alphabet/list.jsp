<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${security.adminOrEditor}">
    <a href="/alphabet/add" class="btn btn-info">${t.g("Добавить")}</a><br/><br/>
</c:if>

<c:choose>
    <c:when test="${alphabet != null && alphabet.letters != null && alphabet.letters.size() != 0}">
        <c:forEach items="${alphabet.letters}" var="letter">
            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <c:if test="${security.adminOrEditor}">
                            <ul class="actions">
                                <li class="dropdown">
                                    <a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="/alphabet/edit/${letter.id}">${t.g("Редактирование")}</a></li>
                                        <li><a href="/alphabet/delete/request/${letter.id}">${t.g("Удаление")}</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </c:if>
                    </div>
                    <div class="card-body text-center">
                        <h1>${letter.letter}</h1>
                        <h4 style="color: #c9c9c9">${letter.lowLetter}</h4><br />

                        <div id="words-${letter.id}"></div>

                        <div id="footer-${letter.id}"></div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        ${t.g("Алфавит пуст")}
    </c:otherwise>
</c:choose>

<script type="text/plain" id="tmpl_audio">
    <audio src="/upload/{{=it.id}}/{{=it.fileName}}" id="audio-{{=it.id}}" preload="none">
        <a href="/upload/{{=it.id}}/{{=it.fileName}}">
            <span class="text-2x"><i class="fa fa-volume-up"></i></span>
        </a>
    </audio>
    <button class="audio-control btn btn-primary btn-block waves-effect"
        data-id="{{=it.id}}" data-played="0">
            Послушать
    </button>
</script>
<script type="text/plain" id="tmpl_words">
    <div id="slide-{{=it.id}}" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
        {{~it.words :value:index}}
            <li{{? index === 0}} class="active"{{?}} data-slide-to="{{=index}}" data-target="#slide-{{=it.id}}"></li>
        {{~}}
        </ol>

        <div class="carousel-inner text-center" role="listbox">
        {{~it.words :value:index}}
            <div class="item{{? index === 0}} active{{?}}">
                {{? value.resources[0]}}
                <audio src="/upload/{{=value.resources[0].id}}/{{=value.resources[0].fileName}}"
                    id="audio-{{=value.resources[0].id}}" preload="none">
                    <a href="/upload/{{=value.resources[0].id}}/{{=value.resources[0].fileName}}">
                        <i class="fa fa-volume-up"></i>
                    </a>
                </audio>
                <span class="audio-control fa fa-play text-success"
                    data-id="{{=value.resources[0].id}}" data-played="0"></span>&nbsp;
                {{?}}
                <span style="color: #c9c9c9;">
                    {{=value.word}}{{? value.translates[0]}} &mdash; {{=value.translates[0].word}}{{?}}
                </span>
            </div>
        {{~}}
        </div>

        <a class="carousel-control left" role="button" data-slide="prev" href="#slide-{{=it.id}}">
            <span class="zmdi zmdi-chevron-left"></span>
        </a>
        <a class="carousel-control right" role="button" data-slide="next" href="#slide-{{=it.id}}">
            <span class="zmdi zmdi-chevron-right"></span>
        </a>
   </div>
</script>

<script>
    $(document).ready(function() {
        // получаем данные по озвучке букв
        $.post('/alphabet/letters/resources/list', function(list) {
            if (list === undefined || list.length == 0) {
                return;
            }

            var tmpl = doT.template($('#tmpl_audio').html());
            $.each(list, function() {
                var letter = this;
                if (letter.resources.length != 0) {
                    var resource = letter.resources[0];
                    $('#footer-' + letter.id).html(tmpl(resource));
                }
            });
        });

        // получаем данные по примерам слов
        $.post('/alphabet/letters/words/list', function(list) {
            if (list === undefined || list.length == 0) {
                return;
            }

            var tmpl = doT.template($('#tmpl_words').html());
            $.each(list, function() {
                var letter = this;
                if (letter.words.length != 0) {
                    $('#words-' + letter.id).html(tmpl(letter));
                } else {
                    $('#words-' + letter.id).html("<br /><br /><br />");
                }
            });
        });
    });
</script>