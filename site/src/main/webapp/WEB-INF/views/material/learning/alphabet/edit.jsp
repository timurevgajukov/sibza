<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link href="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">

<form method="post">
    <div class="panel">
        <div class="panel-body">
            <div class="form-group">
                <input class="form-control" type="text" name="letter" id="letter" required
                       placeholder="${t.g("Введите букву алфавита")}"
                       <c:if test="${letter != null}">value="${letter.letter}"</c:if>>
            </div>
            <div class="form-group">
                <select class="form-control selectpicker" name="resource" data-live-search="true">
                    <option value="0" disabled selected>${t.g("Привяжите ресурс к букве")}</option>
                    <c:forEach items="${resources}" var="resource">
                        <option value="${resource.id}"<c:if
                                test="${letter.resources.size() > 0 && letter.resources.get(0).id == resource.id}"> selected</c:if>>
                                ${resource.fileName}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control selectpicker" name="word1" data-live-search="true">
                    <option value="0" disabled selected>${t.g("Привяжите слово с буквой в начале")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${letter.words.size() > 0 && letter.words.get(0).id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control selectpicker" name="word2" data-live-search="true">
                    <option value="0" disabled selected>${t.g("Привяжите слово с буквой в середине")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${letter.words.size() > 1 && letter.words.get(1).id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control selectpicker" name="word3" data-live-search="true">
                    <option value="0" disabled selected>${t.g("Привяжите слово с буквой в конце")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${letter.words.size() > 2 && letter.words.get(2).id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/alphabet" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>

<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>