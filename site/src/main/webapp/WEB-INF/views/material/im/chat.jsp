<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">${user}</h3>
    </div>
    <div class="panel-body">
        <div class="nano" style="height:440px">
            <div class="nano-content pad-all">
                <ul id="messages-list" class="list-unstyled media-block"></ul>
                <div id="scroll-bottom"></div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-10">
                <input type="text" placeholder="${t.g("Введите свое сообщение")}" class="form-control chat-input">
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary btn-block btn-send">${t.g("Отправить")}</button>
            </div>
        </div>
    </div>
</div>

<script type="text/plain" id="tmpl_message">
<li class="mar-btm">
    <div class="{{=it.mediaClass}}">
        <img src="{{=it.message.source.avatarUrl}}" class="img-circle img-sm" alt="{{=it.message.source.fullName}}">
    </div>
    <div class="media-body pad-hor {{=it.speech}}">
        <div class="speech">
            <a href="/users/{{=it.message.source.profileUrl}}" class="media-heading">{{=it.message.source.fullName}}</a>
            <p class="text-justify">{{=it.message.message}}</p>
            <p class="speech-time">
                <i class="fa fa-clock-o fa-fw"></i>{{=it.message.createDt}}
            </p>
        </div>
    </div>
</li>
</script>

<script src="/resources/lib/moment/moment.min.js"></script>
<script src="/resources/lib/moment/moment-timezone.min.js"></script>
<script src="/resources/lib/moment/locales.min.js"></script>

<script>
    $(document).ready(function() {
        moment.locale('ru');

        var userId = ${user.id};

        $.post('/im/${user.id}', function(messages) {
            if (messages !== undefined) {
                var tmpl = doT.template($('#tmpl_message').html());
                var divList = $('#messages-list');

                for (var i = messages.length - 1; i >= 0; i--) {
                    var message = messages[i];

                    message.createDt = moment(message.createDt).fromNow();

                    var data = {
                        message: message
                    };
                    if (message.source.id == ${security.user.id}) {
                        data.mediaClass = 'media-right';
                        data.speech = 'speech-right';
                    } else {
                        data.mediaClass = 'media-left';
                        data.speech = '';
                    }

                    divList.append(tmpl(data));
                }

                $(".nano").nanoScroller();
                $(".nano").nanoScroller({ scrollTo: $('#scroll-bottom') });
            }
        });

        connectWS('/sibza', '/im/${security.user.confirmCode}', function(message) {
            if (message) {
                if (message.source.id != userId) {
                    // сообщение от другого пользователя, которое покажется как уведомление, но не должно попасть в чат
                    return;
                }

                var tmpl = doT.template($('#tmpl_message').html());
                var divList = $('#messages-list');

                message.createDt = moment(message.createDt).fromNow();

                var data =  {
                    mediaClass: 'media-left',
                    speech: '',
                    message: message
                };

                divList.append(tmpl(data));

                $(".nano").nanoScroller({ scrollTo: $('#scroll-bottom') });

                // сообщаем серверу, что мы получили и просмотрели сообщение
                $.post('/im/shown/' + message.id, function(result) {});
            }
        });

        $('.btn-send').click(function() {
            var message = $('.chat-input').val();
            if (message === undefined || message.trim().length == 0) {
                // сообщение пустое и не нужно ничего отправлять
            }

            $.post("/im/send/${user.id}", { message: message }, function(result) {
                if (result.code == '0') {
                    // отображаем новое сообщение в окне чата
                    console.log("Сообщение успешно отправлено");

                    // очищаем отправленное сообщение из поля ввода
                    $('.chat-input').val('');

                    // добавляем сообщение в окно чата
                    var tmpl = doT.template($('#tmpl_message').html());
                    var divList = $('#messages-list');

                    var message = result.body;

                    message.createDt = moment(message.createDt).fromNow();

                    var data =  {
                        mediaClass: 'media-right',
                        speech: 'speech-right',
                        message: result.body
                    };

                    divList.append(tmpl(data));

                    $(".nano").nanoScroller({ scrollTo: $('#scroll-bottom') });
                } else {
                    // сообщаем о возникшей проблеме
                    showNotification(
                            '${t.g("Сообщения")}',
                            '${t.g("При отправке сообщения возникли проблемы. Попробуйте позже")}',
                            'danger');
                }
            });
        });
    });
</script>