<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<h3 class="panel-title">
    <c:choose>
        <c:when test="${category.getTranslate(security.language) != null}">
            ${category.getTranslate(security.language)}
        </c:when>
        <c:otherwise>${category.name}</c:otherwise>
    </c:choose>
    <c:if test="${security.adminOrEditor}">
        <a href="/categories/edit/${category.id}" class="btn">
            <i class="fa fa-pencil text-light"></i>
        </a>
        <a href="/categories/delete/request/${category.id}" class="btn">
            <i class="fa fa-trash text-light"></i>
        </a>
    </c:if>
</h3>

<c:if test="${security.adminOrEditor}">
    <a href="/words/add?category=${category.id}" class="btn btn-info">${t.g("Добавить")}</a><br/><br/>
</c:if>

<div class="infinite-container">
    <t:insertAttribute name="words-list" defaultValue="/WEB-INF/views/nifty/dictionary/words-list.jsp"/>
</div>
<c:if test="${category.wordsCount > max_count }">
    <a class="infinite-more-link" href="/dictionary/category/${category.id}/more"></a>

    <script src="/resources/lib/waypoints/jquery.waypoints.min.js"></script>
    <script src="/resources/lib/waypoints/shortcuts/infinite.min.js"></script>

    <script>
        $(document).ready(function () {
            var infinite = new Waypoint.Infinite({
                element: $('.infinite-container')[0]
            });
        });
    </script>
</c:if>