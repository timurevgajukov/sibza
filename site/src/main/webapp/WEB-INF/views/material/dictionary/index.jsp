<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${security.adminOrEditor}">
    <a href="/categories/add" class="btn btn-info">${t.g("Добавить")}</a><br /><br />
</c:if>

<div class="card">
    <div class="card-header">
        <h2>${t.g("Категории")}</h2>
    </div>
    <div class="card-body">
        <c:if test="${categories != null}">
            <div class="col-md-6 col-sm-12">
                <c:forEach begin="0" end="${categories.size() / 2}" var="i">
                    <div class="list-group bord-no">
                        <a class="list-group-item" href="/dictionary/category/${categories.get(i).id}">
                            <c:choose>
                                <c:when test="${categories.get(i).getTranslate(security.language) != null}">
                                    ${categories.get(i).getTranslate(security.language)}
                                </c:when>
                                <c:otherwise>${categories.get(i).name}</c:otherwise>
                            </c:choose>

                            <span class="badge badge-primary">${categories.get(i).wordsCount}</span>
                        </a>
                    </div>
                </c:forEach>
            </div>
            <div class="col-md-6 col-sm-12">
                <c:forEach begin="${categories.size() / 2 + 1}" end="${categories.size() - 1}" var="i">
                    <div class="list-group bord-no">
                        <a class="list-group-item" href="/dictionary/category/${categories.get(i).id}">
                            <c:choose>
                                <c:when test="${categories.get(i).getTranslate(security.language) != null}">
                                    ${categories.get(i).getTranslate(security.language)}
                                </c:when>
                                <c:otherwise>${categories.get(i).name}</c:otherwise>
                            </c:choose>

                            <span class="badge badge-primary">${categories.get(i).wordsCount}</span>
                        </a>
                    </div>
                </c:forEach>
            </div>
        </c:if>
    </div>
</div>