<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${words}" var="word">
    <div class="panel infinite-item">
        <div class="panel-heading">
            <c:if test="${security.adminOrEditor}">
                <div class="panel-control">
                    <a href="/translates/add/${word.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Добавление перевода")}">
                        <i class="fa fa-plus text-light"></i>
                    </a>
                    <a href="/words/edit/${word.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Редактирование слова")}">
                        <i class="fa fa-pencil text-light"></i>
                    </a>
                    <a href="/words/delete/request/${word.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Удаление слова")}">
                        <i class="fa fa-trash text-light"></i>
                    </a>
                </div>
            </c:if>
            <h4 class="panel-title">${word.word}</h4>
        </div>
        <div class="panel-body">
            <c:if test="${word.translates != null && word.translates.size() != 0}">
                <c:forEach items="${word.translates}" var="translate">
                    <c:choose>
                        <c:when test="${word.language.code == 'AD'}">
                            <c:if test="${translate.language.code == security.language.code}">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="40px">
                                            <c:if test="${translate.resources != null && translate.resources.size() != 0}">
                                                <audio src="/upload/${translate.resources.get(0).id}/${translate.resources.get(0).fileName}"
                                                       id="audio-${translate.resources.get(0).id}" preload="none">
                                                    <a href="/upload/${translate.resources.get(0).id}/${translate.resources.get(0).fileName}">
                                                        <span class="text-2x"><i class="fa fa-volume-up"></i></span>
                                                    </a>
                                                </audio>
                                                        <span class="audio-control icon-2x fa fa-play text-success"
                                                              data-id="${translate.resources.get(0).id}"
                                                              data-played="0"></span>
                                            </c:if>
                                        </td>
                                        <td>
                                            <span class="text-danger">${translate.word}</span>
                                        </td>
                                        <c:if test="${security.adminOrEditor}">
                                            <td>
                                                <a href="/translates/delete/request/${word.id}/${translate.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Удаление перевода")}">
                                                    <i class="fa fa-minus text-light"></i>
                                                </a>
                                                <a href="/words/edit/${translate.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Редактирование слова")}">
                                                    <i class="fa fa-pencil text-light"></i>
                                                </a>
                                                <a href="/words/delete/request/${translate.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Удаление слова")}">
                                                    <i class="fa fa-trash text-light"></i>
                                                </a>
                                            </td>
                                        </c:if>
                                    </tr>
                                    </tbody>
                                </table>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${translate.language.code == 'AD'}">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="40px">
                                            <c:if test="${translate.resources != null && translate.resources.size() != 0}">
                                                <audio src="/upload/${translate.resources.get(0).id}/${translate.resources.get(0).fileName}"
                                                       id="audio-${translate.resources.get(0).id}" preload="none">
                                                    <a href="/upload/${translate.resources.get(0).id}/${translate.resources.get(0).fileName}">
                                                        <span class="text-2x"><i class="fa fa-volume-up"></i></span>
                                                    </a>
                                                </audio>
                                                        <span class="audio-control icon-2x fa fa-play text-success"
                                                              data-id="${translate.resources.get(0).id}"
                                                              data-played="0"></span>
                                            </c:if>
                                        </td>
                                        <td>
                                            <span class="text-danger">${translate.word}</span>
                                        </td>
                                        <c:if test="${security.adminOrEditor}">
                                            <td>
                                                <a href="/translates/delete/request/${word.id}/${translate.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Удаление перевода")}">
                                                    <i class="fa fa-minus text-light"></i>
                                                </a>
                                                <a href="/words/edit/${translate.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Редактирование слова")}">
                                                    <i class="fa fa-pencil text-light"></i>
                                                </a>
                                                <a href="/words/delete/request/${translate.id}" class="btn add-tooltip" data-toggle="tooltip" data-original-title="${t.g("Удаление слова")}">
                                                    <i class="fa fa-trash text-light"></i>
                                            </td>
                                        </c:if>
                                    </tr>
                                    </tbody>
                                </table>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${word.translates.get(0).note != null}">
                    <br />
                    <span class="help-block"><strong>${t.g("Примечание:")}</strong> ${word.translates.get(0).note}</span>
                </c:if>
            </c:if>
        </div>
    </div>
</c:forEach>