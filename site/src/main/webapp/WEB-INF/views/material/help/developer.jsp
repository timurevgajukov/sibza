<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="card">
    <div class="card-header">
        <h2>Структуры данных</h2>
    </div>
    <div class="card-body card-padding">
        <h4>Общая структура ответа</h4>
<pre>
{
    code: целое число, код ответа
    description: строка, если нет ошибок то "OK", иначе описание ошибки
    body: тело сообщения (тип зависит от запрошенного метода)
}
</pre>

        <h4>Язык</h4>
<pre>
{
    id: целое число, идентификатор языка
    code: строка, уникальное двухбуквенное обозначение языка
    name: строка, наименование языка
    smallName: строка, краткое наименование языка
}
</pre>

        <h4>Категория</h4>
<pre>
{
    id: целое число, идентификатор категории
    name: строка, название категории
    wordsCount: число, количество слов в категории по выбранному языку
    translates: массив переводов названия категории на различные языки (структуру каждого элемента см. ниже)
}
</pre>

        <h4>Перевод категории</h4>
<pre>
{
    id: целое число, идентификатор перевода категории
    name: строка, наименование категории на определенном языке
    language: ссылка на структуру языка
}
</pre>

        <h4>Слово</h4>
<pre>
{
    id: целое число, идентификатор слова
    word: строка, непосредственно само слово
    category: ссылка на категорию, в которое входит слово
    language: ссылка на язык
    translates: массив слов переводов это слова на другие языки
}
</pre>

        <h4>Медиа ресурс</h4>
<pre>
{
    id: целое число, идентификатор ресурса
    type: ссылка на структуру с типом контента
    fileName: строка, имя файла вместе с расширением
}
</pre>

        <h4>Тип контента</h4>
<pre>
{
    id: целое число, идентификатор типа контента
    type: ссылка на структуру с типом ресурса
    contentType: строка, строковое представиление типа контента (MIME)
}
</pre>

        <h4>Тип ресурса</h4>
<pre>
{
    id: целое число, идентификатор типа ресурса
    name: строка, наименование типа ресурса
}
</pre>

        <h4>Алфавит</h4>
<pre>
{
    language: ссылка на структуру с описанием языка
    letters: массив букв
}
</pre>

        <h4>Буква</h4>
<pre>
{
    letter: строка, буква алфавита прописная
    lowLetter: строка, буква алфавита строчная
    resources: массив ммедиа ресурсов к букве
}
</pre>

        <p>Примечание: все параметры в виде GET-полей, все ответы возвращаются в виде JSON структур</p>

    </div>
</div>

<div class="card">
    <div class="card-header">
        <h3>Методы</h3>
    </div>
    <div class="card-body card-padding">
        <h4>1. Возвращает актуальную версию API</h4>

        <p><strong>Ссылка:</strong> /api/version<br/>
            <strong>Параметры:</strong> нет параметров<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает строку с номером версии API</p>

        <h4>2. Сравнивает версию мобильного приложения с версией, которая указана в БД. Может использоваться для
            проверки
            актуальной версии для того, чтобы предложить обновить мобильное приложение</h4>

        <p><strong>Ссылка:</strong> /api/application/version/check<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>version</em> - строка, версия приложения<br/>
            <strong>Ответ:</strong>
            если версии совпадают, то возвращает код ответа - <code>0</code>, иначе возвращает код ответа -
            <code>-3</code> (тело ответа пустое)
        </p>

        <h4>3. Возвращает список доступных языков</h4>

        <p><strong>Ссылка:</strong> /api/language/list<br/>
            <strong>Параметры:</strong> нет параметров<br/>
            <strong>Ответ:</strong> в теле сообщения позвращает массив из списка доступных языков</p>

        <h4>4. Возвращает список категорий словаря/разговорника</h4>

        <p><strong>Ссылка:</strong> /api/category/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>lng</em> - строка, код языка, необходим для расчета количества слов в категории<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает массив из списка категорий</p>

        <h4>5. Возвращает список слов и фраз из словаря/разговорника</h4>

        <p><strong>Ссылка:</strong> /api/word/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>category</em> - целое положительное число, идентификатор категории, для которой позвращаются слова<br/>
            <em>lng</em> - строка, код языка, на котором возвращаются слова<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает массив из списка слов на выбранном языке</p>

        <h4>6. Возвращает список ресурсов для слов в словаре</h4>

        <p><strong>Ссылка:</strong> /api/resource/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>word</em> - целое положительное число, идентификатор слова, для которого возвращаются медиа ресурсы
            (пока что поддерживается только аудио)<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщение возвращает массив из списка ресурсов к слову (только информация по
            ресурсу). Для получения самого файла ресурса необходимо обратиться по ссылке /upload/{id}/{file_name} (id -
            идентификатор ресурса, file_name - наименование файла с расширением, в принципе, наименование файла ресурса
            может быть любым и может не совпадать с реальным названием)</p>

        <h4>7. Возвращает алфавит</h4>

        <p><strong>Ссылка:</strong> /api/alphabet/letter/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>lng</em> - строка, код языка, для которого возвращается алфавит<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает алфавит для выбранного языка</p>

        <h4>8. Возвращает список аудио</h4>

        <p><strong>Ссылка:</strong> /api/audio/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает массив аудио данных</p>

        <h4>9. Возвращает список видео</h4>

        <p><strong>Ссылка:</strong> /api/video/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает массив видео данных</p>

        <h4>10. Возвращает список пословиц</h4>

        <p><strong>Ссылка:</strong> /api/proverb/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>lng</em> - строка, код языка, для которого возвращается алфавит<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщение возвращает массив пословиц</p>

        <h4>11. Возвращает список книг</h4>

        <p><strong>Ссылка:</strong> /api/book/list<br/>
            <strong>Параметры:</strong><br/>
            <em>token</em> - строка, идентификатор мобильного приложения<br/>
            <em>offset</em> - целое положительное число, смещение относительно начала списка<br/>
            <em>count</em> - целое положительное число, количество элементов в списке<br/>
            <strong>Ответ:</strong> в теле сообщения возвращает массив книг</p>
    </div>
</div>