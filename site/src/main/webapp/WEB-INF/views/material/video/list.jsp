<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${security.isAdminOrEditor()}">
    <a href="/video/add" class="btn btn-info">${t.g("Добавить")}</a><br /><br />
</c:if>

<c:choose>
    <c:when test="${videoList != null && videoList.size() != 0}">
        <c:forEach items="${videoList}" var="video">
            <div class="col-md-6 col-sm-12">
                <div class="panel">
                    <c:if test="${video.title != null && video.title.length() != 0}">
                        <div class="panel-heading">
                            <h3 class="panel-title">${video.title}</h3>
                        </div>
                    </c:if>
                    <div class="panel-body">
                        <a href="#" data-toggle="modal" data-target="#modal-video" class="video-img-btn" data-id="${video.youtube}">
                            <img src="//img.youtube.com/vi/${video.youtube}/maxresdefault.jpg" style="max-width: 100%; max-height: 100%;" />
                        </a>
                    </div>
                    <div class="panel-footer">
                        ${t.g("Язык")}: ${video.language.smallName}
                    </div>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <br/><br/>${t.g("Список видео пуст")}
    </c:otherwise>
</c:choose>

<!-- VIDEO MODAL WINDOW: BEGIN -->
<div id="modal-video" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-video" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Видео")}</h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="video-frame" class="embed-responsive-item" src="about:blank" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
            </div>
        </div>
    </div>
</div>
<!-- VIDEO WINDOW: END -->

<script>
    $(document).ready(function() {
        // обрабатываем открытие модального окна с видео
        $('.video-img-btn').click(function() {
            var id = $(this).data('id');
            $('#video-frame').attr('src', '//www.youtube.com/embed/' + id);
        });
    });
</script>