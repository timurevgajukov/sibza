<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Пословица")}</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <select class="form-control chosen" name="language">
                    <option value="0" disabled selected>${t.g("Выберите язык слова")}</option>
                    <c:forEach items="${languages}" var="language">
                        <option value="${language.id}"<c:if
                                test="${proverb.language.id == language.id}"> selected</c:if>>${language.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="proverb">${t.g("Текст пословицы")}</label>
                <input class="form-control" type="text" name="proverb" id="proverb" required
                       placeholder="${t.g("Введите текст пословицы")}"
                       <c:if test="${proverb != null}">value="${proverb.body}"</c:if>>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/proverbs" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>