<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${keys}" var="key">
    <div class="panel">
        <div class="panel-heading">
            <c:if test="${security.adminOrEditor}">
                <div class="panel-control">
                    <c:url var="url" value="/interfaces/edit">
                        <c:param name="key" value="${key}" />
                    </c:url>
                    <a href="${url}" class="btn">
                        <i class="fa fa-pencil text-light"></i>
                    </a>
                </div>
            </c:if>
            <h3 class="panel-title">${key}</h3>
        </div>
        <div class="panel-body">
            <ul>
                <c:forEach items="${list.get(key)}" var="item">
                    <li><strong>${item.language.name}:</strong> ${item.value}</li>
                </c:forEach>
            </ul>
        </div>
    </div>
</c:forEach>