<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${competitions != null && competitions.size() != 0}">
    <h3>${t.g("Конкурсы")}</h3>

    <div class="row">
        <c:forEach items="${competitions}" var="competition">
            <div class="col-sm-6 col-md-3">
                <c:set var="competition" value="${competition}" scope="request" />
                <t:insertAttribute name="competition-panel" defaultValue="/WEB-INF/views/nifty/game/competition-panel.jsp"/>
            </div>
        </c:forEach>
    </div>

    <script type="text/plain" id="tmpl_rating">
    <tr>
        <td class="min-width text-right">{{=it.id}}.</td>
        <td class="min-width">
            <img class="img-circle img-user media-object" src="{{=it.rating.user.avatarUrl}}" alt="{{=it.rating.user.fullName}}">
        </td>
        <td>
            <a href="/users/{{=it.rating.user.profileUrl}}">{{=it.rating.user.fullName}}</a>
        </td>
        <td class="text-right">
            <span class="badge badge-primary">{{=it.rating.score}}</span>
        </td>
    </tr>
    </script>

    <script>
        $(document).ready(function() {
            $('.show-rating').click(function() {
                $.post('/game/words/rating/list', { competition: true }, function(list) {
                    var tmpl = doT.template($('#tmpl_rating').html());
                    var divList = $('#competition-rating-list');
                    divList.empty();
                    if (list.length != 0) {
                        divList.append("<table class='table table-hover table-vcenter'><tbody></tbody></table>");
                        var tableBody = divList.find("tbody");

                        var i = 1;
                        $.each(list, function() {
                            var data = {
                                id: i++,
                                rating: this
                            }
                            tableBody.append(tmpl(data));
                        });
                        $(".nano").nanoScroller();
                    } else {
                        divList.append("${t.g("Рейтинг пуст")}");
                    }
                });
            });
        });
    </script>
</c:if>

<h3>${t.g("Игры")}</h3>

<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-2">
        <a href="/game/words">
            <div class="panel panel-info panel-colorful">
                <div class="pad-all text-center">
            <span class="text-5x">
                <i class="fa fa-wikipedia-w"></i>
            </span>
                    <p>${t.g("Игра в слова")}</p>
                </div>
            </div>
        </a>
    </div>
</div>