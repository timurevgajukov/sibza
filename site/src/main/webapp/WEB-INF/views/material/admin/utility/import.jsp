<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post" enctype="multipart/form-data" action="/utility/import-dsl">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Данные для импорта словарей в формате DSL")}</h3>
        </div>
        <div class="panel-body">
            <input type="file" name="file" />
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Импортировать")}</button>
        </div>
    </div>
</form>

<form method="post" enctype="multipart/form-data" action="/utility/import-words">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Данные для импорта слов")}</h3>
        </div>
        <div class="panel-body">
            <input type="file" name="file" />
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Импортировать")}</button>
        </div>
    </div>
</form>

<form method="post" enctype="multipart/form-data" action="/utility/import-media">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Данные для импорта медиафайлов к словам")}</h3>
        </div>
        <div class="panel-body">
            <input type="file" name="file" />
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Импортировать")}</button>
        </div>
    </div>
</form>

<form method="post" enctype="multipart/form-data" action="/utility/import-proverbs">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Данные для импорта пословиц")}</h3>
        </div>
        <div class="panel-body">
            <input type="file" name="file" />
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Импортировать")}</button>
        </div>
    </div>
</form>