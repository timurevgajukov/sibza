<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-8 col-sm-12">
        <div class="card">
            <div class="card-body">
                <a data-toggle="modal" href="#modal-video">
                    <img src="//img.youtube.com/vi/${video.youtube}/maxresdefault.jpg" style="max-width: 100%; max-height: 100%;" />
                </a>
            </div>

        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <c:if test="${competition != null}">
            <t:insertAttribute name="competition-panel" defaultValue="/WEB-INF/views/nifty/game/competition-panel.jsp"/>
        </c:if>
        <div class="card">
            <div class="listview">
                <div class="lv-header">
                    ${t.g("Пословицы")}
                </div>
                <div class="lv-body">
                    <div class="nano" style="height: 440px;">
                        <div class="nano-content">
                            <c:if test="${proverbs != null && proverbs.size() != 0}">
                                <c:forEach items="${proverbs}" var="proverb">
                                    <span class="lv-item"><i class="zmdi zmdi-quote"></i> ${proverb.body}</span>
                                </c:forEach>
                            </c:if>
                        </div>
                    </div>
                </div>
                <a class="lv-footer" href="/proverbs">${t.g("еще пословицы")}</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-6">
        <a href="/alphabet">
            <div class="card pt-inner">
                <div class="pti-header bgm-cyan">
                    <h2><span id="letters-count-all"></span></h2>
                    <div class="ptih-title">${t.g("Алфавит")}</div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="/dictionary">
            <div class="card pt-inner">
                <div class="pti-header bgm-amber">
                    <h2><span id="words-count-all"></span></h2>
                    <div class="ptih-title">${t.g("Словарь")} / ${t.g("Разговорник")}</div>
                </div>
            </div>
        </a>
    </div>
</div>
<c:if test="${security.admin}">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <a href="/languages">
                <div class="card pt-inner">
                    <div class="pti-header bgm-lightgreen">
                        <h2><span id="languages-count-all"></span></h2>
                        <div class="ptih-title">${t.g("Языки")}</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3 col-sm-6">
            <a href="/users">
                <div class="card pt-inner">
                    <div class="pti-header bgm-bluegray">
                        <h2><span id="users-count-all"></span></h2>
                        <div class="ptih-title">${t.g("Пользователи")}</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</c:if>

<!-- VIDEO MODAL WINDOW: BEGIN -->
<div id="modal-video" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Видео")}</h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="video-frame" class="embed-responsive-item" src="about:blank" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
            </div>
        </div>
    </div>
</div>
<!-- VIDEO WINDOW: END -->

<script>
    $(document).ready(function() {
        $('.nano').nanoScroller();

        // запрашиваем статистику
        $.post('/stat/video/all', function(count) {
            addStatCount($('#video-count-all'), count);
        });
        $.post('/stat/proverbs/all', function(count) {
            addStatCount($('#proverbs-count-all'), count);
        });
        $.post('/stat/alphabet/all', function(count) {
            addStatCount($('#letters-count-all'), count);
        });
        $.post('/stat/words/all', function(count) {
            addStatCount($('#words-count-all'), count);
        });
        <c:if test="${security.admin}">
            $.post('/stat/languages/all', function(count) {
                addStatCount($('#languages-count-all'), count);
            });
            $.post('/stat/users/all', function(count) {
                addStatCount($('#users-count-all'), count);
            });
        </c:if>

        // обрабатываем открытие модального окна с видео
        $('#modal-video').on('shown.bs.modal', function() {
            $('#video-frame').attr('src', '//www.youtube.com/embed/${video.youtube}');
        });
    });

    function addStatCount(src, count) {
        if (count !== undefined && count != 0) {
            src.html(count);
        }
    }
</script>