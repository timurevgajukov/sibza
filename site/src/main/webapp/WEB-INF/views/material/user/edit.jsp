<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="tab-base">

    <!--Nav Tabs-->
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#main-tab">${t.g("Основное")}</a>
        </li>
        <li>
            <a data-toggle="tab" href="#contact-tab">${t.g("Контакты")}</a>
        </li>
    </ul>

    <!--Tabs Content-->
    <div class="tab-content">
        <div id="main-tab" class="tab-pane fade active in">
            <form method="post" action="/users/profile/edit/main">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nick">${t.g("Ник")}</label>
                            <input class="form-control" type="text" id="nick" name="nick"
                                   value="<c:if test="${user.nick != null}">${user.nick}</c:if>"
                                   placeholder="${t.g("Введите свой ник")}"/>
                            <small class="help-block">${t.g("Используется для формирования ссылки на профиль. Должен быть уникальным.")}</small>
                        </div>
                        <div class="form-group">
                            <label for="name">${t.g("Имя")}</label>
                            <input class="form-control" type="text" id="name" name="name"
                                   value="<c:if test="${user.name != null}">${user.name}</c:if>"
                                   placeholder="${t.g("Введите свое имя")}"/>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-md-3 control-label">${t.g("Пол")}</label>

                                    <div class="col-md-9">
                                        <label class="form-radio form-normal">
                                            <input type="radio" name="gender" value="1"
                                                    <c:if test="${user.info.gender == 1}">checked</c:if> /> ${t.g("Мужчина")}
                                        </label><br/>
                                        <label class="form-radio form-normal">
                                            <input type="radio" name="gender" value="2"
                                                   <c:if test="${user.info.gender == 2}">checked</c:if> /> ${t.g("Женщина")}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="birthday">${t.g("Дата рождения")}</label>

                            <div class="input-group date">
                                <input class="form-control" type="text" id="birthday" name="birthday"
                                       value="<c:if test="${user.info.birthdayStr != null}">${user.info.birthdayStr}</c:if>" />
                                <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="about">${t.g("Обо мне")}</label>
                            <textarea class="form-control" rows="8" id="about" name="about"
                                      placeholder="${t.g("Напишите что-нибудь о себе")}"><c:if test="${user.info.about != null}">${user.info.about}</c:if></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">${t.g("Сохранить")}</button>
                    <a class="btn btn-success" href="/users/${user.profileUrl}">${t.g("Вернуться")}</a>
                </div>
            </form>
            <hr/>

            <h3>${t.g("Смена пароля")}</h3>

            <form id="change-password-form" method="post" action="/users/profile/edit/password">
                <div class="form-group">
                    <label for="password_old">${t.g("Старый пароль")}</label>
                    <input class="form-control" type="password" id="password_old" name="password_old"
                           placeholder="${t.g("Введите текущий пароль")}"/>
                </div>
                <div class="form-group">
                    <label for="password1">${t.g("Новый пароль")}</label>
                    <input class="form-control" type="password" id="password1" name="password1"
                           placeholder="${t.g("Введите новый пароль")}"/>
                </div>
                <div class="form-group">
                    <label for="password2">${t.g("Новый пароль")}</label>
                    <input class="form-control" type="password" id="password2" name="password2"
                           placeholder="${t.g("Повторите новый пароль")}"/>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">${t.g("Сменить")}</button>
                    <a class="btn btn-success" href="/users/${user.profileUrl}">${t.g("Вернуться")}</a>
                </div>
            </form>
        </div>
        <div id="contact-tab" class="tab-pane fade">
            <form id="contact-form" method="post" action="/users/profile/edit/contact">
                <div class="form-group">
                    <label for="phone">${t.g("Мобильный номер")}</label>
                    <input class="form-control" type="text" id="phone" name="phone"
                           value="<c:if test="${user.info.phone != null}">${user.info.phone}</c:if>"
                           placeholder="${t.g("Введите номер мобильного телефона")}" />
                </div>
                <div class="form-group">
                    <label for="address">${t.g("Адрес")}</label>
                    <input class="form-control" type="text" id="address" name="address"
                           value="<c:if test="${user.info.address != null}">${user.info.address}</c:if>"
                           placeholder="${t.g("Введите свою страну и город")}" />
                </div>
                <div class="form-group">
                    <label for="site">${t.g("Сайт")}</label>
                    <input class="form-control" type="text" id="site" name="site"
                           value="<c:if test="${user.info.site != null}">${user.info.site}</c:if>"
                           placeholder="${t.g("Введите свой сайт (с http://)")}" />
                </div>
                <hr />

                <h3>${t.g("Социальные сети")}</h3>
                <c:forEach items="${socials}" var="social">
                    <div class="form-group">
                        <label for="social-${social.id}">${social.name}</label>
                        <input class="form-control" type="text" id="social-${social.id}" name="social-${social.id}"
                               value="${user.getSocialNetworkAccount(social.id)}" data-id="${social.id}"
                               placeholder="${social.name}" />
                        <input type="hidden" id="social-hidden-${social.id}"  name="social"
                               value="${social.id}###${user.getSocialNetworkAccount(social.id)}" />
                    </div>
                </c:forEach>

                <div class="form-group">
                    <button class="btn btn-info" id="contact-form-submit">${t.g("Сохранить")}</button>
                    <a class="btn btn-success" href="/users/${user.profileUrl}">${t.g("Вернуться")}</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // добавляем плагин для выбора даты
        $('.date').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        });

        $('#contact-form-submit').click(function() {
            <c:forEach items="${socials}" var="social">
                $('#social-hidden-${social.id}').val('${social.id}###' + $('#social-${social.id}').val());
            </c:forEach>
            $('#contact-form').submit();
        });

        $('#change-password-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'fa fa-check-circle fa-lg text-success',
                invalid: 'fa fa-times-circle fa-lg',
                validating: 'fa fa-refresh'
            },
            fields: {
                password_old: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        }
                    }
                },
                password1: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                        identical: {
                            field: 'password2',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                },
                password2: {
                    validators: {
                        notEmpty: {
                            message: 'The confirm password is required and can\'t be empty'
                        },
                        identical: {
                            field: 'password1',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            }
        }).on('success.field.bv', function(e, data) {});
    });
</script>