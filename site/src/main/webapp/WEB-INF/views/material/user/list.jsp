<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:choose>
    <c:when test="${users != null && users.size() != 0}">
        <div class="infinite-container">
            <c:forEach var="i" begin="0" end="${users.size()-1}" step="2">
                <div class="row infinite-item">
                    <c:forEach var="j" begin="0" end="1" step="1">
                        <div class="col-md-6">
                            <c:if test="${i+j < users.size()}">
                                <c:set var="user" value="${users.get(i+j)}" />
                                <div class="panel media pad-all">
                                    <div class="media-left">
                                        <a href="/users/${user.profileUrl}">
                                            <img src="${user.avatarUrl}" class="img-lg img-border img-circle" alt="${user}">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="/users/${user.profileUrl}">
                                            <h4 class="text-lg text-overflow mar-no">${user}</h4>
                                        </a>
                                        <c:set var="role" value="${user.roles.get(0)}" />
                                        <c:if test="${role.id != 3}">
                                            <p class="text-sm">${t.g(role)}</p>
                                        </c:if>
                                        <c:if test="${security.auth && security.user.id != user.id}">
                                            <br />
                                            <a href="/im/${user.id}">${t.g("Написать сообщение")}</a><br />
                                            <a href="#" data-toggle="modal" data-target="#notwork-modal">
                                                ${t.g("Просмотреть друзей")}
                                            </a><br />
                                            <c:choose>
                                                <c:when test="${security.user.hisFriend(user)}">
                                                    <a href="#" class="add_del_friend" data-id="${user.id}"
                                                       data-action="del" onclick="return false">
                                                        ${t.g("Удалить из друзей")}
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="#" class="add_del_friend" data-id="${user.id}"
                                                       data-action="add" onclick="return false">
                                                        ${t.g("Добавить в друзья")}
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </div>
                                </div>
                            </c:if>
                        </div>
                    </c:forEach>
                </div>
            </c:forEach>
        </div>
        <c:choose>
            <c:when test="${active == 'friends'}"><a class="infinite-more-link" href="/friends/more"></a></c:when>
            <c:otherwise><a class="infinite-more-link" href="/users/more"></a></c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <div class="panel">
            <div class="panel-body">
                <c:choose>
                    <c:when test="${active == 'friends'}">${t.g("Список друзей пуст")}</c:when>
                    <c:otherwise>${t.g("Список пользователей пуст")}</c:otherwise>
                </c:choose>
            </div>
        </div>
    </c:otherwise>
</c:choose>

<!-- NOT YET WORK MODAL WINDOW: BEGIN -->
<div id="notwork-modal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-notwork" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Пока еще не работает")}</h4>
            </div>
            <div class="modal-body">
                <p>${t.g("Функция находится в разработка и будет доступна позже.")}</p>
            </div>
        </div>
    </div>
</div>
<!-- NOT YET WORK MODAL WINDOW: END -->

<script src="/resources/lib/waypoints/jquery.waypoints.min.js"></script>
<script src="/resources/lib/waypoints/shortcuts/infinite.min.js"></script>

<script src="/resources/js/friend.js"></script>

<script>
    $(document).ready(function() {
        var infinite = new Waypoint.Infinite({
            element: $('.infinite-container')[0]
        });

        btnTitleTranslate('${t.g("Добавить в друзья")}', '${t.g("Удалить из друзей")}');
    });
</script>