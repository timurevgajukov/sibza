<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form id="recover-form" method="post" action="/security/recover">
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="password1">${t.g("Новый пароль")}</label>
                        <input class="form-control" type="password" id="password1" name="password1"
                               placeholder="${t.g("Введите новый пароль")}"/>
                    </div>
                    <div class="form-group">
                        <label for="password2">${t.g("Новый пароль")}</label>
                        <input class="form-control" type="password" id="password2" name="password2"
                               placeholder="${t.g("Повторите новый пароль")}"/>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-info" type="submit">${t.g("Сменить")}</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function() {
        $('#recover-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'fa fa-check-circle fa-lg text-success',
                invalid: 'fa fa-times-circle fa-lg',
                validating: 'fa fa-refresh'
            },
            fields: {
                password1: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                        identical: {
                            field: 'password2',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                },
                password2: {
                    validators: {
                        notEmpty: {
                            message: 'The confirm password is required and can\'t be empty'
                        },
                        identical: {
                            field: 'password1',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
            }
        }).on('success.field.bv', function(e, data) {});
    });
</script>