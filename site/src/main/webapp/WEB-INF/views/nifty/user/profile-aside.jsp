<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${active == 'profile'}">
    <!--ASIDE-->
    <!--===================================================-->
    <aside id="aside-container">
        <div id="aside" class="aside-xs-in">
            <div class="nano">
                <div class="nano-content">

                    <div class="text-center pad-all">
                        <div class="pad-ver">
                            <c:choose>
                                <c:when test="${avatars != null && avatars.size() != 0 && security.auth && security.user.id == user.id}">
                                    <a href="#" data-toggle="modal" data-target="#avatar-modal">
                                        <img src="${user.avatarUrl}" class="img-lg img-border img-circle avatar-view" alt="${user}">
                                    </a>

                                    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                </c:when>
                                <c:otherwise>
                                    <img src="${user.avatarUrl}" class="img-lg img-border img-circle" alt="${user}">
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <h4 class="text-lg text-overflow mar-no">${user}</h4>

                        <p class="text-sm">${t.g(user.roles.get(0))}</p>
                        <c:if test="${security.auth && security.user.id == user.id && mode != 'edit'}">
                            <p class="text-sm">
                                <ins><a href="/users/profile/edit">${t.g("редактировать")}</a></ins>
                            </p>
                        </c:if>

                        <div class="pad-ver btn-group">
                            <c:if test="${user.socialNetworks != null && user.socialNetworks.size() != 0}">
                                <c:forEach items="${user.socialNetworks}" var="social">
                                    <a title="" href="${social.accountUrl}"
                                       class="btn btn-icon btn-hover-${social.type.color} fa fa-${social.type.icon} icon-lg add-tooltip"
                                       data-original-title="${social.type.name}" data-container="body"></a>
                                </c:forEach>
                            </c:if>
                        </div>
                        <c:if test="${security.auth && security.user.id != user.id}">
                            <c:choose>
                                <c:when test="${security.user.hisFriend(user)}">
                                    <button class="btn btn-block btn-danger add_del_friend" data-id="${user.id}" data-action="del">
                                        ${t.g("Удалить из друзей")}
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <button class="btn btn-block btn-success add_del_friend" data-id="${user.id}" data-action="add">
                                        ${t.g("Добавить в друзья")}
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </div>

                    <c:if test="${user.info.address != null || user.info.site != null}">
                        <hr/>
                        <ul class="list-group bg-trans">
                            <c:if test="${user.info.address != null && user.info.address.length() != 0 }">
                                <li class="list-group-item list-item-sm">
                                    <i class="fa fa-home fa-fw"></i> ${user.info.address}
                                </li>
                            </c:if>
                            <c:if test="${user.info.site != null && user.info.site.length() != 0}">
                                <li class="list-group-item list-item-sm text-primary text-semibold">
                                    <a href="${user.info.site}" class="btn-link">
                                        <i class="fa fa-globe fa-fw"></i> ${user.info.site}
                                    </a>
                                </li>
                            </c:if>
                        </ul>
                    </c:if>

                    <c:if test="${user.info.about != null && user.info.about.length() != 0}">
                        <hr>
                        <div class="pad-hor">
                            <h5>${t.g("Обо мне")}</h5>
                            <small class="text-thin">${user.info.about}</small>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </aside>
    <!--===================================================-->
    <!--END ASIDE-->

    <c:if test="${avatars != null && avatars.size() != 0 && security.auth && security.user.id == user.id}">
        <link href="/resources/lib/cropper/2.2.5/cropper.min.css" rel="stylesheet" />
        <link href="/resources/css/avatars.css" rel="stylesheet" />

        <!-- CHANGE AVATAR MODAL WINDOW: BEGIN -->
        <div id="avatar-modal" class="modal fade" role="dialog" tabindex="-1"
             aria-labelledby="modal-change-avatar" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="tab-base">

                    <!--Nav Tabs-->
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#avatar-tab1">${t.g("Загрузить")}</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#avatar-tab2">${t.g("Стандартные")}</a>
                        </li>
                    </ul>

                    <!--Tabs Content-->
                    <div class="tab-content">
                        <div id="avatar-tab1" class="tab-pane fade active in">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">${t.g("Загрузка картинки")}</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="crop-avatar">
                                        <form class="avatar-form" action="/users/profile/crop" enctype="multipart/form-data" method="post">
                                            <div class="avatar-body">

                                                <!-- Upload image and data -->
                                                <div class="avatar-upload">
                                                    <input type="hidden" class="avatar-src" name="avatar_src">
                                                    <input type="hidden" class="avatar-data" name="avatar_data">
                                                    <label for="avatarInput">${t.g("Загрузить файл")}</label>
                                                    <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                                                </div>

                                                <!-- Crop and preview -->
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="avatar-wrapper"></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="avatar-preview preview-lg"></div>
                                                        <div class="avatar-preview preview-md"></div>
                                                        <div class="avatar-preview preview-sm"></div>
                                                    </div>
                                                </div>

                                                <div class="row avatar-btns">
                                                    <div class="col-md-9">
                                                        <%--<div class="btn-group">--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="btn-group">--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>--%>
                                                            <%--<button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>--%>
                                                        <%--</div>--%>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="submit" class="btn btn-primary btn-block avatar-save">${t.g("Сохранить")}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="avatar-tab2" class="tab-pane fade">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">${t.g("Смена аватарки")}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <a href="/users/profile/avatar/${avatars.get(0).id}"><img src="${avatars.get(0)}"/></a>
                                        <a href="/users/profile/avatar/${avatars.get(1).id}"><img src="${avatars.get(1)}"/></a>
                                        <a href="/users/profile/avatar/${avatars.get(2).id}"><img src="${avatars.get(2)}"/></a>
                                        <a href="/users/profile/avatar/${avatars.get(3).id}"><img src="${avatars.get(3)}"/></a>
                                        <a href="/users/profile/avatar/${avatars.get(4).id}"><img src="${avatars.get(4)}"/></a>
                                        <a href="/users/profile/avatar/${avatars.get(5).id}"><img src="${avatars.get(5)}"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CHANGE AVATAR MODAL WINDOW: END -->

        <script src="/resources/lib/cropper/2.2.5/cropper.min.js"></script>
        <script src="/resources/js/avatar.js"></script>
    </c:if>

    <script src="/resources/js/friend.js"></script>

    <script>
        $(document).ready(function() {
            btnTitleTranslate('${t.g("Добавить в друзья")}', '${t.g("Удалить из друзей")}');
        });
    </script>
</c:if>