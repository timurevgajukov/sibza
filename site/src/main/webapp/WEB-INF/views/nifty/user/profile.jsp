<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div class="col-lg-6">

        <c:if test="${security.auth && security.user.id == user.id}">
            <!-- Status Form -->
            <!--===================================================-->
            <div class="panel">
                <div class="panel-body">
                    <form method="post" action="/users/profile/post">
                        <textarea class="form-control" name="body" rows="2" placeholder="${t.g("Что ты думаешь?")}"
                                  required></textarea>

                        <div class="mar-top clearfix">
                            <button class="btn btn-sm btn-primary pull-right" type="submit">
                                <i class="fa fa-pencil fa-fw"></i> ${t.g("Опубликовать")}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!--===================================================-->
            <!-- End Status Form -->
        </c:if>

        <div class="panel">
            <div class="panel-body">
                <c:choose>
                    <c:when test="${user.posts != null && user.posts.size() != 0}">
                        <c:forEach items="${user.posts}" var="post">
                            <div class="media-block">
                                <a class="media-left" href="#">
                                    <img class="img-circle img-sm" alt="${user}" src="${user.avatarUrl}">
                                </a>

                                <div class="media-body">
                                    <div class="mar-btm">
                                        <a href="#" class="btn-link text-semibold media-heading box-inline">${user}</a>

                                        <p class="text-muted text-sm">${post.formattedCreateDt}</p>
                                    </div>
                                    <p>${post}</p>

                                    <div class="pad-ver">
                                        <c:if test="${security.auth && security.user.id == user.id}">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-default btn-hover-info" href="#"
                                                   title="${t.g("Редактировать")}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-sm btn-default btn-hover-danger" href="#"
                                                   title="${t.g("Удалить")}">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </c:if>
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-default btn-hover-success" href="#"
                                               title="${t.g("Мне нравится")}">
                                                <i class="fa fa-thumbs-up"></i>
                                            </a>
                                        </div>
                                        <a class="btn btn-sm btn-default btn-hover-primary"
                                           href="#">${t.g("Комментировать")}</a>
                                    </div>
                                    <hr />
                                </div>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <p>${t.g("В ленте новостей пока ничего интересного")}</p>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-body">

            <!-- Timeline -->
            <!--===================================================-->
            <div class="timeline">

                <!-- Timeline header -->
                <div class="timeline-header">
                    <div class="timeline-header-title bg-success">${t.g("Сейчас")}</div>
                </div>

                <div class="timeline-entry">
                    <div class="timeline-stat">
                        <div class="timeline-icon"></div>
                    </div>
                    <div class="timeline-label">
                        <p>${t.g("Сегодня пока ничего интересного не произошло")}</p>
                    </div>
                </div>

                <!-- Timeline header -->
                <div class="timeline-header">
                    <div class="timeline-header-title bg-info">${t.g("Вчера")}</div>
                </div>

                <div class="timeline-entry">
                    <div class="timeline-stat">
                        <div class="timeline-icon"></div>
                    </div>
                    <div class="timeline-label">
                        <p>${t.g("Вчера ничего интересного не происходило")}</p>
                    </div>
                </div>

                <!-- Timeline header -->
                <div class="timeline-header">
                    <div class="timeline-header-title bg-purple">${t.g("Ранее")}</div>
                </div>

                <div class="timeline-entry">
                    <div class="timeline-stat">
                        <div class="timeline-icon"></div>
                    </div>
                    <div class="timeline-label">
                        <p>${t.g("Ранее ничего интересного не происходило")}</p>
                    </div>
                </div>
            </div>
            <!--===================================================-->
            <!-- End Timeline -->

        </div>
    </div>
</div>