<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${books}" var="book">
    <div class="panel">
        <c:if test="${security.adminOrEditor}">
            <div class="panel-heading">
                <div class="panel-control">
                    <a href="/books/edit/${book.id}" class="btn">
                        <i class="fa fa-pencil text-light"></i>
                    </a>
                    <a href="/books/delete/request/${book.id}" class="btn">
                        <i class="fa fa-trash text-light"></i>
                    </a>
                </div>
            </div>
        </c:if>
        <div class="panel-body">
            <table>
                <tbody>
                <tr>
                    <td rowspan="2">
                        <img src="/books/${book.image.id}/${book.image.fileName}" width="120" alt="${book.title}" />
                    </td>
                    <td width="15"></td>
                    <td valign="top">
                        <span class="text-bold">${book.title}</span><br/>
                        <span style="color: #c9c9c9;">${book.language.smallName}</span><br/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="bottom">
                        <c:choose>
                            <c:when test="${book.books.size() != 0}">
                                <span style="color: #c9c9c9;">${book.books.size()} ${t.g("томов")}</span><br/>
                                <a href="#" class="btn btn-warning show-tomes" data-id="${book.id}"
                                   data-toggle="modal" data-target="#modal-tomes">
                                        ${t.g("Выбрать том")}
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="${book.url}" class="btn btn-warning">${t.g("Скачать")}</a>&nbsp;&nbsp;
                                <span style="color: #c9c9c9; text-transform: uppercase;">${book.format}</span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</c:forEach>

<!-- TOMES MODAL WINDOW: BEGIN -->
<div id="modal-tomes" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-tomes" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Книги")}</h4>
            </div>
            <div class="nano" style="height: 400px;">
                <div class="nano-content pad-all">
                    <div id="tomes-list"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
            </div>
        </div>
    </div>
</div>
<!-- TOMES MODAL WINDOW: END -->

<script type="text/plain" id="tmpl_tome">
    <div class="panel">
        <div class="panel-body">
            <table>
                <tbody>
                <tr>
                    <td rowspan="2">
                        <img src="{{=it.imageUrl}}" width="120" alt="${book.title}" />
                    </td>
                    <td width="15"></td>
                    <td valign="top">
                        <span class="text-bold">{{=it.title}}</span><br />
                        <span style="color: #c9c9c9;">{{=it.language.smallName}}</span><br />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="bottom">
                        <a href="{{=it.url}}" class="btn btn-warning">${t.g("Скачать")}</a>&nbsp;&nbsp;
                        <span style="color: #c9c9c9; text-transform: uppercase;">{{=it.format}}</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</script>

<script>
    $(document).ready(function() {
        $('#modal-tomes').on('shown.bs.modal', function() {
            $(".nano").nanoScroller();
        });

        /**
        * Отобразить тома книги в модальном окне
         */
        $('.show-tomes').click(function() {
            currentId = $(this).data("id");
            $.post("/books/" + currentId + "/tomes/", function (tomes) {
                var tmpl = doT.template($('#tmpl_tome').html());

                var divList = $('#tomes-list');
                divList.empty();
                if (tomes.length != 0) {
                    // отображаем список томов
                    $.each(tomes, function() {
                        divList.append(tmpl(this));
                    });
                } else {
                    divList.append("${t.g("Не удалось загрузить список томов!")}");
                }
            });
        });
    });
</script>