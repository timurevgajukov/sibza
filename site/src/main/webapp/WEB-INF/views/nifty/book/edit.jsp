<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-body">
            <div class="form-group">
                <input class="form-control" type="text" name="title" required
                       placeholder="${t.g("Введите заголовок книги")}"
                       <c:if test="${book != null}">value="${book.title}"</c:if>>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="parent">
                    <option value="0" disabled selected>${t.g("Выберите родительскую книгу")}</option>
                    <c:forEach items="${books}" var="parent">
                        <option value="${parent.id}"<c:if
                                test="${book.parent != 0 && book.parent.id == parent.id}"> selected</c:if>>
                                ${parent.title}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="language">
                    <option value="0" disabled selected>${t.g("Выберите язык книги")}</option>
                    <c:forEach items="${languages}" var="language">
                        <option value="${language.id}"<c:if
                                test="${book.language.id == language.id}"> selected</c:if>>
                                ${language.name}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="url" required
                       placeholder="${t.g("Введите ссылку к файлу книги")}"
                       <c:if test="${book != null}">value="${book.url}"</c:if>>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="format" required
                       placeholder="${t.g("Введите формат книги")}"
                       <c:if test="${book != null}">value="${book.format}"</c:if>>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="size" required
                       placeholder="${t.g("Введите размер файла книги (Мб)")}"
                       <c:if test="${book != null}">value="${book.size}"</c:if>>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="image">
                    <option value="0" disabled selected>${t.g("Выберите обложку для книги")}</option>
                    <c:forEach items="${images}" var="image">
                        <option value="${image.id}"<c:if
                                test="${book.image.id == image.id}"> selected</c:if>>
                                ${image.fileName}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/books" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>