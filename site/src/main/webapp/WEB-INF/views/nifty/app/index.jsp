<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="/resources/lib/wow/animate.min.css" rel="stylesheet" />

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <div class="col-md-4 col-sm-4">
                    <img src="/resources/img/phone.jpg" alt="" class="wow flipInY" data-wow-duration="6s">
                </div>
                <div class="col-md-8 col-sm-8">
                    <h4><i class="fa fa-android"></i> <strong class="color-red">Мобильное приложение под Andoid</strong></h4>

                    <p>
                        Для более удобного использования словаря и разговорника, а также прослушивания адыгской мызыки
                        мы создали мобильное приложение под android.
                    </p>

                    <p>
                        <i class="fa fa-check"></i> Словарь и разговорник по категориям<br/>
                        <i class="fa fa-check"></i> Адыгская народная музыка<br/>
                        <i class="fa fa-check"></i> Поддерживается 3 языка интерфейса (русский, английский и
                        турецкий)<br/>
                    </p>
                    <a href="https://play.google.com/store/apps/details?id=ru.evgajukov.mobile.circassian.activity"
                       class="btn btn-success">Загрузить сейчас</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-sm-12">
                <div class="col-md-8 col-sm-8">
                    <h4><i class="fa fa-apple"></i> <strong class="color-red">Мобильное приложение под iOS</strong></h4>

                    <p>
                        Для более удобного использования словаря и разговорника, а также прослушивания адыгской мызыки
                        и доступа к другим разделам мы создали мобильное приложение под iOS.
                    </p>

                    <p>
                        <i class="fa fa-check"></i> Словарь и разговорник по категориям<br/>
                        <i class="fa fa-check"></i> Адыгская народная музыка<br/>
                        <i class="fa fa-check"></i> Просмотр словаря<br/>
                        <i class="fa fa-check"></i> Скачивание книг<br/>
                        <i class="fa fa-check"></i> Просмотр видео<br/>
                    </p>
                    <a href="https://itunes.apple.com/ru/app/si-bze/id1081805831?l=en&mt=8"
                       class="btn btn-success">Загрузить сейчас</a>
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="/resources/img/iphone.jpg" alt="" class="wow flipInY" data-wow-duration="6s">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/resources/lib/wow/wow.min.js"></script>

<script>
    new WOW().init();
</script>