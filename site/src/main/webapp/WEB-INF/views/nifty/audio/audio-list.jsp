<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${audioList}" var="audio">
    <div class="panel">
        <div class="panel-body">
            <table width="100%">
                <tbody>
                <tr>
                    <td width="50px">
                        <audio src="/upload/${audio.resource.id}/${audio.resource.fileName}"
                               id="audio-${audio.resource.id}" preload="none">
                            <a href="/upload/${audio.resource.id}/${audio.resource.fileName}">
                                <span class="text-2x"><i class="fa fa-volume-up"></i></span>
                            </a>
                        </audio>
                        <button class="audio-control btn btn-info btn-icon btn-circle icon-lg fa fa-play"
                                data-id="${audio.resource.id}"></button>
                    </td>
                    <td>
                        <span>${audio.title}</span><br />
                        <div id="audio-progress-${audio.resource.id}" class="progress progress-xs">
                            <div style="width: 0%;" class="progress-bar"></div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</c:forEach>