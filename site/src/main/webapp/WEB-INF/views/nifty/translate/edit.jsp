<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Слово")}: ${word.word}</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <select class="form-control chosen" name="word-id">
                    <option value="0" disabled selected>${t.g("Выберите слово на")} ${lang.smallName}</option>
                    <c:forEach items="${lang.words}" var="wordLang">
                        <option value="${wordLang.id}">${wordLang.word}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/dictionary/category/${word.category.id}" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>