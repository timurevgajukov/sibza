<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:choose>
    <c:when test="${hasResult}">
        <div class="tab-base">
            <!--Nav Tabs-->
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#words-tab">${t.g("Словарь")} / ${t.g("Разговорник")}
                        <c:choose>
                            <c:when test="${words != null && words.size() != 0}">
                                <span class="badge badge-primary">${words.size()}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="badge badge-danger">0</span>
                            </c:otherwise>
                        </c:choose>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#proverbs-tab">${t.g("Пословицы")}
                        <c:choose>
                            <c:when test="${proverbs != null && proverbs.size() != 0}">
                                <span class="badge badge-primary">${proverbs.size()}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="badge badge-danger">0</span>
                            </c:otherwise>
                        </c:choose>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#books-tab">${t.g("Книги")}
                        <c:choose>
                            <c:when test="${books != null && books.size() != 0}">
                                <span class="badge badge-primary">${books.size()}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="badge badge-danger">0</span>
                            </c:otherwise>
                        </c:choose>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#audio-tab">${t.g("Аудио")}
                        <c:choose>
                            <c:when test="${audioList != null && audioList.size() != 0}">
                                <span class="badge badge-primary">${audioList.size()}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="badge badge-danger">0</span>
                            </c:otherwise>
                        </c:choose>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#video-tab">${t.g("Видео")}
                        <c:choose>
                            <c:when test="${videoList != null && videoList.size() != 0}">
                                <span class="badge badge-primary">${videoList.size()}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="badge badge-danger">0</span>
                            </c:otherwise>
                        </c:choose>
                    </a>
                </li>
            </ul>

            <!--Tabs Content-->
            <div class="tab-content">
                <div id="words-tab" class="tab-pane fade active in">
                    <c:choose>
                        <c:when test="${words != null && words.size() != 0}">
                            <t:insertAttribute name="words-list" defaultValue="/WEB-INF/views/nifty/dictionary/words-list.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <p>${t.g("По ващему запросу")} "${query}" ${t.g("ничего не найдено")}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div id="proverbs-tab" class="tab-pane fade">
                    <c:choose>
                        <c:when test="${proverbs != null && proverbs.size() != 0}">
                            <c:forEach items="${proverbs}" var="proverb">
                                <div class="row">
                                    <div class="col-lg-1 col-md-2">
                                        <span class="text-4x"><i class="fa fa-quote-right text-light"></i></span>
                                    </div>
                                    <div class="col-lg-11 col-md-10">
                                        <h4>${proverb.body}</h4>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <p>${t.g("По ващему запросу")} "${query}" ${t.g("ничего не найдено")}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div id="books-tab" class="tab-pane fade">
                    <c:choose>
                        <c:when test="${books != null && books.size() != 0}">
                            <t:insertAttribute name="books-list" defaultValue="/WEB-INF/views/nifty/book/books-list.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <p>${t.g("По ващему запросу")} "${query}" ${t.g("ничего не найдено")}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div id="audio-tab" class="tab-pane fade">
                    <c:choose>
                        <c:when test="${audioList != null && audioList.size() != 0}">
                            <t:insertAttribute name="audio-list" defaultValue="/WEB-INF/views/nifty/audio/audio-list.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <p>${t.g("По ващему запросу")} "${query}" ${t.g("ничего не найдено")}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div id="video-tab" class="tab-pane fade">
                    <c:choose>
                        <c:when test="${videoList != null && videoList.size() != 0}">
                            <c:forEach items="${videoList}" var="video">
                                <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" src="${video.url}"></iframe>
                                        </div>
                                    </div>
                                    <div class="col-md-9 col-sm-12">
                                        <c:if test="${video.title != null && video.title.length() != 0}">
                                            <h3>${video.title}</h3>
                                        </c:if>
                                        ${t.g("Язык")}: ${video.language.smallName}
                                    </div>
                                </div>
                                <hr />
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <p>${t.g("По ващему запросу")} "${query}" ${t.g("ничего не найдено")}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="panel">
            <div class="panel-body">
                <p>${t.g("По ващему запросу")} "${query}" ${t.g("ничего не найдено")}</p>
            </div>
        </div>
    </c:otherwise>
</c:choose>