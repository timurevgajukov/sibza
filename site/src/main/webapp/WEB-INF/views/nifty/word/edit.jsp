<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-body">
            <div class="form-group">
                <select class="form-control chosen" name="category">
                    <option value="0" disabled selected>${t.g("Выберите категорию слова")}</option>
                    <c:forEach items="${categories}" var="category">
                        <option value="${category.id}"<c:if test="${(word == null && currentCategory != null && currentCategory.id == category.id) || word.category.id == category.id}"> selected</c:if>>
                            <c:choose>
                                <c:when test="${category.getTranslate(security.language) != null}">
                                    ${category.getTranslate(security.language)}
                                </c:when>
                                <c:otherwise>${category.name}</c:otherwise>
                            </c:choose>
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="language">
                    <option value="0" disabled selected>${t.g("Выберите язык слова")}</option>
                    <c:forEach items="${languages}" var="language">
                        <option value="${language.id}"<c:if
                                test="${word.language.id == language.id}"> selected</c:if>>${language.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="word" placeholder="${t.g("Введите слово")}"
                       value="<c:if test="${word != null}">${word.word}</c:if>">
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="resource">
                    <option value="0" disabled selected>${t.g("Привяжите ресурс к слову")}</option>
                    <option value="-1">${t.g("Без ресурса")}</option>
                    <c:forEach items="${resources}" var="resource">
                        <option value="${resource.id}"<c:if
                                test="${word.resources.size() > 0 && word.resources.get(0).id == resource.id}"> selected</c:if>>
                                ${resource.fileName}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/dictionary<c:if test="${currentCategory != null}">/category/${currentCategory.id}</c:if>"
               class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>