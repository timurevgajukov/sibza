<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<style>
    #container {
        background-color: white;
    }

    #chart-map {
        height: 500px;
        min-width: 310px;
        max-width: 800px;
        margin: 0 auto;
    }

    .incremental-counter .num {
        background: #f8f8f8 none repeat scroll 0 0;
        border: 1px solid #fff;
        border-radius: 4px;
        color: #00aae6;
        display: inline-block;
        height: 64px;
        line-height: 62px;
        margin: 0 4.5px;
        position: relative;
        text-align: center;
        top: -1px;
        width: 50px;
        font-size: 45px;
        font-size: 3.72625em;
        font-weight: 700;
        box-shadow: 0 2px 3px rgba(0, 0, 0, 0.45);
        font-family: "Open Sans", Arial, Helvetica, sans-serif;
    }

    .incremental-counter .num::before {
        background: #00aae6;
        content: "";
        display: block;
        height: 1px;
        left: -1px;
        margin: -0.5px 0 0;
        position: absolute;
        right: -1px;
        top: 50%;
        width: auto;
    }
</style>

<div id="chart-map" style="max-width: 1000px"></div>

<div class="text-center">
    <h1>АДЫГОВ В МИРЕ:</h1>

    <div class="incremental-counter" data-start="${count - 1500}" data-value="${count}"></div>
    <br/><br/>

    <c:if test="${'localhost' == pageContext.request.serverName}">
        <a href="#" data-toggle="modal" data-target="#modal-reg" class="btn btn-primary btn-lg">РАССКАЗАТЬ О СЕБЕ</a>
    </c:if>
</div>

<c:if test="${'localhost' == pageContext.request.serverName}">
    <!-- REG MODAL WINDOW: BEGIN -->
    <div id="modal-reg" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-reg" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="modal-reg-form" method="post" action="/adygi">
                    <div class="modal-header">
                        <button data-dismiss="modal" class="close" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">${t.g("Регистрация адыга")}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input class="form-control" type="text" id="reg-email" name="email"
                                       placeholder="${t.g("Введите электронную почту")}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                <input class="form-control" type="text" id="reg-name" name="name"
                                       placeholder="${t.g("Введите ваше имя")}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                <input class="form-control" type="password" id="reg-password1" name="password1"
                                       placeholder="${t.g("Введите пароль")}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                <input class="form-control" type="password" id="reg-password2" name="password2"
                                       placeholder="${t.g("Введите пароль повторно")}"/>
                            </div>
                        </div>
                        <input type="hidden" name="redirect" id="reg-redirect" value="/"/>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-primary"
                                id="modal-reg-submit">${t.g("Зарегистрироваться")}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- REG MODAL WINDOW: END -->
</c:if>

<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/data.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/custom/world.js"></script>

<script src="/resources/lib/counter/jquery.incremental-counter.js"></script>

<script>
    $(document).ready(function () {
        // https://www.highcharts.com/samples/data/jsonp.php?filename=world-population-density.json&callback=?
        $.getJSON('/adygi/map/data', function (data) {

            // Initiate the chart
            $('#chart-map').highcharts('Map', {

                title: {
                    text: ''
                },

                mapNavigation: {
                    enabled: true,
                    enableDoubleClickZoomTo: true
                },

                colorAxis: {
                    min: 1,
                    max: ${count},
                    type: 'logarithmic'
                },

                series: [{
                    data: data,
                    mapData: Highcharts.maps['custom/world'],
                    joinBy: ['iso-a2', 'code'],
                    name: 'Проживает',
                    states: {
                        hover: {
                            color: '#BADA55'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' адыгов'
                    }
                }]
            });
        });
        $(".incremental-counter").incrementalCounter({
            numbers: 8
        });
    });
</script>