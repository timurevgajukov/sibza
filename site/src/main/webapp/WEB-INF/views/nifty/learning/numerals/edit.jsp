<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-body">
            <div class="form-group">
                <input class="form-control" type="number" name="number" id="number" required
                       placeholder="${t.g("Введите число")}"
                       <c:if test="${numeral != null}">value="${numeral.number}"</c:if>>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="word">
                    <option value="0" disabled selected>${t.g("Привяжите слово к числу")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${numeral != null && numeral.word.id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/numerals" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>