<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${security.adminOrEditor}">
    <a href="/alphabet/add" class="btn btn-info">${t.g("Добавить")}</a><br/><br/>
</c:if>

<c:choose>
    <c:when test="${alphabet != null && alphabet.letters != null && alphabet.letters.size() != 0}">
        <c:forEach items="${alphabet.letters}" var="letter">
            <div class="col-md-4 col-sm-6">
                <div class="panel">
                    <div class="panel-header">
                        <c:if test="${security.adminOrEditor}">
                            <div class="panel-control">
                                <a href="/alphabet/edit/${letter.id}" class="btn">
                                    <i class="fa fa-pencil text-light"></i>
                                </a>
                                <a href="/alphabet/delete/request/${letter.id}" class="btn">
                                    <i class="fa fa-trash text-light"></i>
                                </a>
                            </div>
                        </c:if>
                    </div>
                    <div class="panel-body text-center">
                        <c:if test="${security.adminOrEditor}"><br/></c:if>
                        <span class="text-5x">${letter.letter}</span><br/>
                        <span class="text-2x" style="color: #c9c9c9">${letter.lowLetter}</span><br/><br/>

                        <div id="words-${letter.id}"></div>
                    </div>
                    <div id="footer-${letter.id}" class="panel-footer text-center"
                         style="padding: 0px; height: 38px;"></div>
                </div>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        ${t.g("Алфавит пуст")}
    </c:otherwise>
</c:choose>

<script type="text/plain" id="tmpl_audio">
    <audio src="/upload/{{=it.id}}/{{=it.fileName}}" id="audio-{{=it.id}}" preload="none">
        <a href="/upload/{{=it.id}}/{{=it.fileName}}">
            <span class="text-2x"><i class="fa fa-volume-up"></i></span>
        </a>
    </audio>
    <button class="audio-control btn btn-primary btn-block btn-icon icon-2x fa fa-play"
        data-id="{{=it.id}}" data-played="0"></button>
</script>
<script type="text/plain" id="tmpl_words">
    <div id="slide-{{=it.id}}" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators dark out">
        {{~it.words :value:index}}
            <li{{? index === 0}} class="active"{{?}} data-slide-to="{{=index}}" data-target="#slide-{{=it.id}}"></li>
        {{~}}
        </ol>

        <div class="carousel-inner text-center">
        {{~it.words :value:index}}
            <div class="item{{? index === 0}} active{{?}}">
                {{? value.resources[0]}}
                <audio src="/upload/{{=value.resources[0].id}}/{{=value.resources[0].fileName}}"
                    id="audio-{{=value.resources[0].id}}" preload="none">
                    <a href="/upload/{{=value.resources[0].id}}/{{=value.resources[0].fileName}}">
                        <i class="fa fa-volume-up"></i>
                    </a>
                </audio>
                <span class="audio-control fa fa-play text-success"
                    data-id="{{=value.resources[0].id}}" data-played="0"></span>&nbsp;
                {{?}}
                <span style="color: #c9c9c9;">
                    {{=value.word}}{{? value.translates[0]}} &mdash; {{=value.translates[0].word}}{{?}}
                </span>
            </div>
        {{~}}
        </div>

        <a class="carousel-control left" data-slide="prev" href="#slide-{{=it.id}}">
            <i class="fa fa-chevron-left fa-2x"></i>
        </a>
        <a class="carousel-control right" data-slide="next" href="#slide-{{=it.id}}">
            <i class="fa fa-chevron-right fa-2x"></i>
        </a>
   </div>
</script>

<script>
    $(document).ready(function() {
        // получаем данные по озвучке букв
        $.post('/alphabet/letters/resources/list', function(list) {
            if (list === undefined || list.length == 0) {
                return;
            }

            var tmpl = doT.template($('#tmpl_audio').html());
            $.each(list, function() {
                var letter = this;
                if (letter.resources && letter.resources.length != 0) {
                    var resource = letter.resources[0];
                    $('#footer-' + letter.id).html(tmpl(resource));
                }
            });
        });

        // получаем данные по примерам слов
        $.post('/alphabet/letters/words/list', function(list) {
            if (list === undefined || list.length == 0) {
                return;
            }

            var tmpl = doT.template($('#tmpl_words').html());
            $.each(list, function() {
                var letter = this;
                if (letter.words.length != 0) {
                    $('#words-' + letter.id).html(tmpl(letter));
                } else {
                    $('#words-' + letter.id).html("<br /><br /><br />");
                }
            });
        });
    });
</script>