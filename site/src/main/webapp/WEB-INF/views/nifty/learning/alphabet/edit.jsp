<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-body">
            <div class="form-group">
                <input class="form-control" type="text" name="letter" id="letter" required
                       placeholder="${t.g("Введите букву алфавита")}"
                       <c:if test="${letter != null}">value="${letter.letter}"</c:if>>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="resource">
                    <option value="0" disabled selected>${t.g("Привяжите ресурс к букве")}</option>
                    <c:forEach items="${resources}" var="resource">
                        <option value="${resource.id}"<c:if
                                test="${letter.resources.size() > 0 && letter.resources.get(0).id == resource.id}"> selected</c:if>>
                                ${resource.fileName}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="word1">
                    <option value="0" disabled selected>${t.g("Привяжите слово с буквой в начале")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${letter.words.size() > 0 && letter.words.get(0).id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="word2">
                    <option value="0" disabled selected>${t.g("Привяжите слово с буквой в середине")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${letter.words.size() > 1 && letter.words.get(1).id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="word3">
                    <option value="0" disabled selected>${t.g("Привяжите слово с буквой в конце")}</option>
                    <c:forEach items="${words}" var="word">
                        <option value="${word.id}"<c:if
                                test="${letter.words.size() > 2 && letter.words.get(2).id == word.id}"> selected</c:if>>
                                ${word.word}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/alphabet" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>