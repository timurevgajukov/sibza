<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form method="post">
    <div class="panel">
        <div class="panel-heading">
            <h4 class="panel-title">${t.g("Слово")}</h4>
        </div>
        <div class="panel-body">
            <c:if test="${similarWords != null && similarWords.size() != 0}">
                <p class="text-danger">${t.g("Найдены похожие слова")}</p>
                <ul class="text-danger">
                    <c:forEach items="${similarWords}" var="similarWord">
                        <li>
                            ${similarWord.word} [${similarWord.category}]
                            <c:if test="${similarWord.translates != null && similarWord.translates.size() != 0}">
                                <ul>
                                    <c:forEach items="${similarWord.translates}" var="translate">
                                        <li>${translate.word}</li>
                                    </c:forEach>
                                </ul>
                            </c:if>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
            <div class="form-group">
                <input class="form-control" type="text" name="word" value="${item.word}"/>
            </div>
            <div class="form-group">
                <textarea class="form-control" name="translates">${item.description}</textarea>
                <small class="help-block">
                    ${t.g("Введите слова перевода разделенные ## (например: слово1##слово2)")}<br/>
                    ${t.g("Если необходимо добавить описание к переводу, то разделяйте перевод и описание @@ (например: перевод@@описание)")}<br/>
                    ${t.g("Для использования переводов от другого слова, просто поставьте перед ним >> (например: >>слово)")}
                </small>
            </div>
            <div class="form-group">
                <select class="form-control chosen" name="category">
                    <option value="0" disabled selected>${t.g("Выберите категорию слова")}</option>
                    <c:forEach items="${categories}" var="category">
                        <option value="${category.id}"<c:if
                                test="${defaultCategory.id == category.id}"> selected</c:if>>${category.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/dsl" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>