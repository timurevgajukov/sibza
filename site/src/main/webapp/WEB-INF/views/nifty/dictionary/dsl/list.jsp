<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="infinite-container">
    <c:forEach items="${list}" var="item">
        <div class="panel infinite-item">
            <div class="panel-heading">
                <h4 class="panel-title">${item.word}</h4>
            </div>
            <div class="panel-body">
                ${item.description}
            </div>
            <div class="panel-footer text-right">
                <a href="/dsl/edit/${item.id}" class="btn btn-primary">${t.g("Обработать")}</a>
                <a href="/dsl/postponed/request/${item.id}" class="btn btn-success">${t.g("Отложить")}</a>
                <a href="/dsl/delete/request/${item.id}" class="btn btn-danger">${t.g("Удалить")}</a>
            </div>
        </div>
    </c:forEach>
</div>

<a class="infinite-more-link" href="/dsl/more"></a>

<script src="/resources/lib/waypoints/jquery.waypoints.min.js"></script>
<script src="/resources/lib/waypoints/shortcuts/infinite.min.js"></script>

<script>
    $(document).ready(function () {
        var infinite = new Waypoint.Infinite({
            element: $('.infinite-container')[0]
        });
    });
</script>