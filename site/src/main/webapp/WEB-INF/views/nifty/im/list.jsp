<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="last-messages-list"></div>

<script type="text/plain" id="tmpl_last_messages_empty">
<div class="panel">
    <div class="panel-body">
        {{=it.message}}
    </div>
</div>
</script>
<script type="text/plain" id="tmpl_last_message">
<div class="col-md-6">
    <a href="/im/{{=it.user.id}}">
        <div class="panel media pad-all">
            <div class="media-left">
                <img src="{{=it.user.avatarUrl}}" class="img-lg img-border img-circle" alt="{{=it.user.fullName}}">
            </div>
            <div class="media-body">
                <h4 class="text-lg text-overflow mar-no">{{=it.user.fullName}}</h4>
                <p class="text-sm">{{=it.createDt}}</p>
                <div class="media">
                    <div class="media-left">
                    {{? it.author}}
                        <img class="img-circle img-user media-object" src="{{=it.author.avatarUrl}}" alt="{{=it.author.fullName}}">
                    {{?}}
                    </div>
                    <div class="media-body">
                        {{=it.message}}
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
</script>

<script src="/resources/lib/moment/moment.min.js"></script>
<script src="/resources/lib/moment/moment-timezone.min.js"></script>
<script src="/resources/lib/moment/locales.min.js"></script>

<script>
    $(document).ready(function() {
        moment.locale('ru');

        $.post('/im/last', function(messages) {
            var divList = $('#last-messages-list');

            if (messages === undefined || messages.length == 0) {
                var tmpl = doT.template($('#tmpl_last_messages_empty').html());
                var data = {
                    message: '${t.g("Список диалогов пуст")}'
                }
                divList.append(tmpl(data));
                return;
            }

            var tmpl = doT.template($('#tmpl_last_message').html());
            for (var i = 0; i < messages.length; i=i+2) {
                var rowDiv = '<div class="row">';
                for (var j = 0; j < 2; j++) {
                    if (i+j < messages.length) {
                        var message = messages[i+j];
                        var data = {
                            message: message.message,
                            createDt: moment(message.createDt).fromNow()
                        }
                        if (message.source.id == ${security.user.id}) {
                            data.user = message.destination;
                            data.author = message.source;
                        } else {
                            data.user = message.source;
                        }
                        rowDiv += tmpl(data);
                    }
                }
                rowDiv += '</div>';
                divList.append(rowDiv);
            }
        });
    });
</script>