<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<a href="/resources/add" class="btn btn-info">${t.g("Добавить")}</a><br /><br />

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">${t.g("Файлы")}</h3>
    </div>
    <div class="panel-body">
        <c:choose>
            <c:when test="${resources != null && resources.size() != 0}">
                <table id="data-table" class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>${t.g("Тип")}</th>
                        <th>${t.g("Медиафайл")}</th>
                        <th>${t.g("Действия")}</th>
                    </tr>
                    </thead>
                    <tbody
                    <c:forEach items="${resources}" var="resource">
                        <tr>
                            <td>${resource.id}</td>
                            <td>${resource.type.type.name}</td>
                            <td>${resource.fileName}</td>
                            <td>
                                <a href="/resources/delete/${resource.id}" class="btn btn-danger">${t.g("удалить")}</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                ${t.g("Список медиафайлов пуст")}
            </c:otherwise>
        </c:choose>
    </div>
</div>