<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="//hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<form method="post">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Медиа файл")}</h3>
        </div>
        <div class="panel-body">
            <div id="fileuploader">${t.g("Выберите файл")}</div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/resources" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>

<script src="//hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>
<script>
    $(document).ready(function () {
        // плагин для загрузки файлов
        $("#fileuploader").uploadFile({
            url: "/resources/upload",
            fileName: "mediafile"
        });
    });

</script>