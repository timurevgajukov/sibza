<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form method="post">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${key}</h3>
        </div>
        <div class="panel-body">
            <c:forEach items="${languages}" var="lang">
                <c:if test="${lang.code != 'AD' && lang.code != 'RU'}">
                    <div class="form-group">
                        <div class="input-group mar-btn">
                            <input class="form-control" type="text" name="text-lang" id="text-lang-${lang.code}"
                                   placeholder="${t.g("Введите текст на")} ${lang.smallName}"
                                   <c:if test="${list.get(lang.code) != null}">value="${list.get(lang.code).value}"</c:if>>
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-translate" data-lng="${lang.code}" type="button">
                                    <i class="fa fa-mail-reply"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </c:if>
            </c:forEach>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/interfaces" class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.btn-translate').click(function () {
            var lng = $(this).data('lng');
            var key = '${key}';
            $.post("/services/translate", {text: '${key}', lng: lng}, function (result) {
                if (result.code == 0) {
                    $('#text-lang-' + lng).val(result.body);
                }
            });
        });
    });
</script>