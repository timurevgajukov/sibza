<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<a href="" class="btn btn-info">${t.g("Добавить")}</a><br /><br />

<div class="panel">
    <div class="panel-body">
        <c:choose>
            <c:when test="${languages != null && languages.size() != 0}">
                <table id="data-table" class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>${t.g("Наименование")}</th>
                        <th>${t.g("Краткое наименование")}</th>
                        <th>${t.g("Действия")}</th>
                    </tr>
                    </thead>
                    <tbody
                    <c:forEach items="${languages}" var="lang">
                        <tr>
                            <td>${lang.id}</td>
                            <td>${lang.name}</td>
                            <td>${lang.smallName}</td>
                            <td>
                                <a href="" class="btn btn-success">${t.g("редактировать")}</a>
                                <a href="" class="btn btn-danger">${t.g("удалить")}</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                </table>
            </c:when>
            <c:otherwise>
                ${t.g("Список доступных языков пуст")}
            </c:otherwise>
        </c:choose>
    </div>
</div>