<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${security.isAdminOrEditor()}">
    <a href="/proverbs/add" class="btn btn-info">${t.g("Добавить")}</a><br/><br/>
</c:if>

<c:choose>
    <c:when test="${proverbs != null && proverbs.size() != 0}">
        <div class="infinite-container">
            <c:forEach items="${proverbs}" var="proverb">
                <div class="row infinite-item">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <div class="panel">
                            <div class="panel-header">
                                <c:if test="${security.adminOrEditor}">
                                    <div class="panel-control">
                                        <a href="/proverbs/edit/${proverb.id}" class="btn">
                                            <i class="fa fa-pencil text-light"></i>
                                        </a>
                                        <a href="/proverbs/delete/request/${proverb.id}" class="btn">
                                            <i class="fa fa-trash text-light"></i>
                                        </a>
                                    </div>
                                </c:if>
                            </div>
                            <div class="panel-body">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td width="60px">
                                            <span class="text-4x"><i class="fa fa-quote-right text-light"></i></span>
                                        </td>
                                        <td><h4>${proverb.body}</h4></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--
                            <div class="panel-footer text-right">
                                <a href="#" class="btn btn-purple show-comments" data-id="${proverb.id}"
                                   data-toggle="modal" data-target="#modal-comments">
                                        ${t.g("Комментарии")}
                                </a>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <a class="infinite-more-link" href="/proverbs/more"></a>
    </c:when>
    <c:otherwise>
        <br/><br/>${t.g("Список пословиц пуст")}
    </c:otherwise>
</c:choose>

<!-- COMMENTS MODAL WINDOW: BEGIN -->
<div id="modal-comments" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-comments"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Комментарии")}</h4>
            </div>
            <div class="modal-body">
                <c:choose>
                    <c:when test="${security.auth}">
                        <textarea class="form-control" id="comment-body" rows="2"
                                  placeholder="${t.g("Ваш комментарий")}"
                                  required></textarea>

                        <div class="mar-top clearfix">
                            <button class="btn btn-sm btn-info pull-right" id="btn-comment-add">
                                <i class="fa fa-pencil fa-fw"></i> ${t.g("Опубликовать")}
                            </button>
                        </div>
                    </c:when>
                    <c:otherwise>
                        ${t.g("Комментировать могут только авторизованные пользователи")}
                    </c:otherwise>
                </c:choose>
                <hr/>
                <div class="nano has-scrollbar" style="height: 400px;">
                    <div class="nano-content pad-all" tabindex="0" style="right: -17px;">
                        <div id="comments-list"></div>
                    </div>
                    <div class="nano-pane">
                        <div class="nano-slider" style="height: 230px; transform: translate(0px, 0px);"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
            </div>
        </div>
    </div>
</div>
<!-- COMMENTS MODAL WINDOW: END -->

<script type="text/plain" id="tmpl_comment">
    <div class="media-block">
        <a class="media-left" href="#">
            <img class="img-circle img-sm" alt="{{=it.user.email}}" src="/resources/img/avatar/{{=it.user.info.defaultAvatar.file}}">
        </a>
        <div class="media-body">
            <div class="mar-btm">
                <a href="#" class="btn-link text-semibold media-heading box-inline">{{=it.user.fullName}}</a>
                <p class="text-muted text-sm">{{=it.formattedCreateDt}}</p>
            </div>
            <p id="comment-body-{{=it.id}}">{{=it.body}}</p>
            <div id="comment-edit-block-{{=it.id}}" style="display: none;">
                <textarea class="form-control" id="comment-body" rows="2" placeholder="${t.g("Ваш комментарий")}"
                        required>{{=it.body}}</textarea>
                <div class="mar-top clearfix">
                    <button class="btn btn-sm btn-info pull-right" id="btn-comment-edit">
                        <i class="fa fa-pencil fa-fw"></i> ${t.g("Редактировать")}
                    </button>
                </div>
            </div>
            <div class="pad-ver">
                {{=it.owner}}
                {{=it.like}}
            </div>
            <hr />
        </div>
    </div>
</script>
<script type="text/plain" id="tmpl_comment_edit">
    <div class="btn-group">
        <button class="btn btn-sm btn-default btn-hover-info btn-edit" href="#" title="${t.g("Редактировать")}">
            <i class="fa fa-pencil"></i>
        </button>
        <a class="btn btn-sm btn-default btn-hover-danger disabled" href="#" title="${t.g("Удалить")}">
            <i class="fa fa-trash"></i>
        </a>
    </div>
</script>
<script type="text/plain" id="tmpl_like">
    <div class="btn-group">
        <a class="btn btn-sm btn-default btn-hover-success disabled" href="#" title="${t.g("Мне нравится")}">
            <i class="fa fa-thumbs-up"></i>
        </a>
    </div>
    <a class="btn btn-sm btn-default btn-hover-primary disabled" href="#">${t.g("Ответить")}</a>
</script>

<script src="/resources/lib/waypoints/jquery.waypoints.min.js"></script>
<script src="/resources/lib/waypoints/shortcuts/infinite.min.js"></script>

<script>
    $(document).ready(function () {
        var currentId = 0;

        var infinite = new Waypoint.Infinite({
            element: $('.infinite-container')[0]
        });

        $('#btn-comment-add').click(function () {
            // добавить новый комментарий
            $.ajax({
                type: 'POST',
                url: "/proverbs/comments/add",
                data: {id: currentId, comment: $('#comment-body').val()},
                success: function (data) {
                    $('#comment-body').val('');

                    var divList = $('#comments-list');
                    var tmpl = doT.template($('#tmpl_comment').html());

                    divList.prepend(tmpl(data));
                }
            });
        });

        $('.show-comments').click(function () {
            // отобразить комментарии к пословице
            currentId = $(this).data("id");
            $.post("/proverbs/comments/list/" + currentId, function (comments) {
                var tmpl = doT.template($('#tmpl_comment').html());
                var tmpl_owner = doT.template($('#tmpl_comment_edit').html());
                var tmpl_like = doT.template($('#tmpl_like').html());

                var divList = $('#comments-list');
                divList.empty();
                if (comments.length != 0) {
                    // отображаем список комментариев
                    $.each(comments, function() {
                        if (this.editable) {
                            this.owner = tmpl_owner();
                            this.like = "";
                        } else {
                            this.owner = "";
                            this.like = tmpl_like();
                        }
                        divList.append(tmpl(this));
                    });
                } else {
                    divList.append("${t.g("Комментариев пока нет. Вы будете первым!")}");
                }
            });
        });
    });
</script>