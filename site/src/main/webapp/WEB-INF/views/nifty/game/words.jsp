<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div<c:if test="${token == null}"> class="col-md-6 col-sm-12"</c:if>>
        <div id="gameBlock" class="panel">
            <div class="panel-heading">
                <div class="panel-control">
                    <button class="btn-help btn btn-default add-tooltip" data-target="#gameBlock" autocomplete="off"
                            data-placement="bottom" data-toggle="tooltip"
                            data-original-title="${t.g("Подскажет правильный перевод для слова")}">
                        <i class="fa fa-question-circle"></i>
                        ${t.g("Подсказка")}
                    </button>
                    <a href="#" class="btn btn-info btn-rounded show-rating" data-toggle="modal" data-target="#modal-rating">
                        <i class="fa fa-trophy fa-fw"></i>
                        <span class="score">${security.score.score}</span>
                    </a>
                </div>
                <h4 class="panel-title">&nbsp;</h4>
            </div>
            <div class="panel-body text-center">
                <span class="word <c:choose><c:when test="${token == null}">text-3x</c:when><c:otherwise>text-2x</c:otherwise></c:choose>">
                    ${word.word}
                </span>

                <div class="word-audio">
                    <c:if test="${word.resources != null && word.resources.size() != 0}">
                        <audio src="/upload/${word.resources.get(0).id}/${word.resources.get(0).fileName}"
                               id="audio-${word.resources.get(0).id}" preload="none">
                            <a href="/upload/${word.resources.get(0).id}/${word.resources.get(0).fileName}">
                                <span class="text-3x"><i class="fa fa-volume-up"></i></span>
                            </a>
                        </audio>
                        <span class="audio-control icon-3x fa fa-play text-success"
                              data-id="${word.resources.get(0).id}"
                              data-played="0"></span>
                    </c:if>
                </div>

                <br/>
                <c:forEach items="${answers}" var="answer">
                    <button class="btn-answer btn btn-block btn-info
                            <c:if test="${rightAnswer.word == answer.word}">answer-right</c:if>"
                            data-id="${answer.id}" data-target="#gameBlock" autocomplete="off">
                            ${answer.word}
                    </button>
                </c:forEach>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <span id="vk_share_btn"></span>
                    </div>
                    <div class="col-md-6 col-xs-6 text-right">
                        <a href="/game/words/restart<c:if test="${token != null}">?token=${token}</c:if>"
                           class="btn-restart btn btn-success" style="display: none;">${t.g("Начать заново")}</a>
                        <button class="btn-next-word btn btn-primary"
                                data-target="#gameBlock" autocomplete="off" disabled>
                                    ${t.g("Следующее слово")}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RATINGS MODAL WINDOW: BEGIN -->
<div id="modal-rating" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-rating" aria-hidden="true">
    <div class="modal-dialog">
        <c:choose>
            <c:when test="${competition != null}">
                <div class="tab-base">

                    <!--Nav Tabs-->
                    <ul id="rating-tabs" class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#rating-tab1">${t.g("Общий")}</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#rating-tab2">${t.g("Конкурс")}</a>
                        </li>
                    </ul>

                    <!--Tabs Content-->
                    <div class="tab-content">
                        <div id="rating-tab1" class="tab-pane fade active in">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">${t.g("Общий рейтинг")}</h4>
                                </div>
                                <div class="nano" style="height: 400px;">
                                    <div class="nano-content pad-all">
                                        <div id="rating-list"></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
                                </div>
                            </div>
                        </div>
                        <div id="rating-tab2" class="tab-pane fade">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">${t.g("Конкурсный рейтинг")}</h4>
                                </div>
                                <div class="nano" style="height: 400px;">
                                    <div class="nano-content pad-all">
                                        <div id="competition-rating-list"></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="modal-content">
                    <div class="modal-header">
                        <button data-dismiss="modal" class="close" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">${t.g("Рейтинг")}</h4>
                    </div>
                    <div class="nano" style="height: 400px;">
                        <div class="nano-content pad-all">
                            <div id="rating-list"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
<!-- RATINGS WINDOW: END -->

<script type="text/plain" id="tmpl_rating">
    <tr>
        <td class="min-width text-right">{{=it.id}}.</td>
        <td class="min-width">
            <img class="img-circle img-user media-object" src="{{=it.rating.user.avatarUrl}}" alt="{{=it.rating.user.fullName}}">
        </td>
        <td>
            <a href="/users/{{=it.rating.user.profileUrl}}">{{=it.rating.user.fullName}}</a>
        </td>
        <td class="text-right">
            <span class="badge badge-primary">{{=it.rating.score}}</span>
        </td>
    </tr>
</script>
<script type="text/plain" id="tmpl_word_audio">
    <audio src="/upload/{{=it.id}}/{{=it.fileName}}" id="audio-{{=it.id}}" preload="none">
        <a href="/upload/{{=it.id}}/{{=it.fileName}}">
            <span class="text-3x"><i class="fa fa-volume-up"></i></span>
        </a>
    </audio>
    <span class="audio-control icon-3x fa fa-play text-success" data-id="{{=it.id}}" data-played="0"></span>
</script>

<script type="text/javascript" src="//vk.com/js/api/share.js?90" charset="utf-8"></script>

<script>
    $(document).ready(function () {
        <c:if test="${competition != null}">
            $('#rating-tabs a:last').tab('show');
        </c:if>

        $('#modal-rating').on('shown.bs.modal', function() {
            $(".nano").nanoScroller();
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function() {
            $(".nano").nanoScroller();
        });

        // Шаринг в ВКонтакте
        document.getElementById('vk_share_btn').innerHTML = VK.Share.button({
            url: 'https://sibza.org/game/words',
            title: '${t.g("Си бзэ: игра в слова")}',
            <c:choose>
                <c:when test="${security.score.score > 10}">
                    description: '${t.g("Мой текущий счет в игре:")} ${security.score.score}',
                </c:when>
                <c:otherwise>
                    description: '${t.g("Узнайте на сколько хорошо вы знаете адыгский язык.")}',
                </c:otherwise>
            </c:choose>
            image: 'https://sibza.org/resources/img/logo-512x512.png',
            noparse: true
        }, {
            type: 'custom',
            text: '<button class="btn btn-icon btn-hover-info fa fa-vk icon-lg add-tooltip" ' +
            'data-original-title="${t.g("Сообщите о своем результате в VK")}" data-container="body"></button>'
        });

        var wordId = ${word.id};
        var rightAnswerId = ${rightAnswer.id};

        $('.btn-answer').niftyOverlay().on('click', function () {
            var btn = $(this);
            btn.niftyOverlay('show');

            // предварительно заблокируем все кнопки с ответами
            $('.btn-answer').attr('disabled', 'true');

            var answerId = $(this).data('id');
            $(this).removeClass('btn-info');
            if (rightAnswerId == answerId) {
                $(this).addClass('btn-success');

                $.post('/game/words/score', {question: wordId, answer: answerId, answer_code: 1}, function (score) {
                    // обновим счет
                    $('.score').html(score);

                    // разблокируем кнопку для перехода на новое слово после прихода ответа
                    $('.btn-next-word').removeAttr('disabled');

                    btn.niftyOverlay('hide');

                    // предварительно заблокируем все кнопки с ответами
                    $('.btn-answer').attr('disabled', 'true');
                    $('.btn-help').attr('disabled', 'true');
                });
            } else {
                $(this).addClass('btn-danger');

                $('.answer-right').removeClass('btn-info');
                $('.answer-right').addClass('btn-success');

                $.post('/game/words/score', {question: wordId, answer: answerId, answer_code: -1}, function (score) {
                    if (score < 0) {
                        // предложим играть заново
                        $('.score').html(0);
                        $('.btn-next-word').hide();
                        $('.btn-restart').show();
                    } else {
                        // обновим счет
                        $('.score').html(score);

                        // разблокируем кнопку для перехода на новое слово после прихода ответа
                        $('.btn-next-word').removeAttr('disabled');
                    }

                    btn.niftyOverlay('hide');

                    // предварительно заблокируем все кнопки с ответами
                    $('.btn-answer').attr('disabled', 'true');
                    $('.btn-help').attr('disabled', 'true');
                });
            }
        });

        $('.btn-help').niftyOverlay().on('click', function () {
            var btn = $(this);
            btn.niftyOverlay('show');

            // предварительно заблокируем все кнопки с ответами
            $('.btn-answer').attr('disabled', 'true');

            $('.answer-right').removeClass('btn-info');
            $('.answer-right').addClass('btn-success');

            $.post('/game/words/score', {question: wordId, answer: 0, answer_code: 0}, function (score) {
                if (score < 0) {
                    // предложим играть заново
                    $('.score').html(0);
                    $('.btn-next-word').hide();
                    $('.btn-restart').show();
                } else {
                    // обновим счет
                    $('.score').html(score);

                    // разблокируем кнопку для перехода на новое слово после прихода ответа
                    $('.btn-next-word').removeAttr('disabled');
                }

                btn.niftyOverlay('hide');

                // предварительно заблокируем все кнопки с ответами
                $('.btn-answer').attr('disabled', 'true');
                $('.btn-help').attr('disabled', 'true');
            });
        });

        $('.show-rating').click(function() {
            <c:choose>
                <c:when test="${competition != null}">
                    $.post('/game/words/rating/list', { competition: true }, function(list) {
                        var tmpl = doT.template($('#tmpl_rating').html());
                        var divList = $('#competition-rating-list');
                        divList.empty();
                        if (list.length != 0) {
                            divList.append("<table class='table table-hover table-vcenter'><tbody></tbody></table>");
                            var tableBody = divList.find("tbody");

                            var i = 1;
                            $.each(list, function() {
                                var data = {
                                    id: i++,
                                    rating: this
                                }
                                tableBody.append(tmpl(data));
                            });
                            $(".nano").nanoScroller();
                        } else {
                            divList.append("${t.g("Рейтинг пуст")}");
                        }
                    });
                    $.post('/game/words/rating/list', { competition: false }, function(list) {
                        var tmpl = doT.template($('#tmpl_rating').html());
                        var divList = $('#rating-list');
                        divList.empty();
                        if (list.length != 0) {
                            divList.append("<table class='table table-hover table-vcenter'><tbody></tbody></table>");
                            var tableBody = divList.find("tbody");

                            var i = 1;
                            $.each(list, function() {
                                var data = {
                                    id: i++,
                                    rating: this
                                }
                                tableBody.append(tmpl(data));
                            });
                            $(".nano").nanoScroller();
                        } else {
                            divList.append("${t.g("Рейтинг пуст")}");
                        }
                    });
                </c:when>
                <c:otherwise>
                    $.post('/game/words/rating/list', { competition: false }, function(list) {
                        var tmpl = doT.template($('#tmpl_rating').html());
                        var divList = $('#rating-list');
                        divList.empty();
                        if (list.length != 0) {
                            divList.append("<table class='table table-hover table-vcenter'><tbody></tbody></table>");
                            var tableBody = divList.find("tbody");

                            var i = 1;
                            $.each(list, function() {
                                var data = {
                                    id: i++,
                                    rating: this
                                }
                                tableBody.append(tmpl(data));
                            });
                            $(".nano").nanoScroller();
                        } else {
                            divList.append("${t.g("Рейтинг пуст")}");
                        }
                    });
                </c:otherwise>
            </c:choose>
        });

        $('.btn-next-word').niftyOverlay().on('click', function() {
            var btn = $(this);
            btn.niftyOverlay('show');

            $.post('/game/words/question/get', function(question) {
                btn.niftyOverlay('hide');

                if (question !== undefined) {
                    // добавляем новое слово
                    $('.word').html(question.word.word);
                    wordId = question.word.id;
                    rightAnswerId = question.rightAnswer.id;

                    // если у слова есть озвучка, то добавляем и его
                    $('.word-audio').empty();
                    if (question.word.resources.length != 0) {
                        var tmpl = doT.template($('#tmpl_word_audio').html());
                        $('.word-audio').append(tmpl(question.word.resources[0]));
                    }

                    // добавляем варианты ответов и разблокируем их
                    $('.btn-answer').removeClass('answer-right');
                    $('.btn-answer').each(function(index) {
                        var btnAnswer = $('.btn-answer').eq(index);
                        btnAnswer.html(question.answers[index].word);
                        btnAnswer.data('id', question.answers[index].id);
                        if (rightAnswerId == question.answers[index].id) {
                            btnAnswer.addClass('answer-right');
                        }
                    });
                    $('.btn-answer').removeClass('btn-success');
                    $('.btn-answer').removeClass('btn-danger');
                    $('.btn-answer').addClass('btn-info');
                    $('.btn-answer').removeAttr('disabled');

                    $('.btn-next-word').attr('disabled', 'true');
                    $('.btn-help').removeAttr('disabled');
                }
            });
        });
    });
</script>