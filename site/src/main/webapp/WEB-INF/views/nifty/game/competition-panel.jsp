<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">${t.g(competition.title)}</h3>
    </div>
    <div class="panel-body">
        <h5>${t.g("Дата проведения")}</h5>

        <p>с 10 по 25 февраля 2016 г.</p>

        <c:if test="${competition.prizes != null && competition.prizes.size() != 0}">
            <h5>${t.g("Призы")}</h5>
            <ol>
                <c:forEach items="${competition.prizes}" var="prize">
                    <li>${t.g(prize)}</li>
                </c:forEach>
            </ol>
        </c:if>

        <c:if test="${competition.note != null}">
            <p class="text-right">
                <a href="#" data-toggle="modal" data-target="#modal-competition-${competittion.id}"
                   class="btn-link">${t.g("подробнее")}</a>
            </p>
        </c:if>
    </div>
    <div class="panel-footer text-right">
        <button class="btn btn-info show-rating" data-toggle="modal" data-target="#modal-rating-${competition.id}">${t.g("Рейтинг")}</button>
    </div>
</div>

<c:if test="${competition.note != null}">
    <!-- COMPETITION MODAL WINDOW: BEGIN -->
    <div id="modal-competition-${competittion.id}" class="modal fade" role="dialog" tabindex="-1"
         aria-labelledby="modal-competition-${competittion.id}"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">${t.g(competition.title)}</h4>
                </div>
                <div class="nano" style="height: 400px;">
                    <div class="nano-content pad-all">
                        <h5>${t.g("Дата проведения")}</h5>

                        <p>с 10 по 25 февраля 2016 г.</p>

                        <c:if test="${competition.prizes != null && competition.prizes.size() != 0}">
                            <h5>${t.g("Призы")}</h5>
                            <ol>
                                <c:forEach items="${competition.prizes}" var="prize">
                                    <li>${t.g(prize)}</li>
                                </c:forEach>
                            </ol>
                        </c:if>

                        <h5>${t.g("Описание")}</h5>
                            ${competition.note}
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- COMPETITION WINDOW: END -->

    <script>
        $(document).ready(function () {
            $('#modal-competition-${competittion.id}').on('shown.bs.modal', function () {
                $(".nano").nanoScroller();
            });
        });
    </script>
</c:if>

<!-- RATINGS MODAL WINDOW: BEGIN -->
<div id="modal-rating-${competition.id}" class="modal fade" role="dialog" tabindex="-1"
     aria-labelledby="modal-rating-${competittion.id}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Конкурсный рейтинг")}</h4>
            </div>
            <div class="nano" style="height: 400px;">
                <div class="nano-content pad-all">
                    <div id="competition-rating-list"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
            </div>
        </div>
    </div>
</div>
<!-- RATINGS WINDOW: END -->

<script>
    $(document).ready(function() {
        $('#modal-rating-${competition.id}').on('shown.bs.modal', function() {
            $(".nano").nanoScroller();
        });
    });
</script>