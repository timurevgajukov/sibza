<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-8 col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-control">
                    <span id="video-count-all" class="badge badge-pink"></span>
                </div>
                <h3 class="panel-title">${t.g("Видео")}</h3>
            </div>
            <div class="panel-body">
                <a href="#" data-toggle="modal" data-target="#modal-video">
                    <img src="//img.youtube.com/vi/${video.youtube}/maxresdefault.jpg" style="max-width: 100%; max-height: 100%;" />
                </a>
            </div>
            <div class="panel-footer">
                <a href="/video" class="btn-link">${t.g("еще видео")}</a>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <c:if test="${competition != null}">
            <t:insertAttribute name="competition-panel" defaultValue="/WEB-INF/views/nifty/game/competition-panel.jsp"/>
        </c:if>
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-control">
                    <span id="proverbs-count-all" class="badge badge-pink"></span>
                </div>
                <h3 class="panel-title">${t.g("Пословицы")}</h3>
            </div>
            <div class="panel-body">
                <div class="nano" style="height: 400px;">
                    <div class="nano-content pad-all">
                        <c:if test="${proverbs != null && proverbs.size() != 0}">
                            <c:forEach items="${proverbs}" var="proverb">
                                <p><i class="fa fa-quote-right fa-fw"></i> ${proverb.body}</p>
                            </c:forEach>
                        </c:if>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="/proverbs" class="btn-link">${t.g("еще пословицы")}</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <a href="/alphabet">
            <div class="panel media pad-all">
                <div class="media-left">
                        <span class="icon-wrap icon-wrap-sm icon-circle bg-info">
                            <i class="fa fa-font fa-2x"></i>
                        </span>
                </div>

                <div class="media-body">
                    <p id="letters-count-all" class="text-2x mar-no text-thin"></p>
                    <p class="text-muted mar-no">${t.g("Алфавит")}</p>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-sm-12">
        <a href="/dictionary">
            <div class="panel media pad-all">
                <div class="media-left">
                        <span class="icon-wrap icon-wrap-sm icon-circle bg-pink">
                            <i class="fa fa-list fa-2x"></i>
                        </span>
                </div>

                <div class="media-body">
                    <p id="words-count-all" class="text-2x mar-no text-thin"></p>
                    <p class="text-muted mar-no">${t.g("Словарь")} / ${t.g("Разговорник")}</p>
                </div>
            </div>
        </a>
    </div>
</div>
<c:if test="${security.admin}">
    <h4>${t.g("Администрирование")}</h4>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <a href="/languages">
                <div class="panel media pad-all">
                    <div class="media-left">
                        <span class="icon-wrap icon-wrap-sm icon-circle bg-success">
                            <i class="fa fa-globe fa-2x"></i>
                        </span>
                    </div>

                    <div class="media-body">
                        <p id="languages-count-all" class="text-2x mar-no text-thin"></p>
                        <p class="text-muted mar-no">${t.g("Языки")}</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-sm-12">
            <a href="/users">
                <div class="panel media pad-all">
                    <div class="media-left">
                        <span class="icon-wrap icon-wrap-sm icon-circle bg-info">
                            <i class="fa fa-user fa-2x"></i>
                        </span>
                    </div>

                    <div class="media-body">
                        <p id="users-count-all" class="text-2x mar-no text-thin"></p>
                        <p class="text-muted mar-no">${t.g("Пользователи")}</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</c:if>

<!-- VIDEO MODAL WINDOW: BEGIN -->
<div id="modal-video" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-video" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">${t.g("Видео")}</h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="video-frame" class="embed-responsive-item" src="about:blank" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
            </div>
        </div>
    </div>
</div>
<!-- VIDEO WINDOW: END -->

<script>
    $(document).ready(function() {
        // запрашиваем статистику
        $.post('/stat/video/all', function(count) {
            addStatCount($('#video-count-all'), count);
        });
        $.post('/stat/proverbs/all', function(count) {
            addStatCount($('#proverbs-count-all'), count);
        });
        $.post('/stat/alphabet/all', function(count) {
            addStatCount($('#letters-count-all'), count);
        });
        $.post('/stat/words/all', function(count) {
            addStatCount($('#words-count-all'), count);
        });
        <c:if test="${security.admin}">
            $.post('/stat/languages/all', function(count) {
                addStatCount($('#languages-count-all'), count);
            });
            $.post('/stat/users/all', function(count) {
                addStatCount($('#users-count-all'), count);
            });
        </c:if>

        // обрабатываем открытие модального окна с видео
        $('#modal-video').on('shown.bs.modal', function() {
            $('#video-frame').attr('src', '//www.youtube.com/embed/${video.youtube}');
        });
    });

    function addStatCount(src, count) {
        if (count !== undefined && count != 0) {
            src.html(count);
        }
    }
</script>