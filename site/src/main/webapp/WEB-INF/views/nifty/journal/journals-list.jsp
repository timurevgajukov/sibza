<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:forEach items="${journals}" var="journal">
    <div class="panel">
        <c:if test="${security.adminOrEditor}">
            <div class="panel-heading">
                <div class="panel-control">
                    <a href="/journals/edit/${journal.id}" class="btn">
                        <i class="fa fa-pencil text-light"></i>
                    </a>
                    <a href="/journals/delete/request/${journal.id}" class="btn">
                        <i class="fa fa-trash text-light"></i>
                    </a>
                </div>
            </div>
        </c:if>
        <div class="panel-body">
            <table>
                <tbody>
                <tr>
                    <td rowspan="2">
                        <img src="/journals/${journal.image.id}/${journal.image.fileName}" width="120" alt="${journal.title}" />
                    </td>
                    <td width="15"></td>
                    <td valign="top">
                        <span class="text-bold">${journal.title}</span><br/>
                        <span style="color: #c9c9c9;">${journal.language.smallName}</span><br/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="bottom">
                        <a href="${journal.url}" class="btn btn-warning">${t.g("Скачать")}</a>&nbsp;&nbsp;
                        <span style="color: #c9c9c9; text-transform: uppercase;">${journal.format}</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</c:forEach>