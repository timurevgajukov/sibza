<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form method="post">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">${t.g("Наименование группы")}</h3>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <input class="form-control" type="text" name="name" id="categoryName" required
                       placeholder="${t.g("Введите наименование каталога по-умолчанию")}"
                       <c:if test="${category != null}">value="${category.name}"</c:if>>
            </div>

            <c:forEach items="${languages}" var="lang">
                <div class="form-group">
                    <div class="input-group mar-btn">
                        <input class="form-control" type="text" name="name-lang" id="name-lang-${lang.code}"
                               placeholder="${t.g("Введите наименование каталога на")} ${lang.smallName}"
                               <c:if test="${category.getTranslate(lang) != null}">value="${category.getTranslate(lang).name}"</c:if>>
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-translate" data-lng="${lang.code}" type="button">
                                <i class="fa fa-mail-reply"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-info">${t.g("Сохранить")}</button>
            <a href="/dictionary<c:if test="${category != null}">/category/${category.id}</c:if>"
               class="btn btn-success">${t.g("Вернуться")}</a>
        </div>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.btn-translate').click(function () {
            var lng = $(this).data('lng');
            var categoryName = $('#categoryName').val();
            if (categoryName != undefined && categoryName.length != 0) {
                $.post(
                        "/services/translate",
                        {
                            text: categoryName,
                            lng: lng
                        }, function (result) {
                            if (result.code == 0) {
                                $('#name-lang-' + lng).val(result.body);
                            }
                        }
                );
            }
        });
    });
</script>