<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h1 class="error-code text-warning">404</h1>

<p class="h4 text-thin pad-btm mar-btm">
    <i class="fa fa-exclamation-circle fa-fw"></i>
    ${t.g("Мы не нашли запрошенную вами страницу")}
</p>

<div class="row mar-btm">
    <form class="col-xs-12 col-sm-10 col-sm-offset-1" action="/search" method="post">
        <input type="text" placeholder="${t.g("Поиск")}..." class="form-control input-lg error-search" name="search-query">
    </form>
</div>
<br>

<div class="pad-top">
    <a class="btn-link" href="/">${t.g("Вернуться на главную страницу")}</a>
    <br /><br />${t.g("или")}<br /><br />
    <a class="btn-link" href="/alphabet">${t.g("посмотреть алфавит")}</a><br />
    <a class="btn-link" href="/numerals">${t.g("посчитать все числа")}</a><br />
    <a class="btn-link" href="/proverbs">${t.g("почитать пословицы и поговорки")}</a><br />
    <a class="btn-link" href="/books">${t.g("почитать интересные и полезные книги")}</a><br />
    <a class="btn-link" href="/audio">${t.g("послушать народные песни и сказания")}</a><br />
    <a class="btn-link" href="/video">${t.g("посмотреть наши мультфильмы")}</a><br />
    <a class="btn-link" href="/game">${t.g("поиграть в познавательные игры")}</a><br />
</div>