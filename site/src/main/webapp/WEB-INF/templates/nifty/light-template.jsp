<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title} | ${t.g("Социально-образовательный проект")} "${t.g("Си бзэ")}"</title>

    <meta name="description" content="${t.g(description)}"/>
    <meta name="keywords" content="${t.g(keywords)}"/>

    <meta property="og:image" content="//sibza.org/resources/img/logo-512x324.png" />
    <meta property="og:title" content="${title}" />
    <meta property="og:description" content="${t.g(description)}" />

    <link rel="shortcut icon" href="/resources/img/favicon.ico">

    <t:insertAttribute name="header-css-js-links" defaultValue="/WEB-INF/templates/nifty/includes/header-css-js-links.jsp"/>
</head>

<body>
<div id="container">

    <div id="page-content">
        <t:insertAttribute name="content"/>
    </div>

    <footer id="footer">
        <p class="pad-lft">
            &#0169; 2013 <a href="https://sibza.org" target="_blank">sibza.org</a>
        </p>
    </footer>

</div>

<t:insertAttribute name="counters" defaultValue="/WEB-INF/templates/nifty/includes/counters.jsp"/>
<t:insertAttribute name="footer-js-links" defaultValue="/WEB-INF/templates/nifty/includes/footer-js-links.jsp"/>

</body>
</html>