<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--Language selector-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<li class="dropdown">
    <a id="lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown">
        <span class="lang-selected"></span>
    </a>

    <!--Language selector menu-->
    <ul class="head-list dropdown-menu with-arrow">
    <c:forEach items="${interfaceLanguages}" var="lng">
        <li>
            <a id="lang-${lng.code}" href="#">
                <img class="lang-flag" src="/resources/img/flags/${lng.icon}" alt="${lng.name}">
                <span class="lang-id">${lng.code}</span>
                <span class="lang-name">${lng.name}</span>
            </a>
        </li>
    </c:forEach>
    </ul>
</li>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End language selector-->