<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--User dropdown-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<c:choose>
    <c:when test="${security.auth}">
        <li id="dropdown-user" class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                <span class="pull-right">
                    <img class="img-circle img-user media-object"
                         src="${security.user.avatarUrl}"
                         alt="${security.user}">
                </span>

                <div class="username hidden-xs">${security.user}</div>

                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">

                    <!-- User dropdown menu -->
                    <ul class="head-list">
                        <li>
                            <a href="/users/${security.user.profileUrl}">
                                <i class="fa fa-user fa-fw fa-lg"></i> ${t.g("Профиль")}
                            </a>
                        </li>
                        <li>
                            <a href="/friends">
                                <i class="fa fa-users fa-fw fa-lg"></i> ${t.g("Друзья")}
                            </a>
                        </li>
                        <li>
                            <a href="/im">
                                <c:if test="${messagesCount != 0}">
                                    <span class="badge badge-danger pull-right">${messagesCount}</span>
                                </c:if>
                                <i class="fa fa-envelope fa-fw fa-lg"></i> ${t.g("Сообщения")}
                            </a>
                        </li>
                    </ul>

                    <!-- Dropdown footer -->
                    <div class="pad-all text-right">
                        <a href="/security/logout" class="btn btn-primary">
                            <i class="fa fa-sign-out fa-fw"></i> ${t.g("Выход")}
                        </a>
                    </div>
                </div>
            </a>
        </li>
    </c:when>
    <c:otherwise>
        <li>
            <a href="#" data-toggle="modal" data-target="#modal-auth">${t.g("Войти")}</a>
        </li>
    </c:otherwise>
</c:choose>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End user dropdown-->