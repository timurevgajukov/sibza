<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${security != null && !security.auth}">
    <!-- SOCIAL MODAL WINDOW: BEGIN -->
    <div id="modal-social" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-social" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">${t.g("Присоединяйтесь к нам!")}</h4>
                </div>
                <div class="modal-body">
                    <h4>${t.g("Зарегистрируйтесь у нас на сайте")}</h4>
                    <hr />
                    <p>${t.g("Регистрация даст много дополнительных преимуществ при работе с сайтом")}:</p>
                    <ul>
                        <li>${t.g("личную страницу, где сможете писать свои посты")};</li>
                        <li>${t.g("находить новых друзей и общаться с ними (в разработке)")};</li>
                        <li>${t.g("комментировать и обсуждать материалы на сайте (в разработке)")};</li>
                        <li>${t.g("участвовать в различных мероприятиях и конкурсах, проводимых нами")};</li>
                        <li>${t.g("и много другое")}.</li>
                    </ul>
                    <a href="#" class="reg-link"><u>${t.g("Зарегистрироваться сейчас")}</u></a>

                    <br /><br /><br />
                    <h4>${t.g("Мы в социальных сетях")}</h4>
                    <hr />
                    <p>${t.g("Присоединяйтесь к нам в социальных сетях, куда мы выкладываем много дополнительной полезной информации.")}</p>
                    <a href="https://www.facebook.com/%D0%A1%D0%B8-%D0%B1%D0%B7%D1%8D-926917587328131/"
                       class="btn btn-primary btn-icon icon-3x fa fa-facebook"
                       title="${t.g("Наша страницы на Facebook")}"></a>
                    <a href="https://vk.com/sibza" class="btn btn-info btn-icon icon-3x fa fa-vk"
                       title="${t.g("Наша страница на VK")}"></a>
                    <a href="https://instagram.com/sibza_org" class="btn btn-purple btn-icon icon-3x fa fa-instagram"
                       title="${t.g("Наша страница в Инстаграм")}"></a>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-primary">${t.g("Закрыть")}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- SOCIAL WINDOW: END -->

    <script>
        $(document).ready(function() {
            <c:if test="${security.showSocialModal}">
            $('#modal-social').modal();
            </c:if>

            $('.reg-link').click(function() {
                $('#modal-social').modal('toggle');
                $('#modal-auth').modal();
                $('#auth-reg-tabs a:last').tab('show');
            });
        });
    </script>
</c:if>