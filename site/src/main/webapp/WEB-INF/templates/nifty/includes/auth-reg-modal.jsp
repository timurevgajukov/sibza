<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${security != null && !security.auth}">
    <!-- AUTH MODAL WINDOW: BEGIN -->
    <div id="modal-auth" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-auth" aria-hidden="true">
        <div class="modal-dialog">
            <div class="tab-base">

                <!--Nav Tabs-->
                <ul id="auth-reg-tabs" class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#auth-tab">${t.g("Авторизация")}</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#reg-tab">${t.g("Регистрация")}</a>
                    </li>
                </ul>

                <!--Tabs Content-->
                <div class="tab-content">
                    <div id="auth-tab" class="tab-pane fade active in">
                        <div class="modal-content">
                            <form id="modal-auth-form" method="post" action="/security/login">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">${t.g("Авторизация на сайте")}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                            <input class="form-control" type="text" id="auth-email" name="email"
                                                   placeholder="${t.g("Введите электронную почту")}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                            <input class="form-control" type="password" id="auth-password" name="password"
                                                   placeholder="${t.g("Введите пароль")}"/>
                                        </div>
                                    </div>
                                    <p class="text-right">
                                        <a href="#" class="recover-password"><u>${t.g("восстановить пароль")}</u></a>
                                            ${t.g("или")}
                                        <a href="#" class="reg-tab-show"><u>${t.g("зарегистрироваться")}</u></a>
                                    </p>
                                    <input type="hidden" name="redirect" id="auth-redirect" value="/"/>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-primary"
                                            id="modal-auth-submit">${t.g("Войти")}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="reg-tab" class="tab-pane fade">
                        <div class="modal-content">
                            <form id="modal-reg-form" method="post" action="/security/registration">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">${t.g("Регистрация нового пользователя")}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                            <input class="form-control" type="email" id="reg-email" name="email"
                                                   placeholder="${t.g("Введите электронную почту")}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                            <input class="form-control" type="text" id="reg-name" name="name"
                                                   placeholder="${t.g("Введите ваше имя")}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                            <input class="form-control" type="password" id="reg-password1" name="password1"
                                                   placeholder="${t.g("Введите пароль")}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                                            <input class="form-control" type="password" id="reg-password2" name="password2"
                                                   placeholder="${t.g("Введите пароль повторно")}"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="redirect" id="reg-redirect" value="/"/>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" id="modal-reg-submit" type="submit">
                                            ${t.g("Зарегистрироваться")}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- AUTH MODAL WINDOW: END -->

    <script>
        $(document).ready(function() {
            $('.reg-tab-show').click(function() {
                $('#auth-reg-tabs a:last').tab('show');
            });

            $('.recover-password').click(function() {
                $('#modal-auth').modal('toggle');
                $('#modal-recover').modal();
            });
        });
    </script>
</c:if>