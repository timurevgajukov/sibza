<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--STYLESHEET-->
<!--=================================================-->

<!--Open Sans Font [ OPTIONAL ] -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">

<!--Bootstrap Stylesheet [ REQUIRED ]-->
<link href="/resources/themes/nifty/2.2.3/template/css/bootstrap.min.css" rel="stylesheet">

<!--Nifty Stylesheet [ REQUIRED ]-->
<link href="/resources/themes/nifty/2.2.3/template/css/nifty.min.css" rel="stylesheet">

<!--Font Awesome [ OPTIONAL ]-->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

<!--Morris.js [ OPTIONAL ]-->
<link href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet">

<!--Bootstrap Select [ OPTIONAL ]-->
<link href="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-select/bootstrap-select.min.css"
      rel="stylesheet">

<!--Bootstrap Tags Input [ OPTIONAL ]-->
<link href="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<!--Bootstrap Datepicker [ OPTIONAL ]-->
<link href="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">

<!--Bootstrap Validator [ OPTIONAL ]-->
<link href="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">

<!--Chosen [ OPTIONAL ]-->
<link href="/resources/themes/nifty/2.2.3/template/plugins/chosen/chosen.min.css" rel="stylesheet">

<!--SCRIPT-->
<!--=================================================-->

<!--jQuery [ REQUIRED ]-->
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>

<!--Page Load Progress Bar [ OPTIONAL ]-->
<link href="/resources/themes/nifty/2.2.3/get-started/css/pace.min.css" rel="stylesheet">
<script src="/resources/themes/nifty/2.2.3/get-started/js/pace.min.js"></script>

<link href="/resources/themes/nifty/2.2.3/template/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="/resources/themes/nifty/2.2.3/template/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css" rel="stylesheet">