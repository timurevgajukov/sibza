<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${security != null && !security.auth}">
    <!-- RECOVER PASSWORD MODAL WINDOW: BEGIN -->
    <div id="modal-recover" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="modal-recover" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="modal-recover-form" method="post" action="/security/recover/request">
                    <div class="modal-header">
                        <button data-dismiss="modal" class="close" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">${t.g("Восстановление пароля")}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                <input class="form-control" type="text" id="email" name="email"
                                       placeholder="${t.g("Введите электронную почту")}"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-primary" id="modal-recover-submit">${t.g("Восстановить")}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- RECOVER PASSWORD MODAL WINDOW: END -->
</c:if>