<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--JAVASCRIPT-->
<!--=================================================-->

<!--BootstrapJS [ RECOMMENDED ]-->
<script src="/resources/themes/nifty/2.2.3/template/js/bootstrap.min.js"></script>

<!--Fast Click [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/get-started/js/fastclick.min.js"></script>

<!--Nifty Admin [ RECOMMENDED ]-->
<script src="/resources/themes/nifty/2.2.3/template/js/nifty.min.js"></script>

<!--Chosen [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/chosen/chosen.jquery.min.js"></script>

<script src="/resources/themes/nifty/2.2.3/template/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script src="/resources/themes/nifty/2.2.3/template/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
<script src="/resources/themes/nifty/2.2.3/template/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<!--Bootstrap Datepicker [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<!--Bootstrap Select [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-select/bootstrap-select.min.js"></script>

<!--Bootstrap Tags Input [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

<!--Bootstrap Validator [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>

<%--<script src="//maps.google.com/maps/api/js?sensor=false"></script>--%>
<%--<script src="/resources/themes/nifty/2.2.3/template/plugins/gmaps/gmaps.js"></script>--%>

<script src="//cdn.jsdelivr.net/sockjs/1.0.3/sockjs.min.js"></script>
<script src="/resources/lib/stomp/stomp.min.js"></script>
<script src="/resources/js/websocket.js"></script>

<script src="/resources/lib/dot/doT.min.js"></script>

<script src="/resources/js/user.js"></script>
<script src="/resources/js/panel.js"></script>
<script src="/resources/js/audio.js"></script>