<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <div id="mainnav">

        <!--Shortcut buttons-->
        <!--================================-->
        <div id="mainnav-shortcut">

        </div>
        <!--================================-->
        <!--End shortcut buttons-->

        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <ul id="mainnav-menu" class="list-group">

                        <li<c:if test="${active == 'alphabet' || active == 'numerals' || active == 'colors'}"> class="active-sub"</c:if>>
                            <a href="#">
                                <i class="fa fa-university"></i>
                                <span class="menu-title">${t.g("Обучение")}</span>
                                <i class="arrow"></i>
                            </a>

                            <ul class="collapse<c:if test="${active == 'alphabet' || active == 'numerals' || active == 'colors'}"> in</c:if>">
                                <li<c:if test="${active == 'alphabet'}"> class="active-link"</c:if>>
                                    <a href="/alphabet">
                                        <i class="fa fa-font"></i>
                                        <span class="menu-title">${t.g("Алфавит")}</span>
                                    </a>
                                </li>
                                <li<c:if test="${active == 'numerals'}"> class="active-link"</c:if>>
                                    <a href="/numerals">
                                        <i class="fa fa-sort-numeric-asc"></i>
                                        <span class="menu-title">${t.g("Числа")}</span>
                                    </a>
                                </li>
                                <%--<li<c:if test="${active == 'colors'}"> class="active-link"</c:if>>--%>
                                    <%--<a href="/colors">--%>
                                        <%--<i class="fa fa-hashtag"></i>--%>
                                        <%--<span class="menu-title">${t.g("Цвета")}</span>--%>
                                    <%--</a>--%>
                                <%--</li>--%>
                            </ul>
                        </li>

                        <li<c:if test="${active == 'dictionary'}"> class="active-link"</c:if>>
                            <a href="/dictionary">
                                <i class="fa fa-list"></i>
                                <span class="menu-title">${t.g("Словарь")} / ${t.g("Разговорник")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'proverbs'}"> class="active-link"</c:if>>
                            <a href="/proverbs">
                                <i class="fa fa-quote-right"></i>
                                <span class="menu-title">${t.g("Пословицы")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'books'}"> class="active-link"</c:if>>
                            <a href="/books">
                                <i class="fa fa-book"></i>
                                <span class="menu-title">${t.g("Книги")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'journals'}"> class="active-link"</c:if>>
                            <a href="/journals">
                                <i class="fa fa-book"></i>
                                <span class="menu-title">${t.g("Журнал Нур")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'audio'}"> class="active-link"</c:if>>
                            <a href="/audio">
                                <i class="fa fa-music"></i>
                                <span class="menu-title">${t.g("Народные песни и сказания")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'video'}"> class="active-link"</c:if>>
                            <a href="/video">
                                <i class="fa fa-youtube-play"></i>
                                <span class="menu-title">${t.g("Видео")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'game'}"> class="active-link"</c:if>>
                            <a href="/game">
                                <i class="fa fa-gamepad"></i>
                                <span class="menu-title">${t.g("Игры")} / ${t.g("Конкурсы")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'apps'}"> class="active-link"</c:if>>
                            <a href="/apps">
                                <i class="fa fa-mobile"></i>
                                <span class="menu-title">${t.g("Приложения")}</span>
                            </a>
                        </li>

                        <li<c:if test="${active == 'users'}"> class="active-link"</c:if>>
                            <a href="/users">
                                <i class="fa fa-user"></i>
                                <span class="menu-title">${t.g("Пользователи")}</span>
                            </a>
                        </li>

                        <c:if test="${security.isAdminOrEditor()}">
                            <li class="list-divider"></li>

                            <li<c:if test="${active == 'dictionary-dsl'}"> class="active-link"</c:if>>
                                <a href="/dsl">
                                    <i class="fa fa-list"></i>
                                    <span class="menu-title">DSL-${t.g("Словарь")}</span>
                                </a>
                            </li>

                            <li<c:if test="${active == 'resources'}"> class="active-link"</c:if>>
                                <a href="/resources">
                                    <i class="fa fa-youtube-play"></i>
                                    <span class="menu-title">${t.g("Медиафайлы")}</span>
                                </a>
                            </li>

                            <li<c:if test="${active == 'interface'}"> class="active-link"</c:if>>
                                <a href="/interfaces">
                                    <i class="fa fa-list"></i>
                                    <span class="menu-title">${t.g("Интерфейс")}</span>
                                </a>
                            </li>

                            <c:if test="${security.isAdmin()}">
                                <li class="list-divider"></li>

                                <li<c:if test="${active == 'languages'}"> class="active-link"</c:if>>
                                    <a href="/languages">
                                        <i class="fa fa-globe"></i>
                                        <span class="menu-title">${t.g("Языки")}</span>
                                    </a>
                                </li>

                                <li<c:if test="${active == 'import'}"> class="active-link"</c:if>>
                                    <a href="/utility/import">
                                        <i class="fa fa-indent"></i>
                                        <span class="menu-title">${t.g("Импорт")}</span>
                                    </a>
                                </li>
                            </c:if>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->

    </div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->