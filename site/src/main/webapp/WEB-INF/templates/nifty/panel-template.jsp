<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title} | ${t.g("Социально-образовательный проект")} "${t.g("Си бзэ")}"</title>

    <meta name="description" content="${t.g(description)}"/>
    <meta name="keywords" content="${t.g(keywords)}"/>

    <meta name='yandex-verification' content='5b92bb9e2d7aaedd' />
    <meta name="google-site-verification" content="H4OLd_2uIHW_gnuH-EBrUUVhYR5Ub5GegzX-zdRaXXI" />

    <meta property="og:image" content="//sibza.org/resources/img/logo-512x324.png" />
    <meta property="og:title" content="${title}" />
    <meta property="og:description" content="${t.g(description)}" />

    <link rel="search" type="application/opensearchdescription+xml" href="https://sibza.org/opensearch.xml" title="Content search" />

    <link rel="shortcut icon" href="/resources/img/favicon.ico">

    <t:insertAttribute name="header-css-js-links" defaultValue="/WEB-INF/templates/nifty/includes/header-css-js-links.jsp"/>
</head>

<body>
<div id="container"
     class="effect mainnav-lg<c:if test="${active == 'profile'}"> aside-in aside-left aside-bright</c:if>">

    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <div id="navbar-container" class="boxed">

            <!--Brand logo & name-->
            <!--================================-->
            <div class="navbar-header">
                <a href="/" class="navbar-brand">
                    <img src="/resources/img/logo-white-small.png" alt="Sibza Logo" class="brand-icon">

                    <div class="brand-title">
                        <span class="brand-text">${t.g("Си бзэ")}</span>
                    </div>
                </a>
            </div>
            <!--================================-->
            <!--End brand logo & name-->

            <!--Navbar Dropdown-->
            <!--================================-->
            <div class="navbar-content clearfix">
                <ul class="nav navbar-top-links pull-left">

                    <!--Navigation toogle button-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="tgl-menu-btn">
                        <a class="mainnav-toggle" href="#">
                            <i class="fa fa-navicon fa-lg"></i>
                        </a>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End Navigation toogle button-->

                    <t:insertAttribute name="offer-material" defaultValue="/WEB-INF/templates/nifty/includes/offer-material.jsp"/>

                </ul>
                <ul class="nav navbar-top-links pull-right">
                    <t:insertAttribute name="language-selectrot" defaultValue="/WEB-INF/templates/nifty/includes/language-selector.jsp"/>
                    <t:insertAttribute name="user-dropdown" defaultValue="/WEB-INF/templates/nifty/includes/user-dropdown.jsp"/>
                </ul>
            </div>
            <!--================================-->
            <!--End Navbar Dropdown-->

        </div>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">

            <c:if test="${active != 'profile'}">
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">${title}</h1>

                    <!--Searchbox-->
                    <div class="searchbox">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" id="search-query" placeholder="${t.g("Поиск")}..."
                                   <c:if test="${query != null}">value="${query}"</c:if>>
                            <span class="input-group-btn">
                                <button class="text-muted" id="search-btn" type="button"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->
            </c:if>

            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">
                <t:insertAttribute name="content"/>
            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->

        <t:insertAttribute name="main-navigation" defaultValue="/WEB-INF/templates/nifty/includes/main-navigation.jsp"/>
        <t:insertAttribute name="aside" defaultValue="/WEB-INF/templates/nifty/includes/aside-null.jsp"/>

    </div>

    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p class="pad-lft">
                    &#0169; 2013 sibza.org&nbsp;&nbsp;&nbsp;
                    <a href="/about">${t.g("О проекте")}</a> |
                    <a href="/help/developer">${t.g("Помощь")}</a>
                </p>
            </div>
            <div class="col-md-6 col-sm-12">
                <p class="text-right pad-rgt">
                    <a href="https://www.facebook.com/%D0%A1%D0%B8-%D0%B1%D0%B7%D1%8D-926917587328131/"
                       class="btn btn-primary btn-icon icon-lg fa fa-facebook"
                       title="${t.g("Наша страница на Facebook")}"></a>
                    <a href="https://vk.com/sibza" class="btn btn-info btn-icon icon-lg fa fa-vk"
                       title="${t.g("Наша страница на VK")}"></a>
                    <a href="https://instagram.com/sibza_org" class="btn btn-purple btn-icon icon-lg fa fa-instagram"
                       title="${t.g("Наша страница в Инстаграм")}"></a>
                </p>
            </div>
        </div>
    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->

    <!-- SCROLL TOP BUTTON -->
    <!--===================================================-->
    <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
    <!--===================================================-->

</div>
<!--===================================================-->
<!-- END OF CONTAINER -->

<t:insertAttribute name="auth-reg-modal" defaultValue="/WEB-INF/templates/nifty/includes/auth-reg-modal.jsp"/>
<t:insertAttribute name="recover-pwd-modal" defaultValue="/WEB-INF/templates/nifty/includes/recover-pwd-modal.jsp"/>
<t:insertAttribute name="social-modal" defaultValue="/WEB-INF/templates/nifty/includes/social-modal.jsp"/>

<t:insertAttribute name="counters" defaultValue="/WEB-INF/templates/nifty/includes/counters.jsp"/>
<t:insertAttribute name="footer-js-links" defaultValue="/WEB-INF/templates/nifty/includes/footer-js-links.jsp"/>

<script>
    $(document).ready(function () {
        var active = '${active}';
        var userId;
        <c:if test="${user != null}">userId = ${user.id}</c:if>

        // если есть, то отобразим обычные нотификации
        <c:if test="${security != null && security.hasMessage()}">
            <c:forEach items="${security.getMessagesWithClear()}" var="msg">
                showAlert("${msg.body}", "${msg.type}");
            </c:forEach>
        </c:if>

        <c:if test="${security != null && security.auth}">
            // подписываемся на общие нотификации
            connectWS('/sibza', '/info/${security.user.confirmCode}', function(message) {
                if (message) {
                    showNotification(message.title, message.body, message.type);
                }
            });

            // подписываемся на уведомления от чата
            connectWS('/sibza', '/im/${security.user.confirmCode}', function(message) {
                if (message) {
                    if (!(active == 'im' && message.source.id == userId)) {
                        var title = '<a href="/im/' + message.source.id + '">' + message.source.fullName + '</a>';
                        var body = '<a href="/im/' + message.source.id + '">' + message.message + '</a>';
                        showNotification(title, body, 'mint');
                    }
                }
            });
        </c:if>

        <c:if test="${security != null && security.admin}">
            // подписываемся на нотификации для администраторов
            connectWS('/sibza', '/info/admin', function(message) {
                if (message) {
                    showNotification(message.title, message.body, message.type);
                }
            });
        </c:if>

        var currentLanguage = '${security.language.code}';

        // инициализируем поле со ссылкой на редирект после авторизации
        $('#auth-redirect').val(window.location.href);
        $('#reg-redirect').val(window.location.href);

        // инициализация выбора языка сайта
        $('#lang-switch').niftyLanguage({
            selectedOn: '#lang-' + currentLanguage,
            onChange: function (language) {
                console.log("ID: " + language.id + " - Language: " + language.name);

                if (language.id != '${security.language.code}') {
                    $.form('/security/language', {
                        'language': language.id,
                        'redirectUrl': window.location.href
                    }, 'POST').submit();
                }
            }
        });
    });
</script>

</body>
</html>