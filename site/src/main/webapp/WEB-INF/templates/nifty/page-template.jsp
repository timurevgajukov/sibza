<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title} | ${t.g("Социально-образовательный проект")} "${t.g("Си бзэ")}"</title>

    <meta name="description" content="${t.g(description)}"/>
    <meta name="keywords" content="${t.g(keywords)}"/>

    <meta property="og:image" content="//sibza.org/resources/img/logo-512x324.png" />
    <meta property="og:title" content="${title}" />
    <meta property="og:description" content="${t.g(description)}" />

    <link rel="shortcut icon" href="/resources/img/favicon.ico">

    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ] -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="/resources/themes/nifty/2.2.3/template/css/bootstrap.min.css" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="/resources/themes/nifty/2.2.3/template/css/nifty.min.css" rel="stylesheet">


    <!--Font Awesome [ OPTIONAL ]-->
    <link href="/resources/themes/nifty/2.2.3/get-started/css/font-awesome.min.css" rel="stylesheet">

    <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">

    <!--SCRIPT-->
    <!--=================================================-->

    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="/resources/themes/nifty/2.2.3/get-started/css/pace.min.css" rel="stylesheet">
    <script src="/resources/themes/nifty/2.2.3/get-started/js/pace.min.js"></script>

</head>

<body>
<div id="container" class="cls-container">

    <!-- HEADER -->
    <!--===================================================-->
    <div class="cls-header">
        <div class="cls-brand">
            <a class="box-inline" href="/">
                <!-- <img alt="Nifty Admin" src="images/logo.png" class="brand-icon"> -->
                <span class="brand-title">${t.g("Си бзэ")} <span class="text-thin">${t.g("Социально-образовательный проект")}</span></span>
            </a>
        </div>
    </div>

    <!-- CONTENT -->
    <!--===================================================-->
    <div class="cls-content">
        <t:insertAttribute name="content"/>
    </div>


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->

<t:insertAttribute name="counters" defaultValue="/WEB-INF/templates/nifty/includes/counters.jsp"/>

<!--JAVASCRIPT-->
<!--=================================================-->

<!--jQuery [ REQUIRED ]-->
<script src="/resources/themes/nifty/2.2.3/template/js/jquery-2.1.1.min.js"></script>

<!--BootstrapJS [ RECOMMENDED ]-->
<script src="/resources/themes/nifty/2.2.3/template/js/bootstrap.min.js"></script>

<!--Fast Click [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/get-started/js/fastclick.min.js"></script>

<!--Nifty Admin [ RECOMMENDED ]-->
<script src="/resources/themes/nifty/2.2.3/get-started/js/nifty.min.js"></script>

<!--Bootstrap Validator [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/bootstrap-validator/bootstrapValidator.min.js"></script>

<!--Masked Input [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/masked-input/jquery.maskedinput.min.js"></script>

<!--Chosen [ OPTIONAL ]-->
<script src="/resources/themes/nifty/2.2.3/template/plugins/chosen/chosen.jquery.min.js"></script>

</body>
</html>