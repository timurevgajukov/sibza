<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Vendor CSS -->
<link href="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
<link href="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
<link href="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<link href="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

<!-- CSS -->
<link href="/resources/themes/material/1.5-2/template/jquery/css/app.min.1.css" rel="stylesheet">
<link href="/resources/themes/material/1.5-2/template/jquery/css/app.min.2.css" rel="stylesheet">

<link href="/resources/lib/nano/nanoscroller.css" rel="stylesheet">

<link href="/resources/css/material.css" rel="stylesheet">

<!--jQuery [ REQUIRED ]-->
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>