<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--Menu-->
<!--================================-->
<ul class="main-menu">
    <li class="sub-menu<c:if test="${active == 'alphabet' || active == 'numerals' || active == 'colors'}"> toggled</c:if>">
        <a href=""><i class="zmdi zmdi-balance"></i> ${t.g("Обучение")}</a>

        <ul>
            <li<c:if test="${active == 'alphabet'}"> class="active"</c:if>>
                <a href="/alphabet"><i class="zmdi zmdi-font"></i> ${t.g("Алфавит")}</a>
            </li>
            <li<c:if test="${active == 'numerals'}"> class="active"</c:if>>
                <a href="/numerals"><i class="zmdi zmdi-collection-item-9"></i> ${t.g("Числа")}</a>
            </li>
            <%--<li<c:if test="${active == 'colors'}"> class="active"</c:if>>--%>
                <%--<a href="/colors"><i class="zmdi zmdi-invert-colors"></i> ${t.g("Цвета")}</a>--%>
            <%--</li>--%>
        </ul>
    </li>

    <li<c:if test="${active == 'dictionary'}"> class="active-"</c:if>>
        <a href="/dictionary"><i class="zmdi zmdi-view-list"></i> ${t.g("Словарь")} / ${t.g("Разговорник")}</a>
    </li>

    <li<c:if test="${active == 'proverbs'}"> class="active"</c:if>>
        <a href="/proverbs"><i class="zmdi zmdi-quote"></i> ${t.g("Пословицы")}</a>
    </li>

    <li<c:if test="${active == 'books'}"> class="active"</c:if>>
        <a href="/books"><i class="zmdi zmdi-book"></i> ${t.g("Книги")}</a>
    </li>

    <li<c:if test="${active == 'audio'}"> class="active"</c:if>>
        <a href="/audio"><i class="zmdi zmdi-collection-music"></i> ${t.g("Народные песни и сказания")}</a>
    </li>

    <li<c:if test="${active == 'video'}"> class="active"</c:if>>
        <a href="/video"><i class="zmdi zmdi-collection-video"></i> ${t.g("Видео")}</a>
    </li>

    <li<c:if test="${active == 'game'}"> class="active"</c:if>>
        <a href="/game"><i class="zmdi zmdi-gamepad"></i> ${t.g("Игры")} / ${t.g("Конкурсы")}</a>
    </li>

    <li<c:if test="${active == 'apps'}"> class="active"</c:if>>
        <a href="/apps"><i class="zmdi zmdi-smartphone"></i> ${t.g("Приложения")}</a>
    </li>

    <li<c:if test="${active == 'users'}"> class="active"</c:if>>
        <a href="/users"><i class="zmdi zmdi-account"></i> ${t.g("Пользователи")}</a>
    </li>

    <c:if test="${security.isAdminOrEditor()}">
        <li<c:if test="${active == 'dictionary-dsl'}"> class="active"</c:if>>
            <a href="/dsl"><i class="zmdi zmdi-view-list"></i> DSL-${t.g("Словарь")}</a>
        </li>

        <li<c:if test="${active == 'resources'}"> class="active"</c:if>>
            <a href="/resources"><i class="zmdi zmdi-collection-video"></i> ${t.g("Медиафайлы")}</a>
        </li>

        <li<c:if test="${active == 'interface'}"> class="active"</c:if>>
            <a href="/interfaces"><i class="zmdi zmdi-view-list"></i> ${t.g("Интерфейс")}</a>
        </li>

        <c:if test="${security.isAdmin()}">
            <li<c:if test="${active == 'languages'}"> class="active"</c:if>>
                <a href="/languages"><i class="zmdi zmdi-globe-alt"></i> ${t.g("Языки")}</a>
            </li>

            <li<c:if test="${active == 'import'}"> class="active"</c:if>>
                <a href="/utility/import"><i class="zmdi zmdi-format-indent-increase"></i> ${t.g("Импорт")}</a>
            </li>
        </c:if>
    </c:if>
</ul>
<!--================================-->
<!--End menu-->