<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--User dropdown-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<c:choose>
    <c:when test="${security.auth}">
        <div class="profile-menu">
            <a href="">
                <div class="profile-pic">
                    <img src="${security.user.avatarUrl}" alt="${security.user}">
                </div>

                <div class="profile-info">${security.user} <i class="zmdi zmdi-caret-down"></i></div>
            </a>

            <ul class="main-menu">
                <li>
                    <a href="/users/${security.user.profileUrl}">
                        <i class="zmdi zmdi-account"></i> ${t.g("Профиль")}
                    </a>
                </li>
                <li>
                    <a href="/friends">
                        <i class="zmdi zmdi-accounts"></i> ${t.g("Друзья")}
                    </a>
                </li>
                <li>
                    <a href="/im">
                        <i class="zmdi zmdi-email"></i> ${t.g("Сообщения")}
                    </a>
                </li>
                <li>
                    <a href="/security/logout">
                        <i class="zmdi zmdi-power"></i> ${t.g("Выход")}
                    </a>
                </li>
            </ul>
        </div>
    </c:when>
    <c:otherwise>
        <div class="profile-menu">
            <a href="">
                <div class="profile-info">${t.g("Гость")} <i class="zmdi zmdi-caret-down"></i></div>
            </a>

            <ul class="main-menu">
                <li>
                    <a href="#">
                        <i class="zmdi zmdi-power"></i> ${t.g("Войти")}
                    </a>
                </li>
            </ul>
        </div>
    </c:otherwise>
</c:choose>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End user dropdown-->