<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Javascript Libraries -->
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/flot/jquery.flot.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/sparklines/jquery.sparkline.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="/resources/themes/material/1.5-2/template/jquery/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<![endif]-->

<script src="/resources/themes/material/1.5-2/template/jquery/js/flot-charts/curved-line-chart.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/js/flot-charts/line-chart.js"></script>
<script src="/resources/themes/material/1.5-2/template/jquery/js/charts.js"></script>

<script src="/resources/themes/material/1.5-2/template/jquery/js/functions.js"></script>

<script src="/resources/lib/nano/jquery.nanoscroller.min.js"></script>

<script src="//cdn.jsdelivr.net/sockjs/1.0.3/sockjs.min.js"></script>
<script src="/resources/lib/stomp/stomp.min.js"></script>
<script src="/resources/js/websocket.js"></script>

<script src="/resources/lib/dot/doT.min.js"></script>

<script src="/resources/js/user.js"></script>
<script src="/resources/js/panel.js"></script>
<script src="/resources/js/audio.js"></script>