<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title} | ${t.g("Социально-образовательный проект")} "${t.g("Си бзэ")}"</title>

    <meta name="description" content="${t.g(description)}"/>
    <meta name="keywords" content="${t.g(keywords)}"/>

    <meta name='yandex-verification' content='5b92bb9e2d7aaedd' />
    <meta name="google-site-verification" content="H4OLd_2uIHW_gnuH-EBrUUVhYR5Ub5GegzX-zdRaXXI" />

    <meta property="og:image" content="//sibza.org/resources/img/logo-512x324.png" />
    <meta property="og:title" content="${title}" />
    <meta property="og:description" content="${t.g(description)}" />

    <link rel="search" type="application/opensearchdescription+xml" href="https://sibza.org/opensearch.xml" title="Content search" />

    <link rel="shortcut icon" href="/resources/img/favicon.ico">

    <t:insertAttribute name="header-css-js-links" defaultValue="/WEB-INF/templates/material/includes/header-css-js-links.jsp"/>
</head>

<body class="toggled sw-toggled">
<header id="header" class="clearfix" data-current-skin="blue">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="/">${t.g("Си бзэ")}</a>
        </li>

        <li class="pull-right">
            <ul class="top-menu">
                <li id="top-search">
                    <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
                </li>
            </ul>
        </li>
    </ul>

    <!-- Top Search Content -->
    <div id="top-search-wrap">
        <div class="tsw-inner">
            <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
            <input type="text" placeholder="${t.g("Поиск")}..." />
        </div>
    </div>
</header>

<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <t:insertAttribute name="main-navigation" defaultValue="/WEB-INF/templates/material/includes/user-dropdown.jsp"/>
        <t:insertAttribute name="main-navigation" defaultValue="/WEB-INF/templates/material/includes/main-navigation.jsp"/>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header">
                <br />
                <h2>${title}</h2>
            </div>

            <t:insertAttribute name="content"/>
        </div>
    </section>
</section>

<footer id="footer">
    &copy; 2015 sibza.org

    <ul class="f-menu">
        <li><a href="/about">${t.g("О проекте")}</a></li>
        <li><a href="/help/developer">${t.g("Помощь")}</a></li>
    </ul>
</footer>

<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader pls-blue">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20"/>
        </svg>

        <p>Please wait...</p>
    </div>
</div>

<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1 class="c-white">Warning!!</h1>

    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
        to access this website.</p>

    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="img/browsers/chrome.png" alt="">

                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="img/browsers/firefox.png" alt="">

                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="img/browsers/opera.png" alt="">

                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="img/browsers/safari.png" alt="">

                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="img/browsers/ie.png" alt="">

                    <div>IE (New)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->

<t:insertAttribute name="footer-js-links" defaultValue="/WEB-INF/templates/material/includes/footer-js-links.jsp"/>

<script>
    $(document).ready(function () {
        var active = '${active}';
        var userId;
        <c:if test="${user != null}">userId = ${user.id}</c:if>

        // если есть, то отобразим обычные нотификации
        <c:if test="${security != null && security.hasMessage()}">
            <c:forEach items="${security.getMessagesWithClear()}" var="msg">
                // showAlert("${msg.body}", "${msg.type}");
            </c:forEach>
        </c:if>

        <c:if test="${security != null && security.auth}">
            // подписываемся на общие нотификации
            connectWS('/sibza', '/info/${security.user.confirmCode}', function(message) {
                if (message) {
                    // showNotification(message.title, message.body, message.type);
                }
            });

            // подписываемся на уведомления от чата
            connectWS('/sibza', '/im/${security.user.confirmCode}', function(message) {
                if (message) {
                    if (!(active == 'im' && message.source.id == userId)) {
                        var title = '<a href="/im/' + message.source.id + '">' + message.source.fullName + '</a>';
                        var body = '<a href="/im/' + message.source.id + '">' + message.message + '</a>';
                        // showNotification(title, body, 'mint');
                    }
                }
            });
        </c:if>

        <c:if test="${security != null && security.admin}">
            // подписываемся на нотификации для администраторов
            connectWS('/sibza', '/info/admin', function(message) {
                if (message) {
                    // showNotification(message.title, message.body, message.type);
                }
            });
        </c:if>

        var currentLanguage = '${security.language.code}';

        // инициализируем поле со ссылкой на редирект после авторизации
        $('#auth-redirect').val(window.location.href);
        $('#reg-redirect').val(window.location.href);
    });
</script>

</body>
</html>