/**
 * Created by Тимур on 16.10.2015.
 *
 * Кнопки управления аудио проигрывателем
 */

$(document).ready(function() {
    var currentAudio;
    var audioEventsList = [];

    $(document).on('click', '.audio-control', function() {
        var btnControl = $(this);
        var id = btnControl.data("id");
        var played = btnControl.data("played");

        var audio = document.getElementById('audio-' + id);

        if (played === undefined || played == "0") {
            // запускаем проигрывание музыки
            if (currentAudio !== undefined) {
                // уже запущен другой плеер, который нужно остановить
                pause(currentAudio.audio, currentAudio.control);
            }
            // запоминаем данные нового плеера
            currentAudio = {
                audio: audio,
                control: btnControl
            };

            if (audioEventsList[id] === undefined) {
                // для этого плеера мы еще не добавляли слушателей
                audio.addEventListener('progress', function () {
                    var progressPercent = Math.round(audio.currentTime / audio.duration * 100) + '%';
                    $('#audio-progress-' + id).children().width(progressPercent);
                });
                audio.addEventListener('ended', function () {
                    btnControl.removeClass("fa-pause");
                    btnControl.addClass("fa-play");
                    btnControl.data("played", "0");
                });
                audioEventsList[id] = audio;
            }
            audio.play();

            btnControl.removeClass("fa-play");
            btnControl.addClass("fa-pause");
            btnControl.data("played", "1");
        } else {
            // останавлиаем проигрывание музыки
            pause(audio, btnControl);
            // сбрасываем данные текущего работающего плеера
            currentAudio = undefined;
        }

    });
});

function pause(audio, btnControl) {
    audio.pause();

    // удаляем все обработчики событий элемента таким способом
    // var audioClone = audio.cloneNode(true);
    // audio.parentNode.replaceChild(audioClone, audio);

    btnControl.removeClass("fa-pause");
    btnControl.addClass("fa-play");
    btnControl.data("played", "0");
}