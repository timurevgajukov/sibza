/**
 * Created by timur on 08.03.16.
 */

var addTitle = "Добавить в друзья";
var delTitle = "Удалить из друзей";

function btnTitleTranslate(addStr, delStr) {
    addTitle = addStr;
    delTitle = delStr;
}

$(document).ready(function() {
    $(document).on('click', '.add_del_friend', function() {
        var btnAddDelFriend = $(this);
        var action = btnAddDelFriend.data('action');
        var friendId = btnAddDelFriend.data('id');
        if ('add' == action) {
            $.post("/friends/add", { friend : friendId }, function(result) {
                console.log('Friend add result = ' + result);
                btnAddDelFriend.html(delTitle);
                btnAddDelFriend.data('action', 'del');
                if (btnAddDelFriend.is('button')) {
                    btnAddDelFriend.removeClass('btn-success');
                    btnAddDelFriend.addClass('btn-danger');
                }
            });
        } else {
            $.post("/friends/delete", { friend : friendId }, function(result) {
                console.log('Friend delete result = ' + result);
                btnAddDelFriend.html(addTitle);
                btnAddDelFriend.data('action', 'add');
                if (btnAddDelFriend.is('button')) {
                    btnAddDelFriend.removeClass('btn-danger');
                    btnAddDelFriend.addClass('btn-success');
                }
            });
        }
    })
});