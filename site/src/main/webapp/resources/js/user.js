/**
 * Created by timur on 25.09.15.
 */
var backUrl = "/";

$(document).ready(function() {
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }

    $('#btn-login').click(function() {
        // history.pushState(null, null, '/security/auth');
    });
    $('.modal-close').click(function() {
        // history.pushState((null, null, backUrl));
    });
    $('#modal-auth-submit').click(function() {
        $('#modal-auth-form').submit();
    });
    $('#modal-recover-submit').click(function() {
        $('#modal-recover-form').submit();
    });

    // валидация данных при регистрации
    $('#modal-reg-form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            email: {
                validators: {
                    notEmtry: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            password1: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password2',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            password2: {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password1',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    }).on('success.field.bv', function(e, data) {
        var $parent = data.element.parents('.form-group');

        // для поля с почтой дополнительно проверим его уникальность
        if (data.field == 'email') {
            $.post('/security/email/check', { email: data.element.val() }, function(checkEmail) {
                if (checkEmail) {
                    // такой пользователь уже имеется
                    $parent.removeClass('has-success');
                    $parent.addClass('has-error');
                }
            });
        }
    });
});