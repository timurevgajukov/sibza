/**
 * Created by Тимур on 19.06.2015.
 *
 * Основной файл со скриптами
 */

$(document).ready(function () {
    // для выпадающих списков подключаем плагин
    $(".chosen").chosen({"width": "100%"});

    $('#data-table').dataTable({
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });
    $('#data-table-order-date').dataTable({
        "responsive": true,
        "order": [[2, 'desc']],
        "language": {
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    // обработка нажатия на enter в строке поиска
    $('#search-query').keyup(function(event) {
        if (event.keyCode == 13) {
            $('#search-btn').click();
        }
    });

    // обработка поискового запроса
    $('#search-btn').click(function () {
        console.log("Отправка поискового запроса на сервер");
        $.form('/search', {'search-query': $('#search-query').val()}, 'POST').submit();
    });

    // Для регистрации push-уведомлений
    $(document).on('click', '.sp_notify_prompt', function() {
        console.log("Add browser push notification");

        var closeBtn = $('.sp_notify_prompt').parent().parent().parent().prev();
        closeBtn.trigger('click');
    });
});

var ALERT_TIME = 5000; // время отображения нотификации

/**
 * Типы нотификаций
 */
var alertType = {
    info: {
        icon: 'fa fa-info fa-lg',
        title: "Info",
        timer: 600 * ALERT_TIME
    },
    primary: {
        icon: 'fa fa-star fa-lg',
        title: "Primary",
        timer: ALERT_TIME
    },
    success: {
        icon: 'fa fa-thumbs-up fa-lg',
        title: "Success",
        timer: ALERT_TIME
    },
    warning: {
        icon: 'fa fa-bolt fa-lg',
        title: "Warning",
        timer: 60 * ALERT_TIME
    },
    danger: {
        icon: 'fa fa-times fa-lg',
        title: "Danger",
        timer: 600 * ALERT_TIME
    },
    mint: {
        icon: 'fa fa-leaf fa-lg',
        title: "Mint",
        timer: 600* ALERT_TIME
    },
    purple: {
        icon: 'fa fa-shopping-cart fa-lg',
        title: "Purple",
        timer: ALERT_TIME
    },
    pink: {
        icon: 'fa fa-heart fa-lg',
        title: "Pink",
        timer: ALERT_TIME
    },
    dark: {
        icon: 'fa fa-sun-o fa-lg',
        title: "Dark",
        timer: ALERT_TIME
    }
};

/**
 * Отображение нотификации
 *
 * @param title
 * @param message
 * @param type
 */
function showAlert(message, type) {
    console.log("Show alert: " + message);

    var op = {
        type: type,
        icon: alertType[type].icon,
        title: alertType[type].title,
        message: message,
        timer: alertType[type].timer
    }

    $.niftyNoty(op);
}

/**
 * Отображение нотификации в виде всплывающего окна
 *
 * @param title
 * @param message
 * @param type
 */
function showNotification(title, message, type) {
    console.log("Show alert: " + message);

    $.niftyNoty({
        type: type,
        icon: alertType[type].icon,
        title: title,
        message: message,
        container: 'floating',
        timer: 0
    });
}

/**
 * Получить куки по имени
 *
 * @param name
 * @returns {null}
 */
function getCookie(name) {
    var results = document.cookie.match ( '(^|;) ?' + name + '=([^;]*)(;|$)' );

    if (results) {
        return (unescape(results[2]));
    } else {
        return null;
    }
}

/**
 * Установить куки
 *
 * @param name
 * @param value
 * @param exp_y
 * @param exp_m
 * @param exp_d
 * @param path
 * @param domain
 * @param secure
 */
function setСookie(name, value, exp_y, exp_m, exp_d, path, domain, secure) {
    var cookie_string = name + "=" + escape ( value );

    if (exp_y) {
        var expires = new Date ( exp_y, exp_m, exp_d );
        cookie_string += "; expires=" + expires.toGMTString();
    }

    if (path) {
        cookie_string += "; path=" + escape(path);
    }

    if (domain) {
        cookie_string += "; domain=" + escape(domain);
    }

    if (secure) {
        cookie_string += "; secure";
    }

    document.cookie = cookie_string;
}

/**
 * Удалить куки по имени
 *
 * @param name
 */
function deleteCookie(name) {
    var cookie_date = new Date ( );  // Текущая дата и время
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = name += "=; expires=" + cookie_date.toGMTString();
}

jQuery(function ($) {
    $.extend({
        form: function (url, data, method) {
            if (method == null) method = 'POST';
            if (data == null) data = {};

            var form = $('<form>').attr({
                method: method,
                action: url
            }).css({
                display: 'none'
            });

            var addData = function (name, data) {
                if ($.isArray(data)) {
                    for (var i = 0; i < data.length; i++) {
                        var value = data[i];
                        addData(name + '[]', value);
                    }
                } else if (typeof data === 'object') {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            addData(name + '[' + key + ']', data[key]);
                        }
                    }
                } else if (data != null) {
                    form.append($('<input>').attr({
                        type: 'hidden',
                        name: String(name),
                        value: String(data)
                    }));
                }
            };

            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    addData(key, data[key]);
                }
            }

            return form.appendTo('body');
        }
    });
});