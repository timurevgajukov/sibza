/**
 * Created by timur on 03.03.16.
 */

function connectWS(endpoint, subscribe, handler) {
    try {
        var socket = new SockJS(endpoint);
        var stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            try {
                stompClient.subscribe(subscribe, function (message) {
                    try {
                        handler(JSON.parse(message.body));
                    } catch (err) {
                        console.log(err);
                    }
                });
            } catch (err) {
                console.log(err);
            }
        });
    } catch (err) {
        console.log(err);
    }
}