package org.sibza.utils;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by timur on 09.05.15.
 *
 * Работа с базой данных
 */
public class DBase {

    public static DataSource getDataSource() {

        try {
            InitialContext initContext= new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/SibzaDC");

            return ds;
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Connection getConnection() throws SQLException {

        DataSource ds = getDataSource();
        if (ds != null) {
            return ds.getConnection();
        }

        return null;
    }
}