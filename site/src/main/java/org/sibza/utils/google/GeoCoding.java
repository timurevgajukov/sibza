package org.sibza.utils.google;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

/**
 * Created by Тимур on 19.11.2015.
 *
 * Класс для работы с гео данными
 * Использует в свое работе Google Geocoding API
 */
public class GeoCoding {

    private static final String GEO_API_KEY = "AIzaSyCLv6HgOhgfth31iS8ANXBa3e1K1tmqvP4";

    /**
     * Возвращает информацию по гео координатам
     *
     * @param lat широта
     * @param lon долгота
     */
    public GeocodingResult[] getGeoInfo(double lat, double lon) {

        GeocodingResult[] results = null;

        GeoApiContext context = new GeoApiContext().setApiKey(GEO_API_KEY);
        try {
            results = GeocodingApi.reverseGeocode(context, new LatLng(lat, lon)).await();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    /**
     * Возвращает информацию по адресу
     *
     * @param address адрес
     */
    public GeocodingResult[] getGeoInfo(String address) {

        GeocodingResult[] results = null;

        GeoApiContext context = new GeoApiContext().setApiKey(GEO_API_KEY);
        try {
            results = GeocodingApi.geocode(context, address).await();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    /**
     * Возвращает код языка по геокоординатам
     *
     * @param lat
     * @param lon
     * @param defaultLanguageCode
     * @return
     */
    public String getLanguageCodeByGeo(double lat, double lon, String defaultLanguageCode) {

        GeocodingResult[] results = getGeoInfo(lat, lon);
        String countryCode = results[results.length-1].addressComponents[0].shortName;

        if ("RU".equalsIgnoreCase(countryCode)) {
            return "RU";
        } else if ("TR".equalsIgnoreCase(countryCode)) {
            return "TU";
        }

        return defaultLanguageCode;
    }

    public static void main(String[] args) {

        System.out.println("Start test");

        GeoCoding geoCoding = new GeoCoding();
        GeocodingResult[] results = geoCoding.getGeoInfo("Турция");

        String name = results[0].addressComponents[0].shortName;
        String typeName = results[0].addressComponents[0].types[0].name();

        System.out.println(String.format("type = %s, name = %s", typeName, name));
    }
}