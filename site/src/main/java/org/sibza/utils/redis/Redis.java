package org.sibza.utils.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by timur on 26.02.16.
 *
 * Класс для работы с хранилищем Redis
 */
public class Redis {

    public static final String REDIS_PASSWORD = "FIRkbo98749";
    public static final String REDIS_EXTERNAL_IP = "194.87.214.100";
    public static final int REDIS_PORT = 6379;

    private Jedis jedis;

    public Redis() {

        JedisShardInfo shardInfo = new JedisShardInfo(REDIS_EXTERNAL_IP, REDIS_PORT);
        shardInfo.setPassword(REDIS_PASSWORD);
        jedis = new Jedis(shardInfo);
    }

    /**
     * Соединение с хранилищем данных
     */
    public void connect() {

        if (jedis != null && !jedis.isConnected()) {
            jedis.connect();
        }
    }

    /**
     * Отсоединение от хранилища данных
     */
    public void disconnect() {

        if (jedis != null && jedis.isConnected()) {
            jedis.disconnect();
        }
    }

    /**
     * Признак, что мы подключены к хранилищу данных
     *
     * @return
     */
    public boolean isConnected() {

        if (jedis == null) {
            return false;
        }

        return jedis.isConnected();
    }

    /**
     * Выбор используемой БД
     *
     * @param index индекс БД, на которую нужно перейти
     */
    public void select(int index) {

        jedis.select(index);
    }

    /**
     * Получить строковое значение по ключу
     *
     * @param key ключ
     * @return
     */
    public String get(String key) {

        return jedis.get(key);
    }

    /**
     * Записать в хранилище данных строковое значение по ключу
     *
     * @param key ключ
     * @param value значение
     */
    public void set(String key, String value) {

        jedis.set(key, value);
    }

    /**
     * Дописать в конец строки, либо, если ключ не определен, то создать новое значение
     *
     * @param key ключ
     * @param value дописываемое значение
     * @return возвращает новую длину значения
     */
    public Long append(String key, String value) {

        return jedis.append(key, value);
    }

    /**
     * Возвращает длину значения по ключу
     *
     * @param key ключ
     * @return
     */
    public Long strlen(String key) {

        return jedis.strlen(key);
    }

    /**
     * Возвращает подстроку по ключу
     *
     * @param key ключ
     * @param start начальный индекс подстроки
     * @param end смещение подстроки
     * @return
     */
    public String getrand(String key, long start, long end) {

        return jedis.getrange(key, start, end);
    }

    /**
     * Интерпретирует значение по ключу как число, увеличивает его на 1 и возвращает новое значение
     *
     * @param key ключ
     * @return
     */
    public Long incr(String key) {

        return jedis.incr(key);
    }

    /**
     * Интерспретирует значение по ключу как число, увеличивает на указанное значение и возвращает новое значение
     *
     * @param key ключ
     * @param value значение, на которое необходимо увеличить
     * @return
     */
    public Long incrBy(String key, long value) {

        return jedis.incrBy(key, value);
    }

    /**
     * Возвращает значение по ключу и полю
     *
     * @param key ключ
     * @param field поле
     * @return
     */
    public String hget(String key, String field) {

        return jedis.hget(key, field);
    }

    /**
     * Сохраняет новое значение по ключу и по полю
     *
     * @param key ключ
     * @param field поле
     * @param value значение
     */
    public void hset(String key, String field, String value) {

        jedis.hset(key, field, value);
    }

    /**
     * Возвращает список значений по ключу и списку полей
     *
     * @param key ключ
     * @param fields список полей
     * @return
     */
    public List<String> hmget(String key, String... fields) {

        return jedis.hmget(key, fields);
    }

    /**
     * Добавляет сразу к нескольким полям значения по ключу
     *
     * @param key ключ
     * @param hash пары поле и значение
     * @return
     */
    public String hmset(String key, Map<String, String> hash) {

        return jedis.hmset(key, hash);
    }

    /**
     * Возвращает все значения полей по ключу
     *
     * @param key ключ
     * @return
     */
    public Map<String, String> hgetAll(String key) {

        return jedis.hgetAll(key);
    }

    /**
     * Возвращяет название всех полей по ключу
     *
     * @param key ключ
     * @return
     */
    public Set<String> hkeys(String key) {

        return jedis.hkeys(key);
    }

    /**
     * Удаляет список полей по ключу
     *
     * @param key ключ
     * @param fields список полей
     * @return
     */
    public Long hdel(String key, String... fields) {

        return jedis.hdel(key, fields);
    }

    public static void main(String[] args) {

        Redis redis = new Redis();
        redis.connect();

        System.out.println(redis.get("foo"));

        redis.disconnect();
    }
}
