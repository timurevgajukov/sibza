package org.sibza.utils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.Properties;

/**
 * Created by timur on 04.06.15.
 *
 * Утилита для работы с электронной почтой
 */
public class Email {

    private String username;
    private String password;
    private Properties props;

    private String template = "###CONTENT##";
    private String resourcePath;

    public Email(String username, String password) {

        this.username = username;
        this.password = password;

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.yandex.ru");
        props.put("mail.smtp.port", "587");
    }

    public Email(String username, String password, String template, String resourcePath) {

        this(username, password);

        this.template = template;
        this.resourcePath = resourcePath;
    }

    public void send(String subject, String text, String toEmail) {

        Session session = Session.getInstance(props, new Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username)); // от кого
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail)); // кому
            message.setSubject(subject); // Заголовок письма

            String body = template.replace("##CONTENT##", text);

            MimeMultipart multipart = new MimeMultipart("related");

            // добавляем основной текст
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html; charset=utf-8");
            multipart.addBodyPart(messageBodyPart);

            // добавляем логотип
            try {
                multipart.addBodyPart(getImageBodyPart(resourcePath + "/img/logo-white-small.png", "logo"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            // добавляем логотип социальных сетей
            try {
                multipart.addBodyPart(getImageBodyPart(resourcePath + "/img/icons/facebook.png", "facebook"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                multipart.addBodyPart(getImageBodyPart(resourcePath + "/img/icons/vk.png", "vk"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                multipart.addBodyPart(getImageBodyPart(resourcePath + "/img/icons/instagram.png", "instagram"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            message.setContent(multipart); // Содержимое

            Transport.send(message); // Отправляем сообщение
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private MimeBodyPart getImageBodyPart(String resourceUrl, String resourceId)
            throws MessagingException, MalformedURLException, FileNotFoundException {

        if (new File(resourceUrl).exists()) {
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(resourceUrl);
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", String.format("<%s>", resourceId));

            return messageBodyPart;
        } else {
            throw new FileNotFoundException("Not found " + resourceUrl);
        }
    }
}