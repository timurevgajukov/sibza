package org.sibza.utils.yandex.translate.api;

import com.google.gson.Gson;
import org.sibza.utils.Net;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;

/**
 * Created by Тимур on 06.10.2015.
 *
 * API для работы с Яндекс переводчиком
 */
public class TranslateApi {

    private static final String API_KEY = "trnsl.1.1.20151006T115816Z.6de9e0649c28ed84.07216aacae99e6b21f80937c5195293706952989";
    private static final String BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/";

    private static final String TRANSLATE_URL = BASE_URL + "translate";

    private static TranslateApi instance = null;

    public static TranslateApi getInstance() {

        if (instance == null) {
            instance = new TranslateApi();
        }

        return instance;
    }

    /**
     * Переводит слова или фразу на выбранный язык
     *
     * @param text
     * @param lang
     * @return
     */
    public String translate(String text, String lang) {

        try {
            String url = TRANSLATE_URL + "?key=" + API_KEY
                    + "&text=" + URLEncoder.encode(text, "UTF-8")
                    + "&lang=" + lang.toLowerCase();
            String resultJson = Net.get(url);

            System.out.println(url);
            System.out.println(resultJson);

            Gson gson = new Gson();
            TranslateApiResult result = gson.fromJson(resultJson, TranslateApiResult.class);

            if (result != null && result.getText() != null) {
                if (result.getText().size() != 0) {
                    return result.getText().get(0);
                }
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {

        TranslateApi api = TranslateApi.getInstance();
        api.translate("Как замечательны в России вечера!", "EN");
    }

    class TranslateApiResult {

        private int code;
        private String lang;
        private List<String> text;

        public int getCode() {

            return code;
        }

        public void setCode(int code) {

            this.code = code;
        }

        public String getLang() {

            return lang;
        }

        public void setLang(String lang) {

            this.lang = lang;
        }

        public List<String> getText() {

            return text;
        }

        public void setText(List<String> text) {

            this.text = text;
        }
    }
}