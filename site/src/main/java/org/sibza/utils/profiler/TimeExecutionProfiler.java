package org.sibza.utils.profiler;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by Тимур on 20.11.2015.
 */
@Component
@Aspect
public class TimeExecutionProfiler {

    @Around("org.sibza.utils.profiler.SystemArchitecture.businessController()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {

        long start = System.currentTimeMillis();
        Object output = pjp.proceed();
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        System.out.println(String.format("%s - elapsed: %d ms", pjp.getSignature(), elapsed));

        return output;
    }
}