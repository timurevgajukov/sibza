package org.sibza.utils.profiler;

/**
 * Created by Тимур on 20.11.2015.
 *
 * Профилеровщик для анализа работы сайта
 */
public class Profiler {

    private static Profiler instance = null;

    private long startTimerValue;
    private long endTimerValue;

    public static Profiler getInstance() {

        if (instance == null) {
            instance = new Profiler();
        }

        return instance;
    }

    public void startTimer() {

        startTimerValue = System.currentTimeMillis();
    }

    public long endTimer() {

        return endTimer(false);
    }

    public long endTimer(boolean print) {

        endTimerValue = System.currentTimeMillis();

        if (print) {
            printTimerDiff();
        }

        return endTimerValue - startTimerValue;
    }

    public void printTimerDiff() {

        System.out.println(String.format("Diff: %d ms", endTimerValue - startTimerValue));
    }
}