package org.sibza.utils.profiler;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Created by Тимур on 20.11.2015.
 */
@Component
@Aspect
public class SystemArchitecture {

    @Pointcut("execution(* org.sibza.site..*.*(..))")
    public void businessController() {}
}