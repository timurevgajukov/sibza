package org.sibza.utils;

import com.google.gson.Gson;

import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by timur on 30.09.15.
 *
 * Утилита для импорта данных со страрого сайта на wiki
 */
public class ImportWiki {

    private static final String BASE_API_URL = "https://sibza.org/api";
    private static final String API_CATEGORIES_LIST = "/mediawiki/categories/list";
    private static final String API_PAGES_LIST = "/pages/list";
    private static final String API_PAGE_GET = "/mediawiki/pages/get";

    private static final String[] IGNORE_CATEGORIES_LIST = { "Алфавит", "Народные песни и сказания" };

    public List<Category> importCategories(String lng) {

        System.out.println(">>> Запуск импорта категорий");

        String resultStr = Net.get(BASE_API_URL + API_CATEGORIES_LIST + "?lng=" + lng);
        // System.out.println(resultStr);

        Gson gson = new Gson();
        ResultCategoriesList result = gson.fromJson(resultStr, ResultCategoriesList.class);

        System.out.println(">>> Импорт категорий успешно завершен");

        return result.getBody();
    }

    public List<Page> importPages(String category) {

        System.out.println(">>> Запуск импорта страниц для категории " + category);

        try {
            String resultStr = Net.get(BASE_API_URL + API_PAGES_LIST + "?category=" + URLEncoder.encode(category, "UTF-8"));
            // System.out.println(resultStr);

            Gson gson = new Gson();
            ResultPagesList result = gson.fromJson(resultStr, ResultPagesList.class);

            System.out.println(">>> Импорт страниц успешно завершен");

            return result.getBody();
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

    public PageBody importPageInfo(String title) {

        System.out.println(">>> Запуск получения страницы с заголовком " + title);

        try {
            String resultStr = Net.get(BASE_API_URL + API_PAGE_GET + "?title=" + URLEncoder.encode(title, "UTF-8"));
            // System.out.println(resultStr);

            Gson gson = new Gson();
            ResultPageBody result = gson.fromJson(resultStr, ResultPageBody.class);

            System.out.println(">>> Получение страницы успешно завершено");

            return result.getBody();
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

    public void run(String root) {

        long tm = System.currentTimeMillis();

        StringBuilder table = new StringBuilder("Категория;Слово;Перевод;Аудио\n");
        long count = 0;

        System.out.println("Запуск импорта");

        List<Category> categories = importCategories(root);

        // по каждой категории получаем список статей
        if (categories == null || categories.size() == 0) {
            System.out.println("!!! Не удалось получить список категорий");
        } else {
            for (Category category : categories) {
                if (!inIgroreList(category.getName())) {
                    List<Page> pages = importPages(category.getName());
                    // по каждой статье получаем ее содержимое
                    if (pages == null || pages.size() == 0) {
                        System.out.println("!!! Не удалось получить список статей по категории " + category.getName());
                    } else {
                        for (Page page : pages) {
                            PageBody body = importPageInfo(page.getTitle());
                            if (body == null) {
                                System.out.println("!!! Не удалось получить тело страницы");
                            } else {
                                // Из текста статьи получаем список переводов
                                String[] translates = body.getText().split("\\*");

                                // System.out.println(">>>>>>>>>>>>>> " + body.getText());
                                for (String item : translates) {
                                    String translate = item.trim().replace("\n", " ");
                                    if (translate.length() != 0) {
                                        // System.out.println(">>>>>>>>>>>>>>>>>> " + translate);
                                        table.append(category.getName()).append(";").append(page.getTitle()).append(";")
                                                .append(translate).append(";").append(body.getAudioUrl()).append("\n");
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        System.out.println("Импорт успешно завершен. Время работы: "
                + (System.currentTimeMillis() - tm) / 1000.0 + " сек. Всего слов с переводом: " + count);

        // сохраняем все в файл
        System.out.println("Сохраняем данные в файл...");
        try {
            PrintWriter write = new PrintWriter("C:/Users/Тимур/Downloads/sibza-" + root + ".csv", "UTF-8");
            write.print(table.toString());
            write.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean inIgroreList(String category) {

        for (String item : IGNORE_CATEGORIES_LIST) {
            if (item.equalsIgnoreCase(category)) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {

        new ImportWiki().run("TR");
    }

    class ResultCategoriesList {

        private int code;
        private String description;
        private List<Category> body;

        public int getCode() {

            return code;
        }

        public void setCode(int code) {

            this.code = code;
        }

        public String getDescription() {

            return description;
        }

        public void setDescription(String description) {

            this.description = description;
        }

        public List<Category> getBody() {

            return body;
        }

        public void setBody(List<Category> body) {

            this.body = body;
        }
    }

    class ResultPagesList {

        private int code;
        private String description;
        private List<Page> body;

        public int getCode() {

            return code;
        }

        public void setCode(int code) {

            this.code = code;
        }

        public String getDescription() {

            return description;
        }

        public void setDescription(String description) {

            this.description = description;
        }

        public List<Page> getBody() {

            return body;
        }

        public void setBody(List<Page> body) {

            this.body = body;
        }
    }

    class ResultPageBody {

        private int code;
        private String description;
        private PageBody body;

        public int getCode() {

            return code;
        }

        public void setCode(int code) {

            this.code = code;
        }

        public String getDescription() {

            return description;
        }

        public void setDescription(String description) {

            this.description = description;
        }

        public PageBody getBody() {

            return body;
        }

        public void setBody(PageBody body) {

            this.body = body;
        }
    }

    class Category {

        private String name;
        private int pagesCount;

        public String getName() {

            return name;
        }

        public void setName(String name) {

            this.name = name;
        }

        public int getPagesCount() {

            return pagesCount;
        }

        public void setPagesCount(int pagesCount) {

            this.pagesCount = pagesCount;
        }
    }

    class Page {

        private int id;
        private String title;

        public int getId() {

            return id;
        }

        public void setId(int id) {

            this.id = id;
        }

        public String getTitle() {

            return title;
        }

        public void setTitle(String title) {

            this.title = title;
        }
    }

    class PageBody {

        private int id;
        private String text;
        private String audioUrl;
        private List<String> categories;

        public int getId() {

            return id;
        }

        public void setId(int id) {

            this.id = id;
        }

        public String getText() {

            return text;
        }

        public void setText(String text) {

            this.text = text;
        }

        public String getAudioUrl() {

            return audioUrl;
        }

        public void setAudioUrl(String audioUrl) {

            this.audioUrl = audioUrl;
        }

        public List<String> getCategories() {

            return categories;
        }

        public void setCategories(List<String> categories) {

            this.categories = categories;
        }
    }
}