package org.sibza.utils;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.sibza.utils.sendpulse.Sendpulse;
import org.sibza.utils.sendpulse.auth.AuthRequest;
import org.sibza.utils.sendpulse.auth.AuthResponse;
import org.sibza.utils.sendpulse.push.TasksRequest;
import org.sibza.utils.sendpulse.push.WebsitesRequest;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

/**
 * Класс для работы с сетью
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Net {

    private static final int TIMEOUT = 5000;


    public static String get(String request) {

        return get(request, null);
    }

    public static String get(String request, String auth) {

        try {
            trustCert();

            Proxy proxy = Proxy.NO_PROXY;
            URL url = new URL(request);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection(proxy);
            conn.setConnectTimeout(TIMEOUT);

            if (auth != null) {
                conn.setRequestProperty("Authorization", auth);
            }

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                String result = builder.toString();
                if (result != null && result.length() > 0) {
                    return result;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String post(String link, String request) {

        return post(link, request, null);
    }

    public static String post(String link, String request, String auth) {

        try {
            trustCert();

            Proxy proxy = Proxy.NO_PROXY;
            URL url = new URL(link);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection(proxy);
            conn.setConnectTimeout(TIMEOUT);
            conn.setRequestMethod("POST");

            if (auth != null) {
                conn.setRequestProperty("Authorization", auth);
            }

            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            osw.write(request);
            osw.flush();
            osw.close();

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                String result = builder.toString();
                if (result != null && result.length() > 0) {
                    return result;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Возвращает данные по сети
     *
     * @param request
     * @return
     */
    public static byte[] getByteArray(String request) {

        try {
            Proxy proxy = Proxy.NO_PROXY;
            URL url = new URL(request);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
            conn.setConnectTimeout(TIMEOUT);

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                return IOUtils.toByteArray(is);
            }
        } catch (Exception e) {

        }

        return null;
    }

    private static void trustCert() {

        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{ new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {

                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {

                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {

                }
            } };

            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {

        }
    }
}