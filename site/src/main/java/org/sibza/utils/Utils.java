package org.sibza.utils;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.List;

/**
 * Полузные методы
 *
 * @author Евгажуков Т.Х.
 */
public class Utils {

    /**
     * Возвращает false, если массив или объект равен null или пуст, иначе true
     *
     * @param value проверяемый массив
     * @return
     */
    public static boolean isEmpty(Object value) {

        if (value == null) {
            return true;
        }

        if (value instanceof List) {
            return ((List) value).size() == 0;
        } else {
            return false;
        }
    }

    /**
     * Возвращает слово с первой заглавной буквой
     *
     * @param word
     * @return
     */
    public static String firstUpperCase(String word) {

        if (word == null || word.isEmpty()) {
            return word;
        }

        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    /**
     * Возвращает атрибут xml-узла по его имени
     *
     * @param node
     * @param name
     * @return
     */
    public static String getAttribute(Node node, String name) {

        NamedNodeMap attr = node.getAttributes();
        for (int i = 0; i < attr.getLength(); i++) {
            if (attr.item(i).getNodeName().equalsIgnoreCase(name)) {
                return attr.item(i).getNodeValue();
            }
        }

        return null;
    }

    /**
     * Нормализаует слова на адыгском языке приводя все похожие символы к I
     *
     * @param value
     * @return
     */
    public static String adygAlphabetNorm(String value) {

        return value.replaceAll("l", "I").replaceAll("1", "I").replaceAll("!", "I");
    }

    /**
     * Возвращает расстояние Левенштейна для двух строк
     *
     * @param val1
     * @param val2
     * @return
     */
    public static int levensh(String val1, String val2) {

        int m = val1.length();
        int n = val2.length();
        int[] D1;
        int[] D2 = new int[n + 1];

        for (int i = 0; i <= n; i++)
            D2[i] = i;

        for (int i = 1; i <= m; i++) {
            D1 = D2;
            D2 = new int[n + 1];
            for (int j = 0; j <= n; j++) {
                if (j == 0) {
                    D2[j] = i;
                } else {
                    int cost = (val1.charAt(i - 1) != val2.charAt(j - 1)) ? 1 : 0;
                    if (D2[j - 1] < D1[j] && D2[j - 1] < D1[j - 1] + cost) {
                        D2[j] = D2[j - 1] + 1;
                    } else if (D1[j] < D1[j - 1] + cost) {
                        D2[j] = D1[j] + 1;
                    } else {
                        D2[j] = D1[j - 1] + cost;
                    }
                }
            }
        }

        return D2[n];
    }
}
