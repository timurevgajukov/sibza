package org.sibza.utils.sendpulse.push.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Тимур on 15.01.2016.
 *
 * Фильтрация пользователей про различным условиям
 */
public class Filter {

    public static final String OP_AND = "and";
    public static final String OP_OR = "or";

    private String variableName;
    private String operator;
    private List<Condition> conditions;

    public String getVariableName() {

        return variableName;
    }

    public void setVariableName(String variableName) {

        this.variableName = variableName;
    }

    public String getOperator() {

        return operator;
    }

    public void setOperator(String operator) {

        this.operator = operator;
    }

    public List<Condition> getConditions() {

        return conditions;
    }

    public void setConditions(List<Condition> conditions) {

        this.conditions = conditions;
    }

    public Filter addCondition(Condition condition) {

        if (conditions == null) {
            conditions = new ArrayList<Condition>();
        }

        conditions.add(condition);

        return this;
    }

    @Override
    public String toString() {

        String result = String.format("filter.variable_name=%s&filter.operator=%s", variableName, operator);
        if (conditions != null && conditions.size() != 0) {
            for (int i=0; i<conditions.size(); i++) {
                result += String.format("&filter.conditions[%d].condition=%s", i, conditions.get(i).getCondition());
                result += String.format("&filter.conditions[%d].value=%s", i, conditions.get(i).getValue());
            }
        }

        return result;
    }
}
