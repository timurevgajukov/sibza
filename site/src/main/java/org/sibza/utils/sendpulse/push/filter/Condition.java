package org.sibza.utils.sendpulse.push.filter;

/**
 * Created by Тимур on 15.01.2016.
 *
 * Элемент проверки для фильтров
 */
public class Condition {

    public static final String EQUAL = "equal"; // полностью равно
    public static final String NOTEQUAL = "notequal"; // полностью не равно
    public static final String GREATERTHAN = "greaterthan"; // больше чем
    public static final String LESSTHAN = "lessthan"; // меньше чем
    public static final String STARTWITH = "startwith"; // начинается с
    public static final String ENDWITH = "endwith"; // заканчивается этим значением
    public static final String LIKEWITH = "likewith"; // содержит в себе
    public static final String NOTLIKEWITH = "notlikewith"; // не соджержит в себе

    private String condition;
    private String value;

    public Condition(String condition, String value) {

        this.condition = condition;
        this.value = value;
    }

    public String getCondition() {

        return condition;
    }

    public void setCondition(String condition) {

        this.condition = condition;
    }

    public String getValue() {

        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }
}
