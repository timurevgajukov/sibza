package org.sibza.utils.sendpulse;

/**
 * Created by Тимур on 15.01.2016.
 *
 * Общий ответ для запросов
 */
public class Response {

    private int id;
    private boolean result;

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public boolean isResult() {

        return result;
    }

    public void setResult(boolean result) {

        this.result = result;
    }
}
