package org.sibza.utils.sendpulse.auth;

/**
 * Created by Тимур on 13.01.2016.
 *
 * Класс для запроса авторизации
 */
public class AuthRequest {

    public static final String URL = "https://api.sendpulse.com/oauth/access_token";

    private String grantType;
    private String clientId;
    private String clientSecret;

    {
        grantType = "client_credentials";
    }

    public AuthRequest(String clientId, String clientSecret) {

        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    public String getGrantType() {

        return grantType;
    }

    public void setGrantType(String grantType) {

        this.grantType = grantType;
    }

    public String getClientId() {

        return clientId;
    }

    public void setClientId(String clientId) {

        this.clientId = clientId;
    }

    public String getClientSecret() {

        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {

        this.clientSecret = clientSecret;
    }

    @Override
    public String toString() {

        return String.format("grant_type=%s&client_id=%s&client_secret=%s", grantType, clientId, clientSecret);
    }
}