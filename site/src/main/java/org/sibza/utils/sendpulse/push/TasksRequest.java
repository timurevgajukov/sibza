package org.sibza.utils.sendpulse.push;

import org.sibza.utils.sendpulse.push.filter.Filter;

import java.net.URLEncoder;

/**
 * Created by Тимур on 13.01.2016.
 *
 * Класс для отправки задания на push
 */
public class TasksRequest {

    public static final String URL = "https://api.sendpulse.com/push/tasks";

    private String title;
    private int websiteId;
    private String body;
    private int ttl;
    private int stretchTime;
    private Filter filter;

    public TasksRequest(String title, int websiteId, String body, int ttl, int stretchTime) {

        this.title = title;
        this.websiteId = websiteId;
        this.body = body;
        this.ttl = ttl;
        this.stretchTime = stretchTime;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public int getWebsiteId() {

        return websiteId;
    }

    public void setWebsiteId(int websiteId) {

        this.websiteId = websiteId;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public int getTtl() {

        return ttl;
    }

    public void setTtl(int ttl) {

        this.ttl = ttl;
    }

    public int getStretchTime() {

        return stretchTime;
    }

    public void setStretchTime(int stretchTime) {

        this.stretchTime = stretchTime;
    }

    public Filter getFilter() {

        return filter;
    }

    public void setFilter(Filter filter) {

        this.filter = filter;
    }

    @Override
    public String toString() {

        try {
            String result = String.format("title=%s&website_id=%d&body=%s&ttl=%d&stretch_time=%d",
                    URLEncoder.encode(title, "UTF-8"),
                    websiteId,
                    URLEncoder.encode(body, "UTF-8"),
                    ttl,
                    stretchTime);

            if (filter != null) {
                // добавляем параметры фильтрации
                result += "&" + filter.toString();
            }

            return result;
        } catch (Exception e) {

        }

        return "";
    }
}