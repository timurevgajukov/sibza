package org.sibza.utils.sendpulse;

import com.google.gson.Gson;
import org.sibza.utils.Net;
import org.sibza.utils.sendpulse.auth.AuthRequest;
import org.sibza.utils.sendpulse.auth.AuthResponse;
import org.sibza.utils.sendpulse.push.TasksRequest;
import org.sibza.utils.sendpulse.push.WebsitesRequest;

/**
 * Created by Тимур on 15.01.2016.
 * <p/>
 * Основной класс для отправки push на браузеры через сервис SendPulse
 */
public class Sendpulse {

    private static final String CLIENT_ID = "d60a9a696e33a2b15c66abd523a9db4f";
    private static final String CLIENT_SECRET = "2f0200630eca529138f3f86b6cc754a5";

    public static final int WEBSITE_ID = 1679;
    public static final int TTL = 3600;
    public static final int STRETCH_TIME = 1;

    public boolean send(String title, String body) {

        TasksRequest tasks = new TasksRequest(title, WEBSITE_ID, body, TTL, STRETCH_TIME);
        return send(tasks);
    }

    public boolean send(TasksRequest tasks) {

        Gson gson = new Gson();

        AuthRequest auth = new AuthRequest(CLIENT_ID, CLIENT_SECRET);
        String authResultJSON = Net.post(AuthRequest.URL, auth.toString());
        AuthResponse authResponse = gson.fromJson(authResultJSON, AuthResponse.class);

        String tasksResultJSON = Net.post(TasksRequest.URL, tasks.toString(), authResponse.toString());
        Response tasksResponse = gson.fromJson(tasksResultJSON, Response.class);

        if (tasksResponse == null) {
            return false;
        }

        return tasksResponse.isResult();
    }
}
