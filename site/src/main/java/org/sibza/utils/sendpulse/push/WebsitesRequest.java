package org.sibza.utils.sendpulse.push;

/**
 * Created by Тимур on 15.01.2016.
 *
 * Класс для отправки заданий на получение списка сайтов
 */
public class WebsitesRequest {

    public static final String URL = "https://api.sendpulse.com/push/websites/";

    private int limit;
    private int offset;

    public WebsitesRequest(int limit, int offset) {

        this.limit = limit;
        this.offset = offset;
    }

    public int getLimit() {

        return limit;
    }

    public void setLimit(int limit) {

        this.limit = limit;
    }

    public int getOffset() {

        return offset;
    }

    public void setOffset(int offset) {

        this.offset = offset;
    }

    @Override
    public String toString() {

        return String.format("limit=%d&offset=%d", limit, offset);
    }
}
