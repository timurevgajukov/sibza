package org.sibza.utils.pushall.request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Тимур on 18.01.2016.
 *
 * Параметры для отправки уведомлений
 */
public class PushRequest {

    public static final String TYPE_SELF = "self"; // самому себе;
    public static final String TYPE_BROADCAST = "broadcast"; // вещание по каналу
    public static final String TYPE_UNICAST = "unicast"; // отправка одному пользователю
    public static final String TYPE_SHOWLIST = "showlist"; // получение ленты канала

    public static final int HIDDEN_NO = 0; // не скрывать сообщение (по-умолчанию)
    public static final int HIDDEN_HISTORY = 1; // сразу скрыть уведомление из истории пользователей
    public static final int HIDDEN_TAPE = 2; // скрыть только из ленты

    public static final int PRIORITY_NORMAL = 0;
    public static final int PRIORITY_NOT_IMP = -1;
    public static final int PRIORITY_IMP = 1;

    private String type; // тип запроса
    private int id; // номер вашей подписки, в случае self - ваш ID
    private String key; // ключ вашей подписки, или ключ безопасности для отправки push себе
    private String title; // заголовок push-уведомления
    private String text; // основной текст push-уведомления
    private String icon; // иконка уведомления (не обязательно)
    private String url; // адрес, по которому будет осуществлен переход по клику (не обязательно)
    private int hidden;
    private String encode; // ваша кодировка (не обязательно)
    private int priority;

    public PushRequest(String type, int id, String key) {

        this.type = type;
        this.id = id;
        this.key = key;
    }

    public PushRequest(String type, int id, String key, String text) {

        this.type = type;
        this.id = id;
        this.key = key;
        this.text = text;
    }

    public String getType() {

        return type;
    }

    public PushRequest setType(String type) {

        this.type = type;

        return this;
    }

    public int getId() {

        return id;
    }

    public PushRequest setId(int id) {

        this.id = id;

        return this;
    }

    public String getKey() {

        return key;
    }

    public PushRequest setKey(String key) {

        this.key = key;

        return this;
    }

    public String getTitle() {

        return title;
    }

    public PushRequest setTitle(String title) {

        this.title = title;

        return this;
    }

    public String getText() {

        return text;
    }

    public PushRequest setText(String text) {

        this.text = text;

        return this;
    }

    public String getIcon() {

        return icon;
    }

    public PushRequest setIcon(String icon) {

        this.icon = icon;

        return this;
    }

    public String getUrl() {

        return url;
    }

    public PushRequest setUrl(String url) {

        this.url = url;

        return this;
    }

    public int getHidden() {

        return hidden;
    }

    public PushRequest setHidden(int hidden) {

        this.hidden = hidden;

        return this;
    }

    public String getEncode() {

        return encode;
    }

    public PushRequest setEncode(String encode) {

        this.encode = encode;

        return this;
    }

    public int getPriority() {

        return priority;
    }

    public PushRequest setPriority(int priority) {

        this.priority = priority;

        return this;
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        result.append("type=").append(type);
        result.append("&").append("id=").append(id);
        result.append("&").append("key=").append(key);
        if (title != null && title.length() != 0) {
            try {
                result.append("&").append("title=").append(URLEncoder.encode(title, "UTF-8"));
            } catch (UnsupportedEncodingException e) {

            }
        }
        if (text != null && text.length() != 0) {
            try {
                result.append("&").append("text=").append(URLEncoder.encode(text, "UTF-8"));
            } catch (UnsupportedEncodingException e) {

            }
        }
        if (icon != null && icon.length() != 0) {
            result.append("&").append("icon=").append(icon);
        }
        if (url != null && url.length() != 0) {
            result.append("&").append("url=").append(url);
        }
        if (hidden != 0) {
            result.append("&").append("hidden=").append(hidden);
        }
        if (encode != null && encode.length() != 0) {
            result.append("&").append("encode=").append(encode);
        }
        if (priority != 0) {
            result.append("&").append("priority=").append(priority);
        }

        return result.toString();
    }
}