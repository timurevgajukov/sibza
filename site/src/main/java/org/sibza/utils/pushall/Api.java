package org.sibza.utils.pushall;

import org.sibza.utils.Net;
import org.sibza.utils.pushall.request.PushRequest;

/**
 * Created by Тимур on 18.01.2016.
 *
 * Общее api для работы с сервисом
 */
public class Api {

    private static final String API_URL = "https://pushall.ru/api.php";

    // данные для отправки себе уведомлений
    public static final int SELF_ID = 17691;
    public static final String SELF_KEY = "cfd54a3fbf41a8a87f5a05da493e2494";

    // Канал: "Си бзэ: админ"
    public static final int CHANNEL_SIBZA_ADMIN_ID = 1043;
    public static final String CHANNEL_SIBZA_ADMIN_KEY = "c067ae541985c66e725a489f082acead";

    // Канал: "Си бзэ"
    public static final int CHANNEL_SIBZA_ID = 1044;
    public static final String CHANNEL_SIBZA_KEY = "3c850b0050fca00c82b9f430fd2da497";

    public static final String SITE_URL = "https://sibza.org";
    public static final String ICON_URL =  SITE_URL + "/resources/img/logo-150x150.png";

    /**
     * Отправка уведомления
     *
     * @param request
     */
    public static String push(PushRequest request) {

        return Net.post(API_URL, request.toString());
    }

    public static void main(String[] args) {

        String result = Api.push(new PushRequest(PushRequest.TYPE_SHOWLIST, CHANNEL_SIBZA_ID, CHANNEL_SIBZA_KEY));
        System.out.println(result);
    }
}
