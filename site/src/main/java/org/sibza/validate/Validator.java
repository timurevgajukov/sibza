package org.sibza.validate;

import org.sibza.model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Тимур on 18.12.2015.
 *
 * Класс для валидации данных
 */
public class Validator {

    private Model model; // объект валидации
    private List<IValidateRule> rules; // правила валидации

    private boolean error;
    private List<String> errorMessages;

    {
        error = false;
        errorMessages = new ArrayList<String>();
    }

    public Validator(Model model, List<IValidateRule> rules) {

        this.model = model;
        this.rules = rules;
    }

    /**
     * Валидация данных по всем правилам
     *
     * @return
     */
    public boolean validate() {

        if (rules == null || rules.size() == 0) {
            return true;
        }

        int errorCount = 0;
        for (IValidateRule rule : rules) {
            if (!rule.validate(model)) {
                errorCount++;
                errorMessages.add(rule.getMessage());
            }
        }

        error = errorCount != 0;

        return !error;
    }

    public boolean isError() {

        return error;
    }

    public List<String> getErrorMessages() {

        return errorMessages;
    }
}