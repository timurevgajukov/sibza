package org.sibza.validate;

import org.sibza.model.Model;

/**
 * Created by Тимур on 18.12.2015.
 *
 * Абстрактный класс правила валидации
 */
public abstract class AValidateRule implements IValidateRule {

    private String message;

    public AValidateRule(String message) {

        this.message = message;
    }

    @Override
    public abstract boolean validate(Model model);

    @Override
    public String getMessage() {

        return message;
    }

    @Override
    public void setMessage(String message) {

        this.message = message;
    }
}