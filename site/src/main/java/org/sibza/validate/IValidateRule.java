package org.sibza.validate;

import org.sibza.model.Model;

/**
 * Created by Тимур on 18.12.2015.
 *
 * Интерфейс правила валидации
 */
public interface IValidateRule {

    /**
     * Проверка модели по правилу
     *
     * @param model
     * @return
     */
    boolean validate(Model model);

    /**
     * Возвращает сообщение, которое нужно отобразить, если не прошли валидацию
     *
     * @return
     */
    String getMessage();

    /**
     * Устанавливает сообщение, которое нужно отобразить, если не прошли валидацию
     *
     * @param message
     */
    void setMessage(String message);
}