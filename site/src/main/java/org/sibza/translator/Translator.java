package org.sibza.translator;

import org.sibza.cache.SiteTexts;
import org.sibza.model.language.Language;
import org.sibza.model.site.SiteText;
import org.sibza.model.site.dao.SiteTextDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Security;
import org.sibza.utils.yandex.translate.api.TranslateApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Тимур on 02.10.2015.
 *
 * Класс переводчик для отображения интерфейса сайта на разных языках
 */
@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Translator implements Serializable {

    @Autowired
    protected Security security;

    // на каком языке указываются ключи для перевода
    private String DEFAULT_KEY_LNG = Language.RU;

    // кэш переводов для интерфейса
    private HashMap<Language, HashMap<String, String>> cache;

    {
        cache = new HashMap<Language, HashMap<String, String>>();
    }

    public String g(String text) {

        if (security == null || security.getLanguage() == null) {
            return text;
        }

        if (DEFAULT_KEY_LNG.equalsIgnoreCase(security.getLanguage().getCode())) {
            // текущий язык сайта совпадает с языком ключей и можно просто их вернуть
            return text;
        }

        // смотрим в БД нужные нам значения
        SiteTexts siteTexts = SiteTexts.getInstance(security.getLanguage());
        SiteText siteText = siteTexts.get(text);
        if (siteText != null) {
            return siteText.getValue();
        }

        // в нашей БД пока нет такого значения, ищем дальше
        HashMap<String, String> languageCache = cache.get(security.getLanguage());
        if (languageCache == null) {
            cache.put(security.getLanguage(), new HashMap<String, String>());
            languageCache = cache.get(security.getLanguage());
        }

        // в начале ищем к кэше
        if (languageCache.get(text) != null) {
            // нашли, возвращаем его, предварительно сохранив в БД
            new SiteTextDAOImpl().save(new SiteText(text, languageCache.get(text), security.getLanguage()));
            siteTexts.clear();
            return languageCache.get(text);
        }

        // попытыемся найти перевод на нужный нам язык в словаре
        Word word = null;
        List<Word> words = new WordDAOImpl().search(text);
        if (words != null && words.size() != 0) {
            for (Word item : words) {
                if (text.equalsIgnoreCase(item.getWord())) {
                    word = item;
                }
            }
        }

        if (word != null) {
            Word translate = word.getTranslate(security.getLanguage());
            if (translate != null) {
                // нашли нужный нам текст
                // добавляем в кэш
                languageCache.put(text, translate.getWord());

                // добавляем в тексты для интерфейса
                new SiteTextDAOImpl().save(new SiteText(text, translate.getWord(), security.getLanguage()));
                siteTexts.clear();

                // возвращаем перевод
                return translate.getWord();
            }
        }

        // попытаемся перевести через переводчик
        System.out.println("Не нашли слово '" + text + "' у себя в базе и приходится использовать переводчик");
        TranslateApi api = TranslateApi.getInstance();
        String translate = api.translate(text, security.getLanguage().getCode());
        if (translate != null && translate.length() != 0) {
            // получили перевод из переводчика
            // добавляем в кэш
            languageCache.put(text, translate);

            // добавляем в тексты для интерфейса
            new SiteTextDAOImpl().save(new SiteText(text, translate, security.getLanguage()));
            siteTexts.clear();

            // возвращаем перевод
            return translate;
        }

        // не нашли перевод и возвращаем сам текст
        return text;
    }
}