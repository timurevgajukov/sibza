package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.TypeDAOImpl;

import java.util.List;

/**
 * Created by timur on 10.05.15.
 *
 * Контейнер для хранения списка типов ресурсов
 */
public class ResourceTypes implements ICache<Type> {

    private static ResourceTypes instance;

    private List<Type> list;

    public static ResourceTypes getInstance() {

        if (instance == null) {
            instance = new ResourceTypes();
        }

        return instance;
    }

    private ResourceTypes() {

        list = new TypeDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<Type> list() {

        return list;
    }

    @Override
    public Type get(int id) {

        if (list == null) {
            return null;
        }

        for (Type type : list) {
            if (type.getId() == id) {
                return type;
            }
        }

        return null;
    }
}
