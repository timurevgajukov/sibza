package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.user.SocialNetworkType;
import org.sibza.model.user.dao.SocialNetworkTypeDAOImpl;

import java.util.List;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Контейнер для хранения списка поддерживаемых типов социальных сетей
 */
public class SocialNetworkTypes implements ICache<SocialNetworkType> {

    private static SocialNetworkTypes instance = null;

    private List<SocialNetworkType> list;

    public static SocialNetworkTypes getInstance() {

        if (instance == null) {
            instance = new SocialNetworkTypes();
        }

        return instance;
    }

    private SocialNetworkTypes() {

        list = new SocialNetworkTypeDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<SocialNetworkType> list() {

        return list;
    }

    @Override
    public SocialNetworkType get(int id) {

        if (list == null) {
            return null;
        }

        for (SocialNetworkType item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}