package org.sibza.cache.interfaces;

import org.sibza.model.Model;

import java.util.List;

/**
 * Created by Тимур on 02.10.2015.
 *
 * Интерфейс для работы с кэшем
 */
public interface ICache<T> {

    /**
     * Очищает кэш для перезагрузки
     */
    void clear();

    /**
     * Возвращает список закешированных данных
     *
     * @return
     */
    List<T> list();

    /**
     * Возвращает элемент по его порядковому номеру
     *
     * @param id
     * @return
     */
    T get(int id);
}