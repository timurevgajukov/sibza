package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.alphabet.dao.LetterDAOImpl;
import org.sibza.model.language.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Тимур on 20.11.2015.
 *
 * Контейнер для хранения алфавита
 */
public class Letters implements ICache<Letter> {

    private static Letters instance = null;

    private Language language;
    private List<Letter> list;

    public static Letters getInstance(Language language) {

        if (instance == null) {
            instance = new Letters(language);
        }

        if (instance.getLanguage().getId() != language.getId()) {
            instance.clear();
            instance = new Letters(language);
        }

        return instance;
    }

    private Letters(Language language) {

        this.language = language;
        list = new LetterDAOImpl().list(language);
    }

    public Language getLanguage() {

        return language;
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<Letter> list() {

        return list;
    }

    @Override
    public Letter get(int id) {

        if (list == null) {
            return null;
        }

        for (Letter letter : list) {
            if (letter.getId() == id) {
                return letter;
            }
        }

        return null;
    }

    /**
     * Возвращает список похожих букв без самой буквы
     *
     * @param letter
     * @return
     */
    public List<Letter> filter(Letter letter) {

        if (list == null) {
            return null;
        }

        List<Letter> result = null;
        for (Letter item : list) {
            if (item.getId() != letter.getId() && item.getLetter().indexOf(letter.getLetter()) == 0) {
                if (result == null) {
                    result = new ArrayList<Letter>();
                }
                result.add(item);
            }
        }

        return result;
    }
}