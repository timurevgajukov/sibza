package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.LanguageDAOImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 09.05.15.
 *
 * Контейнер для хранения списка поддерживаемых языков
 */
public class Languages implements ICache<Language> {

    private static Languages instance = null;

    private List<Language> list;

    public static Languages getInstance() {

        if (instance == null) {
            instance = new Languages();
        }

        return instance;
    }

    private Languages() {

        list = new LanguageDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<Language> list() {

        return list;
    }

    @Override
    public Language get(int id) {

        if (list == null) {
            return null;
        }

        for (Language language : list) {
            if (language.getId() == id) {
                return language;
            }
        }

        return null;
    }

    public Language get(String code) {

        if (list == null) {
            return null;
        }

        for (Language language : list) {
            if (code.equalsIgnoreCase(language.getCode())) {
                return language;
            }
        }

        return null;
    }

    /**
     * Возвращает список языков для изучения
     *
     * @return
     */
    public List<Language> listLearn() {

        List<Language> list = new ArrayList<Language>();

        if (this.list != null) {
            for (Language language : this.list) {
                if (language.isLearn()) {
                    list.add(language);
                }
            }
        }

        return list;
    }

    /**
     * Возвращает список языков интерфейса
     *
     * @return
     */
    public List<Language> listInterface() {

        List<Language> list = new ArrayList<Language>();

        if (this.list != null) {
            for (Language language : this.list) {
                if (language.isSiteInterface()) {
                    list.add(language);
                }
            }
        }

        return list;
    }
}