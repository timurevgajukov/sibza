package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;

import java.util.List;

public class Resources implements ICache<Resource> {

    List<Resource> list;

    public Resources() {

        this.list = new ResourceDAOImpl().list(true);
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
    }

    @Override
    public List<Resource> list() {

        return list;
    }

    @Override
    public Resource get(int id) {

        if (list == null) {
            return null;
        }

        for (Resource resource : list) {
            if (resource.getId() == id) {
                return resource;
            }
        }

        return null;
    }
}