package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.dsl.DSLStatus;
import org.sibza.model.dsl.dao.DSLStatusDAOImpl;

import java.util.List;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Контейнер для хранения списка статусов обработки dsl-словаря
 */
public class DSLDictionaryStatuses implements ICache<DSLStatus> {

    private static DSLDictionaryStatuses instance = null;

    private List<DSLStatus> list;

    public static DSLDictionaryStatuses getInstance() {

        if (instance == null) {
            instance = new DSLDictionaryStatuses();
        }

        return instance;
    }

    private DSLDictionaryStatuses() {

        list = new DSLStatusDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<DSLStatus> list() {

        return list;
    }

    @Override
    public DSLStatus get(int id) {

        if (list == null) {
            return null;
        }

        for (DSLStatus status : list) {
            if (status.getId() == id) {
                return status;
            }
        }

        return null;
    }
}