package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.language.Language;
import org.sibza.model.site.SiteText;
import org.sibza.model.site.dao.SiteTextDAOImpl;

import java.util.List;

/**
 * Created by Тимур on 19.11.2015.
 *
 * Контейнер для хранения текстов для интерфейса сайта
 */
public class SiteTexts implements ICache<SiteText> {

    private static SiteTexts instance = null;

    private Language language;
    private List<SiteText> list;

    public static SiteTexts getInstance(Language language) {

        if (instance == null) {
            instance = new SiteTexts(language);
        }

        if (instance.getLanguage().getId() != language.getId()) {
            instance.clear();
            instance = new SiteTexts(language);
        }

        return instance;
    }

    private SiteTexts(Language language) {

        this.language = language;
        list = new SiteTextDAOImpl().list(language);
    }

    public Language getLanguage() {

        return language;
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<SiteText> list() {

        return list;
    }

    @Override
    public SiteText get(int id) {

        if (list == null) {
            return null;
        }

        for (SiteText text : list) {
            if (text.getId() == id) {
                return text;
            }
        }

        return null;
    }

    public SiteText get(String key) {

        if (list == null) {
            return null;
        }

        for (SiteText text : list) {
            if (key.equalsIgnoreCase(text.getKey())) {
                return text;
            }
        }

        return null;
    }
}