package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.book.BookType;
import org.sibza.model.book.dao.BookTypeDAOImpl;

import java.util.List;

/**
 * Created by Тимур on 11.05.2016.
 *
 * Контейнер для хранения списка типов книг
 */
public class BookTypes implements ICache<BookType> {

    private static BookTypes instance = null;

    private List<BookType> list;

    public static BookTypes getInstance() {

        if (instance == null) {
            instance = new BookTypes();
        }

        return instance;
    }

    private BookTypes() {

        list = new BookTypeDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<BookType> list() {

        return list;
    }

    @Override
    public BookType get(int id) {

        if (list == null) {
            return null;
        }

        for (BookType item : list) {
            if (id == item.getId()) {
                return item;
            }
        }

        return null;
    }
}