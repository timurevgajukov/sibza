package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.resource.ContentType;
import org.sibza.model.resource.dao.ContentTypeDAOImpl;

import java.util.List;

/**
 * Created by timur on 16.05.15.
 *
 * Контейнер для хранения списка типов контента
 */
public class ContentTypes implements ICache<ContentType> {

    private static ContentTypes instance = null;

    private List<ContentType> list;

    public static ContentTypes getInstance() {

        if (instance == null) {
            instance = new ContentTypes();
        }

        return instance;
    }

    public ContentTypes() {

        list = new ContentTypeDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<ContentType> list() {

        return list;
    }

    @Override
    public ContentType get(int id) {

        if (list == null) {
            return null;
        }

        for (ContentType contentType : list) {
            if (contentType.getId() == id) {
                return contentType;
            }
        }

        return null;
    }

    public ContentType get(String mimeType) {

        if (list == null) {
            return null;
        }

        for (ContentType contentType : list) {
            if (contentType.getContentType().equalsIgnoreCase(mimeType)) {
                return contentType;
            }
        }

        return null;
    }
}
