package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.avatar.DefaultAvatar;
import org.sibza.model.avatar.dao.DefaultAvatarDAOImpl;

import java.util.List;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Контейнер для хранения списка стандартных аватарок
 */
public class DefaultAvatars implements ICache<DefaultAvatar> {

    private static DefaultAvatars instance;

    private List<DefaultAvatar> list;

    public static DefaultAvatars getInstance() {

        if (instance == null) {
            instance = new DefaultAvatars();
        }

        return instance;
    }

    private DefaultAvatars() {

        list = new DefaultAvatarDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<DefaultAvatar> list() {

        return list;
    }

    @Override
    public DefaultAvatar get(int id) {

        if (list == null) {
            return null;
        }

        for (DefaultAvatar item : list) {
            if (item.getId() == id) {
                return item;
            }
        }

        return null;
    }
}