package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.user.Role;
import org.sibza.model.user.dao.RoleDAOImpl;

import java.util.List;

/**
 * Created by timur on 09.05.15.
 *
 * Контейнер для хранения списка ролей пользователей
 */
public class Roles implements ICache<Role> {

    private static Roles instance = null;

    private List<Role> list;

    public static Roles getInstance() {

        if (instance == null) {
            instance = new Roles();
        }

        return instance;
    }

    private Roles() {

        list = new RoleDAOImpl().list();
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<Role> list() {

        return list;
    }

    @Override
    public Role get(int id) {

        if (list == null) {
            return null;
        }

        for (Role role : list) {
            if (role.getId() == id) {
                return role;
            }
        }

        return null;
    }
}
