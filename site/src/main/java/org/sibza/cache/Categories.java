package org.sibza.cache;

import org.sibza.cache.interfaces.ICache;
import org.sibza.model.category.Category;
import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.language.Language;

import java.util.List;

/**
 * Created by timur on 13.05.15.
 *
 * Контейнер для хранения списка категорий
 */
public class Categories implements ICache<Category> {

    private static Categories instance = null;

    private Language language;
    private List<Category> list;

    public static Categories getInstance(Category parent, Language language) {

        if (instance == null) {
            instance = new Categories(parent, language);
        } else if (instance.getLanguage().getId() != language.getId()) {
            instance.clear();
            instance = new Categories(parent, language);
        }

        return instance;
    }

    private Categories(Category parent, Language language) {

        this.language = language;
        list = new CategoryDAOImpl().list(parent, language);
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    @Override
    public void clear() {

        list.clear();
        list = null;
        instance = null;
    }

    @Override
    public List<Category> list() {

        return list;
    }

    @Override
    public Category get(int id) {

        if (list == null) {
            return null;
        }

        for (Category category : list) {
            if (category.getId() == id) {
                return category;
            }
        }

        return null;
    }
}
