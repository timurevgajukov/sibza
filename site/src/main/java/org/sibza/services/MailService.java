package org.sibza.services;

import org.apache.commons.io.IOUtils;
import org.sibza.model.user.User;
import org.sibza.translator.Translator;
import org.sibza.utils.Email;

import javax.servlet.ServletContext;
import java.io.FileInputStream;

/**
 * Created by Тимур on 10.02.2016.
 *
 * Сервис для отправки сообщений по почте
 */
public class MailService {

    private static final String BASE_URL = "https://sibza.org";

    private static final String DEFAULT_EMAIL = "info@sibza.org";
    private static final String DEFAULT_EMAIL_PWD = "Corvus2128506";

    private static final String ADMIN_EMAIL = "evgajukov@gmail.com";

    private Translator t;

    {
        t = new Translator();
    }

    private String template = "##CONTENT##";
    private String resourcePath;

    public MailService(ServletContext servletContext) {

        this.resourcePath = servletContext.getRealPath("/resources");

        // загрузим шаблон писем
        try {
            FileInputStream fis = new FileInputStream(servletContext.getRealPath("/mail-template/mail.html"));
            template = IOUtils.toString(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendConfirmEmail(User user) {

        String confirmEmailUrl = BASE_URL + "/security/email/confirm?email=" + user.getEmail()
                + "&code=" + user.getConfirmCode();
        Email emailSender = new Email(DEFAULT_EMAIL, DEFAULT_EMAIL_PWD);
        emailSender.send(
                t.g("Верификация"),
                t.g("Для верификации вашего почтового ящика перейдите по ссылке") +
                        String.format("<a href='%s'>%s</a>", confirmEmailUrl, confirmEmailUrl),
                user.getEmail());
    }

    public void sendRecoverEmail(User user) {

        String recoverUrl = BASE_URL + "/security/recover?email=" + user.getEmail()
                + "&code=" + user.getConfirmCode();
        Email emailSender = new Email(DEFAULT_EMAIL, DEFAULT_EMAIL_PWD);
        emailSender.send(
                t.g("Восстановление пароля"),
                t.g("Для восстановления пароля перейдите по ссылке " +
                        String.format("<a href='%s'>%s</a>", recoverUrl, recoverUrl) + " и введите новый."),
                user.getEmail());
    }

    public void sendMessage(String name, String email, String message) {

        String text = String.format("От: %s<br />Почта: %s<br /><br />Сообщение:<br />%s", name, email, message);

        Email emailSender = new Email(DEFAULT_EMAIL, DEFAULT_EMAIL_PWD, template, resourcePath);
        emailSender.send("Сообщение с сайта", text, String.format("%s,%s", DEFAULT_EMAIL, ADMIN_EMAIL));
    }
}
