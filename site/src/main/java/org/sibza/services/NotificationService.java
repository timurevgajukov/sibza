package org.sibza.services;

import org.sibza.model.notification.Notification;
import org.sibza.model.notification.dao.NotificationDAOImpl;
import org.sibza.security.Message;
import org.sibza.utils.pushall.Api;
import org.sibza.utils.pushall.request.PushRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by timur on 12.01.16.
 *
 * Сервис для работы с нотификациями на сайте
 */
public class NotificationService {

    private SimpMessagingTemplate template;

    public NotificationService(SimpMessagingTemplate template) {

        this.template = template;
    }

    public boolean send(Notification notification) {

        // сохраняем нотификацию в БД для дальнейшего показа через ajax
        // boolean result = new NotificationDAOImpl().save(notification);
        boolean result = true;

        // одновременно отправляем нотификацию через push на браузер
        String title = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date());
        String body = notification.getMessage().getBody();
        PushRequest request = new PushRequest(PushRequest.TYPE_SELF, Api.SELF_ID, Api.SELF_KEY, body)
                .setTitle(title).setUrl(Api.SITE_URL).setIcon(Api.ICON_URL);
        Api.push(request);

        if (template != null) {
            // отправляем нотификацию через websocket админам
            Message message = notification.getMessage();
            if (message.getTitle() == null) {
                message.setTitle(title);
            }
            template.convertAndSend("/info/admin", notification.getMessage());
        }

        return result;
    }
}