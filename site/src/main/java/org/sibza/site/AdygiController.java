package org.sibza.site;

import org.sibza.model.population.PopulationItem;
import org.sibza.model.population.dao.PopulationDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Тимур on 09.12.2015.
 * <p/>
 * Контроллер для переписи адыгов в Мире
 */
@Controller
public class AdygiController extends PanelBaseController {

    private static final String ACTIVE = "adygi";

    @RequestMapping(value = "/adygi", method = RequestMethod.GET)
    public String indexGetAction(ModelMap model) {

        setModelMap(model, t.g("Перепись адыгов в Мире"), ACTIVE);
        model.addAttribute("count", getPopulationCount());

        return security.getTemplate() + "adygi/index";
    }

    @RequestMapping(value = "/adygi", method = RequestMethod.POST)
    public String indexPostAction() {

        return "redirect:/adygi";
    }

    @RequestMapping("/adygi/map/data")
    public @ResponseBody List<PopulationItem> mapDataAjax() {

        return new PopulationDAOImpl().list();
    }

    private int getPopulationCount() {

        int count = 0;
        for (PopulationItem item : new PopulationDAOImpl().list()) {
            count += item.getValue();
        }

        return count;
    }
}