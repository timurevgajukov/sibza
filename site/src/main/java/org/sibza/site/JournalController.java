package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.book.Book;
import org.sibza.model.book.BookType;
import org.sibza.model.book.dao.BookDAOImpl;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Тимур on 11.05.2016.
 *
 * Контроллер для работы с журналами
 */
@Controller
public class JournalController extends PanelBaseController {

    private static final String ACTIVE = "journals";

    @RequestMapping("/journals")
    public String listAction(ModelMap model) {

        setModelMap(model, t.g("Журнал Нур"), ACTIVE);
        model.addAttribute("journals", new BookDAOImpl().list(0, Integer.MAX_VALUE, new BookType(BookType.TYPE_JOURNAL)));

        return security.getTemplate() + "journal/list";
    }

    @RequestMapping("/journals/{imgId}/{imgFileName}")
    public @ResponseBody byte[] imageAjax(@PathVariable int imgId, @PathVariable String imgFileName) {

        Resource resource = new ResourceDAOImpl().get(imgId);

        if (resource.getFileName().length() > 0) {
            return resource.getResource();
        }

        return null;
    }

    @RequestMapping(value = "/journals/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        setModelMap(model, t.g("Добавление журнала"), ACTIVE);
        model.addAttribute("journals", new BookDAOImpl().list(0, Integer.MAX_VALUE, new BookType(BookType.TYPE_JOURNAL)));
        model.addAttribute("languages", languages.list());
        model.addAttribute("images", new ResourceDAOImpl().list(Type.IMAGE, true));

        return security.getTemplate() + "journal/edit";
    }

    @RequestMapping(value = "/journals/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(@PathVariable int id, ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Book journal = new BookDAOImpl().get(id);

        Languages languages = Languages.getInstance();

        setModelMap(model, t.g("Редактирование журнала"), ACTIVE);
        model.addAttribute("journal", journal);
        model.addAttribute("journals", new BookDAOImpl().list(0, Integer.MAX_VALUE, new BookType(BookType.TYPE_JOURNAL)));
        model.addAttribute("languages", languages.list());
        model.addAttribute("images", new ResourceDAOImpl().list(Type.IMAGE, true));

        return security.getTemplate() + "journal/edit";
    }
}