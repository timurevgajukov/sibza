package org.sibza.site.schedule;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import org.sibza.cache.Languages;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.language.Language;
import org.sibza.model.proverb.dao.ProverbDAOImpl;
import org.sibza.security.Security;
import org.sibza.utils.pushall.Api;
import org.sibza.utils.pushall.request.PushRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by timur on 30.05.15.
 *
 * Обработка задач по расписанию
 */
@Controller
public class Schedule {

    private List<String> ipFilters;

    {
        ipFilters = new ArrayList<String>();
        ipFilters.add("192.168.0.221");
    }

    @Autowired
    protected Security security;

    @Autowired
    protected SimpMessagingTemplate template;

    // на сервере 10 часов соответствует Московскому 13 часам
    // ежедневно в 10, 14 и 16 часов: 0 0 10,14,16 * * ?
    private static final String CRON_DELAY_PROVERBS = "0 0 10 * * ?";
    private static final String CRON_DELAY_DATE = "0 0 6 * * ?";

    /**
     * Рассылка пословиц
     */
    @Scheduled(cron = Schedule.CRON_DELAY_PROVERBS)
    public void pushAllProverbsTask() {

        if (checkFilter()) {
            return;
        }

        Languages languages = Languages.getInstance();

        List<Proverb> proverbs = new ProverbDAOImpl().list(languages.get(Language.AD), 0, 1000000);
        if (proverbs == null || proverbs.size() == 0) {
            System.out.println("Cannot load proverbs list. Finish task");
            return;
        }

        // случайный индекс пословицы
        int index = (int) Math.round((proverbs.size() - 1) * Math.random());

        PushRequest request = new PushRequest(PushRequest.TYPE_BROADCAST, Api.CHANNEL_SIBZA_ID, Api.CHANNEL_SIBZA_KEY, proverbs.get(index).getBody())
                .setTitle("Псалъэжь").setUrl(Api.SITE_URL + "/proverbs").setIcon(Api.ICON_URL);
        Api.push(request);
    }

    /**
     * Утренняя рассылка даты и пожелания с добрым утром
     */
    @Scheduled(cron = Schedule.CRON_DELAY_DATE)
    public void pushAllDateInfoTask() {

        if (checkFilter()) {
            return;
        }

        PushRequest request = new PushRequest(PushRequest.TYPE_BROADCAST, Api.CHANNEL_SIBZA_ID, Api.CHANNEL_SIBZA_KEY, getAdygaDate(new Date()))
                .setTitle("Фи пщэдджыжь фIыуэ!").setUrl(Api.SITE_URL).setIcon(Api.ICON_URL);
        Api.push(request);
    }

    /**
     * Возвращает строку с датой на адыгском языке
     *
     * @param date
     * @return
     */
    private String getAdygaDate(Date date) {

        String[] daysOfWeek = {
                "", "тхьэмахуэщ", "блыщхьэщ", "гъубжщ", "бэрэжьейщ", "махуэкущ", "мэремщ", "щэбэтщ"
        };

        String[] daysOfMonth = {
                "0", "япэ", "етIуанэ", "ещанэ", "еплIанэ", "етхуанэ",
                "еханэ", "ебланэ", "еянэ", "ебгъуанэ", "епщIанэ",
                "епщыкIузанэ", "епщыкIутIанэ", "13", "14", "15",
                "16", "17", "18", "19", "етIощIанэ",
                "тIощIрэ езанэ", "тIощIрэ етIуанэ", "тIощIрэ ещанэ", "тIощIрэ еплIанэ", "тIощIрэ етхуанэ",
                "тIощIрэ еханэ", "тIощIрэ ебланэ", "тIощIрэ еянэ", "тIощIрэ ебгъуанэ", "ещэщIанэ",
                "щэщI езанэ"
        };

        String[] months = {
                "щIымахуэку мазэм", "щIымахуэкIэ мазэм", "гъатхэпэ мазэм",
                "гъатхэку мазэм", "гъатхэкIэ мазэм", "",
                "", "", "",
                "", "", "дыгъэгъазэ мазэм"
        };

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayOfWeekInd = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonthInd = calendar.get(Calendar.DAY_OF_MONTH);
        int monthInd = calendar.get(Calendar.MONTH);

        return String.format("Нобэ %s, %s и %s махуэщ",
                daysOfWeek[dayOfWeekInd],
                months[monthInd],
                daysOfMonth[dayOfMonthInd]);
    }

    /**
     * Сверяет ip сервера со списком адресов, с которых рассылка не должна срабатывать
     *
     * @return
     */
    private boolean checkFilter() {

        try {
            String ip = InetAddress.getLocalHost().getHostAddress();
            for (String filter : ipFilters) {
                if (filter.equals(ip)) {
                    return true;
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void main(String[] args) {

        ApnsService service = APNS.newService().withCert("/Users/timur/Downloads/sibza_push.p12", "1").withSandboxDestination().build();

        String payload = APNS.newPayload().alertBody("Тестовое сообщение от sibza.org 2").build();
        String token = "e4cf00b9964f769f0ad3a8bbac9bc0dc57c34de4c0771dd305eb719fd04c6480";
        service.push(token, payload);

        Map<String, Date> inactiveDevices = service.getInactiveDevices();
        for (String deviceToken : inactiveDevices.keySet()) {
            Date inactiveAsOf = inactiveDevices.get(deviceToken);
        }
    }
}