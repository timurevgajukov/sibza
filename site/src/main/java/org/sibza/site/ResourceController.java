package org.sibza.site;

import org.sibza.cache.ContentTypes;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.resource.dao.TypeDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 15.05.15.
 *
 * Контроллер для работы с медиафайлами
 */
@Controller
@SessionAttributes("files")
public class ResourceController extends PanelBaseController {

    private static final String ACTIVE = "resources";

    @ModelAttribute("files")
    public List<Resource> createFilesCache() {

        return new ArrayList<Resource>();
    }

    @RequestMapping("/resources")
    public String listAction(@ModelAttribute("files") List<Resource> files, ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        // мы могли сюда вернуться из раздела добавления медиафайлов
        // необходимо удалить все временные файлы из кэша
        clearFilesCache(files);

        setModelMap(model, t.g("Медиа ресурсы"), ACTIVE);
        model.addAttribute("types", new TypeDAOImpl().list());
        model.addAttribute("resources", new ResourceDAOImpl().list(true));

        return security.getTemplate() + "resource/list";
    }

    @RequestMapping(value = "/resources/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        setModelMap(model, t.g("Добавление медиа ресурса"), ACTIVE);
        model.addAttribute("types", new TypeDAOImpl().list());

        return security.getTemplate() + "resource/edit";
    }

    @RequestMapping(value = "/resources/add", method = RequestMethod.POST)
    public String addPostAction(@ModelAttribute("files") List<Resource> files) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        for (Resource resource : files) {
            new ResourceDAOImpl().save(resource, security.getUser());
        }
        
        return "redirect:/resources";
    }

    @RequestMapping("/resources/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        new ResourceDAOImpl().delete(new Resource(id), security.getUser());

        return "redirect:/resources";
    }

    @RequestMapping(value = "/resources/upload", method = RequestMethod.POST)
    public @ResponseBody List<String> filesUploadAction(
            @ModelAttribute("files") List<Resource> files,
            @RequestParam(value = "mediafile") MultipartFile file) {

        System.out.println("Upload file: " + file.getOriginalFilename());
        System.out.println("Upload file MIME-type: " + file.getContentType());

        ContentTypes contentTypes = ContentTypes.getInstance();

        // сохраняем файл в кэш
        try {
            files.add(new Resource(contentTypes.get(file.getContentType()),
                    file.getOriginalFilename(), file.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> result = new ArrayList<String>();
        result.add(file.getName());

        return result;
    }

    @RequestMapping("/upload/{id}/{file}")
    public @ResponseBody byte[] fileDownloadAction(@PathVariable int id, @PathVariable String file) {

        Resource resource = new ResourceDAOImpl().get(id);

        if (resource.getFileName().length() > 0) {
            return resource.getResource();
        }

        return null;
    }

    /**
     * Удалает из кэша все файлы
     *
     * @param files
     */
    private void clearFilesCache(List<Resource> files) {

        // очищаем массив
        files.clear();
    }
}