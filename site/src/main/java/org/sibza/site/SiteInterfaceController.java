package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.language.Language;
import org.sibza.model.site.SiteText;
import org.sibza.model.site.dao.SiteTextDAOImpl;
import org.sibza.security.Message;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by timur on 05.03.16.
 *
 * Контроллер для работы с сообщениями в интерфейсе
 */
@Controller
public class SiteInterfaceController extends PanelBaseController {

    private static final String ACTIVE = "interface";

    @RequestMapping("/interfaces")
    public String listAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы редактировать интерфейс
            return "redirect:/";
        }

        List<String> keys = new SiteTextDAOImpl().keys();
        Map<String, List<SiteText>> siteTextListMap = new HashMap<String, List<SiteText>>();
        for (String key : keys) {
            siteTextListMap.put(key, new SiteTextDAOImpl().list(key));
        }

        setModelMap(model, t.g("Интерфейс"), ACTIVE);
        model.addAttribute("keys", keys);
        model.addAttribute("list", siteTextListMap);

        return security.getTemplate() + "interface/list";
    }

    @RequestMapping(value = "/interfaces/edit", method = RequestMethod.GET)
    public String editGetAction(@RequestParam(value = "key") String key, ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы редактировать интерфейс
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        Map<String, SiteText> siteTextMap = new HashMap<String, SiteText>();
        for (SiteText item : new SiteTextDAOImpl().list(key)) {
            siteTextMap.put(item.getLanguage().getCode(), item);
        }

        setModelMap(model, t.g("Редактирование текста интерфейса"), ACTIVE);
        model.addAttribute("key", key);
        model.addAttribute("languages", languages.list());
        model.addAttribute("list", siteTextMap);

        return security.getTemplate() + "interface/edit";
    }

    @RequestMapping(value = "/interfaces/edit", method = RequestMethod.POST)
    public String editPostAction(
            @RequestParam(value = "key") String key,
            @RequestParam(value = "text-lang") List<String> textLangArr,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы редактировать интерфейс
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        int j=0;
        for (int i=0; i<languages.list().size(); i++) {
            Language language = languages.list().get(i);
            if (!language.getCode().equals(Language.AD) && !language.getCode().equals(Language.RU)) {
                SiteText siteText = new SiteTextDAOImpl().get(key, language);
                if (siteText != null) {
                    String text = textLangArr.get(j++);
                    if (text != null && text.length() != 0) {
                        siteText.setValue(text);
                        // теперь можем сохранить
                        new SiteTextDAOImpl().save(siteText);
                    }
                } else {
                    // перевода на этот язык еще не было
                    String text = textLangArr.get(j++);
                    if (text != null && text.length() != 0) {
                        siteText = new SiteText(key, text, language);
                        // теперь можем сохранить
                        new SiteTextDAOImpl().save(siteText);
                    }
                }
            }
        }

        security.addMessage(t.g("Интерфейс успешно обновлен"), Message.TYPE_INFO);

        return "redirect:/interfaces";
    }
}