package org.sibza.site;

import org.sibza.security.Message;
import org.sibza.services.MailService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Контроллер для обработки запросов на страницы о проекте
 */
@Controller
public class AboutController extends PanelBaseController {

    private static final String ACTIVE = "about";

    @RequestMapping("/about")
    public String aboutAction(ModelMap model) {

        setModelMap(model, t.g("О проекте"), ACTIVE);

        return security.getTemplate() + "about";
    }

    @RequestMapping(value = "/about/contact", method = RequestMethod.POST)
    public String contactPostAction(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "email") String email,
            @RequestParam(value = "message") String message) {

        new MailService(servletContext).sendMessage(name, email, message);

        security.addMessage(t.g("Сообщение успешно отправлено!"), Message.TYPE_SUCCESS);

        return "redirect:/about";
    }
}