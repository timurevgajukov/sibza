package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by timur on 27.09.15.
 *
 * Контроллер для отображения словаря
 */
@Controller
public class DictionaryController extends PanelBaseController {

    private static final String ACTIVE = "dictionary";

    private static final int COUNT = 10;

    private int currentPage;

    @RequestMapping("/dictionary")
    public String indexAction(ModelMap model) {

        setModelMap(model, t.g("Словарь") + " / " + t.g("Разговорник"), ACTIVE);
        model.addAttribute("categories", new CategoryDAOImpl().list(null, security.getLanguage()));

        return security.getTemplate() + "dictionary/index";
    }

    @RequestMapping("/dictionary/category/{id}")
    public String categoryAction(@PathVariable int id, ModelMap model) {

        currentPage = 1;

        Category category = new CategoryDAOImpl().get(id, security.getLanguage());
        List<Word> words = new WordDAOImpl().list(security.getLanguage(), category, 0, COUNT);
        for (Word word : words) {
            word.loadTranslates();
        }

        setModelMap(model, t.g("Словарь") + " / " + t.g("Разговорник"), ACTIVE);
        model.addAttribute("category", category);
        model.addAttribute("words", words);
        model.addAttribute("max_count", COUNT);

        return security.getTemplate() + "dictionary/category";
    }

    @RequestMapping("/dictionary/category/{id}/more")
    public String categoryMoreAction(@PathVariable int id, ModelMap model) {

        currentPage++;

        Category category = new CategoryDAOImpl().get(id, security.getLanguage());
        List<Word> words = new WordDAOImpl().list(security.getLanguage(), category, (currentPage - 1) * COUNT, COUNT);
        for (Word word : words) {
            word.loadTranslates();
        }

        setModelMap(model, t.g("Словарь") + " / " + t.g("Разговорник"), ACTIVE);
        model.addAttribute("category", category);
        model.addAttribute("words", words);
        model.addAttribute("max_count", COUNT);

        return security.getTemplate() + "dictionary/category";
    }
}