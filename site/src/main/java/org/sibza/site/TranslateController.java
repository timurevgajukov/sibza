package org.sibza.site;

import org.sibza.model.language.Language;
import org.sibza.cache.Languages;
import org.sibza.model.notification.Notification;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Message;
import org.sibza.services.NotificationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Created by timur on 21.05.15.
 *
 * Контроллер для работы с переводами
 */
@Controller
public class TranslateController extends PanelBaseController {

    private static final String ACTIVE = "dictionary";

    @RequestMapping(value = "/translates/add/{id}", method = RequestMethod.GET)
    public String addGetAction(@PathVariable int id, ModelMap model) {

        Word word = new WordDAOImpl().get(id);
        if (word == null) {
            // не нашли слово, к которому будет присоединяться перевод
            return "redirect:/dictionary";
        }

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы добавлять перевод для слова
            return "redirect:/dictionary/category/" + word.getCategory().getId();
        }

        Languages languages = Languages.getInstance();

        setModelMap(model, t.g("Добавление перевода"), ACTIVE);
        model.addAttribute("lang", languages.get(Language.AD));
        model.addAttribute("word", word);

        return security.getTemplate() + "translate/edit";
    }

    @RequestMapping(value = "/translates/add/{id}", method = RequestMethod.POST)
    public String addPostAction(@PathVariable int id,
                                @RequestParam(value = "word-id", required = false) Integer wordId) {

        Word word1 = new WordDAOImpl().get(id);
        if (word1 == null) {
            // не нашли слово, к которому будет присоединяться перевод
            return "redirect:/dictionary";
        }

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы добавлять перевод для слова
            return "redirect:/dictionary/category/" + word1.getCategory().getId();
        }

        if (wordId == null || wordId == 0) {
            // не указано слово перевод, возвращаемся в нашу форму
            security.addMessage(t.g("Не указано слово перевод"), Message.TYPE_DANGER);
            return "redirect:/translates/add/" + word1.getId();
        }

        // проверяем, имеется ли уже привязка этого слова
        word1.loadTranslates();
        for (Word word : word1.getTranslates()) {
            if (word.getId() == wordId) {
                security.addMessage(t.g("Выбранное слово уже подключено как перевод. Выберите другое слово."),
                        Message.TYPE_WARNING);
                return "redirect:/translates/add/" + word1.getId();
            }
        }

        // ищем второе слово в БД
        Word word2 = new WordDAOImpl().get(wordId);
        if (word2 == null) {
            // не указано слово перевод, возвращаемся в нашу форму
            security.addMessage(t.g("Не найдено слово перевод"), Message.TYPE_DANGER);
            return "redirect:/translates/add/" + word1.getId();
        }

        // теперь можем создавать связку перевода
        new WordDAOImpl().save(word1, word2, null, true, security.getUser());
        if (security.getUser().getId() != Notification.ADMIN_ID) {
            new NotificationService(template).send(
                    Notification.create(
                            "Пользователь " + security.getUser().getEmail() + " добавил новый перевод к слову \""
                                    + word1.getWord(), Message.TYPE_INFO));
        }

        return "redirect:/dictionary/category/" + word1.getCategory().getId();
    }

    @RequestMapping("/translates/delete/request/{id1}/{id2}")
    public String deleteRequestAction(@PathVariable int id1, @PathVariable int id2) {

        Word word1 = new WordDAOImpl().get(id1);
        Word word2 = new WordDAOImpl().get(id2);
        if (word1 == null) {
            // не нашли одно из слово, для которого нужно удалить связку перевода
            return "redirect:/dictionary";
        }
        if (word2 == null) {
            // не нашли одно из слово, для которого нужно удалить связку перевода
            return "redirect:/dictionary/category/" + word1.getCategory().getId();
        }

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы удалять перевод
            return "redirect:/dictionary/category/" + word1.getCategory().getId();
        }

        String msg = t.g("Вы уверены, что хотите удалить связку перевода для слов?") +
                " <a href='/translates/delete/" + id1 + "/" + id2 + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return "redirect:/dictionary/category/" + word1.getCategory().getId();
    }

    @RequestMapping("/translates/delete/{id1}/{id2}")
    public String deleteAction(@PathVariable int id1, @PathVariable int id2) {

        Word word1 = new WordDAOImpl().get(id1);
        Word word2 = new WordDAOImpl().get(id2);
        if (word1 == null) {
            // не нашли одно из слово, для которого нужно удалить связку перевода
            return "redirect:/dictionary";
        }
        if (word2 == null) {
            // не нашли одно из слово, для которого нужно удалить связку перевода
            return "redirect:/dictionary/category/" + word1.getCategory().getId();
        }

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы удалять перевод
            return "redirect:/dictionary/category/" + word1.getCategory().getId();
        }

        new WordDAOImpl().delete(word1, word2, security.getUser());
        if (security.getUser().getId() != Notification.ADMIN_ID) {
            new NotificationService(template).send(
                    Notification.create(
                            "Пользователь " + security.getUser().getEmail() + " удалил перевод к слову \""
                                    + word1.getWord(), Message.TYPE_DANGER));
        }

        return "redirect:/dictionary/category/" + word1.getCategory().getId();
    }
}