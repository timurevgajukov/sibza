package org.sibza.site;

import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.category.dao.TranslateDAOImpl;
import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.category.Translate;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.notification.Notification;
import org.sibza.security.Message;
import org.sibza.services.NotificationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 12.05.15.
 *
 * Контроллер для работы с категориями
 */
@Controller
public class CategoryController extends PanelBaseController {

    private static final String ACTIVE = "dictionary";

    @RequestMapping(value = "/categories/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для добавления новой категории
            return "redirect:/dictionary";
        }

        setModelMap(model, t.g("Добавление категории"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());

        return security.getTemplate() + "category/edit";
    }

    @RequestMapping(value = "/categories/add", method = RequestMethod.POST)
    public String addPostAction(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "name-lang") List<String> nameLangArr,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для добавления новой категории
            return "redirect:/dictionary";
        }

        Category category = new Category();
        category.setName(name);
        if (new CategoryDAOImpl().save(category, security.getUser())) {
            List<Translate> translates = new ArrayList<Translate>();
            Languages languages = Languages.getInstance();
            for (int i = 0; i < languages.list().size(); i++) {
                if (nameLangArr.get(i) != null && nameLangArr.get(i).trim().length() != 0) {
                    Translate translate = new Translate();
                    translate.setName(nameLangArr.get(i).trim());
                    translate.setCategory(category);
                    translate.setLanguage(languages.list().get(i));
                    translates.add(translate);

                    new TranslateDAOImpl().save(translate, security.getUser());
                }
            }

            if (security.getUser().getId() != Notification.ADMIN_ID) {
                new NotificationService(template).send(
                        Notification.create(
                                "Пользователь " + security.getUser().getEmail() + " добавил новую категорию \""
                                        + category.getName(), Message.TYPE_INFO));
            }

            return "redirect:/dictionary";
        }

        // не удалось сохранить корректно, сообщаем об этом и отображаем форму редактирования
        setModelMap(model, t.g("Добавление категории"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("category", category);

        return security.getTemplate() + "category/edit";
    }

    @RequestMapping(value = "/categories/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(ModelMap model, @PathVariable int id) {

        String redirect = "redirect:/dictionary";

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для редактирования категории
            return redirect;
        }

        Category category = new CategoryDAOImpl().get(id, security.getLanguage());

        setModelMap(model, t.g("Редактирование категории"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("category", category);

        return security.getTemplate() + "category/edit";
    }

    @RequestMapping(value = "/categories/edit/{id}", method = RequestMethod.POST)
    public String editPostAction(
            @PathVariable int id,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "name-lang") List<String> nameLangArr,
            ModelMap model) {

        String redirect = "redirect:/dictionary";

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для редактирования категории
            return redirect;
        }

        Category category = new CategoryDAOImpl().get(id, security.getLanguage());
        category.setName(name);

        if (new CategoryDAOImpl().save(category, security.getUser())) {
            new TranslateDAOImpl().delete(category, security.getUser());
            List<Translate> translates = new ArrayList<Translate>();
            Languages languages = Languages.getInstance();
            for (int i = 0; i < languages.list().size(); i++) {
                if (nameLangArr.get(i) != null && nameLangArr.get(i).trim().length() != 0) {
                    Translate translate = new Translate();
                    translate.setName(nameLangArr.get(i).trim());
                    translate.setCategory(category);
                    translate.setLanguage(languages.list().get(i));
                    translates.add(translate);

                    new TranslateDAOImpl().save(translate, security.getUser());
                }
            }

            if (security.getUser().getId() != Notification.ADMIN_ID) {
                new NotificationService(template).send(
                        Notification.create(
                                "Пользователь " + security.getUser().getEmail() + " обновил категорию \""
                                        + category.getName(), Message.TYPE_INFO));
            }

            return redirect + "/category/" + category.getId();
        }

        // не удалось сохранить корректно, сообщаем об этом и отображаем форму редактирования
        setModelMap(model, t.g("Редактирование категории"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("category", category);

        return security.getTemplate() + "category/edit";
    }

    @RequestMapping("/categories/delete/request/{id}")
    public String deleteRequestAction(@PathVariable int id) {

        String redirect = "redirect:/dictionary";

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы удалить директорию
            return redirect;
        }

        Category category = new CategoryDAOImpl().get(id, security.getLanguage());
        if (category == null) {
            // не нашли категорию для удаления
            return redirect;
        }

        String msg = t.g("Вы уверены, что хотите удалить категорию?") +
                " <a href='/categories/delete/" + id + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return redirect + "/category/" + category.getId();
    }

    @RequestMapping("/categories/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        String redirect = "redirect:/dictionary";

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для удаления директории
            return redirect;
        }

        Category category = new CategoryDAOImpl().get(id, security.getLanguage());
        if (category == null) {
            return redirect;
        }

        new CategoryDAOImpl().delete(new Category(id), security.getUser());

        if (security.getUser().getId() != Notification.ADMIN_ID) {
            new NotificationService(template).send(
                    Notification.create(
                            "Пользователь " + security.getUser().getEmail() + " удалил категорию \""
                                    + category.getName(), Message.TYPE_INFO));
        }

        return redirect;
    }
}