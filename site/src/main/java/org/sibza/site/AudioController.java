package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.audio.Audio;
import org.sibza.model.audio.dao.AudioDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.security.Message;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Тимур on 28.09.2015.
 *
 * Контроллер для работы с аудио данными
 */
@Controller
public class AudioController extends PanelBaseController {

    private static final String ACTIVE = "audio";

    @RequestMapping("/audio")
    public String listAction(ModelMap model) {

        setModelMap(model, t.g("Народные песни и сказания"), ACTIVE);
        model.addAttribute("audioList", new AudioDAOImpl().list());

        return security.getTemplate() + "audio/list";
    }

    @RequestMapping(value = "/audio/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        setModelMap(model, t.g("Добавление музыки"), ACTIVE);
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));

        return security.getTemplate() + "audio/edit";
    }

    @RequestMapping(value = "/audio/add", method = RequestMethod.POST)
    public String addPostAction(
            @RequestParam(value = "title") String title,
            @RequestParam(value = "resource", required = false) Integer resourceId) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        // TODO: провалидировать данные
        Audio audio = new Audio();
        audio.setTitle(title);
        audio.setLanguage(languages.get(Language.AD));
        audio.setResource(new Resource(resourceId));
        new AudioDAOImpl().save(audio, security.getUser());

        return "redirect:/audio";
    }

    @RequestMapping(value = "/audio/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(ModelMap model, @PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Audio audio = new AudioDAOImpl().get(id);

        setModelMap(model, t.g("Редактирование музыки"), ACTIVE);
        model.addAttribute("audio", audio);
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));

        return security.getTemplate() + "audio/edit";
    }

    @RequestMapping(value = "/audio/edit/{id}", method = RequestMethod.POST)
    public String editPostAction(
            @PathVariable int id,
            @RequestParam(value = "title") String title,
            @RequestParam(value = "resource", required = false) Integer resourceId) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        // TODO: предварительно провалидируем данные
        Audio audio = new AudioDAOImpl().get(id);
        audio.setTitle(title);
        audio.setResource(new Resource(resourceId));
        new AudioDAOImpl().save(audio, security.getUser());

        return "redirect:/audio";

    }

    @RequestMapping("/audio/delete/request/{id}")
    public String deleteRequestAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        String msg = t.g("Вы уверены, что хотите удалить музыку?") +
                "<a href='/audio/delete/" + id + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return "redirect:/audio";
    }

    @RequestMapping("/audio/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        new AudioDAOImpl().delete(new Audio(id), security.getUser());

        return "redirect:/audio";
    }
}