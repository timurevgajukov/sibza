package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.message.dao.MessageDAOImpl;
import org.sibza.security.Message;
import org.sibza.security.Security;
import org.sibza.services.NotificationService;
import org.sibza.translator.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.ui.ModelMap;

import javax.servlet.ServletContext;

/**
 * Created by timur on 24.09.15.
 * <p/>
 * Базовый класс для обработчиков запросов в панели
 */
public class PanelBaseController {

    @Autowired
    protected Security security;

    @Autowired
    protected Translator t;

    @Autowired
    protected SimpMessagingTemplate template;

    @Autowired
    protected ServletContext servletContext;

    private static final String DESCRIPTION = "Сайт в помощь изучающим адыгский язык и культуру. Содержит адыгский словарь, адыгский разговорник, пословицы, аудио и видео.";
    private static final String KEYWORDS = "адыгский, кабардинский, черкесский, словарь, разговорник, язык, грамматика, слова";

    protected void setModelMap(ModelMap model, String title, String active) {

        setModelMap(model, title, DESCRIPTION, KEYWORDS, active);
    }

    protected void setModelMap(ModelMap model, String title, String description, String keywords, String active) {

        Languages languages = Languages.getInstance();

        model.addAttribute("title", title);
        model.addAttribute("description", description);
        model.addAttribute("keywords", keywords);
        model.addAttribute("active", active);
        model.addAttribute("security", security);
        model.addAttribute("t", t);
        model.addAttribute("interfaceLanguages", languages.listInterface());

        if (!security.isShowMobileAlert()) {
            security.addMessage(t.g("Загрузите наше <a href='/apps'>мобильное приложение</a>"), Message.TYPE_INFO);
            security.setShowMobileAlert(true);
        }

        security.incShowPagesCount();

        if (security.isAuth()) {
            model.addAttribute("messagesCount", new MessageDAOImpl().count(security.getUser()));
        }
    }
}