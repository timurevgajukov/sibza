package org.sibza.site.api.response;

import java.util.HashMap;

/**
 * Created by ����� on 26.10.2015.
 */
public class ResponseCode {

    public static HashMap<Integer, String> CODES;

    static {
        CODES = new HashMap<Integer, String>();
        CODES.put(0, "OK");
        CODES.put(-1, "ERROR");
        CODES.put(-2, "An application with this token not found");
        CODES.put(-3, "An application with this token is blocked");
        CODES.put(-4, "App old. Please update it");
        CODES.put(-5, "Invalid object ID");
    }
}
