package org.sibza.site.api;

import org.sibza.cache.Languages;
import org.sibza.model.alphabet.Alphabet;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.alphabet.dao.AlphabetDAOImpl;
import org.sibza.model.application.Application;
import org.sibza.model.application.dao.ApplicationDAOImpl;
import org.sibza.model.audio.Audio;
import org.sibza.model.audio.dao.AudioDAOImpl;
import org.sibza.model.book.Book;
import org.sibza.model.book.BookType;
import org.sibza.model.book.dao.BookDAOImpl;
import org.sibza.model.category.Category;
import org.sibza.model.category.Translate;
import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.proverb.dao.ProverbDAOImpl;
import org.sibza.model.resource.Resource;
import org.sibza.model.video.Video;
import org.sibza.model.video.dao.VideoDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.site.api.response.Response;
import org.sibza.site.api.response.ResponseCode;
import org.sibza.utils.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Тимур on 22.06.2015.
 *
 * Основной класс для работы с api
 */
@Controller
public class ApiController {

    private static final String API_VER = "1.0";

    @RequestMapping("/api/version")
    public @ResponseBody Response apiVersionApi() {

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(API_VER);

        return response;
    }

    @RequestMapping("/api/application/version/check")
    public @ResponseBody Response appVersionApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "version") String version) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        if (app.getVersion().equalsIgnoreCase(version)) {
            return new Response(0, ResponseCode.CODES.get(0));
        } else {
            return new Response(-3, ResponseCode.CODES.get(-3));
        }
    }

    @RequestMapping("/api/language/list")
    public @ResponseBody Response listLanguageApi(
            @RequestParam(value = "token") String token) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(new LanguageDAOImpl().list());

        return response;
    }

    @RequestMapping("/api/category/list")
    public @ResponseBody Response listCategoryApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "lng", required = false) String lng,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        Languages languages = Languages.getInstance();

        String languageCode = lng;
        if (languageCode == null) {
            // по-умолчанию подставляем русский язык
            languageCode = Language.RU;
        }

        List<Category> list = new CategoryDAOImpl().list(null, languages.get(languageCode));
        if (list != null) {
            for (Category category : list) {
                // по каждой категории добавляем его переводы
                category.getTranslates();

                // очищаем от лишних данных
                if (category.getTranslates() != null) {
                    for (Translate item : category.getTranslates()) {
                        item.setCategory(null);
                    }
                }

                // если не указан язык, то не нужно возвращать количество слов в категории
                if (lng == null) {
                    category.setWordsCount(null);
                }
            }
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(list);

        return response;
    }

    @RequestMapping("/api/word/list")
    public @ResponseBody Response listWordApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "category") int categoryId,
            @RequestParam(value = "lng") String lng,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        if (offset == null) {
            offset = 0;
        }
        if (count == null) {
            count = Integer.MAX_VALUE;
        }

        Languages languages = Languages.getInstance();

        List<Word> list = new WordDAOImpl().list(languages.get(lng), new Category(categoryId), offset, count);
        if (list != null) {
            for (Word word : list) {
                // по каждому слову добавляем переводы
                word.getTranslate(languages.get("AD"));

                // убераем лишние данные
                word.setLanguage(null);
                word.setCategory(null);
                for (Word translate : word.getTranslates()) {
                    translate.setCategory(null);
                    translate.getLanguage().setSmallName(null);
                    translate.getLanguage().setName(null);
                }
            }
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(list);

        return response;
    }

    @RequestMapping("/api/resource/list")
    public @ResponseBody Response listResourceApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "word") int wordId,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        Word word = new WordDAOImpl().get(wordId);
        List<Resource> resources = word.getResources();
        // удаляем все битовые данные, они будет после загружены при необходимости
        if (resources != null && resources.size() != 0) {
            for (Resource resource : resources) {
                resource.setResource((byte[]) null);
            }
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(resources);

        return response;
    }

    @RequestMapping("/api/alphabet/letter/list")
    public @ResponseBody Response getAlphabetApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "lng", required = false) String lng,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        Languages languages = Languages.getInstance();

        if (lng == null) {
            lng = "AD";
        }

        Alphabet alphabet = new AlphabetDAOImpl().get(languages.get(lng));

        if (alphabet != null && alphabet.getLetters() != null) {
            for (Letter letter : alphabet.getLetters()) {
                letter.loadResources();
            }
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(alphabet);

        return response;
    }

    @RequestMapping("/api/audio/list")
    public @ResponseBody Response listAudioApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(new AudioDAOImpl().list());

        return response;
    }

    @RequestMapping("/api/video/list")
    public @ResponseBody Response listVideoApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        if (offset == null) {
            offset = 0;
        }
        if (count == null) {
            count = Integer.MAX_VALUE;
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(new VideoDAOImpl().list(offset, count));

        return response;
    }

    @RequestMapping("/api/proverb/list")
    public @ResponseBody Response listProverbApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "lng", required = false) String lng,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        if (lng == null) {
            lng = Language.AD;
        }

        Languages languages = Languages.getInstance();

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(new ProverbDAOImpl().list(languages.get(lng), offset, count));

        return response;
    }

    @RequestMapping("/api/book/list")
    public @ResponseBody Response listBookApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "count", required = false) Integer count) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        if (offset == null) {
            offset = 0;
        }
        if (count == null) {
            count = Integer.MAX_VALUE;
        }

        List<Book> books = new BookDAOImpl().list(offset, count, new BookType(BookType.TYPE_BOOK));

        // если есть, то добавляем тома и очищаем от лишней информации
        for (Book book : books) {
            book.setImage(null);
            if (book.getUrl() == null || book.getUrl().length() == 0) {
                book.getBooks();
                if (book.getBooks() != null && book.getBooks().size() != 0) {
                    for (Book child : book.getBooks()) {
                        child.setImage(null);
                        child.setParent(null);
                    }
                }
            }
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(books);

        return response;
    }

    @RequestMapping("/api/book/{id}/list")
    public @ResponseBody Response parentListBookApi(
            @PathVariable int id, @RequestParam(value = "token") String token) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        // находим нужную книгу
        Book book = new BookDAOImpl().get(id);
        if (book == null) {
            // не нашли книгу по его идентификатору
            return new Response(-4, ResponseCode.CODES.get(-4));
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));
        response.setBody(new BookDAOImpl().list(book));

        return response;
    }

    @RequestMapping("/api/search")
    public @ResponseBody Response searchApi(
            @RequestParam(value = "token") String token,
            @RequestParam(value = "q") String query) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(token);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        Response response = new Response(0, ResponseCode.CODES.get(0));

        if (query != null && query.trim().length() != 0) {
            String queryNorm = query.trim();

            List<Word> words = new WordDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            for (Word word : words) {
                word.loadTranslates();
                word.clearCategory();
                for (Word translate : word.getTranslates()) {
                    translate.clearCategory();
                }
            }
            List<Proverb> proverbs = new ProverbDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            List<Book> books = new BookDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            List<Audio> audioList = new AudioDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            List<Video> videoList = new VideoDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));

            boolean hasResult = !(Utils.isEmpty(words) && Utils.isEmpty(proverbs) && Utils.isEmpty(books)
                    && Utils.isEmpty(audioList) && Utils.isEmpty(videoList));

            HashMap<String, Object> map = new HashMap<String, Object>();

            map.put("hasResult", hasResult);
            map.put("words", words);
            map.put("proverbs", proverbs);
            map.put("books", books);
            map.put("audioList", audioList);
            map.put("videoList", videoList);

            response.setBody(map);
        }

        return response;
    }
}