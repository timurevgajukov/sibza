package org.sibza.site.api;

import org.sibza.model.language.Language;
import org.sibza.model.notification.Notification;
import org.sibza.model.notification.dao.NotificationDAOImpl;
import org.sibza.security.Message;
import org.sibza.site.PanelBaseController;
import org.sibza.site.api.response.Response;
import org.sibza.utils.google.GeoCoding;
import org.sibza.utils.yandex.translate.api.TranslateApi;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;

/**
 * Created by Тимур on 24.11.2015.
 *
 * Контроллер с общими сервисами
 */
@Controller
public class ServiceController extends PanelBaseController {

    @RequestMapping(value = "/services/geolocation", method = RequestMethod.POST)
    public @ResponseBody String geolocationPostAction(
            @RequestParam(value = "lat") double lat,
            @RequestParam(value = "lng") double lng) {

        GeoCoding geoCoding = new GeoCoding();
        return geoCoding.getLanguageCodeByGeo(lat, lng, Language.EN);
    }

    @RequestMapping(value = "/services/translate", method = RequestMethod.POST)
    public @ResponseBody Response translateService(
            @RequestParam(value = "text") String text,
            @RequestParam(value = "lng") String languageCode) {

        if (text.trim().length() == 0) {
            return new Response(-1, "ERROR");
        }

        TranslateApi api = TranslateApi.getInstance();
        String translate = api.translate(text, languageCode);
        if (translate != null && translate.length() != 0) {
            // получили перевод из переводчика
            // возвращаем перевод
            Response response = new Response(0, "OK");
            response.setBody(translate);
            return response;
        }

        // что-то пошло не так, возвращаем сам текст
        Response response = new Response(-2, "ERROR");
        response.setBody(text);
        return response;
    }

    @Deprecated
    @RequestMapping(value = "/services/notification", method = RequestMethod.POST)
    public @ResponseBody Message notificationService() {

        if (security.getUser() == null) {
            return null;
        }

        Notification notification = new NotificationDAOImpl().get(security.getUser());
        if (notification == null) {
            return null;
        }
        new NotificationDAOImpl().delete(notification);

        Message message = notification.getMessage();
        if (message.getTitle() == null) {
            message.setTitle(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(notification.getCreateDt()));
        }

        return notification.getMessage();
    }
}