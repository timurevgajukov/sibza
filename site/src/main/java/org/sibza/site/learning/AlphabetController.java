package org.sibza.site.learning;

import com.google.gson.Gson;
import org.sibza.cache.Languages;
import org.sibza.model.alphabet.dao.AlphabetDAOImpl;
import org.sibza.model.alphabet.dao.LetterDAOImpl;
import org.sibza.model.alphabet.dao.LetterWordDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.alphabet.Alphabet;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Message;
import org.sibza.site.PanelBaseController;
import org.sibza.utils.redis.Redis;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * Created by timur on 13.05.15.
 *
 * Контроллер для работы с алфавитом
 */
@Controller
public class AlphabetController extends PanelBaseController {

    private static final String ACTIVE = "alphabet";

    @RequestMapping("/alphabet")
    public String listAction(ModelMap model) {

        Languages languages = Languages.getInstance();
        Alphabet alphabet = new AlphabetDAOImpl().get(languages.get(Language.AD));

        setModelMap(model, t.g("Алфавит"), ACTIVE);
        model.addAttribute("alphabet", alphabet);

        return security.getTemplate() + "alphabet/list";
    }

    @RequestMapping(value = "/alphabet/letters/resources/list", produces={"application/json; charset=UTF-8"})
    public @ResponseBody String listResourcesAjax() {

        String redisKey = String.format("letters:resources:%s", Language.AD);

        // в начале проверим сохраненный кэш
        Redis redis = new Redis();
        redis.connect();
        String result = redis.get(redisKey);
        redis.disconnect();

        if (result != null && result.length() != 0) {
            return result;
        }

        // пока еще не сохранили в кэш ответ для запроса
        Languages languages = Languages.getInstance();
        Alphabet alphabet = new AlphabetDAOImpl().get(languages.get(Language.AD));
        for (Letter letter : alphabet.getLetters()) {
            letter.setLanguage(null);
            letter.loadResources();
        }

        Gson gson = new Gson();
        result = gson.toJson(alphabet.getLetters());

        // сохраняем в кэш для дальнейшего использования
        redis.connect();
        redis.set(redisKey, result);
        redis.disconnect();

        return result;
    }

    @RequestMapping(value = "/alphabet/letters/words/list", produces={"application/json; charset=UTF-8"})
    public @ResponseBody String listWordsAjax() {

        String redisKey = String.format("letters:words:%s:%s", Language.AD, security.getLanguage().getCode());

        // в начале проверим сохраненный кэш
        Redis redis = new Redis();
        redis.connect();
        String result = redis.get(redisKey);
        redis.disconnect();

        if (result != null && result.length() != 0) {
            return result;
        }

        // пока еще не сохранили в кэш ответ для запроса
        Languages languages = Languages.getInstance();
        Alphabet alphabet = new AlphabetDAOImpl().get(languages.get(Language.AD));
        for (Letter letter : alphabet.getLetters()) {
            letter.setLanguage(null);
            letter.loadWords();
            for (Word word : letter.getWords()) {
                word.getResources();
                word.setLanguage(null);
                word.setCategory(null);
                Word translate = word.getTranslate(security.getLanguage(), true);
                if (translate != null) {
                    translate.setLanguage(null);
                    translate.setCategory(null);
                }
            }
        }

        Gson gson = new Gson();
        result = gson.toJson(alphabet.getLetters());

        // сохраняем в кэш для дальнейшего использования
        redis.connect();
        redis.set(redisKey, result);
        redis.disconnect();

        return result;
    }

    @RequestMapping(value = "/alphabet/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы добавлять буквы
            return "redirect:/alphabet";
        }

        Languages languages = Languages.getInstance();

        setModelMap(model, t.g("Добавление буквы"), ACTIVE);
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));
        model.addAttribute("words", new WordDAOImpl().list(languages.get(Language.AD)));

        return security.getTemplate() + "alphabet/edit";
    }

    @RequestMapping(value = "/alphabet/add", method = RequestMethod.POST)
    public String addPostAction(
            @RequestParam(value = "letter") String letterStr,
            @RequestParam(value = "resource", required = false) Integer resourceId,
            @RequestParam(value = "word1", required = false) Integer wordId1,
            @RequestParam(value = "word2", required = false) Integer wordId2,
            @RequestParam(value = "word3", required = false) Integer wordId3,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы добавлять буквы
            return "redirect:/alphabet";
        }

        Languages languages = Languages.getInstance();

        Letter letter = new Letter();
        letter.setLetter(letterStr);
        if (new LetterDAOImpl().save(languages.get(Language.AD), letter, security.getUser())) {
            if (resourceId != null && resourceId != 0) {
                new LetterDAOImpl().save(letter, new Resource(resourceId), security.getUser());
            }

            // Удаляем все привязки слов к букве
            new LetterWordDAOImpl().delete(letter, security.getUser());

            // Если нужно то привязываем слова к букве
            if (wordId1 != null && wordId1 > 0) {
                new LetterWordDAOImpl().save(letter, new Word(wordId1), security.getUser());
            }
            if (wordId2 != null && wordId2 > 0) {
                new LetterWordDAOImpl().save(letter, new Word(wordId2), security.getUser());
            }
            if (wordId3 != null && wordId3 > 0) {
                new LetterWordDAOImpl().save(letter, new Word(wordId3), security.getUser());
            }

            return "redirect:/alphabet";
        }

        setModelMap(model, t.g("Добавление буквы"), ACTIVE);
        model.addAttribute("letter", letter);
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));
        model.addAttribute("words", new WordDAOImpl().list(languages.get(Language.AD)));

        return security.getTemplate() + "alphabet/edit";
    }

    @RequestMapping(value = "/alphabet/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(ModelMap model, @PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        Letter letter = new LetterDAOImpl().get(id);

        setModelMap(model, t.g("Редактирование буквы"), ACTIVE);
        model.addAttribute("letter", letter);
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));
        model.addAttribute("words", new WordDAOImpl().list(languages.get(Language.AD)));

        return security.getTemplate() + "alphabet/edit";
    }

    @RequestMapping(value = "/alphabet/edit/{id}", method = RequestMethod.POST)
    public String editPostAction(
            @PathVariable int id,
            @RequestParam(value = "letter") String letterStr,
            @RequestParam(value = "resource", required = false) Integer resourceId,
            @RequestParam(value = "word1", required = false) Integer wordId1,
            @RequestParam(value = "word2", required = false) Integer wordId2,
            @RequestParam(value = "word3", required = false) Integer wordId3,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        Letter letter = new LetterDAOImpl().get(id);
        letter.setLetter(letterStr);
        if (new LetterDAOImpl().save(languages.get(Language.AD), letter, security.getUser())) {
            if (resourceId != null && resourceId != 0) {
                new LetterDAOImpl().deleteResources(letter, security.getUser());
                new LetterDAOImpl().save(letter, new Resource(resourceId), security.getUser());
            }

            // Удаляем все привязки слов к букве
            new LetterWordDAOImpl().delete(letter, security.getUser());

            // Если нужно то привязываем слова к букве
            if (wordId1 != null && wordId1 > 0) {
                new LetterWordDAOImpl().save(letter, new Word(wordId1), security.getUser());
            }
            if (wordId2 != null && wordId2 > 0) {
                new LetterWordDAOImpl().save(letter, new Word(wordId2), security.getUser());
            }
            if (wordId3 != null && wordId3 > 0) {
                new LetterWordDAOImpl().save(letter, new Word(wordId3), security.getUser());
            }

            return "redirect:/alphabet";
        }

        setModelMap(model, t.g("Редактирование буквы"), ACTIVE);
        model.addAttribute("letter", letter);
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));
        model.addAttribute("words", new WordDAOImpl().list(languages.get(Language.AD)));

        return security.getTemplate() + "alphabet/edit";

    }

    @RequestMapping("/alphabet/delete/request/{id}")
    public String deleteRequestAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        String msg = t.g("Вы уверены, что хотите удалить букву из алфавита?") +
                " <a href='/alphabet/delete/" + id + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return "redirect:/alphabet";
    }

    @RequestMapping("/alphabet/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        new LetterDAOImpl().delete(new Letter(id), security.getUser());

        return "redirect:/alphabet";
    }
}