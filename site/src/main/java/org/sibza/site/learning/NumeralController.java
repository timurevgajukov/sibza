package org.sibza.site.learning;

import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.language.Language;
import org.sibza.model.notification.Notification;
import org.sibza.model.numeral.Numeral;
import org.sibza.model.numeral.dao.NumeralDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Message;
import org.sibza.services.NotificationService;
import org.sibza.site.PanelBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Тимур on 25.01.2016.
 *
 * Контроллер для работы с числительными
 */
@Controller
public class NumeralController extends PanelBaseController {

    private static final String ACTIVE = "numerals";

    // категория с числительными
    private static final Category category = new Category(12);

    @RequestMapping("/numerals")
    public String listAction(ModelMap model) {

        Languages languages = Languages.getInstance();
        Language language = languages.get(Language.AD);

        setModelMap(model, t.g("Числа"), ACTIVE);
        model.addAttribute("numerals", new NumeralDAOImpl().list(language));

        return security.getTemplate() + "numeral/list";
    }

    @RequestMapping(value = "/numerals/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы добавлять числа
            return "redirect:/numerals";
        }

        Languages languages = Languages.getInstance();
        Language language = languages.get(Language.AD);

        setModelMap(model, t.g("Добавление числа"), ACTIVE);
        model.addAttribute("words", new WordDAOImpl().list(language, category, 0, Integer.MAX_VALUE));

        return security.getTemplate() + "numeral/edit";
    }

    @RequestMapping(value = "/numerals/add", method = RequestMethod.POST)
    public String addPostAction(
            @RequestParam(value = "number", required = false) String numberStr,
            @RequestParam(value = "word", required = false) Integer wordId,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы добавлять числа
            return "redirect:/numerals";
        }

        // валидируем данные
        Integer number = null;
        Word word = null;
        try {
            number = Integer.parseInt(numberStr);
        } catch (Exception e) {
            security.addMessage(t.g("Необходимо указать корректное число"), Message.TYPE_DANGER);
        }
        if (wordId != null) {
            word = new WordDAOImpl().get(wordId);
            if (word == null) {
                security.addMessage(t.g("Слово с указанным идентификатором не найдено"), Message.TYPE_DANGER);
            }
        } else {
            security.addMessage(t.g("Необходимо выбрать слово для числа"), Message.TYPE_DANGER);
        }

        Languages languages = Languages.getInstance();
        Language language = languages.get(Language.AD);

        if (number == null || word == null) {
            setModelMap(model, t.g("Добавление числа"), ACTIVE);
            model.addAttribute("words", new WordDAOImpl().list(language, category, 0, Integer.MAX_VALUE));

            return security.getTemplate() + "numeral/edit";
        }

        // все хорошо, можем сохранить
        Numeral numeral = new Numeral(number, language, word);
        new NumeralDAOImpl().save(numeral, security.getUser());

        security.addMessage(t.g("Новое число успешно добавлено"), Message.TYPE_SUCCESS);
        if (security.getUser().getId() != Notification.ADMIN_ID) {
            new NotificationService(template).send(
                    Notification.create(
                            "Пользователь " + security.getUser().getEmail() + " добавил число \"" + numeral.getNumber(),
                            Message.TYPE_WARNING));
        }

        return "redirect:/numerals";
    }
}
