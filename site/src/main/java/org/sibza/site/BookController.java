package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.book.Book;
import org.sibza.model.book.BookType;
import org.sibza.model.book.dao.BookDAOImpl;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Тимур on 15.10.2015.
 *
 * Контроллер для работы с книгами
 */
@Controller
public class BookController extends PanelBaseController {

    private static final String ACTIVE = "books";

    @RequestMapping("/books")
    public String listAction(ModelMap model) {

        setModelMap(model, t.g("Книги"), ACTIVE);
        model.addAttribute("books", new BookDAOImpl().list(0, Integer.MAX_VALUE, new BookType(BookType.TYPE_BOOK)));

        return security.getTemplate() + "book/list";
    }

    @RequestMapping("/books/{id}/tomes")
    public @ResponseBody List<Book> listTomesAjax(@PathVariable int id) {

        List<Book> books = new Book(id).getBooks();

        if (books != null) {
            // очищаем от лишних данных
            for (Book book : books) {
                book.setParent(null);
            }
        }

        return books;
    }

    @RequestMapping("/books/{imgId}/{imgFileName}")
    public @ResponseBody byte[] imageAjax(@PathVariable int imgId, @PathVariable String imgFileName) {

        Resource resource = new ResourceDAOImpl().get(imgId);

        if (resource.getFileName().length() > 0) {
            return resource.getResource();
        }

        return null;
    }

    @RequestMapping(value = "/books/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        setModelMap(model, t.g("Добавление книги"), ACTIVE);
        model.addAttribute("books", new BookDAOImpl().list(0, Integer.MAX_VALUE, new BookType(BookType.TYPE_BOOK)));
        model.addAttribute("languages", languages.list());
        model.addAttribute("images", new ResourceDAOImpl().list(Type.IMAGE, true));

        return security.getTemplate() + "book/edit";
    }

    @RequestMapping(value = "/books/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(@PathVariable int id, ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Book book = new BookDAOImpl().get(id);

        Languages languages = Languages.getInstance();

        setModelMap(model, t.g("Редактирование книги"), ACTIVE);
        model.addAttribute("book", book);
        model.addAttribute("books", new BookDAOImpl().list(0, Integer.MAX_VALUE, new BookType(BookType.TYPE_BOOK)));
        model.addAttribute("languages", languages.list());
        model.addAttribute("images", new ResourceDAOImpl().list(Type.IMAGE, true));

        return security.getTemplate() + "book/edit";
    }
}