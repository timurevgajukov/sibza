package org.sibza.site.game;

import org.sibza.cache.Languages;
import org.sibza.model.application.Application;
import org.sibza.model.application.dao.ApplicationDAOImpl;
import org.sibza.model.game.Competition;
import org.sibza.model.game.Game;
import org.sibza.model.game.Score;
import org.sibza.model.game.dao.CompetitionDAOImpl;
import org.sibza.model.game.dao.ScoreDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.notification.Notification;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Message;
import org.sibza.services.NotificationService;
import org.sibza.site.PanelBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by timur on 21.11.15.
 *
 * Контроллер для игры в слова
 */
@Controller
public class WordsGameController extends PanelBaseController  {

    private static final String ACTIVE = "game";

    private static final String DESCRIPTION = "Игра в слова. Узнайте на сколько хорошо вы знаете адыгский язык.";
    private static final String KEYWORDS = "адыгский, кабардинский, черкесский, словарь, разговорник, язык, грамматика, слова, игры";

    private static final int MAX_LEN = 15;

    private static final boolean TECH_STOP = false;

    @RequestMapping("/game/words")
    public String indexAction(@RequestParam(value = "token", required = false) String token, ModelMap model) {

        if (TECH_STOP) {
            // остановить игру по техническим причинам
            security.addMessage(t.g("Конкурс остановлен по техническим причинам до 12:00 13 февраля. Приносим свои извинения за доставленные неудобства"), Message.TYPE_DANGER);
            return "redirect:/";
        }

        // в начале проверяем приложение
        if (token != null) {
            Application app = new ApplicationDAOImpl().get(token);
            if (app == null || app.isHold()) {
                return "redirect:/game/words";
            }
            model.addAttribute("withoutCounters", true);
            security.clearShowPageCount(); // для виджетов этот механизм нужно отключать
        } else {
            // сообщение для незарегистрированных пользователей
            if (!security.isAuth()) {
                security.addMessage(t.g("Для незарегистрированных пользователей счет после выхода из игры может быть потерян"), Message.TYPE_PURPLE);
            }
        }

        setModelMap(model, "Игра в слова", DESCRIPTION, KEYWORDS, ACTIVE);

        QuestionResponse question = getQuestion();

        Game game = new Game(Game.GAME_WORDS_ID);

        if (security.isAuth()) {
            // получаем общий счет пользователя
            Score score = new ScoreDAOImpl().get(game, null, security.getUser());
            if (score == null) {
                score = new Score(game, security.getUser(), 10);
            }
            security.setScore(score);

            // ищем текущие конкурсы и получаем счет по ним
            List<Competition> competitions = new CompetitionDAOImpl().list(true);
            if (competitions != null && competitions.size() != 0) {
                // TODO: пока ограничим одним активным конкурсом за раз
                Competition competition = competitions.get(0);
                Score scoreCompetition = new ScoreDAOImpl().get(game, competition, security.getUser());
                if (scoreCompetition == null) {
                    scoreCompetition = new Score(game, security.getUser(), 10);
                    scoreCompetition.setCompetition(competition);
                }
                if (scoreCompetition.getScore() > score.getScore()) {
                    // конкурсные баллы не могут превышать общие, они могут быть либо меньше, либо равны
                    scoreCompetition.setScore(score.getScore());
                    new ScoreDAOImpl().save(scoreCompetition, security.getSessionId());

                    new NotificationService(template).send(
                            Notification.create(
                                    "Для пользователя " + security.getUser().getEmail() + " опять рассинхронизировались баллы",
                                    Message.TYPE_DANGER));
                }

                model.addAttribute("competition", competition);
            }
        }

        security.getScore().setQuestion(question.word);
        security.getScore().setAnswer(question.rightAnswer);

        model.addAttribute("word", question.word);
        model.addAttribute("rightAnswer", question.rightAnswer);
        model.addAttribute("answers", question.answers);

        if (token == null || token.trim().length() == 0) {
            return security.getTemplate() + "game/words";
        } else {
            model.addAttribute("token", token);
            return security.getTemplate() + "widget/game/words";
        }
    }

    @RequestMapping(value = "/game/words/score", method = RequestMethod.POST)
    public @ResponseBody Integer scoreAjax(
            @RequestParam(value = "question") int questionWordId,
            @RequestParam(value = "answer") int answerWordId,
            @RequestParam(value = "answer_code") int answer) {

        // получаем общий счет
        Score score = security.getScore();

        if (score.getQuestion() == null) {
            String msg = "Уже зафиксировали по слову " + questionWordId + ", " + score.getQuestion().getId();
            if (security.isAuth()) {
                msg += " для " + security.getUser().getEmail();
            }
            new NotificationService(template).send(Notification.create(msg, Message.TYPE_DANGER));
            return score.getScore();
        }

        if (questionWordId != score.getQuestion().getId()) {
            String msg = "Не совпадают идентификаторы слов " + questionWordId + ", " + score.getQuestion().getId();
            if (security.isAuth()) {
                msg += " для " + security.getUser().getEmail();
            }
            new NotificationService(template).send(Notification.create(msg, Message.TYPE_DANGER));
            return score.getScore();
        }

        if (answer == 1 && answerWordId != score.getAnswer().getId()) {
            new NotificationService(template).send(Notification.create(
                    "Правильный ответ и присланный не совпадают",
                    Message.TYPE_DANGER
            ));
            return score.getScore();
        }

        // изменяем счет в зависимости от ответа
        changeScore(score, answer);

        // сбрасываем слово
        score.setQuestion(null);

        if (security.isAuth()) {
            Game game = score.getGame();

            // ищем текущие конкурсы и получаем счет по ним
            List<Competition> competitions = new CompetitionDAOImpl().list(true);
            if (competitions != null && competitions.size() != 0) {
                // TODO: пока ограничим одним активным конкурсом за раз
                Competition competition = competitions.get(0);
                Score scoreCompetition = new ScoreDAOImpl().get(game, competition, security.getUser());
                if (scoreCompetition == null) {
                    scoreCompetition = new Score(game, security.getUser(), 10);
                    scoreCompetition.setCompetition(competition);
                }

                // изменяем счет в зависимости от ответа
                changeScore(scoreCompetition, answer);
            }
        }

        return score.getScore();
    }

    @RequestMapping("/game/words/restart")
    public String restartAction(@RequestParam(value = "token", required = false) String token) {

        security.getScore().setScore(10);

        if (security.getScore().getUser() != null) {
            new ScoreDAOImpl().save(security.getScore(), security.getSessionId());
        }

        if (token == null) {
            return "redirect:/game/words";
        } else {
            return "redirect:/game/words?token=" + token;
        }
    }

    @RequestMapping(value = "/game/words/rating/list", method = RequestMethod.POST)
    public @ResponseBody List<Score> ratingsListAjax(
            @RequestParam(value = "competition") boolean competition) {

        Game game = new Game(Game.GAME_WORDS_ID);
        List<Score> scores;
        if (competition) {
            // TODO: необходимо возвращать идентификатор конкурса
            List<Competition> competitions = new CompetitionDAOImpl().all();
            scores = new ScoreDAOImpl().list(game, competitions.get(0));
        } else {
            scores = new ScoreDAOImpl().list(game, null);
        }

        return scores;
    }

    @RequestMapping("/game/words/question/get")
    public @ResponseBody QuestionResponse questionAjax() {

        QuestionResponse question = getQuestion();

        security.getScore().setQuestion(question.word);
        security.getScore().setAnswer(question.rightAnswer);

        return question;
    }

    private QuestionResponse getQuestion() {

        Random random = new Random();
        Languages languages = Languages.getInstance();
        List<Word> translates = getWords(new WordDAOImpl().list(security.getLanguage()), MAX_LEN);

        // слово, которое нужно отгадать и правильный ответ к нему
        Word rightAnswer;
        Word word;
        do {
            rightAnswer = translates.get(random.nextInt((translates.size())));
            word = rightAnswer.getTranslate(languages.get(Language.AD));
        } while (word == null);

        // список переводов
        List<Word> answers = new ArrayList<Word>();
        int rightAnswerInd = random.nextInt(4);
        for (int i=0; i<4; i++) {
            if (i == rightAnswerInd) {
                answers.add(rightAnswer);
            } else {
                Word randomWord = translates.get(random.nextInt(translates.size()));
                while (rightAnswer.getWord().equalsIgnoreCase(randomWord.getWord()) || !isUnique(answers, randomWord)) {
                    randomWord = translates.get(random.nextInt(translates.size()));
                }
                answers.add(randomWord);
            }
        }

        // убираем лишние данные по слову
        word.setCategory(null);
        word.setLanguage(null);

        // убираем лишние данные по правильному ответу
        rightAnswer.setCategory(null);
        rightAnswer.setLanguage(null);

        // убираем лишние данные по вариантам ответов
        for (Word answer : answers) {
            answer.setCategory(null);
            answer.setLanguage(null);
        }

        return new QuestionResponse(word, rightAnswer, answers);
    }

    /**
     * Возвращает список слов, по длине не превышающих указанной максимальной длины
     *
     * @param words
     * @param maxLen
     * @return
     */
    private List<Word> getWords(List<Word> words, int maxLen) {

        if (words == null) {
            return null;
        }

        List<Word> result = new ArrayList<Word>();
        for (Word word : words) {
            if (word.getWord().length() <= maxLen) {
                result.add(word);
            }
        }

        return result;
    }

    /**
     * Пока показывает, что написание слова уникально и не присутствует в списке
     *
     * @param words
     * @param word
     * @return
     */
    private boolean isUnique(List<Word> words, Word word) {

        if (words == null || words.size() == 0) {
            return true;
        }

        for (Word item : words) {
            if (word.getWord().equalsIgnoreCase(item.getWord())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Изменяет и сохраняет счет в зависимости от статуса ответа
     *
     * @param score
     * @param answer
     */
    private void changeScore(Score score, int answer) {

        if (answer == 1) {
            score.setScore(score.getScore() + 1);
        } else if (answer == -1) {
            score.setScore(score.getScore() - 3);
        } else if (answer == 0) {
            score.setScore(score.getScore() - 1);
        }

        if (security.isAuth() && score.getUser() == null) {
            new NotificationService(template).send(
                    Notification.create("Не совпадает авторизованный пользователь и в игре", Message.TYPE_DANGER));
        }

        if (score.getUser() != null) {
            new ScoreDAOImpl().save(score, security.getSessionId());
        }
    }

    class QuestionResponse {

        public Word word;
        public Word rightAnswer;
        public List<Word> answers;

        public QuestionResponse(Word word, Word rightAnswer, List<Word> answers) {

            this.word = word;
            this.rightAnswer = rightAnswer;
            this.answers = answers;
        }
    }
}