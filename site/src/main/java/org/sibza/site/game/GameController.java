package org.sibza.site.game;

import org.sibza.model.game.Competition;
import org.sibza.model.game.dao.CompetitionDAOImpl;
import org.sibza.site.PanelBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Тимур on 26.01.2016.
 *
 * Контроллер для работы с разделом игр
 */
@Controller
public class GameController extends PanelBaseController {

    private static final String ACTIVE = "game";

    @RequestMapping("/game")
    public String indexAction(ModelMap model) {

        setModelMap(model, t.g("Игры и Конкурсы"), ACTIVE);
        model.addAttribute("competitions", new CompetitionDAOImpl().all());

        return security.getTemplate() + "game/index";
    }
}
