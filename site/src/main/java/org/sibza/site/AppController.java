package org.sibza.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by timur on 10.01.16.
 *
 * Контроллер для отображения приложений проекта
 */
@Controller
public class AppController extends PanelBaseController {

    private static final String ACTIVE = "apps";

    @RequestMapping("/apps")
    public String indexAction(ModelMap model) {

        setModelMap(model, t.g("Приложения"), ACTIVE);
        return security.getTemplate() + "app/index";
    }
}