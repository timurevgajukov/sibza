package org.sibza.site.security;

import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.notification.Notification;
import org.sibza.model.user.Role;
import org.sibza.model.user.User;
import org.sibza.model.user.UserReg;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.model.user.validate.UserRegValidate;
import org.sibza.security.Message;
import org.sibza.services.MailService;
import org.sibza.services.NotificationService;
import org.sibza.site.PanelBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by timur on 17.05.15.
 *
 * Контроллер для обработки запросов безопасности
 */
@Controller
public class SecurityController extends PanelBaseController {

    private static final String ACTIVE = "dashboard";

    @RequestMapping(value = "/security/login", method = RequestMethod.POST)
    public String authPostAction(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "redirect") String url) {

        security.setUser(new UserDAOImpl().auth(email, password));
        if (security.isAuth()) {
            security.addMessage(t.g("Рады, что вы к нам вернулись!"), Message.TYPE_SUCCESS);
            if (security.getUser().getId() != Notification.ADMIN_ID) {
                new NotificationService(template).send(Notification.create("Авторизация пользователя " + email, Message.TYPE_INFO));
            }
        } else {
            security.addMessage(t.g("Не унедалось вас авторизировать. Возможно логин или пароль не верны."), Message.TYPE_WARNING);
        }

        return "redirect:" + url;
    }

    @RequestMapping("/security/logout")
    public String logoutAction() {

        security.logout();

        return "redirect:/";
    }

    @RequestMapping(value = "/security/registration", method = RequestMethod.POST)
    public String registrationPostAction(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "password1") String password1,
            @RequestParam(value = "password2") String password2,
            @RequestParam(value = "redirect") String url) {

        UserReg userReg = new UserReg(email, name, password1, password2);

        UserRegValidate validate = new UserRegValidate(t);
        if (validate.validate(userReg)) {
            // прошли валидацию и можно регистировать нового пользователя
            String confirmCode = User.genConfirmCode();
            User user = new UserDAOImpl().registration(
                    userReg.getEmail(),
                    userReg.getPassword1(),
                    userReg.getName(),
                    confirmCode,
                    new Role(Role.ROLE_USER));
            security.setUser(user);

            // отправляем письмо на указанный ящик для верификации
            new MailService(servletContext).sendConfirmEmail(security.getUser());

            security.addMessage(t.g("Поздравляем! Вы успешно зарегистированы! " +
                    "На указанный почтовый ящик отправлено письмо с кодом верификации."), Message.TYPE_SUCCESS);

            new NotificationService(template).send(Notification.create("Регистрация нового пользователя " + email, Message.TYPE_SUCCESS));
        } else {
            security.addMessages(validate.getErrors(), Message.TYPE_DANGER);
        }

        return "redirect:" + url;
    }

    @RequestMapping(value = "/security/email/check", method = RequestMethod.POST)
    public @ResponseBody Boolean checkEmailAjax(@RequestParam(value = "email") String email) {

        return new UserDAOImpl().checkEmail(email.trim());
    }

    @RequestMapping(value = "/security/language", method = RequestMethod.POST)
    public String languageAction(
            HttpServletResponse response,
            @RequestParam("language") String code,
            @RequestParam("redirectUrl") String url) {

        security.setLanguage(new LanguageDAOImpl().get(code));
        security.setUserLanguageStatus(true);
        // Cookie cookie = new Cookie(SiteInterceptor.COOKIE_LANGUAGE_NAME, security.getLanguage().getCode());
        // response.addCookie(cookie);

        return "redirect:" + url;
    }

    @RequestMapping("/security/email/sendConfirm")
    public String emailSendConfirmAction() {

        if (security.isAuth()) {
            new MailService(servletContext).sendConfirmEmail(security.getUser());
        }

        // TODO: возвращаться на исходную страницу
        return "redirect:/";
    }

    @RequestMapping("/security/email/confirm")
    public String emailConfirmAction(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "code") String code) {

        if (new UserDAOImpl().confirmEmail(email, code)) {
            security.addMessage(t.g("Электронная почта успешно подтверждена!"), Message.TYPE_SUCCESS);
        } else {
            security.addMessage(t.g("Не удалось подтвердить электронную почту."), Message.TYPE_WARNING);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/security/recover", method = RequestMethod.GET)
    public String recoverGetPasswordAction(
            @RequestParam(value = "email") String email,
            @RequestParam(value = "code") String code,
            ModelMap model) {

        User user = new UserDAOImpl().getByEmail(email, code);
        if (user != null) {
            security.setUser(user);
            setModelMap(model, t.g("Восстановление пароля"), ACTIVE);
            return security.getTemplate() + "user/recover";
        } else {
            security.addMessage(t.g("Некорректные данные для восстановления пароля."), Message.TYPE_WARNING);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/security/recover", method = RequestMethod.POST)
    public String recoverPostPasswordAction(
            @RequestParam(value = "password1") String password1,
            @RequestParam(value = "password2") String password2,
            ModelMap model) {

        if (!security.isAuth()) {
            security.addMessage(t.g("Для смены пароля необходимо перейти по ссылке из письма"), Message.TYPE_WARNING);
            return "redirect:/";
        }

        if (!password1.equals(password2)) {
            security.addMessage(t.g("Указанные пароли не совпадают"), Message.TYPE_DANGER);
            setModelMap(model, t.g("Восстановление пароля"), ACTIVE);
            return security.getTemplate() + "user/recover";
        }

        if (new UserDAOImpl().recoverPassword(security.getUser(), password1)) {
            security.addMessage(t.g("Пароль успешно изменен"), Message.TYPE_SUCCESS);
        } else {
            security.addMessage(t.g("Не удалось сменить пароль"), Message.TYPE_DANGER);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/security/recover/request", method = RequestMethod.POST)
    public String recoverRequestPostPasswordAction(@RequestParam(value = "email") String email) {

        User user = new UserDAOImpl().getByEmail(email);

        if (user != null) {
            // отправляем пользователю письмо со ссылкой на восстановление пароля
            new MailService(servletContext).sendRecoverEmail(user);
        } else {
            // не нашли такого пользователя
            security.addMessage(t.g("Не нашли пользователя по указанной электронной почте"), Message.TYPE_WARNING);
        }

        return "redirect:/";
    }
}
