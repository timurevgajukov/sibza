package org.sibza.site;

import org.sibza.model.language.dao.LanguageDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by timur on 03.10.15.
 *
 * Контроллер для работя с языками
 */
@Controller
public class LanguageController extends PanelBaseController {

    private static final String ACTIVE = "languages";

    @RequestMapping("/languages")
    public String languagesAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdmin()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        setModelMap(model, t.g("Языки"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());

        return security.getTemplate() + "languages";
    }
}