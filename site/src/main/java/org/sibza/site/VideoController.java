package org.sibza.site;

import org.sibza.model.video.dao.VideoDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by timur on 26.09.15.
 *
 * Контроллер для работы с видео
 */
@Controller
public class VideoController extends PanelBaseController {

    private static final String ACTIVE = "video";

    @RequestMapping("/video")
    public String listAction(ModelMap model) {

        setModelMap(model, t.g("Видео"), ACTIVE);
        model.addAttribute("videoList", new VideoDAOImpl().list(0, Integer.MAX_VALUE));

        return security.getTemplate() + "video/list";
    }
}