package org.sibza.site;

import org.sibza.cache.Categories;
import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.UserDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.XMLMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Тимур on 27.11.2015.
 *
 * Для генерации различных технических файлов и страниц
 */
@Controller
public class TechPagesController {

    private final static String SITE = "sibza.org";
    private final static String URL = "https://" + SITE;
    private final static SimpleDateFormat SIMEMAP_DATE = new SimpleDateFormat("yyyy-MM-dd");

    private final static String ALWAYS = "always";
    private final static String HOURLY = "hourly";
    private final static String DAILY = "daily";
    private final static String WEEKLY = "weekly";
    private final static String MONTHLY = "monthly";

    @RequestMapping("/robots.txt")
    public @ResponseBody String robotsTxtAction() {

        StringBuilder result = new StringBuilder();
        result.append("User-agent: *\n");
        result.append("Disallow: /index.php\n");
        result.append("Disallow: /api.php\n");
        result.append("Host: " + URL + "\n");
        result.append(String.format("Sitemap: %s/sitemap.xml\n", URL));

        return result.toString();
    }

    @RequestMapping(value = "/sitemap.xml", produces = "application/xml; charset=utf-8")
    public @ResponseBody String sitemapAction() {

        XMLMessage xml = new XMLMessage("urlset");
        BodyElement urlset = xml.getRoot();
        urlset.setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

        // основные разделы сайта
        urlset.addChild(createUrlElement("/", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/alphabet", new Date(), ALWAYS, "0.8"));
        urlset.addChild(createUrlElement("/numerals", new Date(), ALWAYS, "0.8"));
        urlset.addChild(createUrlElement("/dictionary", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/proverbs", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/books", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/audio", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/video", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/game/words", new Date(), ALWAYS, "0.8"));
        urlset.addChild(createUrlElement("/apps", new Date(), ALWAYS, "0.8"));
        urlset.addChild(createUrlElement("/users", new Date(), ALWAYS, "0.8"));
        urlset.addChild(createUrlElement("/about", new Date(), MONTHLY, "0.8"));
        urlset.addChild(createUrlElement("/help/developer", new Date(), MONTHLY, "0.8"));

        // добавляем ссылки по категориям словаря
        Languages languages = Languages.getInstance();
        Categories categories = Categories.getInstance(null, languages.get(Language.RU));
        for (Category category : categories.list()) {
            urlset.addChild(createUrlElement("/dictionary/category/" + category.getId(), new Date(), DAILY, "0.8"));
        }

        // добавляем ссылки на пользователей
        for (User user : new UserDAOImpl().list(0, Integer.MAX_VALUE)) {
            urlset.addChild(createUrlElement("/users/" + (user.getNick() != null ? user.getNick() : ("id" + user.getId())),
                    new Date(), MONTHLY, "0.8"));
        }

        return xml.renderSafe();
    }

    @RequestMapping(value = "/opensearch.xml", produces = "application/xml; charset=utf-8")
    public @ResponseBody String opensearchAction() {

        XMLMessage xml = new XMLMessage("OpenSearchDescription");
        BodyElement osd = xml.getRoot();
        osd.setAttribute("xmlns", "http://a9.com/-/spec/opensearch/1.1/");

        BodyElement image = new BodyElement("Image", "https://sibza.org/resources/img/favicon.ico");
        image.setAttribute("width", "16");
        image.setAttribute("height", "16");
        image.setAttribute("type", "image/x-icon");

        BodyElement url = new BodyElement("Url");
        url.setAttribute("type", "text/html");
        url.setAttribute("template", "https://sibza.org/search?search-query={searchTerms}");

        osd.addChild(new BodyElement("ShortName", "Si bza"));
        osd.addChild(new BodyElement("Description", "Si bza site search"));
        osd.addChild(image);
        osd.addChild(url);
        osd.addChild(new BodyElement("InputEncoding", "UTF-8"));

        return xml.renderSafe();
    }

    private BodyElement createUrlElement(String loc, Date lastmod, String changefreq, String priority) {

        BodyElement url = new BodyElement("url");
        url.addChild(new BodyElement("loc", URL + loc));
        url.addChild(new BodyElement("lastmod", SIMEMAP_DATE.format(lastmod)));
        url.addChild(new BodyElement("changefreq", changefreq));
        url.addChild(new BodyElement("priority", priority));

        return url;
    }
}