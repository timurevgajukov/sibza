package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.alphabet.dao.AlphabetDAOImpl;
import org.sibza.model.audio.Audio;
import org.sibza.model.audio.dao.AudioDAOImpl;
import org.sibza.model.book.Book;
import org.sibza.model.book.dao.BookDAOImpl;
import org.sibza.model.game.Competition;
import org.sibza.model.game.dao.CompetitionDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.proverb.dao.ProverbDAOImpl;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.model.video.Video;
import org.sibza.model.video.dao.VideoDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.utils.Utils;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by timur on 11.05.15.
 *
 * Контроллер для обработки страниц административной панели
 */
@Controller
public class PanelController extends PanelBaseController {

    private static final String ACTIVE = "dashboard";

    @RequestMapping("/")
    public String indexAction(ModelMap model) {

        setModelMap(model, t.g("Главная"), ACTIVE);

        Languages languages = Languages.getInstance();

        List<Competition> competitions = new CompetitionDAOImpl().list(false);
        if (competitions != null && competitions.size() != 0) {
            model.addAttribute("competition", competitions.get(0));
        }

        model.addAttribute("proverbs", new ProverbDAOImpl().list(languages.get(Language.AD), 0, 10));
        model.addAttribute("video", new VideoDAOImpl().get(1));

        return security.getTemplate() + "index";
    }

    @RequestMapping(value = "/search")
    public String searchAction(ModelMap model, @RequestParam(value = "search-query") String query, SitePreference sitePreference) {

        setModelMap(model, t.g("Результат поиска"), ACTIVE);
        model.addAttribute("query", query);

        if (query != null && query.trim().length() != 0) {
            String queryNorm = query.trim();

            List<Word> words = new WordDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            for (Word word : words) {
                word.loadTranslates();
            }
            List<Proverb> proverbs = new ProverbDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            List<Book> books = new BookDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            List<Audio> audioList = new AudioDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));
            List<Video> videoList = new VideoDAOImpl().search(Utils.adygAlphabetNorm(queryNorm));

            boolean hasResult = !(Utils.isEmpty(words) && Utils.isEmpty(proverbs) && Utils.isEmpty(books)
                    && Utils.isEmpty(audioList) && Utils.isEmpty(videoList));

            model.addAttribute("hasResult", hasResult);
            model.addAttribute("words", words);
            model.addAttribute("proverbs", proverbs);
            model.addAttribute("books", books);
            model.addAttribute("audioList", audioList);
            model.addAttribute("videoList", videoList);
        }

        if ("NORMAL".equalsIgnoreCase(sitePreference.name())) {
            return security.getTemplate() + "search/normal";
        } else {
            return security.getTemplate() + "search/mobile";
        }
    }

    @RequestMapping("/404")
    public String error404Action(ModelMap model) {

        setModelMap(model, t.g("Ошибка") + " 404", "");

        return "404";
    }

    @RequestMapping("/stat/video/all")
    public @ResponseBody Integer statVideoCountAllAjax() {

        return new VideoDAOImpl().count();
    }

    @RequestMapping("/stat/proverbs/all")
    public @ResponseBody Integer statProverbsCountAllAjax() {

        return new ProverbDAOImpl().count();
    }

    @RequestMapping("/stat/alphabet/all")
    public @ResponseBody Integer statAlphabetCountAllAjax() {

        return new AlphabetDAOImpl().count();
    }

    @RequestMapping("/stat/words/all")
    public @ResponseBody Integer statWordsCountAllAjax() {

        return new WordDAOImpl().count();
    }

    @RequestMapping("/stat/languages/all")
    public @ResponseBody Integer statLanguagesCountAllAjax() {

        return new LanguageDAOImpl().count();
    }

    @RequestMapping("/stat/users/all")
    public @ResponseBody Integer statUsersCountAllAjax() {

        return new UserDAOImpl().count();
    }
}