package org.sibza.site.push;

import org.sibza.model.application.Application;
import org.sibza.model.application.Device;
import org.sibza.model.application.dao.ApplicationDAOImpl;
import org.sibza.model.application.dao.DeviceDAOImpl;
import org.sibza.site.api.response.Response;
import org.sibza.site.api.response.ResponseCode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Тимур on 11.01.2016.
 *
 * Контроллер для работы с push notification
 */
@Controller
public class PushController {

    @RequestMapping("/push/device/set")
    public @ResponseBody Response setDeviceToken(
            @RequestParam(value = "app") String appToken,
            @RequestParam(value = "token") String deviceToken) {

        // в начале проверяем приложение
        Application app = new ApplicationDAOImpl().get(appToken);
        if (app == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }
        if (app.isHold()) {
            return new Response(-2, ResponseCode.CODES.get(-2));
        }

        // сохраняем токен для мобильного устройства
        Device device = new Device(app, deviceToken);
        new DeviceDAOImpl().save(device);

        return new Response(0, ResponseCode.CODES.get(0));
    }
}