package org.sibza.site.user;

import org.sibza.model.user.User;
import org.sibza.model.user.dao.FriendDAOImpl;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.site.PanelBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by timur on 03.10.15.
 *
 * Контроллер для работы с пользователями
 */
@Controller
public class UserController extends PanelBaseController {

    private static final String ACTIVE_USERS = "users";
    private static final String ACTIVE_FRIENDS = "friends";

    private static final int COUNT = 20;

    private int currentPage;

    @RequestMapping("/users")
    public String listUsersAction(ModelMap model) {

        currentPage = 1;

        List<User> users = new UserDAOImpl().list(0, COUNT);

        setModelMap(model, t.g("Пользователи"), ACTIVE_USERS);
        model.addAttribute("users", users);

        return security.getTemplate() + "user/list";
    }

    @RequestMapping("/users/more")
    public String listUsersMoreAction(ModelMap model) {

        currentPage++;

        List<User> users = new UserDAOImpl().list((currentPage - 1) * COUNT, COUNT);

        setModelMap(model, t.g("Пользователи"), ACTIVE_USERS);
        model.addAttribute("users", users);

        return security.getTemplate() + "user/list";
    }

    @RequestMapping("/friends")
    public String listFriendsAction(ModelMap model) {

        if (!security.isAuth()) {
            // только для зарегистрированных пользователей
            return "redirect:/users";
        }

        currentPage = 1;

        User user = security.getUser();
        user.loadFriends(0, COUNT);
        List<User> users = user.getFriends();

        setModelMap(model, t.g("Друзья"), ACTIVE_FRIENDS);
        model.addAttribute("users", users);

        return security.getTemplate() + "user/list";
    }

    @RequestMapping("/friends/more")
    public String listFriendsMoreAction(ModelMap model) {

        if (!security.isAuth()) {
            // только для зарегистрированных пользователей
            return "redirect:/users";
        }

        currentPage++;

        User user = security.getUser();
        user.loadFriends((currentPage - 1) * COUNT, COUNT);
        List<User> users = user.getFriends();

        setModelMap(model, t.g("Друзья"), ACTIVE_FRIENDS);
        model.addAttribute("users", users);

        return security.getTemplate() + "user/list";
    }

    @RequestMapping(value = "/friends/add", method = RequestMethod.POST)
    public @ResponseBody String friendsAddPostAjax(@RequestParam(value = "friend") int friendId) {

        if (!security.isAuth()) {
            // только для зарегистрированных пользователей
            return "redirect:/users";
        }

        // проверяем, что такой пользователь имеется
        User friend = new UserDAOImpl().get(friendId);
        if (friend == null) {
            return "ERROR";
        }

        User user = security.getUser();
        if (user.hisFriend(friend)) {
            // уже в друзьях
            return "OK";
        }

        // проверяем, что мы не пытаемся добавить в друзья самого себя
        if (user.getId() == friend.getId()) {
            return "ERROR";
        }

        // теперь можем добавить в друзья
        user.clearFriends();
        new FriendDAOImpl().save(user, friend);

        return "OK";
    }

    @RequestMapping(value = "/friends/delete", method = RequestMethod.POST)
    public @ResponseBody String friendDeletePostAjax(@RequestParam(value = "friend") int friendId) {

        if (!security.isAuth()) {
            // только для зарегистрированных пользователей
            return "redirect:/users";
        }

        // проверяем, что такой пользователь имеется
        User friend = new UserDAOImpl().get(friendId);
        if (friend == null) {
            return "ERROR";
        }

        User user = security.getUser();
        if (!user.hisFriend(friend)) {
            // и так не друзья
            return "OK";
        }

        // проверяем, что мы не пытаемся добавить в друзья самого себя
        if (user.getId() == friend.getId()) {
            return "ERROR";
        }

        // теперь можем удалить из друзей
        user.clearFriends();
        new FriendDAOImpl().delete(user, friend);

        return "OK";
    }
}