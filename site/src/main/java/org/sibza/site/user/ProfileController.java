package org.sibza.site.user;

import com.google.gson.Gson;
import org.sibza.cache.ContentTypes;
import org.sibza.cache.DefaultAvatars;
import org.sibza.cache.SocialNetworkTypes;
import org.sibza.model.avatar.DefaultAvatar;
import org.sibza.model.post.Post;
import org.sibza.model.post.dao.PostDAOImpl;
import org.sibza.model.resource.ContentType;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.user.SocialNetwork;
import org.sibza.model.user.SocialNetworkType;
import org.sibza.model.user.User;
import org.sibza.model.user.UserInfo;
import org.sibza.model.user.dao.SocialNetworkDAOImpl;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.security.Message;
import org.sibza.site.PanelBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Контроллер для обработки запросов по профилю пользователя
 */
@Controller
public class ProfileController extends PanelBaseController {

    private static final String ACTIVE = "profile";

    @RequestMapping("/users/id{id}")
    public String userPageByIdAction(@PathVariable int id, ModelMap model) {

        User user = new UserDAOImpl().get(id);
        if (user == null) {
            security.addMessage(t.g("Не нашли пользователя с указанным идентификатором"), Message.TYPE_DANGER);
            return "redirect:/";
        }

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();

        user.loadSocialNetwords();
        user.loadPosts();

        setModelMap(model, user.toString(), ACTIVE);
        model.addAttribute("user", user);
        model.addAttribute("avatars", defaultAvatars.list());

        return security.getTemplate() + "user/profile";
    }

    @RequestMapping("/users/{nick}")
    public String userPageByNickAction(@PathVariable String nick, ModelMap model) {

        User user = new UserDAOImpl().get(nick);
        if (user == null) {
            security.addMessage(t.g("Не нашли пользователя с указанным идентификатором"), Message.TYPE_DANGER);
            return "redirect:/";
        }

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();

        user.loadSocialNetwords();
        user.loadPosts();

        setModelMap(model, user.toString(), ACTIVE);
        model.addAttribute("user", user);
        model.addAttribute("avatars", defaultAvatars.list());

        return security.getTemplate() + "user/profile";
    }

    @RequestMapping(value = "/users/profile/edit", method = RequestMethod.GET)
    public String profileEditGetAction(ModelMap model) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        SocialNetworkTypes socials = SocialNetworkTypes.getInstance();

        User user = security.getUser();

        setModelMap(model, t.g("Редактирование пользователя"), ACTIVE);
        model.addAttribute("mode", "edit");
        model.addAttribute("socials", socials.list());
        model.addAttribute("user", user);

        return security.getTemplate() + "user/profile/edit";
    }

    @RequestMapping(value = "/users/profile/edit/main", method = RequestMethod.POST)
    public String profileMainEditPostAction(
            @RequestParam(value = "nick", required = false) String nick,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "gender", required = false) Integer gender,
            @RequestParam(value = "birthday", required = false) String birthday,
            @RequestParam(value = "about", required = false) String about) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        User user = security.getUser();

        // если указан ник, то проверяем его уникальность
        if (nick != null) {
            User nickUser = new UserDAOImpl().get(nick.trim());
            if (nickUser != null && security.getUser().getId() != nickUser.getId()) {
                // мы нашли пользователя с таким ником и это не мы
                security.addMessage(t.g("Пользователь с таким ником уже существует. Пожалуйста, используйте другой ник."), Message.TYPE_WARNING);
            } else {
                user.setNick(nick);
            }
        } else {
            user.setNick(null);
        }

        user.setName(name);
        UserInfo info = user.getInfo();
        info.setGender(gender);
        try {
            info.setBirthday(new SimpleDateFormat("dd.MM.yyyy").parse(birthday));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        info.setAbout(about);
        new UserDAOImpl().save(user);

        security.addMessage(t.g("Дополнительные данные по пользователю сохранены успешно!"), Message.TYPE_SUCCESS);

        return "redirect:/users/profile/edit";
    }

    @RequestMapping(value = "/users/profile/edit/contact", method = RequestMethod.POST)
    public String profileContactEditPostAction(
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "address") String address,
            @RequestParam(value = "site") String site,
            @RequestParam(value = "social") List<String> socials) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        // сохраним дополнительную контактную информацию
        UserInfo info = security.getUser().getInfo();
        info.setPhone(phone);
        info.setAddress(address);
        info.setSite(site);
        new UserDAOImpl().save(security.getUser());

        // сохраним социальные сети
        for (String social : socials) {
            String[] items = social.split("###");
            SocialNetwork socialNetwork = security.getUser().getSocialNetwork(Integer.parseInt(items[0]));
            if (socialNetwork != null) {
                // ранее уже указывали аккаунт в этой сети
                if (items.length == 2) {
                    socialNetwork.setAccount(items[1]);
                }
            } else {
                if (items.length == 2) {
                    socialNetwork = new SocialNetwork(0, new SocialNetworkType(Integer.parseInt(items[0])), items[1]);
                }
            }
            if (items.length == 2) {
                new SocialNetworkDAOImpl().save(socialNetwork, security.getUser());
            } else {
                if (socialNetwork != null && socialNetwork.getId() != 0) {
                    new SocialNetworkDAOImpl().delete(socialNetwork, security.getUser());
                }
            }
        }

        security.addMessage(t.g("Контактные данные по пользователю сохранены успешно!"), Message.TYPE_SUCCESS);

        return "redirect:/users/profile/edit";
    }

    @RequestMapping(value = "/users/profile/edit/password", method = RequestMethod.POST)
    public String profilePasswordEditPostAction(
            @RequestParam(value = "password_old") String passwordOld,
            @RequestParam(value = "password1") String password1,
            @RequestParam(value = "password2") String password2) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        // проверяем корректность текущего пароля
        User user = new UserDAOImpl().auth(security.getUser().getEmail(), passwordOld);
        if (user == null || user.getId() != security.getUser().getId()) {
            security.addMessage(t.g("Некорректно указан текущий пароль"), Message.TYPE_DANGER);
            return "redirect:/users/profile/edit";
        }

        // валидируем новый пароль
        if (!password1.equals(password2)) {
            security.addMessage(t.g("Указанные пароли не совпадают"), Message.TYPE_DANGER);
            return "redirect:/users/profile/edit";
        }

        // теперь можем сменить пароль
        if (new UserDAOImpl().changePassword(security.getUser(), passwordOld, password1)) {
            security.addMessage(t.g("Пароль успешно изменен"), Message.TYPE_SUCCESS);
        } else {
            security.addMessage(t.g("Не удалось сменить пароль"), Message.TYPE_DANGER);
        }

        return "redirect:/users/profile/edit";
    }

    @RequestMapping(value = "/users/profile/post", method = RequestMethod.POST)
    public String postPostAction(@RequestParam(value = "body") String body) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        if (body != null && body.trim().length() != 0) {
            String saveBody = body.trim().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            Post post = new Post(security.getUser(), saveBody);

            new PostDAOImpl().save(post);
        }

        security.addMessage(t.g("Добавили новую статью"), Message.TYPE_SUCCESS);

        return "redirect:/users/" + security.getUser().getProfileUrl();
    }

    @RequestMapping("/users/profile/avatar/{id}")
    public String avatarChangeAction(@PathVariable int id) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();
        DefaultAvatar avatar = defaultAvatars.get(id);

        if (avatar != null) {
            Resource image = security.getUser().getInfo().getAvatar();
            if (image != null) {
                security.getUser().getInfo().setAvatar(null);
            }

            security.getUser().getInfo().setDefaultAvatar(avatar);
            new UserDAOImpl().save(security.getUser());

            if (image != null) {
                new ResourceDAOImpl().delete(image, security.getUser());
            }

            security.addMessage(t.g("Успешно сменили аватарку"), Message.TYPE_SUCCESS);
        } else {
            security.addMessage(t.g("Не удалось сменить аватарку"), Message.TYPE_DANGER);
        }

        return "redirect:/users/" + security.getUser().getProfileUrl();
    }

    @RequestMapping("/users/profile/{id}/avatar/{imgId}/{imgFileName}")
    public @ResponseBody byte[] imageAction(
            @PathVariable int id, @PathVariable int imgId, @PathVariable String imgFileName) {

        Resource resource = new ResourceDAOImpl().get(imgId);

        if (resource != null) {
            return resource.getResource();
        }

        return null;
    }

    @RequestMapping(value = "/users/profile/crop", method = RequestMethod.POST)
    public String avatarCropAction(
            @RequestParam(value = "avatar_src") String src,
            @RequestParam(value = "avatar_data") String data,
            @RequestParam(value = "avatar_file") MultipartFile file) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        System.out.println(src);
        System.out.println(data);
        System.out.println("Upload image: " + file.getOriginalFilename());
        System.out.println("Upload image MIME-type: " + file.getContentType());

        ContentTypes contentTypes = ContentTypes.getInstance();
        ContentType type = contentTypes.get(file.getContentType());

        CropData cropData = new Gson().fromJson(data, CropData.class);

        ByteArrayOutputStream baos = null;
        try {
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
            image = image.getSubimage(
                    cropData.getIntX(), cropData.getIntY(),
                    cropData.getIntWidth(), cropData.getIntHeight());

            baos = new ByteArrayOutputStream();
            ImageIO.write(image, type.getName(), baos);
            baos.flush();

            Resource resource = new Resource(type, file.getOriginalFilename(), baos.toByteArray());
            new ResourceDAOImpl().save(resource, security.getUser());

            Resource oldAvatar = security.getUser().getInfo().getAvatar();

            security.getUser().getInfo().setAvatar(resource);
            new UserDAOImpl().save(security.getUser());

            if (oldAvatar != null) {
                new ResourceDAOImpl().delete(oldAvatar, security.getUser());
            }

            security.addMessage(t.g("Успешно сменили аватарку"), Message.TYPE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {}
            }
        }

        return "redirect:/users/" + security.getUser().getProfileUrl();
    }

    class CropData {

        private double x;
        private double y;
        private double height;
        private double width;
        private double rotate;

        public CropData(double x, double y, double height, double width, double rotate) {

            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.rotate = rotate;
        }

        public double getX() {

            return x;
        }

        public int getIntX() {

            return (int) Math.round(x);
        }

        public void setX(double x) {

            this.x = x;
        }

        public double getY() {

            return y;
        }

        public int getIntY() {

            return (int) Math.round(y);
        }

        public void setY(double y) {

            this.y = y;
        }

        public double getHeight() {

            return height;
        }

        public int getIntHeight() {

            return (int) Math.round(height);
        }

        public void setHeight(double height) {

            this.height = height;
        }

        public double getWidth() {

            return width;
        }

        public int getIntWidth() {

            return (int) Math.round(width);
        }

        public void setWidth(double width) {

            this.width = width;
        }

        public double getRotate() {

            return rotate;
        }

        public void setRotate(double rotate) {

            this.rotate = rotate;
        }
    }
}