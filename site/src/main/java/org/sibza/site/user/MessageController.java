package org.sibza.site.user;

import org.sibza.model.message.Message;
import org.sibza.model.message.dao.MessageDAOImpl;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.site.PanelBaseController;
import org.sibza.site.api.response.Response;
import org.sibza.site.api.response.ResponseCode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by timur on 08.03.16.
 *
 * Контроллер для общения между пользователями
 */
@Controller
public class MessageController extends PanelBaseController {

    private static final String ACTIVE = "im";

    private static final int COUNT = 100;

    @RequestMapping("/im")
    public String imAction(ModelMap model) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        setModelMap(model, t.g("Диалоги"), ACTIVE);

        return security.getTemplate() + "im/list";
    }

    @RequestMapping("/im/{userId}")
    public String chatAction(@PathVariable int userId, ModelMap model) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return "redirect:/";
        }

        // проверим, что пользователь, с которым хотим пообщаться существует
        User user = new UserDAOImpl().get(userId);
        if (user == null) {
            return "redirect:/";
        }

        // не можем организовать чат с самим собой
        if (user.getId() == security.getUser().getId()) {
            return "redirect:/";
        }

        // делаем просмотренными все сообщения диалога
        new MessageDAOImpl().shown(security.getUser(), user);

        setModelMap(model, t.g("Сообщения"), ACTIVE);
        model.addAttribute("user", user);

        return security.getTemplate() + "im/chat";
    }

    @RequestMapping(value = "/im/{userId}", method = RequestMethod.POST)
    public @ResponseBody List<Message> listMessagesPostAjax(@PathVariable int userId) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return null;
        }

        // проверим, что пользователь, с которым хотим пообщаться существует
        User user = new UserDAOImpl().get(userId);
        if (user == null) {
            return null;
        }

        // не можем организовать чат с самим собой
        if (user.getId() == security.getUser().getId()) {
            return null;
        }

        // если ранее уже общались, то загрузим последние сообщения
        List<Message> messages = new MessageDAOImpl().chat(security.getUser(), user, 0, COUNT);

        return messages;
    }

    @RequestMapping(value = "/im/last", method = RequestMethod.POST)
    public @ResponseBody List<Message> listLastMessagesPostAjax() {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return null;
        }

        List<Message> messages = new MessageDAOImpl().last(security.getUser(), 0, COUNT);

        return messages;
    }

    @RequestMapping("/im/send/{userId}")
    public @ResponseBody Response sendMessagePostAjax(
            @PathVariable int userId,
            @RequestParam(value = "message") String text) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return new Response(-1, ResponseCode.CODES.get(-1));
        }

        // проверяем корректность пользователя получателя
        User user = new UserDAOImpl().get(userId);
        if (user == null) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }

        // запрещено отправлять сообщение самому себе
        if (user.getId() == security.getUser().getId()) {
            return new Response(-1, ResponseCode.CODES.get(-1));
        }

        // отправляем сообщение пользователю и сохраняем его в БД
        Message message = new Message(security.getUser(), user, text);
        new MessageDAOImpl().save(message);
        if (template != null) {
            template.convertAndSend("/im/" + user.getConfirmCode(), message);
        }

        return new Response(0, ResponseCode.CODES.get(0), message);
    }

    @RequestMapping(value = "/im/shown/{messageId}", method = RequestMethod.POST)
    public @ResponseBody Response shownMessageAjax(@PathVariable int messageId) {

        if (!security.isAuth()) {
            // только для авторизованных пользователей
            return new Response(-1, ResponseCode.CODES.get(-1));
        }

        // переводим сообщение в статус просмотренного
        new MessageDAOImpl().shown(new Message(messageId));

        return new Response(0, ResponseCode.CODES.get(0));
    }
}