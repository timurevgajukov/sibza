package org.sibza.site;

import org.sibza.cache.Categories;
import org.sibza.cache.DSLDictionaryStatuses;
import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.dsl.DSLStatus;
import org.sibza.model.dsl.DictionaryItem;
import org.sibza.model.dsl.dao.DictionaryDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Message;
import org.sibza.utils.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Контроллер для работы с загруженными dsl-словарями
 */
@Controller
public class DictionaryDSLController extends PanelBaseController {

    private static final String ACTIVE = "dictionary-dsl";

    private static final int COUNT = 10;

    private int currentPage;

    @RequestMapping("/dsl")
    public String listAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для работы с dsl-словарем
            return "redirect:/";
        }

        currentPage = 1;

        DSLDictionaryStatuses statuses = DSLDictionaryStatuses.getInstance();

        setModelMap(model, "DSL-словарь", ACTIVE);
        model.addAttribute("list", new DictionaryDAOImpl().list(statuses.get(DSLStatus.NEW), 0, COUNT));

        return security.getTemplate() + "dsl/list";
    }

    @RequestMapping("/dsl/more")
    public String listMoreAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для работы с dsl-словарем
            return "redirect:/";
        }

        currentPage++;

        DSLDictionaryStatuses statuses = DSLDictionaryStatuses.getInstance();

        setModelMap(model, "DSL-словарь", ACTIVE);
        model.addAttribute("list", new DictionaryDAOImpl().list(statuses.get(DSLStatus.NEW), (currentPage - 1) * COUNT, COUNT));

        return security.getTemplate() + "dsl/list";
    }

    @RequestMapping(value = "/dsl/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(@PathVariable int id, ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для обработки слова из dsl-словаря
            return "redirect:/";
        }

        DictionaryItem item = new DictionaryDAOImpl().get(id);

        Languages languages = Languages.getInstance();

        List<Word> similarWords = new WordDAOImpl().search(item.getWord());
        for (Word word : similarWords) {
            word.loadTranslates();
        }

        setModelMap(model, "Обработка DSL-словаря", ACTIVE);
        model.addAttribute("item", item);
        model.addAttribute("similarWords", similarWords);
        model.addAttribute("defaultCategory", new Category(26)); // категория "Разное"
        model.addAttribute("categories", new CategoryDAOImpl().list(null, languages.get(Language.RU)));

        return security.getTemplate() + "dsl/edit";
    }

    @RequestMapping(value = "/dsl/edit/{id}", method = RequestMethod.POST)
    public String editPostAction(
            @PathVariable int id,
            @RequestParam(value = "word") String wordString,
            @RequestParam(value = "translates") String translatesString,
            @RequestParam(value = "category") int categoryId) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для обработки слова из dsl-словаря
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();
        Category category = new CategoryDAOImpl().get(categoryId, languages.get(Language.RU));

        Word word = new WordDAOImpl().get(wordString.trim());
        if (word == null) {
            // новое слово
            // формируем объект основного слова и сохраняем его
            word = new Word();
            word.setWord(Utils.firstUpperCase(wordString.trim()));
            word.setLanguage(languages.get(Language.AD));
            word.setCategory(category);
            new WordDAOImpl().save(word, security.getUser());
        }

        boolean isProcessed = false;

        if (translatesString.indexOf(">>") > -1) {
            // необходимо взять перевод от другого слова
            Word otherWord = new WordDAOImpl().get(translatesString.replaceAll(">>", "").trim());
            if (otherWord != null) {
                // устанавливаем такую же категорию, как у слова донора
                if (word.getCategory().getId() != otherWord.getCategory().getId()) {
                    word.setCategory(otherWord.getCategory());
                    new WordDAOImpl().save(word, security.getUser());
                }

                List<Word> translates = new WordDAOImpl().translate(otherWord);
                if (translates != null && translates.size() != 0) {
                    for (Word translate : translates) {
                        // предварительно удаляем связь, если такая имеется
                        // это нужно, если ранее мы уже добавляли эти слова
                        new WordDAOImpl().delete(word, translate, security.getUser());

                        // связываем двунаправленной связью с основным словом
                        new WordDAOImpl().save(word, translate, translate.getNote(), true, security.getUser());
                    }

                    isProcessed = true;
                }
            }
        } else {
            // сохраняем слова переводов
            String[] translates = translatesString.split("##");
            for (String translateWithNoteString : translates) {
                // формируем объект слова перевода, сохраняем его, а после связываем с основным
                Word translate = new Word();
                String[] translateWithNote = translateWithNoteString.split("@@");
                String translateString = translateWithNote[0].trim();
                String note = null;
                if (translateWithNote.length == 2) {
                    note = translateWithNote[1].trim();
                }

                // проверяем слово на дубликаты
                Word duplicate = new WordDAOImpl().get(translateString);
                if (duplicate == null) {
                    // все в порядке, дубликатов не нашли
                    translate.setWord(Utils.firstUpperCase(translateString));
                    translate.setLanguage(languages.get(Language.RU));
                    translate.setCategory(category);
                    new WordDAOImpl().save(translate, security.getUser());
                } else {
                    // найден дубликат
                    translate = duplicate;
                }

                // предварительно удаляем связь, если такая имеется
                // это нужно, если ранее мы уже добавляли эти слова
                new WordDAOImpl().delete(word, translate, security.getUser());

                // связываем двунаправленной связью с основным словом
                new WordDAOImpl().save(word, translate, Utils.firstUpperCase(note), true, security.getUser());
            }

            isProcessed = true;
        }

        if (isProcessed) {
            // переводим слово из dsl-словаря в статус обработанного
            DSLDictionaryStatuses statuses = DSLDictionaryStatuses.getInstance();
            DictionaryItem item = new DictionaryDAOImpl().get(id);
            item.setStatus(statuses.get(DSLStatus.PROCESSED));
            new DictionaryDAOImpl().save(item, security.getUser());
        }

        return "redirect:/dsl";
    }

    @RequestMapping("/dsl/postponed/request/{id}")
    public String postponedRequestAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав
            return "redirect:/";
        }

        String msg = t.g("Вы уверены, что хотите отложить элемент DSL-словаря?") +
                " <a href='/dsl/postponed/" + id + "'>" + t.g("Отложить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return "redirect:/dsl";
    }

    @RequestMapping("/dsl/postponed/{id}")
    public String postponedAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав
            return "redirect:/";
        }

        DSLDictionaryStatuses statuses = DSLDictionaryStatuses.getInstance();

        DictionaryItem item = new DictionaryDAOImpl().get(id);
        item.setStatus(statuses.get(DSLStatus.POSTPONED));

        new DictionaryDAOImpl().save(item, security.getUser());

        return "redirect:/dsl";
    }

    @RequestMapping("/dsl/delete/request/{id}")
    public String deleteRequestAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для удаления элемента dsl-словаря
            return "redirect:/";
        }

        String msg = t.g("Вы уверены, что хотите удалить элемент DSL-словаря?") +
                " <a href='/dsl/delete/" + id + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return "redirect:/dsl";
    }

    @RequestMapping("/dsl/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для удаления элемента dsl-словаря
            return "redirect:/";
        }

        DSLDictionaryStatuses statuses = DSLDictionaryStatuses.getInstance();

        DictionaryItem item = new DictionaryDAOImpl().get(id);
        item.setStatus(statuses.get(DSLStatus.DELETED));

        new DictionaryDAOImpl().save(item, security.getUser());

        return "redirect:/dsl";
    }
}