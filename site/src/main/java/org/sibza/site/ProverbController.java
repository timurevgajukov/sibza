package org.sibza.site;

import org.sibza.model.comment.Comment;
import org.sibza.model.comment.dao.CommentDAOImpl;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.proverb.Proverb;
import org.sibza.cache.Languages;
import org.sibza.model.language.Language;
import org.sibza.model.proverb.dao.ProverbDAOImpl;
import org.sibza.model.user.User;
import org.sibza.security.Message;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 30.05.15.
 *
 * Контроллер для работы с пословицами
 */
@Controller
public class ProverbController extends PanelBaseController {

    private static final String ACTIVE = "proverbs";

    private static final int COUNT = 10;

    private int currentPage;

    @RequestMapping("/proverbs")
    public String listAction(ModelMap model) {

        Languages languages = Languages.getInstance();

        currentPage = 1;

        setModelMap(model, t.g("Пословицы"), ACTIVE);
        model.addAttribute("proverbs", new ProverbDAOImpl().list(languages.get(Language.AD), 0, COUNT));

        return security.getTemplate() + "proverb/list";
    }

    @RequestMapping("/proverbs/more")
    public String listMoreAction(ModelMap model) {

        Languages languages = Languages.getInstance();

        currentPage++;

        setModelMap(model, t.g("Пословицы"), ACTIVE);
        model.addAttribute("proverbs", new ProverbDAOImpl().list(languages.get(Language.AD), (currentPage - 1) * COUNT, COUNT));

        return security.getTemplate() + "proverb/list";
    }

    @RequestMapping(value = "/proverbs/add", method = RequestMethod.GET)
    public String addGetAction(ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        setModelMap(model, t.g("Добавление пословицы"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());

        return security.getTemplate() + "proverb/edit";
    }

    @RequestMapping(value = "/proverbs/add", method = RequestMethod.POST)
    public String addPostAction(
            @RequestParam(value = "language") int languageId,
            @RequestParam(value = "proverb") String body,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Languages languages = Languages.getInstance();

        Proverb proverb = new Proverb();
        proverb.setLanguage(languages.get(languageId));
        proverb.setBody(body);
        if (new ProverbDAOImpl().save(proverb, security.getUser())) {
            return "redirect:/proverbs";
        }

        setModelMap(model, t.g("Добавление пословицы"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());

        return security.getTemplate() + "proverb/edit";
    }

    @RequestMapping(value = "/proverbs/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(ModelMap model, @PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Proverb proverb = new ProverbDAOImpl().get(id);

        setModelMap(model, t.g("Редактирование пословицы"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("proverb", proverb);

        return security.getTemplate() + "proverb/edit";
    }

    @RequestMapping(value = "/proverbs/edit/{id}", method = RequestMethod.POST)
    public String editPostAction(
            @PathVariable int id,
            @RequestParam(value = "language") int languageId,
            @RequestParam(value = "proverb") String body,
            ModelMap model) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        Proverb proverb = new ProverbDAOImpl().get(id);
        proverb.setBody(body);
        if (new ProverbDAOImpl().save(proverb, security.getUser())) {
            return "redirect:/proverbs";
        }

        // не удалось сохранить корректно, сообщаем об этом и отображаем форму редактирования
        setModelMap(model, t.g("Редактирование пословицы"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("proverb", proverb);

        return security.getTemplate() + "proverb/edit";
    }

    @RequestMapping("/proverbs/delete/request/{id}")
    public String deleteRequestAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        String msg = t.g("Вы уверены, что хотите удалить пословицу?") +
                " <a href='/proverbs/delete/" + id + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return "redirect:/proverbs";
    }

    @RequestMapping("/proverbs/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        new ProverbDAOImpl().delete(new Proverb(id), security.getUser());

        return "redirect:/proverbs";
    }

    @RequestMapping("/proverbs/comments/list/{id}")
    public @ResponseBody List<Comment> commentsListAjax(@PathVariable int id) {

        // TODO: пока берем тестового пользователя с Московской временной зоной
        List<Comment> comments = new CommentDAOImpl().list(new User(2), new Proverb(id));
        if (security.isAuth()) {
            for (Comment comment : comments) {
                if (comment.getUser().getId() == security.getUser().getId()) {
                    comment.setEditable(true);
                }
            }
        }
        return comments;
    }

    @RequestMapping(value = "/proverbs/comments/add", method = RequestMethod.POST)
    public @ResponseBody Comment commentsAddAjax(
            @RequestParam(value = "id") int id,
            @RequestParam(value = "comment") String body) {

        if (!security.isAuth()) {
            return null;
        }

        if (id == 0) {
            return null;
        }

        if (body == null || body.trim().length() == 0) {
            return null;
        }

        Comment comment = new Comment(security.getLanguage(), security.getUser(), body.trim());
        new CommentDAOImpl().save(comment);
        new CommentDAOImpl().save(comment, new Proverb(id));

        return comment;
    }
}