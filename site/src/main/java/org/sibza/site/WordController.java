package org.sibza.site;

import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.category.Category;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.notification.Notification;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.model.word.validate.CategoryRule;
import org.sibza.model.word.validate.LanguageRule;
import org.sibza.model.word.validate.WordRule;
import org.sibza.security.Message;
import org.sibza.services.NotificationService;
import org.sibza.validate.IValidateRule;
import org.sibza.validate.Validator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 13.05.15.
 *
 * Контроллер для работы со словами
 */
@Controller
public class WordController extends PanelBaseController {

    private static final String ACTIVE = "dictionary";

    @RequestMapping(value = "/words/add", method = RequestMethod.GET)
    public String addGetAction(
            @RequestParam(value = "category", required = false) Integer categoryId,
            ModelMap model) {

        String redirect = "redirect:/dictionary";

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для добавления слов
            return redirect;
        }

        Category category = null;
        if (categoryId != null) {
            category = new CategoryDAOImpl().get(categoryId, security.getLanguage());
        }

        setModelMap(model, t.g("Добавление слова"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("currentCategory", category);
        model.addAttribute("categories", new CategoryDAOImpl().list(null, security.getLanguage()));
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));

        return security.getTemplate() + "word/edit";
    }

    @RequestMapping(value = "/words/add", method = RequestMethod.POST)
    public String addPostAction(
            @RequestParam(value = "category", required = false, defaultValue = "0") Integer categoryId,
            @RequestParam(value = "language", required = false, defaultValue = "0") Integer languageId,
            @RequestParam(value = "word", required = false) String wordStr,
            @RequestParam(value = "resource", required = false) Integer resourceId) {

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав добавления новых слов
            return "redirect:/";
        }

        Word word = new Word();
        word.setCategory(new Category(categoryId));
        word.setLanguage(new Language(languageId));
        word.setWord(wordStr.trim());

        // предварительно проверим на дубликаты
        Word duplicate = new WordDAOImpl().get(word.getWord());
        if (duplicate != null) {
            // такое слово уже имеется в словаре
            security.addMessage(t.g("Такое слово найдено"), Message.TYPE_WARNING);
            return "redirect:/dictionary/category/" + duplicate.getCategory().getId();
        }

        Validator validator = new Validator(word, getValidateRules());
        if (validator.validate()) {
            new WordDAOImpl().save(word, security.getUser());

            // если имеется, то добавляем озвучку слова
            if (resourceId != null && resourceId != 0 && resourceId != -1) {
                new WordDAOImpl().save(word, new Resource(resourceId), security.getUser());
            }

            security.addMessage(t.g("Успешно добавили новое слово"), Message.TYPE_SUCCESS);
            if (security.getUser().getId() != Notification.ADMIN_ID) {
                new NotificationService(template).send(
                        Notification.create(
                                "Пользователь " + security.getUser().getEmail() + " добавил новое слово \""
                                        + word.getWord() + "\" на " + word.getLanguage().getSmallName(),
                                Message.TYPE_INFO));
            }

            return "redirect:/dictionary/category/" + word.getCategory().getId();
        } else {
            security.addMessages(validator.getErrorMessages(), Message.TYPE_DANGER);

            if (word.getCategory().getId() != 0) {
                return "redirect:/dictionary/category/" + categoryId;
            } else {
                return "redirect:/dictionary";
            }
        }
    }

    @RequestMapping(value = "/words/edit/{id}", method = RequestMethod.GET)
    public String editGetAction(ModelMap model, @PathVariable int id) {

        Word word = new WordDAOImpl().get(id);
        if (word == null) {
            // не нашли слово для редактирования
            return "redirect:/dictionary";
        }

        String redirect = "redirect:/dictionary/category/" + word.getCategory().getId();

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы редактировать слова
            return redirect;
        }

        setModelMap(model, t.g("Редактирование слова"), ACTIVE);
        model.addAttribute("languages", new LanguageDAOImpl().list());
        model.addAttribute("categories", new CategoryDAOImpl().list(null, security.getLanguage()));
        model.addAttribute("resources", new ResourceDAOImpl().list(Type.AUDIO, true));
        model.addAttribute("word", word);
        model.addAttribute("currentCategory", word.getCategory());

        return security.getTemplate() + "word/edit";
    }

    @RequestMapping(value = "/words/edit/{id}", method = RequestMethod.POST)
    public String editPostAction(
            @PathVariable int id,
            @RequestParam(value = "category", required = false, defaultValue = "0") Integer categoryId,
            @RequestParam(value = "language", required = false, defaultValue = "0") int languageId,
            @RequestParam(value = "word", required = false) String wordStr,
            @RequestParam(value = "resource", required = false) Integer resourceId) {

        Word word = new WordDAOImpl().get(id);
        if (word == null) {
            // не нашли слово для редактирования
            return "redirect:/dictionary";
        }

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав для редактирования слова
            return "redirect:/dictionary/category/" +  word.getCategory().getId();
        }

        word.setCategory(new Category(categoryId));
        word.setLanguage(new Language(languageId));
        word.setWord(wordStr.trim());

        // предварительно проверим на дубликаты
        Word duplicate = new WordDAOImpl().get(word.getWord());
        if (duplicate != null && word.getId() != duplicate.getId()) {
            // такое слово уже имеется в словаре это не то слово, которое мы изначально редактировали
            security.addMessage(t.g("Такое слово найдено"), Message.TYPE_WARNING);
            return "redirect:/dictionary/category/" + duplicate.getCategory().getId();
        }

        Validator validator = new Validator(word, getValidateRules());
        if (validator.validate()) {
            new WordDAOImpl().save(word, security.getUser());

            List<Resource> resources = word.getResources();
            if (resources.size() != 0) {
                // пока поддерживается только связка с одним ресурсом
                new WordDAOImpl().delete(word, resources.get(0), security.getUser());
            }
            if (resourceId != null && resourceId != 0 && resourceId != -1) {
                new WordDAOImpl().save(word, new Resource(resourceId), security.getUser());
            }

            security.addMessage(t.g("Успешно обновили слово"), Message.TYPE_SUCCESS);
            if (security.getUser().getId() != Notification.ADMIN_ID) {
                new NotificationService(template).send(
                        Notification.create(
                                "Пользователь " + security.getUser().getEmail() + " обновил слово \""
                                        + word.getWord() + "\" на " + word.getLanguage().getSmallName(),
                                Message.TYPE_WARNING));
            }

            return "redirect:/dictionary/category/" + word.getCategory().getId();
        } else {
            security.addMessages(validator.getErrorMessages(), Message.TYPE_DANGER);

            if (word.getCategory().getId() != 0) {
                return "redirect:/dictionary/category/" + categoryId;
            } else {
                return "redirect:/dictionary";
            }
        }
    }

    @RequestMapping("/words/delete/request/{id}")
    public String deleteRequestAction(@PathVariable int id) {

        Word word = new WordDAOImpl().get(id);
        if (word == null) {
            // не нашли слово для удаления
            return "redirect:/dictionary";
        }

        String redirect = "redirect:/dictionary/category/" + word.getCategory().getId();

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы удалить слово
            return redirect;
        }

        String msg = t.g("Вы уверены, что хотите удалить слово?") +
                " <a href='/words/delete/" + id + "'>" + t.g("Удалить") + "</a>";
        security.addMessage(msg, Message.TYPE_WARNING);

        return redirect;
    }

    @RequestMapping("/words/delete/{id}")
    public String deleteAction(@PathVariable int id) {

        Word word = new WordDAOImpl().get(id);
        if (word == null) {
            // не нашли слово для удаления
            return "redirect:/dictionary";
        }

        String redirect = "redirect:/dictionary/category/" + word.getCategory().getId();

        if (!security.isAuth() || !security.isAdminOrEditor()) {
            // не достаточно прав, чтобы удалить слово
            return redirect;
        }

        new WordDAOImpl().delete(word, security.getUser());
        if (security.getUser().getId() != Notification.ADMIN_ID) {
            new NotificationService(template).send(
                    Notification.create(
                            "Пользователь " + security.getUser().getEmail() + " удалил слово \""
                                    + word.getWord() + "\" на " + word.getLanguage().getSmallName(),
                            Message.TYPE_DANGER));
        }

        return redirect;
    }

    private List<IValidateRule> getValidateRules() {

        List<IValidateRule> rules = new ArrayList<IValidateRule>();
        rules.add(new WordRule(t.g("Добавляемое слово не должно быть пустым")));
        rules.add(new CategoryRule(t.g("Необходимо указать категорию слова")));
        rules.add(new LanguageRule(t.g("Необходимо указать язык слова")));

        return rules;
    }
}