package org.sibza.site;

import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.category.dao.CategoryDAOImpl;
import org.sibza.model.dsl.DictionaryItem;
import org.sibza.model.dsl.dao.DictionaryDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.proverb.dao.ProverbDAOImpl;
import org.sibza.model.resource.ContentType;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.security.Message;
import org.sibza.utils.Net;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 01.10.15.
 * <p/>
 * Контроллер для импорта данных на сайт
 */
@Controller
public class ImportController extends PanelBaseController {

    private static final String ACTIVE = "import";

    @RequestMapping(value = "/utility/import", method = RequestMethod.GET)
    public String importGetАction(ModelMap model) {

        if (!security.isAuth() || !security.isAdmin()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        setModelMap(model, t.g("Импорт данных"), ACTIVE);

        return security.getTemplate() + "utility/import";
    }

    @RequestMapping(value = "/utility/import-words", method = RequestMethod.POST)
    public String importWordsPostAction(@RequestParam(value = "file") MultipartFile file) {

        if (!security.isAuth() || !security.isAdmin()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        System.out.println("Upload file: " + file.getOriginalFilename());
        System.out.println("Upload file MIME-type: " + file.getContentType());

        try {
            String data = new String(file.getBytes(), "UTF-8");
            String[] lines = data.split("\n");
            for (int i = 1; i < lines.length; i++) {
                // обрабатываем каждую строку
                String[] items = lines[i].split("\t");

                // работаем над категорией
                Category category = null;
                String categoryName = items[0].trim();
                if (categoryName.length() != 0) {
                    // пытаемся найти категорию с таким именем
                    List<Category> categories = new CategoryDAOImpl().search(categoryName);
                    if (categories == null || categories.size() == 0) {
                        // сохраняем новую категорию в БД и присваиваем ее переменной category
                        category = new Category();
                        category.setName(categoryName);
                        new CategoryDAOImpl().save(category, security.getUser());

                        System.out.println("Новая категория: " + categoryName);
                    } else {
                        // нашли похожие категории
                        for (Category item : categories) {
                            if (categoryName.equalsIgnoreCase(item.getName())) {
                                category = item;
                            }
                        }

                        if (category == null) {
                            // сохраняем новую категорию в БД и присваиваем ее переменной category
                            category = new Category();
                            category.setName(categoryName);
                            new CategoryDAOImpl().save(category, security.getUser());

                            System.out.println("Новая категория: " + categoryName);
                        }
                    }
                }

                Languages languages = Languages.getInstance();

                // работаем над первым словом
                Word word1 = null;
                String wordStr = items[1].trim();
                if (wordStr.length() != 0) {
                    // патуемся найти такое слово
                    List<Word> words = new WordDAOImpl().search(wordStr);
                    if (words == null || words.size() == 0) {
                        // сохраняем слово в БД и присваиваем ее переменной word1
                        word1 = new Word();
                        word1.setCategory(category);
                        word1.setLanguage(languages.get(Language.RU));
                        word1.setWord(wordStr);
                        new WordDAOImpl().save(word1, security.getUser());

                        System.out.println("Новое слово: " + wordStr);
                    } else {
                        // нашли похожие слова
                        for (Word item : words) {
                            if (wordStr.equalsIgnoreCase(item.getWord())) {
                                word1 = item;
                            }
                        }

                        if (word1 == null) {
                            // сохраняем слово в БД и присваиваем ее переменной word1
                            word1 = new Word();
                            word1.setCategory(category);
                            word1.setLanguage(languages.get(Language.RU));
                            word1.setWord(wordStr);
                            new WordDAOImpl().save(word1, security.getUser());

                            System.out.println("Новое слово: " + wordStr);
                        }
                    }
                }

                // работаем над вторым словом
                Word word2 = null;
                wordStr = items[2].trim();
                if (wordStr.length() != 0) {
                    // патуемся найти такое слово
                    List<Word> words = new WordDAOImpl().search(wordStr);
                    if (words == null || words.size() == 0) {
                        // сохраняем слово в БД и присваиваем ее переменной word2
                        word2 = new Word();
                        word2.setCategory(category);
                        word2.setLanguage(languages.get(Language.AD));
                        word2.setWord(wordStr);
                        new WordDAOImpl().save(word2, security.getUser());

                        System.out.println("Новое слово: " + wordStr);
                    } else {
                        // нашли похожие слова
                        for (Word item : words) {
                            if (wordStr.equalsIgnoreCase(item.getWord())) {
                                word2 = item;
                            }
                        }

                        if (word2 == null) {
                            // сохраняем слово в БД и присваиваем ее переменной word2
                            word2 = new Word();
                            word2.setCategory(category);
                            word2.setLanguage(languages.get(Language.AD));
                            word2.setWord(wordStr);
                            new WordDAOImpl().save(word2, security.getUser());

                            System.out.println("Новое слово: " + wordStr);
                        }
                    }
                }

                if (word2 != null && items.length > 3) {
                    // работаем с аудио данными если имеются
                    String audioUrl = items[3].trim();
                    if (audioUrl.length() != 0) {
                        // TODO: загружаем в БД аудио ресурс
                    }
                }

                if (word1 == null || word2 == null) {
                    System.out.println("Что-то пошло не так и одно из слов отсутствует");
                } else {
                    // если ранее уже были связки переводов между этому двуми словами, то удаляем их
                    new WordDAOImpl().delete(word1, word2, security.getUser());

                    // сохраняем связку двустороннего перевода
                    new WordDAOImpl().save(word1, word2, null, true, security.getUser());
                }
            }

            security.addMessage(t.g("Ура! Мы успешно импортировали все данные!"), Message.TYPE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            security.addMessage(t.g("Что-то пошло не так и мы не смогли импортировать все данные"), Message.TYPE_DANGER);
        }

        return "redirect:/utility/import";
    }

    @RequestMapping(value = "/utility/import-media", method = RequestMethod.POST)
    public String importMediaPostAction(@RequestParam(value = "file") MultipartFile file) {

        if (!security.isAuth() || !security.isAdmin()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        System.out.println("Upload file: " + file.getOriginalFilename());
        System.out.println("Upload file MIME-type: " + file.getContentType());

        try {
            String data = new String(file.getBytes(), "UTF-8");
            String[] lines = data.split("\n");
            for (int i = 1; i < lines.length; i++) {
                // обрабатываем каждую строку
                String[] items = lines[i].split("\t");

                String wordStr = items[2];
                String mediaFileUrl = items[3];

                if (mediaFileUrl != null && mediaFileUrl.trim().length() != 0) {
                    // указан медиафайл и его нужно загрузить, если к слову еще не привязан ресурс
                    // ищем слово
                    Word word = null;
                    List<Word> words = new WordDAOImpl().search(wordStr);
                    if (words != null && words.size() != 0) {
                        for (Word item : words) {
                            if (wordStr.equalsIgnoreCase(item.getWord())) {
                                word = item;
                            }
                        }
                    }

                    if (word != null) {
                        // слово нашли и теперь вроверяем на наличие привязанных ресурсов
                        List<Resource> resources = word.getResources();
                        if (resources == null || resources.size() == 0) {
                            // пока не присоединен ни один медиа ресурс
                            // скачиваем файл и присоединяем его к слову
                            byte[] resourceBytes = Net.getByteArray(mediaFileUrl);

                            if (resourceBytes != null && resourceBytes.length != 0) {
                                Resource resource = new Resource(new ContentType(2), word.getWord() + ".mp3", resourceBytes);
                                new ResourceDAOImpl().save(resource, security.getUser());
                                new WordDAOImpl().save(word, resource, security.getUser());
                            }
                        }
                    }
                }
            }

            security.addMessage(t.g("Ура! Мы успешно импортировали все данные!"), Message.TYPE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            security.addMessage(t.g("Что-то пошло не так и мы не смогли импортировать все данные"), Message.TYPE_DANGER);
        }

        return "redirect:/utility/import";
    }

    @RequestMapping(value = "/utility/import-proverbs", method = RequestMethod.POST)
    public String importProverbsPostAction(@RequestParam(value = "file") MultipartFile file) {

        if (!security.isAuth() || !security.isAdmin()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        System.out.println("Upload file: " + file.getOriginalFilename());
        System.out.println("Upload file MIME-type: " + file.getContentType());

        Languages languages = Languages.getInstance();

        try {
            String data = new String(file.getBytes(), "UTF-8");
            String[] lines = data.split("\r\n");
            for (int i = 0; i < lines.length; i++) {
                String proverbStr = lines[i];
                // пытаемся найти такую пословицу в БД
                List<Proverb> proverbs = new ProverbDAOImpl().search(proverbStr);
                if (proverbs == null || proverbs.size() == 0) {
                    // не нашли и можем добавить
                    Proverb proverb = new Proverb(0, languages.get(Language.AD), proverbStr);
                    new ProverbDAOImpl().save(proverb, security.getUser());
                } else {
                    System.out.println("Пословица найдена: " + proverbStr);
                }
            }

            security.addMessage(t.g("Ура! Мы успешно импортировали все данные!"), Message.TYPE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            security.addMessage(t.g("Что-то пошло не так и мы не смогли импортировать все данные"), Message.TYPE_DANGER);
        }

        return "redirect:/utility/import";
    }

    @RequestMapping(value = "/utility/import-dsl", method = RequestMethod.POST)
    public String importDSLPostAction(@RequestParam(value = "file") MultipartFile file) {

        if (!security.isAuth() || !security.isAdmin()) {
            // не достаточно прав, чтобы находится в панели администратора
            return "redirect:/";
        }

        System.out.println("Upload file: " + file.getOriginalFilename());
        System.out.println("Upload file MIME-type: " + file.getContentType());

        Languages languages = Languages.getInstance();

        try {
            String data = new String(file.getBytes(), "UTF-8");
            String[] lines = data.split("\r\n");
            for (int i = 0; i < lines.length; i = i + 2) {
                // сохраняем данные в БД
                DictionaryItem item = new DictionaryItem(lines[i], lines[i + 1]);
                new DictionaryDAOImpl().save(item, security.getUser());
            }

            security.addMessage(t.g("Ура! Мы успешно импортировали все данные!"), Message.TYPE_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            security.addMessage(t.g("Что-то пошло не так и мы не смогли импортировать все данные"), Message.TYPE_DANGER);
        }

        return "redirect:/utility/import";
    }
}