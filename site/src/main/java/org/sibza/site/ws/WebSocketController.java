package org.sibza.site.ws;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by timur on 03.03.16.
 *
 * Контроллер для работы с websocket сообщениями
 */
@Controller
public class WebSocketController {

    @MessageMapping("/sibza")
    @SendTo("/info/hello")
    public String helloAction(String message) {

        return "Hello from Websocket";
    }
}