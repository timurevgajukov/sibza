package org.sibza.site;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by timur on 22.11.15.
 *
 * Контроллер для отображения помощи
 */
@Controller
public class HelpController extends PanelBaseController {

    private static final String ACTIVE = "help";

    @RequestMapping("/help")
    public String indexAction() {

        return "redirect:/help/developer";
    }

    @RequestMapping("/help/developer")
    public String developerAction(ModelMap model) {

        setModelMap(model, "Sibza API ver. 1.0", ACTIVE);

        return security.getTemplate() + "help/developer";
    }
}