package org.sibza.model.site;

import org.sibza.model.Model;
import org.sibza.model.language.Language;

/**
 * Created by Тимур on 19.11.2015.
 *
 * Модель для текстового элемента интерфейса сайта
 */
public class SiteText extends Model {

    private String key;
    private String value;
    private Language language;

    public SiteText(String key, String value, Language language) {

        super();
        this.key = key;
        this.value = value;
        this.language = language;
    }

    public SiteText(int id, String key, String value, Language language) {

        super(id);
        this.key = key;
        this.value = value;
        this.language = language;
    }

    public String getKey() {

        return key;
    }

    public void setKey(String key) {

        this.key = key;
    }

    public String getValue() {

        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    @Override
    public String toString() {

        return value;
    }
}