package org.sibza.model.site.dao;

import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.site.SiteText;
import org.sibza.model.site.dao.interfaces.SiteTextDAO;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 19.11.2015.
 *
 * Реализация интерфейса для работы с текстами сайта
 */
public class SiteTextDAOImpl extends ModelDAOImpl implements SiteTextDAO {

    private static final String QUERY_GET_BY_KEY_AND_LANGUAGE = "{ call site_texts_get_by_key_and_language(?,?) }";
    private static final String QUERY_LIST_BY_LANGUAGE = "{ call site_texts_list_by_language(?) }";
    private static final String QUERY_LIST_BY_KEY = "{ call site_texts_list_by_key(?) }";
    private static final String QUERY_LIST_KEYS = "{ call site_texts_list_keys() }";
    private static final String QUERY_SAVE = "{ call site_texts_save(?,?,?,?) }";

    @Override
    public SiteText get(String key, final Language language) {

        List<SiteText> list = jdbcTemplate.query(QUERY_GET_BY_KEY_AND_LANGUAGE, new Object[]{key, language.getId()},
                new RowMapper<SiteText>() {

                    @Override
                    public SiteText mapRow(ResultSet resultSet, int i) throws SQLException {

                        SiteText siteText = load(resultSet);
                        siteText.setLanguage(language);

                        return siteText;
                    }
                });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    public List<SiteText> list(final Language language) {

        List<SiteText> list = jdbcTemplate.query(QUERY_LIST_BY_LANGUAGE, new RowMapper<SiteText>() {

            @Override
            public SiteText mapRow(ResultSet resultSet, int i) throws SQLException {

                SiteText siteText = load(resultSet);
                siteText.setLanguage(language);

                return siteText;
            }
        }, language.getId());

        return list;
    }

    @Override
    public List<SiteText> list(String key) {

        final Languages languages = Languages.getInstance();

        List<SiteText> list = jdbcTemplate.query(QUERY_LIST_BY_KEY, new RowMapper<SiteText>() {

            @Override
            public SiteText mapRow(ResultSet resultSet, int i) throws SQLException {

                SiteText siteText = load(resultSet);
                siteText.setLanguage(languages.get(siteText.getLanguage().getId()));

                return siteText;
            }
        }, key);

        return list;
    }

    @Override
    public List<String> keys() {

        List<String> list = jdbcTemplate.query(QUERY_LIST_KEYS, new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getString("text_key");
            }
        });

        return list;
    }

    @Override
    public boolean save(SiteText text) {

        SiteText newObject = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        text.getId() == 0 ? null : text.getId(),
                        text.getKey(),
                        text.getValue(),
                        text.getLanguage().getId()
                }, new RowMapper<SiteText>() {

                    @Override
                    public SiteText mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        text.setId(newObject.getId());

        return true;
    }

    private SiteText load(ResultSet rs) throws SQLException {

        Language language = new Language(rs.getInt("language_id"));

        return new SiteText(rs.getInt("id"), rs.getString("text_key"), rs.getString("text_value"), language);
    }
}