package org.sibza.model.site.dao.interfaces;

import org.sibza.model.language.Language;
import org.sibza.model.site.SiteText;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 19.11.2015.
 *
 * Интерфейс для работы с текстами для сайта
 */
public interface SiteTextDAO {

    /**
     * Возвращает данные по ключу и языку
     *
     * @param key
     * @param language
     * @return
     */
    SiteText get(String key, Language language);

    /**
     * Возвращает список текстов для сайта с определенным языком
     *
     * @param language
     * @return
     */
    List<SiteText> list(Language language);

    /**
     * Возвращает список текстов по ключу
     *
     * @param key
     * @return
     */
    List<SiteText> list(String key);

    /**
     * Возвращает список уникальных ключей
     *
     * @return
     */
    List<String> keys();

    /**
     * Сохраняет текст для интерфейса сайта
     *
     * @param text
     * @return
     */
    boolean save(SiteText text);
}
