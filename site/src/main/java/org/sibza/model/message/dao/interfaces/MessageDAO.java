package org.sibza.model.message.dao.interfaces;

import org.sibza.model.message.Message;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 08.03.16.
 *
 * Интерфейс для работы с сообщениями между пользователями
 */
public interface MessageDAO {

    /**
     * Возвращает переписку между двумя пользователями
     *
     * @param user1
     * @param user2
     * @param offset
     * @param count
     * @return
     */
    List<Message> chat(User user1, User user2, int offset, int count);

    /**
     * Возвращает список последних сообщений и диалогах пользователя
     *
     * @param user
     * @param offset
     * @param count
     * @return
     */
    List<Message> last(User user, int offset, int count);

    /**
     * Сохраняет сообщение
     *
     * @param message
     * @return
     */
    boolean save(Message message);

    /**
     * Удаляет сообщение
     *
     * @param message
     * @return
     */
    boolean delete(Message message);

    /**
     * Устанавливает признак сообщения на ПРОСМОТРЕНО
     *
     * @param message
     * @return
     */
    boolean shown(Message message);

    /**
     * Устанавливает признак сообщений в диалоге на ПРОСМОТРЕНО
     *
     * @param user1
     * @param user2
     * @return
     */
    boolean shown(User user1, User user2);

    /**
     * Возвращает количество непросмотренных сообщений для пользователя
     *
     * @param user
     * @return
     */
    int count(User user);
}