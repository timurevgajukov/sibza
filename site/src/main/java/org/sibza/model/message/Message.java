package org.sibza.model.message;

import org.sibza.model.Model;
import org.sibza.model.user.User;

import java.util.Date;

/**
 * Created by timur on 08.03.16.
 *
 * Модель сообщения между пользователями
 */
public class Message extends Model {

    private Date createDt;
    private User source;
    private User destination;
    private String message;
    private boolean shown;

    public Message(int id) {

        super(id);
    }

    public Message(User source, User destination, String message) {

        super();

        this.source = source;
        this.destination = destination;
        this.message = message;
    }

    public Message(int id, Date createDt, User source, User destination, String message, boolean shown) {

        super(id);

        this.createDt = createDt;
        this.source = source;
        this.destination = destination;
        this.message = message;
        this.shown = shown;
    }

    public Date getCreateDt() {

        return createDt;
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public User getSource() {

        return source;
    }

    public void setSource(User source) {

        this.source = source;
    }

    public User getDestination() {

        return destination;
    }

    public void setDestination(User destination) {

        this.destination = destination;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public boolean isShown() {

        return shown;
    }

    public void setShown(boolean shown) {

        this.shown = shown;
    }

    @Override
    public String toString() {

        return message;
    }
}