package org.sibza.model.message.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.message.Message;
import org.sibza.model.message.dao.interfaces.MessageDAO;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.UserDAOImpl;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 08.03.16.
 * <p/>
 * Реализация интерфейса для работы с сообщениями между пользователями
 */
public class MessageDAOImpl extends ModelDAOImpl implements MessageDAO {

    private static final String QUERY_CHAT = "{ call messages_chat(?,?,?,?) }";
    private static final String QUERY_LAST = "{ call messages_list_last(?,?,?) }";
    private static final String QUERY_SAVE = "{ call messages_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call messages_delete(?,?) }";
    private static final String QUERY_SHOWN = "{ call messages_shown(?) }";
    private static final String QUERY_SHOWN_BY_USERS = "{ call messages_shown_by_users(?,?) }";
    private static final String QUERY_COUNT = "{ call messages_not_shown_count(?) }";

    @Override
    public List<Message> chat(final User user1, final User user2, int offset, int count) {

        List<Message> list = jdbcTemplate.query(QUERY_CHAT,
                new Object[]{ user1.getId(), user2.getId(), offset, count },
                new RowMapper<Message>() {

                    @Override
                    public Message mapRow(ResultSet resultSet, int i) throws SQLException {

                        Message message = load(resultSet);
                        if (message.getSource().getId() == user1.getId()) {
                            message.setSource(user1);
                            message.setDestination(user2);
                        } else {
                            message.setSource(user2);
                            message.setDestination(user1);
                        }

                        return message;
                    }
                });

        return list;
    }

    @Override
    public List<Message> last(User user, int offset, int count) {

        List<Message> list = jdbcTemplate.query(QUERY_LAST, new Object[] { user.getId(), offset, count },
                new RowMapper<Message>() {

                    @Override
                    public Message mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (list != null && list.size() != 0) {
            for (Message message : list) {
                if (message.getSource().getId() == user.getId()) {
                    message.setSource(user);
                    message.setDestination(new UserDAOImpl().get(message.getDestination().getId()));
                } else {
                    message.setSource(new UserDAOImpl().get(message.getSource().getId()));
                    message.setDestination(user);
                }
            }
        }

        return list;
    }

    @Override
    public boolean save(Message message) {

        Message newMessage = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        message.getId() == 0 ? null : message.getId(),
                        message.getSource().getId(),
                        message.getDestination().getId(),
                        message.getMessage()
                }, new RowMapper<Message>() {

                    @Override
                    public Message mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        message.setId(newMessage.getId());
        message.setCreateDt(newMessage.getCreateDt());

        return true;
    }

    @Override
    public boolean delete(Message message) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ message.getId() }) != 0;
    }

    @Override
    public boolean shown(Message message) {

        return jdbcTemplate.update(QUERY_SHOWN, new Object[] { message.getId() }) != 0;
    }

    @Override
    public boolean shown(User user1, User user2) {

        return jdbcTemplate.update(QUERY_SHOWN_BY_USERS, new Object[] { user1.getId(), user2.getId() }) != 0;
    }

    @Override
    public int count(User user) {

        int cnt = jdbcTemplate.queryForObject(QUERY_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        }, user.getId());

        return cnt;
    }

    private Message load(ResultSet rs) throws SQLException {

        User source = new User(rs.getInt("user1_id"));
        User destination = new User(rs.getInt("user2_id"));

        return new Message(rs.getInt("id"), rs.getTimestamp("create_dt"), source, destination,
                rs.getString("message"), rs.getBoolean("shown"));
    }
}