package org.sibza.model.resource.dao.interfaces;

import org.sibza.model.alphabet.Letter;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с медиа ресурсами
 */
public interface ResourceDAO {

    /**
     * Возвращает медиа ресурс
     *
     * @param id идентификатор медиа ресурса
     * @return
     */
    Resource get(int id);

    /**
     * Возвращает медиа ресурс
     *
     * @param id
     * @param fast если true, то загружает без бинарных данных
     * @return
     */
    Resource get(int id, boolean fast);

    /**
     * Возвращает список всех медиа ресурсов
     *
     * @return
     */
    List<Resource> list();

    /**
     * Возвращает список всех медиа ресурсов
     *
     * @param fast если true, то загружает без бинарных данных
     * @return
     */
    List<Resource> list(boolean fast);

    /**
     * Возвращает список ресурсов определенного типа
     *
     * @param type тип ресурса
     * @param fast признак загрузки без бинарных данных
     * @return
     */
    List<Resource> list(Type type, boolean fast);

    /**
     * Возвращает список медиа ресурсов для слова
     *
     * @param word
     * @return
     */
    List<Resource> list(Word word);

    /**
     * Возвращает список медиа ресурсов для буквы алфавита
     *
     * @param letter
     * @param fast
     * @return
     */
    List<Resource> list(Letter letter, boolean fast);

    /**
     * Сохраняет медиа ресурс
     *
     * @param resource
     * @param user
     * @return
     */
    boolean save(Resource resource, User user);

    /**
     * Удаляет медиа ресурс
     *
     * @param resource
     * @param user
     * @return
     */
    boolean delete(Resource resource, User user);
}