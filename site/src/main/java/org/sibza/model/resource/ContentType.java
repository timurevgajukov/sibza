package org.sibza.model.resource;

import org.sibza.model.Model;
import org.sibza.cache.ResourceTypes;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 16.05.15.
 *
 * Модель типа контента
 */
public class ContentType extends Model {

    private Type type;
    private String contentType;

    public ContentType(int id) {

        super(id);
    }

    public ContentType(int id, Type type, String name, String contentType) {

        super(id, name);
        this.type = type;
        this.contentType = contentType;
    }

    public Type getType() {

        return type;
    }

    public void setType(Type type) {

        this.type = type;
    }

    public String getContentType() {

        return contentType;
    }

    public void setContentType(String contentType) {

        this.contentType = contentType;
    }

    @Override
    public String toString() {

        return contentType;
    }
}