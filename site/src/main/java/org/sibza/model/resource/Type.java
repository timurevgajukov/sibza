package org.sibza.model.resource;

import org.sibza.model.Model;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 10.05.15.
 *
 * Модель типа ресурса
 */
public class Type extends Model {

    public static final int IMAGE_ID = 1;
    public static final int AUDIO_ID = 2;
    public static final int VIDEO_ID = 3;
    public static final int DOCUMENT_ID = 4;

    public static final Type IMAGE = new Type(IMAGE_ID);
    public static final Type AUDIO = new Type(AUDIO_ID);
    public static final Type VIDEO = new Type(VIDEO_ID);
    public static final Type DOCUMENT = new Type(DOCUMENT_ID);

    public Type(int id) {

        super(id);
    }

    public Type(int id, String name) {

        super(id, name);
    }
}
