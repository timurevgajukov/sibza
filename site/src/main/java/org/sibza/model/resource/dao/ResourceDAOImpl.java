package org.sibza.model.resource.dao;

import org.sibza.cache.ContentTypes;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.resource.ContentType;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.interfaces.ResourceDAO;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;
import org.springframework.jdbc.core.RowMapper;

import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 * <p/>
 * Реализация интерфейса для работы с медиа ресурсами
 */
public class ResourceDAOImpl extends ModelDAOImpl implements ResourceDAO {

    private static final String QUERY_GET_BY_ID = "{ call resources_get(?,?) }";
    private static final String QUERY_LIST = "{ call resources_list(?) }";
    private static final String QUERY_LIST_BY_TYPE = "{ call resources_list_by_type(?,?) }";
    private static final String QUERY_LIST_BY_WORD = "{ call words_resources_list(?) }";
    private static final String QUERY_LIST_BY_LETTER = "{ call alphabet_resources_list(?) }";
    private static final String QUERY_SAVE = "{ call resources_save(?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call resources_delete(?,?) }";

    @Override
    public Resource get(int id) {

        return get(id, false);
    }

    @Override
    public Resource get(int id, final boolean fast) {

        List<Resource> resources = jdbcTemplate.query(QUERY_GET_BY_ID, new Object[]{id, fast},
                new RowMapper<Resource>() {

                    @Override
                    public Resource mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, fast);
                    }
                });

        if (resources == null || resources.size() == 0) {
            return null;
        }

        Resource resource = resources.get(0);

        ContentTypes types = ContentTypes.getInstance();
        resource.setType(types.get(resource.getType().getId()));

        return resource;
    }

    @Override
    public List<Resource> list() {

        return list(false);
    }

    @Override
    public List<Resource> list(final boolean fast) {

        List<Resource> resources = jdbcTemplate.query(QUERY_LIST, new RowMapper<Resource>() {

            @Override
            public Resource mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet, fast);
            }
        }, fast);

        if (resources == null) {
            return null;
        }

        ContentTypes types = ContentTypes.getInstance();
        for (Resource resource : resources) {
            resource.setType(types.get(resource.getType().getId()));
        }

        return resources;
    }

    @Override
    public List<Resource> list(Type type, final boolean fast) {

        List<Resource> resources = jdbcTemplate.query(QUERY_LIST_BY_TYPE,
                new Object[] { type.getId(), fast },
                new RowMapper<Resource>() {

                    @Override
                    public Resource mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, fast);
                    }
                });

        if (resources == null) {
            return null;
        }

        ContentTypes types = ContentTypes.getInstance();
        for (Resource resource : resources) {
            resource.setType(types.get(resource.getType().getId()));
        }

        return resources;
    }

    @Override
    public List<Resource> list(Word word) {

        List<Resource> resources = jdbcTemplate.query(QUERY_LIST_BY_WORD, new RowMapper<Resource>() {

            @Override
            public Resource mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet, true);
            }
        }, word.getId());

        if (resources == null) {
            return null;
        }

        ContentTypes types = ContentTypes.getInstance();
        for (Resource resource : resources) {
            resource.setType(types.get(resource.getType().getId()));
        }

        return resources;
    }

    @Override
    public List<Resource> list(Letter letter, final boolean fast) {

        List<Resource> resources = jdbcTemplate.query(QUERY_LIST_BY_LETTER, new RowMapper<Resource>() {

            @Override
            public Resource mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet, fast);
            }
        }, letter.getId());

        if (resources == null) {
            return null;
        }

        ContentTypes types = ContentTypes.getInstance();
        for (Resource resource : resources) {
            resource.setType(types.get(resource.getType().getId()));
        }

        return resources;
    }

    @Override
    public boolean save(Resource resource, User user) {

        Resource newResource = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        resource.getId() == 0 ? null : resource.getId(),
                        resource.getType().getId(),
                        resource.getFileName(),
                        new ByteArrayInputStream(resource.getResource()),
                        user.getId()
                }, new RowMapper<Resource>() {

                    @Override
                    public Resource mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, true);
                    }
                });

        resource.setId(newResource.getId());

        return true;
    }

    @Override
    public boolean delete(Resource resource, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { resource.getId(), user.getId() }) != 0;
    }

    private Resource load(ResultSet rs, boolean fast) throws SQLException {

        ContentType type = new ContentType(rs.getInt("content_type_id"));
        if (fast) {
            return new Resource(rs.getInt("id"), type, rs.getString("file_name"));
        } else {
            return new Resource(rs.getInt("id"), type, rs.getString("file_name"), rs.getBlob("resource"));
        }
    }
}