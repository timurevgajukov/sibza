package org.sibza.model.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.sibza.model.Model;
import org.sibza.cache.ContentTypes;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.utils.DBase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 16.05.15.
 *
 * Модель медиа ресурса
 */
public class Resource extends Model {

    private ContentType type;
    private String fileName;

    @JsonIgnore
    private byte[] resource;

    public Resource(int id) {

        super(id);
    }

    public Resource(ContentType type, String fileName, byte[] resource) {

        super();
        this.type = type;
        this.fileName = fileName;
        setResource(resource);
    }

    public Resource(int id, ContentType type, String fileName) {

        super(id);
        this.type = type;
        this.fileName = fileName;
    }

    public Resource(int id, ContentType type, String fileName, Blob blob) {

        super(id);
        this.type = type;
        this.fileName = fileName;
        if (blob != null) {
            setResource(blob);
        }
    }

    public ContentType getType() {

        return type;
    }

    public void setType(ContentType type) {

        this.type = type;
    }

    public String getFileName() {

        return fileName;
    }

    public void setFileName(String fileName) {

        this.fileName = fileName;
    }

    public byte[] getResource() {

        return resource;
    }

    public void setResource(byte[] resource) {

        this.resource = resource;
    }

    public void setResource(Blob blob) {

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            InputStream in = blob.getBinaryStream();

            int n = 0;
            while ((n = in.read(buf)) >= 0) {
                baos.write(buf, 0, n);
            }
            in.close();

            this.resource = baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadResource() {

        // если мы еще не подгрузили данные, то сейчас самое время
        if (resource == null || resource.length == 0) {
            Resource tmp = new ResourceDAOImpl().get(id);
            resource = tmp.getResource();
        }
    }
}