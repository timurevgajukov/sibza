package org.sibza.model.resource.dao;

import org.sibza.cache.ResourceTypes;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.resource.ContentType;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.interfaces.ContentTypeDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с типом контента
 */
public class ContentTypeDAOImpl extends ModelDAOImpl implements ContentTypeDAO {

    private static final String QUERY_LIST = "{ call content_types_list() }";

    @Override
    public List<ContentType> list() {

        List<ContentType> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<ContentType>() {

            @Override
            public ContentType mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        ResourceTypes types = ResourceTypes.getInstance();
        if (list != null) {
            for (ContentType item : list) {
                item.setType(types.get(item.getType().getId()));
            }
        }

        return list;
    }

    private ContentType load(ResultSet rs) throws SQLException {

        Type type = new Type(rs.getInt("resource_type_id"));
        return new ContentType(rs.getInt("id"), type, rs.getString("name"), rs.getString("content_type"));
    }
}