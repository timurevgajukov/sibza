package org.sibza.model.resource.dao.interfaces;

import org.sibza.model.resource.ContentType;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с типом контента
 */
public interface ContentTypeDAO {

    /**
     * Возвращает список типов контента
     *
     * @return
     */
    List<ContentType> list();
}
