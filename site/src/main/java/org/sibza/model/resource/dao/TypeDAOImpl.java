package org.sibza.model.resource.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.resource.Type;
import org.sibza.model.resource.dao.interfaces.TypeDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с типом ресурса
 */
public class TypeDAOImpl extends ModelDAOImpl implements TypeDAO {

    private static final String QUERY_LIST = "{ call resource_types_list() }";

    @Override
    public List<Type> list() {

        List<Type> types = jdbcTemplate.query(QUERY_LIST, new RowMapper<Type>() {

            @Override
            public Type mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return null;
    }

    private Type load(ResultSet rs) throws SQLException {

        return new Type(rs.getInt("id"), rs.getString("name"));
    }
}