package org.sibza.model.resource.dao.interfaces;

import org.sibza.model.resource.Type;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с типом ресурса
 */
public interface TypeDAO {

    /**
     * Возвращает список типов ресурса
     *
     * @return
     */
    List<Type> list();
}
