package org.sibza.model.population.dao.interfaces;

import org.sibza.model.population.PopulationItem;

import java.util.List;

/**
 * Created by Тимур on 11.12.2015.
 *
 * Интерфейс для работы с популяцией народа в странах
 */
public interface PopulationDAO {

    /**
     * Возвращает список популяций народа в различных странах
     *
     * @return
     */
    List<PopulationItem> list();
}