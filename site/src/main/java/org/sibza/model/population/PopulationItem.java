package org.sibza.model.population;

/**
 * Created by Тимур on 11.12.2015.
 *
 * Элемент с данными по полуляции народа в стране
 */
public class PopulationItem {

    private String code;
    private int value;
    private String name;

    public PopulationItem(String code, int value, String name) {

        this.code = code;
        this.value = value;
        this.name = name;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public int getValue() {

        return value;
    }

    public void setValue(int value) {

        this.value = value;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }
}