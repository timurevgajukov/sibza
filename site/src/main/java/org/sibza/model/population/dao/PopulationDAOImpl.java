package org.sibza.model.population.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.population.PopulationItem;
import org.sibza.model.population.dao.interfaces.PopulationDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 11.12.2015.
 *
 * Реализация интерфейса по популяции народа в странах
 */
public class PopulationDAOImpl extends ModelDAOImpl implements PopulationDAO {

    private static final String QUERY_LIST = "{ call population_oficial_list() }";

    @Override
    public List<PopulationItem> list() {

        List<PopulationItem> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<PopulationItem>() {

            @Override
            public PopulationItem mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return list;
    }

    private PopulationItem load(ResultSet rs) throws SQLException {

        return new PopulationItem(rs.getString("code"), rs.getInt("value"), rs.getString("name"));
    }
}