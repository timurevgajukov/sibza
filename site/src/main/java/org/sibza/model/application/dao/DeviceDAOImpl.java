package org.sibza.model.application.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.application.Application;
import org.sibza.model.application.Device;
import org.sibza.model.application.dao.interfaces.DeviceDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Тимур on 11.01.2016.
 *
 * Реализация интерфейса для работы с данными по мобильным устройствам
 */
public class DeviceDAOImpl extends ModelDAOImpl implements DeviceDAO {

    private static final String QUERY_SAVE = "{ call app_devices_save(?,?) }";
    private static final String QUERY_DELETE = "{ call app_devices_delete_by_token(?) }";

    @Override
    public boolean save(Device device) {

        return jdbcTemplate.update(QUERY_SAVE, new Object[]{ device.getApplication().getId(), device.getToken() }) != 0;
    }

    @Override
    public boolean delete(Device device) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { device.getToken() }) != 0;
    }

    private Device load(ResultSet rs) throws SQLException {

        Application app = new Application(rs.getInt("app_id"));
        return new Device(rs.getInt("id"), rs.getTimestamp("create_dt"), app, rs.getString("token"));
    }
}