package org.sibza.model.application.dao.interfaces;

import org.sibza.model.application.Device;

/**
 * Created by Тимур on 11.01.2016.
 *
 * Интерфейс для работы с мобильными устройствами (их токенами для push)
 */
public interface DeviceDAO {

    /**
     * Сохраняет новое мобильное устройство с токеном для push уведомлений
     *
     * @param device
     * @return
     */
    boolean save(Device device);

    /**
     * Удаляет информацию по моб. устройству, с которого удалили приложение
     *
     * @param device
     * @return
     */
    boolean delete(Device device);
}