package org.sibza.model.application;

import org.sibza.model.Model;

import java.util.UUID;

/**
 * Created by Тимур on 26.10.2015.
 *
 * Модель мобильного приложения для работы с api сайта
 */
public class Application extends Model {

    private String token;
    private String version;
    private boolean hold;

    public Application(int id) {

        super(id);
    }

    public Application(int id, String name, String version, String token, boolean hold) {

        super(id, name);
        this.token = token;
        this.version = version;
        this.hold = hold;
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getVersion() {

        return version;
    }

    public void setVersion(String version) {

        this.version = version;
    }

    public boolean isHold() {

        return hold;
    }

    public void setHold(boolean hold) {

        this.hold = hold;
    }

    public static String genCode() {

        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static void main(String[] args) {

        // генерация нового кода для приложения
        System.out.println(genCode());
    }
}