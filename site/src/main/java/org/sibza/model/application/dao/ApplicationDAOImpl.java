package org.sibza.model.application.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.application.Application;
import org.sibza.model.application.dao.interfaces.ApplicationDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 26.10.2015.
 *
 * Реализация интерфейса для работы с приложениями
 */
public class ApplicationDAOImpl extends ModelDAOImpl implements ApplicationDAO {

    private static final String QUERY_GET_BY_TOKEN = "{ call applications_get_by_token(?) }";

    @Override
    public Application get(String token) {

        List<Application> apps = jdbcTemplate.query(QUERY_GET_BY_TOKEN, new RowMapper<Application>() {

            @Override
            public Application mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, token);

        if (apps == null || apps.size() == 0) {
            return null;
        }

        return apps.get(0);
    }

    private Application load(ResultSet rs) throws SQLException {

        return new Application(rs.getInt("id"), rs.getString("name"), rs.getString("version"), rs.getString("token"), rs.getBoolean("hold"));
    }
}