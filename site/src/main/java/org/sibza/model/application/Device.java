package org.sibza.model.application;

import org.sibza.model.Model;

import java.util.Date;

/**
 * Created by Тимур on 11.01.2016.
 *
 * Модель мобильного устройства
 */
public class Device extends Model {

    private Date createDt;
    private Application application;
    private String token;

    public Device(Application application, String token) {

        this.application = application;
        this.token = token;
    }

    public Device(int id, Date createDt, Application application, String token) {

        super(id);
        this.createDt = createDt;
        this.application = application;
        this.token = token;
    }

    public Date getCreateDt() {

        return createDt;
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public Application getApplication() {

        return application;
    }

    public void setApplication(Application application) {

        this.application = application;
    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {

        this.token = token;
    }
}