package org.sibza.model.application.dao.interfaces;

import org.sibza.model.application.Application;

/**
 * Created by Тимур on 26.10.2015.
 *
 * Интерфейс для работы с приложениями
 */
public interface ApplicationDAO {

    /**
     * Возвращает информацию по приложению по его токену
     *
     * @param token
     * @return
     */
    Application get(String token);
}