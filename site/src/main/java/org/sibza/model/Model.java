package org.sibza.model;

import java.io.Serializable;

/**
 * Created by timur on 09.05.15.
 *
 * Базовая модель
 */
public class Model implements Serializable {

    protected int id;
    protected String name;

    public Model() {

        this.id = 0;
    }

    public Model(int id) {

        this.id = id;
    }

    public Model(int id, String name) {

        this.id = id;
        this.name = name;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return name;
    }
}
