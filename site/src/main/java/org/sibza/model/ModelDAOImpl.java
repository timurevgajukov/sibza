package org.sibza.model;

import org.sibza.utils.DBase;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с моделями данных
 */
public class ModelDAOImpl {

    protected static final String QUERY_LAST_INSERT_ID = "SELECT LAST_INSERT_ID()";

    protected JdbcTemplate jdbcTemplate;

    {
        jdbcTemplate = new JdbcTemplate(DBase.getDataSource());
    }
}