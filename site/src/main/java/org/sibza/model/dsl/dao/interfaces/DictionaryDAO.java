package org.sibza.model.dsl.dao.interfaces;

import org.sibza.model.dsl.DSLStatus;
import org.sibza.model.dsl.DictionaryItem;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Интерфейс для работы с элементами dsl-словаря
 */
public interface DictionaryDAO {

    /**
     * Возвращает элемент dsl-словаря
     *
     * @param id
     * @return
     */
    DictionaryItem get(int id);

    /**
     * Возвращает все элементы словаря
     *
     * @param status
     * @param offset
     * @param count
     * @return
     */
    List<DictionaryItem> list(DSLStatus status, int offset, int count);

    /**
     * Сохраняет элемент словаря
     *
     * @param item
     * @param user
     * @return
     */
    boolean save(DictionaryItem item, User user);
}