package org.sibza.model.dsl.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.dsl.DSLStatus;
import org.sibza.model.dsl.dao.interfaces.DSLStatusDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Реализация интерфейса для работы с dsl-словарем
 */
public class DSLStatusDAOImpl extends ModelDAOImpl implements DSLStatusDAO {

    private static final String QUERY_LIST = "{ call dsl_statuses_list() }";

    @Override
    public List<DSLStatus> list() {

        List<DSLStatus> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<DSLStatus>() {

            @Override
            public DSLStatus mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return list;
    }

    private DSLStatus load(ResultSet rs) throws SQLException {

        return new DSLStatus(rs.getInt("id"), rs.getString("name"));
    }
}