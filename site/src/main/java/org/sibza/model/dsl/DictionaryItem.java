package org.sibza.model.dsl;

import org.sibza.model.Model;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Элемент DSL-словаря
 */
public class DictionaryItem extends Model {

    private String word;
    private String description;
    private DSLStatus status;

    public DictionaryItem(String word, String description) {

        super();
        this.word = word;
        this.description = description;
    }

    public DictionaryItem(int id, String word, String description, DSLStatus status) {

        super(id);
        this.word = word;
        this.description = description;
        this.status = status;
    }

    public String getWord() {

        return word;
    }

    public void setWord(String word) {

        this.word = word;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public DSLStatus getStatus() {

        return status;
    }

    public void setStatus(DSLStatus status) {

        this.status = status;
    }

    @Override
    public String toString() {

        return word;
    }
}