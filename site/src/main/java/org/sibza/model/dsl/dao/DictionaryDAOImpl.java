package org.sibza.model.dsl.dao;

import org.sibza.cache.DSLDictionaryStatuses;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.dsl.DSLStatus;
import org.sibza.model.dsl.DictionaryItem;
import org.sibza.model.dsl.dao.interfaces.DictionaryDAO;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 30.11.2015.
 * <p/>
 * Реализация интерфейса для работы с dsl-словарями
 */
public class DictionaryDAOImpl extends ModelDAOImpl implements DictionaryDAO {

    private static final String QUERY_GET_BY_ID = "{ call dsl_get(?) }";
    private static final String QUERY_LIST = "{ call dsl_list(?,?,?) }";
    private static final String QUERY_SAVE = "{ call dsl_save(?,?,?,?,?) }";

    public DictionaryDAOImpl() {

        super();

        // если еще не загрузили справочник, то загружаем его из БД
        DSLDictionaryStatuses.getInstance();
    }

    @Override
    public DictionaryItem get(int id) {

        DictionaryItem item = jdbcTemplate.queryForObject(QUERY_GET_BY_ID, new RowMapper<DictionaryItem>() {

            @Override
            public DictionaryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        return item;
    }

    @Override
    public List<DictionaryItem> list(DSLStatus status, int offset, int count) {

        List<DictionaryItem> list = jdbcTemplate.query(QUERY_LIST,
                new Object[] {
                        status == null ? null : status.getId(),
                        offset,
                        count
                }, new RowMapper<DictionaryItem>() {

                    @Override
                    public DictionaryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        return list;
    }

    @Override
    public boolean save(DictionaryItem item, User user) {

        DictionaryItem newItem = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        item.getId() == 0 ? null : item.getId(),
                        item.getWord(),
                        item.getDescription(),
                        item.getStatus() == null ? DSLStatus.NEW : item.getStatus().getId(),
                        user.getId()
                }, new RowMapper<DictionaryItem>() {

                    @Override
                    public DictionaryItem mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        item.setId(newItem.getId());
        item.setStatus(newItem.getStatus());

        return true;
    }

    private DictionaryItem load(ResultSet rs) throws SQLException {

        DSLDictionaryStatuses statuses = DSLDictionaryStatuses.getInstance();
        return new DictionaryItem(rs.getInt("id"), rs.getString("word"), rs.getString("description"), statuses.get(rs.getInt("status_id")));
    }
}