package org.sibza.model.dsl;

import org.sibza.model.Model;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Статус обработки записи для DSL-словаря
 */
public class DSLStatus extends Model {

    public static final int NEW = 1;
    public static final int PROCESSED = 2;
    public static final int POSTPONED = 3;
    public static final int DELETED = 4;

    public DSLStatus(int id, String name) {

        super(id, name);
    }
}