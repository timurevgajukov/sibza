package org.sibza.model.dsl.dao.interfaces;

import org.sibza.model.dsl.DSLStatus;

import java.util.List;

/**
 * Created by Тимур on 30.11.2015.
 *
 * Интерфейс для работы со статусами dsl-словаря
 */
public interface DSLStatusDAO {

    /**
     * Возвращает список всех статусов
     *
     * @return
     */
    List<DSLStatus> list();
}