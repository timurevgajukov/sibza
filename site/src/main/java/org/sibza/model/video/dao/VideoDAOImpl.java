package org.sibza.model.video.dao;

import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.User;
import org.sibza.model.video.Video;
import org.sibza.model.video.dao.interfaces.VideoDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 26.09.15.
 *
 * Реализация интерфейса по работе с видео
 */
public class VideoDAOImpl extends ModelDAOImpl implements VideoDAO {

    private static final String QUERY_GET_BY_ID = "{ call video_get(?) }";
    private static final String QUERY_LIST = "{ call video_list(?,?) }";
    private static final String QUERY_LIST_BY_LANGUAGE = "{ call video_list_by_language(?,?,?) }";
    private static final String QUERY_SEARCH = "{ call video_search(?)}";
    private static final String QUERY_SAVE = "{ call video_save(?,?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call video_delete(?,?) }";
    private static final String QUERY_STAT_COUNT = "{ call statistics_video_count_all() }";

    @Override
    public Video get(int id) {

        Video video = jdbcTemplate.queryForObject(QUERY_GET_BY_ID, new RowMapper<Video>() {

            @Override
            public Video mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        Languages languages = Languages.getInstance();
        video.setLanguage(languages.get(video.getLanguage().getId()));

        return video;
    }

    @Override
    public List<Video> list(int offset, int count) {

        List<Video> list = jdbcTemplate.query(QUERY_LIST, new Object[] { offset, count }, new RowMapper<Video>() {

            @Override
            public Video mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        for (Video video : list) {
            video.setLanguage(languages.get(video.getLanguage().getId()));
        }

        return list;
    }

    @Override
    public List<Video> list(final Language language, int offset, int count) {

        List<Video> list = jdbcTemplate.query(QUERY_LIST_BY_LANGUAGE,
                new Object[] { language.getId(), offset, count },
                new RowMapper<Video>() {

            @Override
            public Video mapRow(ResultSet resultSet, int i) throws SQLException {

                Video video = load(resultSet);
                video.setLanguage(language);

                return video;
            }
        });

        return list;
    }

    @Override
    public List<Video> search(String query) {

        List<Video> list = jdbcTemplate.query(QUERY_SEARCH, new RowMapper<Video>() {

            @Override
            public Video mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, "%" + query + "%");

        if (list == null) {
            return null;
        }

        // загрузим информацию по языку
        Languages languages = Languages.getInstance();
        for (Video item : list) {
            item.setLanguage(languages.get(item.getLanguage().getId()));
        }

        return list;
    }

    @Override
    public boolean save(Video video, User user) {

        Video newVideo = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        video.getId() == 0 ? null : video.getId(),
                        video.getLanguage().getId(),
                        video.getResource() == null ? null : video.getResource().getId(),
                        video.getUrl() == null || video.getUrl().length() == 0 ? null : video.getUrl(),
                        video.getTitle(),
                        video.getNote() == null || video.getNote().length() == 0 ? null : video.getNote(),
                        user.getId()
                }, new RowMapper<Video>() {

                    @Override
                    public Video mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        video.setId(newVideo.getId());

        return true;
    }

    @Override
    public boolean delete(Video video, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { video.getId(), user.getId() }) != 0;
    }

    @Override
    public int count() {

        return jdbcTemplate.queryForObject(QUERY_STAT_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }

    private Video load(ResultSet rs) throws SQLException {

        Language language = new Language(rs.getInt("lang_id"));
        Video video = null;
        if (rs.getInt("resource_id") != 0) {
            Resource resource = new Resource(rs.getInt("resource_id"));
            video = new Video(rs.getInt("id"), language, resource, rs.getString("title"), rs.getString("note"));
        } else if (rs.getString("resource_url").length() != 0) {
            video = new Video(rs.getInt("id"), language, rs.getString("resource_url"), rs.getString("title"), rs.getString("note"));
        }
        video.setYoutube(rs.getString("youtube"));

        return video;
    }
}