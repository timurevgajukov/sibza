package org.sibza.model.video.dao.interfaces;

import org.sibza.model.language.Language;
import org.sibza.model.user.User;
import org.sibza.model.video.Video;

import java.util.List;

/**
 * Created by timur on 26.09.15.
 *
 * Интерфейс для работы с видео
 */
public interface VideoDAO {

    /**
     * Возвращает видео по его идентификатору
     *
     * @param id идентификатор видео
     * @return
     */
    Video get(int id);

    /**
     * Возвращает список всех видео
     *
     * @param offset
     * @param count
     * @return
     */
    List<Video> list(int offset, int count);

    /**
     * Возвращает список видео по определенному языку
     *
     * @param language язык
     * @param offset
     * @param count
     * @return
     */
    List<Video> list(Language language, int offset, int count);

    /**
     * Поиск видео
     *
     * @param query
     * @return
     */
    List<Video> search(String query);

    /**
     * Сохраняет данные по видео
     *
     * @param video
     * @param user
     * @return
     */
    boolean save(Video video, User user);

    /**
     * Удаляет видео
     *
     * @param video
     * @param user
     * @return
     */
    boolean delete(Video video, User user);

    /**
     * Возвращает статистику по количеству видео роликов
     *
     * @return
     */
    int count();
}