package org.sibza.model.video;

import org.sibza.model.Model;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;

/**
 * Created by timur on 26.09.15.
 *
 * Модель видео
 */
public class Video extends Model {

    private Language language;
    private Resource resource;
    private String url;
    private String title;
    private String note;
    private String youtube;

    public Video(int id, Language language, Resource resource, String title, String note) {

        super(id);
        this.language = language;
        this.resource = resource;
        this.title = title;
        this.note = note;
    }

    public Video(int id, Language language, String url, String title, String note) {

        super(id);
        this.language = language;
        this.url = url;
        this.title = title;
        this.note = note;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public Resource getResource() {

        return resource;
    }

    public void setResource(Resource resource) {

        this.resource = resource;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getNote() {

        return note;
    }

    public void setNote(String note) {

        this.note = note;
    }

    public String getYoutube() {

        return youtube;
    }

    public void setYoutube(String youtube) {

        this.youtube = youtube;
    }

    @Override
    public String toString() {

        return title;
    }
}