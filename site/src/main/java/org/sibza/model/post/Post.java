package org.sibza.model.post;

import org.sibza.model.Model;
import org.sibza.model.user.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Тимур on 13.10.2015.
 * <p/>
 * Модель статьи пользователя
 */
public class Post extends Model {

    private User user;
    private Date createDt;
    private String body;

    public Post(User user, String body) {

        super();
        this.user = user;
        this.body = body;
    }

    public Post(int id, User user, Date createDt, String body) {

        super(id);
        this.user = user;
        this.createDt = createDt;
        this.body = body;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Date getCreateDt() {

        return createDt;
    }

    public String getFormattedCreateDt() {

        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(createDt);
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    @Override
    public String toString() {

        return body;
    }
}