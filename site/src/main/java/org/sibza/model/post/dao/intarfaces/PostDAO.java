package org.sibza.model.post.dao.intarfaces;

import org.sibza.model.post.Post;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Интерфейс для работы со статьями
 */
public interface PostDAO {

    /**
     * Возвращает список постов пользователя
     *
     * @param user
     * @return
     */
    List<Post> list(User user);

    /**
     * Сохраняет статью
     *
     * @param post
     * @return
     */
    boolean save(Post post);

    /**
     * Удаляет статью
     *
     * @param post
     * @return
     */
    boolean delete(Post post);
}