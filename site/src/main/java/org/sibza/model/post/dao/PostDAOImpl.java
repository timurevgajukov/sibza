package org.sibza.model.post.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.post.Post;
import org.sibza.model.post.dao.intarfaces.PostDAO;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Реализация интерфейса для работы со статьями
 */
public class PostDAOImpl extends ModelDAOImpl implements PostDAO {

    private static final String QUERY_LIST_BY_USER = "{ call posts_list_by_user(?) }";
    private static final String QUERY_SAVE = "{ call posts_save(?,?,?) }";
    private static final String QUERY_DELETE = "{ call posts_delete(?,?) }";

    @Override
    public List<Post> list(final User user) {

        List<Post> posts = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<Post>() {

            @Override
            public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                Post post = load(resultSet);
                post.setUser(user);

                return post;
            }
        }, user.getId());

        return posts;
    }

    @Override
    public boolean save(Post post) {

        Post newPost = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        post.getId() == 0 ? null : post.getId(),
                        post.getUser().getId(),
                        post.getBody()
                }, new RowMapper<Post>() {

                    @Override
                    public Post mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        post.setId(newPost.getId());
        post.setCreateDt(newPost.getCreateDt());

        return true;
    }

    @Override
    public boolean delete(Post post) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { post.getId(), post.getUser().getId() }) != 0;
    }

    private Post load(ResultSet rs) throws SQLException {

        User user = new User(rs.getInt("user_id"));

        return new Post(rs.getInt("id"), user, rs.getTimestamp("create_dt_tz"), rs.getString("body"));
    }
}