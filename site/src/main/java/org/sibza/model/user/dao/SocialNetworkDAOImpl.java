package org.sibza.model.user.dao;

import org.sibza.cache.SocialNetworkTypes;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.user.SocialNetwork;
import org.sibza.model.user.SocialNetworkType;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.interfaces.SocialNetworkDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Реализация интерфейса для работы с пользовательскими аккаунтами в социальных сетях
 */
public class SocialNetworkDAOImpl extends ModelDAOImpl implements SocialNetworkDAO {

    private static final String QUERY_LIST_BY_USER = "{ call user_social_networks_list_by_user(?) }";
    private static final String QUERY_SAVE = "{ call user_social_networks_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call user_social_networks_delete(?,?) }";

    @Override
    public List<SocialNetwork> list(User user) {

        List<SocialNetwork> list = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<SocialNetwork>() {

            @Override
            public SocialNetwork mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, user.getId());

        if (list == null) {
            return null;
        }

        SocialNetworkTypes types = SocialNetworkTypes.getInstance();
        for (SocialNetwork item : list) {
            item.setType(types.get(item.getType().getId()));
        }

        return list;
    }

    @Override
    public boolean save(SocialNetwork socialNetwork, User user) {

        SocialNetwork newItem = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        socialNetwork.getId() == 0 ? null : socialNetwork.getId(),
                        user.getId(),
                        socialNetwork.getType().getId(),
                        socialNetwork.getAccount()
                }, new RowMapper<SocialNetwork>() {

                    @Override
                    public SocialNetwork mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        socialNetwork.setId(newItem.getId());

        return true;
    }

    @Override
    public boolean delete(SocialNetwork socialNetwork, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { socialNetwork.getId(), user.getId() }) != 0;
    }

    private SocialNetwork load(ResultSet rs) throws SQLException {

        return new SocialNetwork(rs.getInt("id"), new SocialNetworkType(rs.getInt("type_id")), rs.getString("account"));
    }
}