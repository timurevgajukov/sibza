package org.sibza.model.user;

import org.sibza.model.Model;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 09.05.15.
 *
 * Роль пользователя
 */
public class Role extends Model {

    public static final int ROLE_ADMIN = 1;
    public static final int ROLE_EDITOR = 2;
    public static final int ROLE_USER = 3;

    public Role(int id) {

        super(id);
    }

    public Role(int id, String name) {

        super(id, name);
    }
}
