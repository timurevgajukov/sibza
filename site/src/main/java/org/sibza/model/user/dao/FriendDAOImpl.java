package org.sibza.model.user.dao;

import org.sibza.cache.DefaultAvatars;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.interfaces.FriendDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 07.03.16.
 *
 * Реализация интерфейса по работе со списком друзей пользователя
 */
public class FriendDAOImpl extends ModelDAOImpl implements FriendDAO {

    private static final String QUERY_LIST = "{ call friends_list(?,?,?) }";
    private static final String QUERY_SAVE = "{ call friends_save(?,?) }";
    private static final String QUERY_DELETE = "{ call friends_delete(?,?) }";

    @Override
    public List<User> list(User user, int offset, int count) {

        final DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();

        List<User> list = jdbcTemplate.query(QUERY_LIST, new Object[] { user.getId(), offset, count },
                new RowMapper<User>() {

                    @Override
                    public User mapRow(ResultSet resultSet, int i) throws SQLException {

                        User user = new UserDAOImpl().load(resultSet);
                        user.getInfo().setDefaultAvatar(defaultAvatars.get(user.getInfo().getDefaultAvatar().getId()));

                        return user;
                    }
                });


        return list;
    }

    @Override
    public boolean save(User user, User friend) {

        return jdbcTemplate.update(QUERY_SAVE, new Object[] { user.getId(), friend.getId() }) != 0;
    }

    @Override
    public boolean delete(User user, User friend) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { user.getId(), friend.getId() }) != 0;
    }
}