package org.sibza.model.user.dao.interfaces;

import org.sibza.model.user.Role;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с ролями
 */
public interface RoleDAO {

    /**
     * Возвращает список ролей пользователей
     *
     * @return
     */
    List<Role> list();

    /**
     * Возвращает роли пользователя
     *
     * @param user
     * @return
     */
    List<Role> list(User user);
}