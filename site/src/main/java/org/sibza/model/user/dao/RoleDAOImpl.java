package org.sibza.model.user.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.user.Role;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.interfaces.RoleDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с ролями пользователей
 */
public class RoleDAOImpl extends ModelDAOImpl implements RoleDAO {

    private static final String QUERY_LIST = "{ call roles_list() }";
    private static final String QUERY_LIST_BY_USER = "{ call users_roles_list_by_user(?) }";

    @Override
    public List<Role> list() {

        List<Role> roles = jdbcTemplate.query(QUERY_LIST, new RowMapper<Role>() {

            @Override
            public Role mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return roles;
    }

    @Override
    public List<Role> list(User user) {

        List<Role> roles = jdbcTemplate.query(QUERY_LIST_BY_USER, new RowMapper<Role>() {

            @Override
            public Role mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, user.getId());

        return roles;
    }

    private Role load(ResultSet rs) throws SQLException {

        return new Role(rs.getInt("id"), rs.getString("name"));
    }
}