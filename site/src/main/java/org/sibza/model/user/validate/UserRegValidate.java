package org.sibza.model.user.validate;

import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.translator.Translator;
import org.sibza.validator.IValidator;
import org.sibza.model.user.UserReg;
import org.sibza.validator.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by timur on 10.10.15.
 *
 * Валидация регистрационных данных
 */
public class UserRegValidate extends Validator implements IValidator<UserReg> {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public UserRegValidate(Translator t) {

        super(t);
    }

    @Override
    public boolean validate(UserReg item) {

        // проверяем, что указан почтовый ящик
        if (item.getEmail() == null) {
            addError("Необходимо указать адрес электронной почты");
        }

        // проверяем на корректность адреса эл. почты
        if (!validateEmail(item.getEmail())) {
            addError("Необходимо указать корректный адрес электронной почты");
        }

        // проверяем почтовый ящик на уникальность
        if (new UserDAOImpl().checkEmail(item.getEmail())) {
            // найден пользователь с такой почтой
            addError("Пользователь с такой электронной почтой найден");
        }

        // проверяем пароль
        if (item.getPassword1() == null) {
            addError("Необходимо указать пароль");
        } else {
            // пароли должны совпадать
            if (!item.getPassword1().equals(item.getPassword2())) {
                addError("Пароли должны совпадать");
            }
        }

        return !hasErrors();
    }

    public boolean validateEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }
}