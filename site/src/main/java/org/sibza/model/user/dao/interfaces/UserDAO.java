package org.sibza.model.user.dao.interfaces;

import org.sibza.model.user.Role;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с пользователями
 */
public interface UserDAO {

    /**
     * Возвращает информацию по пользователю
     *
     * @param id идентификатор пользователя
     * @return
     */
    User get(int id);

    /**
     * Возвращает информацию по пользователю
     *
     * @param nick ник пользователя
     * @return
     */
    User get(String nick);

    /**
     * Возвращает информацию по пользователю
     *
     * @param email электронная почта
     * @return
     */
    User getByEmail(String email);

    /**
     * Возвращает информацию по пользователю
     *
     * @param email электронная почта
     * @param code код верификации
     * @return
     */
    User getByEmail(String email, String code);

    /**
     * Авторизация пользователя
     *
     * @param email эл. почта пользователя
     * @param password пароль пользователя
     * @return
     */
    User auth(String email, String password);

    /**
     * Регистрация нового пользователя
     *
     * @param email эл. почта пользователя
     * @param password пароль пользователя
     * @param name имя пользователя
     * @param confirmCode код подтверждения эл. почты
     * @param role роль пользователя
     * @return
     */
    User registration(String email, String password, String name, String confirmCode, Role role);

    /**
     * Возвращает список всех пользователей
     *
     * @param offset
     * @param count
     * @return
     */
    List<User> list(int offset, int count);

    /**
     * Ищет пользователей с такой эл. почтой
     *
     * @param email
     * @return
     */
    boolean checkEmail(String email);

    /**
     * Подтверждение адреса эл. почты
     *
     * @param email подтверждаемый адрес эл. почты
     * @param confirmCode код подтверждения
     * @return
     */
    boolean confirmEmail(String email, String confirmCode);

    /**
     * Сохранение допольной информации
     *
     * @param user
     * @return
     */
    boolean save(User user);

    /**
     * Изменяет пароль пользователя через профиль
     *
     * @param user
     * @param currentPassword
     * @param newPassword
     * @return
     */
    boolean changePassword(User user, String currentPassword, String newPassword);

    /**
     * Изменяет пароль пользователя через механизм восстановления
     *
     * @param user
     * @param newPassword
     * @return
     */
    boolean recoverPassword(User user, String newPassword);

    /**
     * Возвращает общее поличество зарегистрированных пользователей
     *
     * @return
     */
    int count();
}