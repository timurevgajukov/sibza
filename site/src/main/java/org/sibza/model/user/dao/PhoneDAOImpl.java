package org.sibza.model.user.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.user.Phone;
import org.sibza.model.user.dao.interfaces.PhoneDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с номерами телефонов
 */
public class PhoneDAOImpl extends ModelDAOImpl implements PhoneDAO {

    private static final String QUERY_LIST = "{ call phones_list() }";

    @Override
    public List<Phone> list() {

        List<Phone> phones = jdbcTemplate.query(QUERY_LIST, new RowMapper<Phone>() {

            @Override
            public Phone mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return phones;
    }

    private Phone load(ResultSet rs) throws SQLException {

        return new Phone(rs.getInt("id"), rs.getString("phone"));
    }
}