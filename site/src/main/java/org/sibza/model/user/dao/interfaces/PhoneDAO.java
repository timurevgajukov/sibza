package org.sibza.model.user.dao.interfaces;

import org.sibza.model.user.Phone;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с номером телефона
 */
public interface PhoneDAO {

    /**
     * Возвращает список номеров
     *
     * @return
     */
    List<Phone> list();
}
