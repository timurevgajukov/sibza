package org.sibza.model.user.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.user.SocialNetworkType;
import org.sibza.model.user.dao.interfaces.SocialNetworkTypeDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 08.10.2015.
 * <p/>
 * Реализация интерфейса для работы с типами социальных сетей
 */
public class SocialNetworkTypeDAOImpl extends ModelDAOImpl implements SocialNetworkTypeDAO {

    private static final String QUERY_LIST = "{ call social_network_types_list() }";

    @Override
    public List<SocialNetworkType> list() {

        List<SocialNetworkType> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<SocialNetworkType>() {

            @Override
            public SocialNetworkType mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return list;
    }

    private SocialNetworkType load(ResultSet rs) throws SQLException {

        return new SocialNetworkType(rs.getInt("id"), rs.getString("name"), rs.getString("account_url"),
                rs.getString("color"), rs.getString("icon"));
    }
}