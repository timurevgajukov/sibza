package org.sibza.model.user;

import org.sibza.model.Model;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Модель ссылки на аккаунт пользователя в социальной сети
 */
public class SocialNetwork extends Model {

    private SocialNetworkType type;
    private String account;

    public SocialNetwork(int id, SocialNetworkType type, String account) {

        super(id);
        this.type = type;

        if (account.trim().length() == 0) {
            this.account = null;
        } else {
            this.account = account;
        }
    }

    public SocialNetworkType getType() {

        return type;
    }

    public void setType(SocialNetworkType type) {

        this.type = type;
    }

    public String getAccount() {

        return account;
    }

    public void setAccount(String account) {

        if (account != null && account.trim().length() == 0) {
            this.account = null;
        } else {
            this.account = account;
        }
    }

    public String getAccountUrl() {

        return type.getAccountUrl() + account;
    }

    @Override
    public String toString() {

        return account;
    }
}