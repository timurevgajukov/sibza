package org.sibza.model.user;

import org.sibza.model.Model;
import org.sibza.cache.Roles;
import org.sibza.model.post.Post;
import org.sibza.model.post.dao.PostDAOImpl;
import org.sibza.model.user.dao.FriendDAOImpl;
import org.sibza.model.user.dao.RoleDAOImpl;
import org.sibza.model.user.dao.SocialNetworkDAOImpl;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by timur on 09.05.15.
 *
 * Модель пользователя
 */
public class User extends Model {

    private String fullName;
    private String email;
    private String nick;
    private UserInfo info;
    private String confirmCode;
    private boolean confirmed;
    private List<Role> roles;
    private List<SocialNetwork> socialNetworks;
    private List<Post> posts;
    private List<User> friends;
    private boolean hold;

    public User() {

        super();
    }

    public User(int id) {

        super(id);
    }

    public User(int id, String email) {

        super(id);
        this.email = email;
    }

    @Override
    public void setName(String name) {

        if (name != null && name.trim().length() == 0) {
            this.name = null;
        } else {
            this.name = name;
        }

        generateFullName();
    }

    public String getFullName() {

        return fullName;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;

        generateFullName();
    }

    public String getNick() {

        return nick;
    }

    public void setNick(String nick) {

        this.nick = nick != null ? nick.trim() : null;
    }

    public UserInfo getInfo() {

        if (info == null) {
            info = new UserInfo();
        }

        return info;
    }

    public String getConfirmCode() {

        return confirmCode;
    }

    public void setConfirmCode(String confirmCode) {

        this.confirmCode = confirmCode;
    }

    public boolean isConfirmed() {

        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {

        this.confirmed = confirmed;
    }

    public List<Role> getRoles() {

        if (roles == null) {
            loadRoles();
        }

        return roles;
    }

    private void loadRoles() {

        if (roles != null) {
            roles.clear();
        }

        roles = new RoleDAOImpl().list(this);
    }

    public List<SocialNetwork> getSocialNetworks() {

        return socialNetworks;
    }

    public void loadSocialNetwords() {

        if (socialNetworks != null) {
            socialNetworks.clear();
        }

        socialNetworks = new SocialNetworkDAOImpl().list(this);
    }

    public SocialNetwork getSocialNetwork(int socialTypeId) {

        if (socialNetworks == null) {
            loadSocialNetwords();
        }

        if (socialNetworks != null) {
            for (SocialNetwork social : socialNetworks) {
                if (social.getType().getId() == socialTypeId) {
                    return social;
                }
            }
        }

        return null;
    }

    public String getSocialNetworkAccount(int socialTypeId) {

        SocialNetwork social = getSocialNetwork(socialTypeId);
        if (social != null) {
            return social.getAccount();
        }

        return "";
    }

    public List<Post> getPosts() {

        return posts;
    }

    public void loadPosts() {

        if (posts != null) {
            posts.clear();
        }

        posts = new PostDAOImpl().list(this);
    }

    public List<User> getFriends() {

        return friends;
    }

    public void loadFriends() {

        loadFriends(0, Integer.MAX_VALUE);
    }

    public void loadFriends(int offset, int count) {

        if (friends != null) {
            friends.clear();
        }

        friends = new FriendDAOImpl().list(this, offset, count);
    }

    public void clearFriends() {

        if (friends != null) {
            friends.clear();
        }

        friends = null;
    }

    public boolean hisFriend(User friend) {

        if (friends == null) {
            loadFriends();
        }

        if (friends == null || friends.size() == 0) {
            return false;
        }

        for (User user : friends) {
            if (user.getId() == friend.getId()) {
                return true;
            }
        }

        return false;
    }

    public String getProfileUrl() {

        if (nick != null && nick.length() != 0) {
            return nick;
        }

        return "id" + id;
    }

    public String getAvatarUrl() {

        final String AVATAR_BASE_URL = String.format("/users/profile/%d/avatar", id);

        if (info.getAvatar() != null) {
            return String.format("%s/%d/%s", AVATAR_BASE_URL, info.getAvatar().getId(), info.getAvatar().getFileName());
        } else {
            return info.getDefaultAvatar().toString();
        }
    }

    public boolean isHold() {

        return hold;
    }

    public void setHold(boolean hold) {

        this.hold = hold;
    }

    @Override
    public String toString() {

        return fullName;
    }

    /**
     * Генерирует новый код подтверждения
     *
     * @return
     */
    public static String genConfirmCode() {

        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    private void generateFullName() {

        if (name != null) {
            fullName = name;
        } else if (nick != null) {
            fullName = nick;
        } else {
            fullName = email;
        }
    }
}