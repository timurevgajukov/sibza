package org.sibza.model.user;

/**
 * Created by timur on 10.10.15.
 *
 * Данные по регистрации пользователя
 */
public class UserReg {

    private String email;
    private String name;
    private String password1;
    private String password2;

    public UserReg(String email, String name, String password1, String password2) {

        this.email = email != null ? email.trim() : null;
        this.name = name != null ? name.trim() : null;
        this.password1 = password1 != null ? password1.trim() : null;
        this.password2 = password2 != null ? password2.trim() : null;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getPassword1() {

        return password1;
    }

    public void setPassword1(String password1) {

        this.password1 = password1;
    }

    public String getPassword2() {

        return password2;
    }

    public void setPassword2(String password2) {

        this.password2 = password2;
    }
}