package org.sibza.model.user.dao;

import org.sibza.cache.DefaultAvatars;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.avatar.DefaultAvatar;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.Role;
import org.sibza.model.user.User;
import org.sibza.model.user.UserInfo;
import org.sibza.model.user.dao.interfaces.UserDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с пользователями
 */
public class UserDAOImpl extends ModelDAOImpl implements UserDAO {

    private static final String QUERY_GET_BY_ID = "{ call users_get(?) }";
    private static final String QUERY_GET_BY_NICK = "{ call users_get_by_nick(?) }";
    private static final String QUERY_GET_BY_EMAIL_AND_CODE = "{ call users_get_by_email_and_code(?,?) }";
    private static final String QUERY_AUTH = "{ call users_auth(?,?) }";
    private static final String QUERY_REG = "{ call users_reg(?,?,?,?,?) }";
    private static final String QUERY_LIST = "{ call users_list(?,?) }";
    private static final String QUERY_CHECK_EMAIL = "{ call users_check_email(?) }";
    private static final String QUERY_CONFIRM_EMAIL = "{ call users_confirm_email(?,?) }";
    private static final String QUERY_SAVE = "{ call users_info_save(?,?,?,?,?,?,?,?,?,?,?) }";
    private static final String QUERY_CHANGE_PWD = "{ call users_change_password(?,?,?) }";
    private static final String QUERY_RECOVER_PWD = "{ call users_recover_password(?,?) }";
    private static final String QUERY_STAT_COUNT = "{ call statistics_users_count_all() }";

    @Override
    public User get(int id) {

        List<User> users = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (users == null || users.size() == 0) {
            return null;
        }

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();
        User user = users.get(0);
        user.getInfo().setDefaultAvatar(defaultAvatars.get(user.getInfo().getDefaultAvatar().getId()));

        return users.get(0);
    }

    @Override
    public User get(String nick) {

        List<User> users = jdbcTemplate.query(QUERY_GET_BY_NICK, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, nick);

        if (users == null || users.size() == 0) {
            return null;
        }

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();
        User user = users.get(0);
        user.getInfo().setDefaultAvatar(defaultAvatars.get(user.getInfo().getDefaultAvatar().getId()));

        return users.get(0);
    }

    @Override
    public User getByEmail(String email) {

        List<User> users = jdbcTemplate.query(QUERY_CHECK_EMAIL, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, email);

        if (users == null || users.size() == 0) {
            return null;
        }

        return users.get(0);
    }

    @Override
    public User getByEmail(String email, String code) {

        List<User> users = jdbcTemplate.query(QUERY_GET_BY_EMAIL_AND_CODE,
                new Object[] { email, code }, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (users == null || users.size() == 0) {
            return null;
        }

        return users.get(0);
    }

    @Override
    public User auth(String email, String password) {

        List<User> users = jdbcTemplate.query(QUERY_AUTH,
                new Object[]{email, password},
                new RowMapper<User>() {

                    @Override
                    public User mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (users == null || users.size() == 0) {
            return null;
        }

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();
        User user = users.get(0);
        user.getInfo().setDefaultAvatar(defaultAvatars.get(user.getInfo().getDefaultAvatar().getId()));

        return users.get(0);
    }

    @Override
    public User registration(String email, String password, String name, String confirmCode, Role role) {

        User user = jdbcTemplate.queryForObject(QUERY_REG,
                new Object[] { email, password, name, confirmCode, role.getId() },
                new RowMapper<User>() {

                    @Override
                    public User mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();
        user.getInfo().setDefaultAvatar(defaultAvatars.get(user.getInfo().getDefaultAvatar().getId()));

        return user;
    }

    @Override
    public List<User> list(int offset, int count) {

        final DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();

        List<User> users = jdbcTemplate.query(QUERY_LIST, new Object[] { offset, count }, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                User user = load(resultSet);
                user.getInfo().setDefaultAvatar(defaultAvatars.get(user.getInfo().getDefaultAvatar().getId()));

                return user;
            }
        });

        return users;
    }

    @Override
    public boolean checkEmail(String email) {

        List<User> users = jdbcTemplate.query(QUERY_CHECK_EMAIL, new RowMapper<User>() {

            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, email);

        return users != null && users.size() != 0;
    }

    @Override
    public boolean confirmEmail(String email, String confirmCode) {

        return jdbcTemplate.update(QUERY_CONFIRM_EMAIL, new Object[] { email, confirmCode }) != 0;
    }

    @Override
    public boolean save(User user) {

        UserInfo info = user.getInfo();

        return jdbcTemplate.update(QUERY_SAVE,
                new Object[] {
                        user.getId(),
                        user.getNick() != null ? user.getNick() : null,
                        user.getName() != null ? user.getName() : null,
                        info.getGender() != 0 ? info.getGender() : null,
                        info.getBirthday() != null ? new java.sql.Date(info.getBirthday().getTime()) : null,
                        info.getAddress() != null ? info.getAddress() : null,
                        info.getPhone() != null ? info.getPhone() : null,
                        info.getSite() != null ? info.getSite() : null,
                        info.getAbout() != null ? info.getAbout() : null,
                        info.getDefaultAvatar().getId(),
                        info.getAvatar() != null ? info.getAvatar().getId() : null
                }) != 0;
    }

    @Override
    public boolean changePassword(User user, String currentPassword, String newPassword) {

        return jdbcTemplate.update(QUERY_CHANGE_PWD, new Object[]{ user.getId(), currentPassword, newPassword }) != 0;
    }

    @Override
    public boolean recoverPassword(User user, String newPassword) {

        return jdbcTemplate.update(QUERY_RECOVER_PWD, new Object[]{ user.getId(), newPassword }) != 0;
    }

    @Override
    public int count() {

        return jdbcTemplate.queryForObject(QUERY_STAT_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }

    public User load(ResultSet rs) throws SQLException {

        User user = new User(rs.getInt("id"), rs.getString("email"));
        user.setName(rs.getString("name"));
        user.setNick(rs.getString("nick"));
        user.setConfirmCode(rs.getString("confirm_code"));
        user.setConfirmed(rs.getBoolean("is_confirmed"));
        user.setHold(rs.getBoolean("hold"));

        // дополнительная информация
        user.getInfo().setGender(rs.getInt("gender"));
        user.getInfo().setBirthday(rs.getDate("birthday"));
        user.getInfo().setAbout(rs.getString("about"));
        user.getInfo().setPhone(rs.getString("phone"));
        user.getInfo().setAddress(rs.getString("address"));
        user.getInfo().setSite(rs.getString("site"));
        user.getInfo().setDefaultAvatar(new DefaultAvatar(rs.getInt("default_avatar_id")));

        int avatarId = rs.getInt("avatar_id");
        if (avatarId != 0) {
            Resource avatar = new Resource(avatarId);
            String fileName = "img" + avatarId + "." + rs.getString("avatar_extension");
            avatar.setFileName(fileName);
            user.getInfo().setAvatar(avatar);
        }

        return user;
    }
}
