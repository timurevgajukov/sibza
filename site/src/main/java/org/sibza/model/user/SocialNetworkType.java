package org.sibza.model.user;

import org.sibza.model.Model;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Модель типа социальной сети
 */
public class SocialNetworkType extends Model {

    private String accountUrl;
    private String color;
    private String icon;

    public SocialNetworkType(int id) {

        super(id);
    }

    public SocialNetworkType(int id, String name, String accountUrl, String color, String icon) {

        super(id, name);
        this.accountUrl = accountUrl;
        this.color = color;
        this.icon = icon;
    }

    public String getAccountUrl() {

        return accountUrl;
    }

    public void setAccountUrl(String accountUrl) {

        this.accountUrl = accountUrl;
    }

    public String getColor() {

        return color;
    }

    public void setColor(String color) {

        this.color = color;
    }

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }
}