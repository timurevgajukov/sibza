package org.sibza.model.user.dao.interfaces;

import org.sibza.model.user.SocialNetwork;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Интерфейс для работы с аккаунтами пользователей в социальных сетях
 */
public interface SocialNetworkDAO {

    /**
     * Возвращает список аккаунтов пользователя
     *
     * @param user
     * @return
     */
    List<SocialNetwork> list(User user);

    /**
     * Сохраняет пользовательский аккаунт
     *
     * @param socialNetwork
     * @param user
     * @return
     */
    boolean save(SocialNetwork socialNetwork, User user);

    /**
     * Удаляет пользовательский аккаунт
     *
     * @param socialNetwork
     * @param user
     * @return
     */
    boolean delete(SocialNetwork socialNetwork, User user);
}
