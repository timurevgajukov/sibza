package org.sibza.model.user;

import org.sibza.model.Model;
import org.sibza.model.avatar.DefaultAvatar;
import org.sibza.model.resource.Resource;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by timur on 09.10.15.
 *
 * Дополнительная инфорация по пользователю
 */
public class UserInfo extends Model {

    private int gender;
    private Date birthday;
    private String about;
    private String address;
    private String phone;
    private String site;
    private DefaultAvatar defaultAvatar;
    private Resource avatar;

    public int getGender() {

        return gender;
    }

    public void setGender(int gender) {

        this.gender = gender;
    }

    public Date getBirthday() {

        return birthday;
    }

    public String getBirthdayStr() {

        if (birthday == null) {
            return null;
        }

        return new SimpleDateFormat("dd.MM.yyyy").format(birthday);
    }

    public void setBirthday(Date birthday) {

        this.birthday = birthday;
    }

    public String getAbout() {

        return about;
    }

    public void setAbout(String about) {

        if (about != null && about.trim().length() == 0) {
            this.about = null;
        } else {
            this.about = about;
        }
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        if (address != null && address.trim().length() == 0) {
            this.address = null;
        } else {
            this.address = address;
        }
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        if (phone != null && phone.trim().length() == 0) {
            this.phone = null;
        } else {
            this.phone = phone;
        }
    }

    public String getSite() {

        return site;
    }

    public void setSite(String site) {

        if (site != null && site.trim().length() == 0) {
            this.site = null;
        } else {
            this.site = site;
        }
    }

    public DefaultAvatar getDefaultAvatar() {

        return defaultAvatar;
    }

    public void setDefaultAvatar(DefaultAvatar defaultAvatar) {

        this.defaultAvatar = defaultAvatar;
    }

    public Resource getAvatar() {

        return avatar;
    }

    public void setAvatar(Resource avatar) {

        this.avatar = avatar;
    }
}