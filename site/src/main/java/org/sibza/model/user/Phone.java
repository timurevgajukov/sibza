package org.sibza.model.user;

import org.sibza.model.Model;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 31.05.15.
 *
 * Модель номера телефона
 */
public class Phone extends Model {

    private String phone;

    public Phone(int id, String phone) {

        super(id);
        this.phone = phone;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    @Override
    public String toString() {

        return phone;
    }
}