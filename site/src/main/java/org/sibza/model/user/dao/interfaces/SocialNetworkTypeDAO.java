package org.sibza.model.user.dao.interfaces;

import org.sibza.model.user.SocialNetworkType;

import java.util.List;

/**
 * Created by Тимур on 08.10.2015.
 *
 * Интерфейс для работы с типами социальных сетей
 */
public interface SocialNetworkTypeDAO {

    /**
     * Возвращает список поддерживаемых социальных сетей
     * @return
     */
    List<SocialNetworkType> list();
}