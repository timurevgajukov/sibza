package org.sibza.model.user.dao.interfaces;

import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 07.03.16.
 *
 * Интерфейс для работы со списками друзей пользователя
 */
public interface FriendDAO {

    /**
     * Возвращает список друзей пользователя
     *
     * @param user
     * @param offset
     * @param count
     * @return
     */
    List<User> list(User user, int offset, int count);

    /**
     * Сохраняет связку пользователя с его другом
     *
     * @param user
     * @param friend
     * @return
     */
    boolean save(User user, User friend);

    /**
     * Удаляет связку пользователя с его другом
     *
     * @param user
     * @param friend
     * @return
     */
    boolean delete(User user, User friend);
}