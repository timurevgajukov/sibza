package org.sibza.model.comment.dao;

import org.sibza.cache.Languages;
import org.sibza.model.Model;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.audio.Audio;
import org.sibza.model.comment.Comment;
import org.sibza.model.comment.dao.interfaces.CommentDAO;
import org.sibza.model.language.Language;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.model.video.Video;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 19.10.2015.
 *
 * Реализация интерфейса для работы с комментариями
 */
public class CommentDAOImpl extends ModelDAOImpl implements CommentDAO {

    private static final String QUERY_LIST_BY_OBJECT = "{ call comments_list_by_object(?,?,?) }";
    private static final String QUERY_SAVE = "{ call comments_save(?,?,?,?) }";
    private static final String QUERY_SAVE_BY_OBJECT = "{ call comments_save_by_object(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call comments_delete(?,?) }";

    @Override
    public List<Comment> list(User user, Model object) {

        final Languages languages = Languages.getInstance();

        List<Comment> comments = jdbcTemplate.query(QUERY_LIST_BY_OBJECT,
                new Object[]{ user.getId(), getType(object), object.getId() },
                new RowMapper<Comment>() {

                    @Override
                    public Comment mapRow(ResultSet resultSet, int i) throws SQLException {

                        Comment comment = load(resultSet);
                        comment.setLanguage(languages.get(comment.getLanguage().getId()));

                        return comment;
                    }
                });

        // добавляем информацию по пользователям
        if (comments != null) {
            for (Comment comment : comments) {
                comment.setUser(new UserDAOImpl().get(comment.getUser().getId()));
            }
        }

        return comments;
    }

    @Override
    public boolean save(Comment comment) {

        Comment newComment = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        comment.getId() == 0 ? null : comment.getId(),
                        comment.getLanguage().getId(),
                        comment.getUser().getId(),
                        comment.getBody()
                }, new RowMapper<Comment>() {

                    @Override
                    public Comment mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        comment.setId(newComment.getId());
        comment.setCreateDt(newComment.getCreateDt());

        return true;
    }

    @Override
    public boolean save(Comment comment, Model object) {

        return jdbcTemplate.update(QUERY_SAVE_BY_OBJECT,
                new Object[] { comment.getId(), getType(object), object.getId(), comment.getUser().getId() }) != 0;
    }

    @Override
    public boolean delete(Comment comment) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { comment.getId(), comment.getUser().getId() }) != 0;
    }

    private Comment load(ResultSet rs) throws SQLException {

        User user = new User(rs.getInt("user_id"));
        Language language = new Language(rs.getInt("lang_id"));
        Comment comment = new Comment(rs.getInt("id"), rs.getTimestamp("create_dt_tz"), language, user, rs.getString("body"));

        return comment;
    }

    private String getType(Model object) {

        if (object instanceof Comment) {
            return Comment.TYPE_COMMENT;
        } else if (object instanceof Proverb) {
            return Comment.TYPE_PROVERB;
        } else if (object instanceof Audio) {
            return Comment.TYPE_AUDIO;
        } else if (object instanceof Video) {
            return Comment.TYPE_VIDEO;
        }

        return "UNKNOWN";
    }
}