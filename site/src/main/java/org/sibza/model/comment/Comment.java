package org.sibza.model.comment;

import org.sibza.model.Model;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Тимур on 19.10.2015.
 *
 * Модель комментария
 */
public class Comment extends Model {

    public static final String TYPE_COMMENT = "COMMENT";
    public static final String TYPE_PROVERB = "PROVERB";
    public static final String TYPE_AUDIO = "AUDIO";
    public static final String TYPE_VIDEO = "VIDEO";

    private Date createDt;
    private Language language;
    private User user;
    private String body;

    // вычисляемые поля
    private String formattedCreateDt;
    private boolean editable;

    public Comment(Language language, User user, String body) {

        super();
        setCreateDt(null);
        this.language = language;
        this.user = user;
        this.body = body;
    }

    public Comment(int id, Date createDt, Language language, User user, String body) {

        super(id);
        setCreateDt(createDt);
        this.language = language;
        this.user = user;
        this.body = body;
    }

    public Date getCreateDt() {

        return createDt;
    }

    public String getFormattedCreateDt() {

        return formattedCreateDt;
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
        if (createDt != null) {
            formattedCreateDt = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(createDt);
        } else {
            formattedCreateDt = "";
        }
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public boolean isEditable() {

        return editable;
    }

    public void setEditable(boolean editable) {

        this.editable = editable;
    }

    @Override
    public String toString() {

        return body;
    }
}