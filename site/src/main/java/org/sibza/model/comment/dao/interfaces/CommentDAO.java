package org.sibza.model.comment.dao.interfaces;

import org.sibza.model.Model;
import org.sibza.model.comment.Comment;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 19.10.2015.
 *
 * Интерфейс для работы с комментариями
 */
public interface CommentDAO {

    /**
     * Возвращает список комментариев к объекту
     *
     * @param user пользователь, который запросил комментарии (нужно для корректировки времени)
     * @param object объект, по которому получаем комментарии
     * @return
     */
    List<Comment> list(User user, Model object);

    /**
     * Сохраняет комментарий без привязки к объекту
     *
     * @param comment
     * @return
     */
    boolean save(Comment comment);

    /**
     * Сохраняет привязку комментария к объекту
     *
     * @param comment привязываемый комментарий
     * @param object привязываемый объект
     * @return
     */
    boolean save(Comment comment, Model object);

    /**
     * Удаление комментария
     *
     * @param comment
     * @return
     */
    boolean delete(Comment comment);
}