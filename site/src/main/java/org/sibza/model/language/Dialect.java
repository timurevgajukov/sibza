package org.sibza.model.language;

import org.sibza.model.Model;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 30.05.15.
 *
 * Модель диалекта языка
 */
public class Dialect extends Model {

    private Language language;

    public Dialect(int id, String name, Language language) {

        super(id, name);
        this.language = language;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }
}
