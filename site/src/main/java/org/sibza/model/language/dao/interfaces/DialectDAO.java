package org.sibza.model.language.dao.interfaces;

import org.sibza.model.language.Dialect;
import org.sibza.model.language.Language;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с диалектом языка
 */
public interface DialectDAO {

    /**
     * Возвращает список доступных диалектов для языка
     *
     * @param language
     * @return
     */
    List<Dialect> list(Language language);
}
