package org.sibza.model.language;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.sibza.model.Model;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.WordDAOImpl;

import java.util.List;

/**
 * Created by timur on 09.05.15.
 *
 * Модель языка
 */
public class Language extends Model {

    // мнемонические обозначения языков
    public static final String AD = "AD";
    public static final String RU = "RU";
    public static final String EN = "EN";
    public static final String TU = "TU";

    private String code;
    private String smallName;
    @JsonIgnore
    private boolean learn;
    @JsonIgnore
    private boolean siteInterface;
    @JsonIgnore
    private String icon;

    public Language(int id) {

        super(id);
    }

    public Language(String code) {

        super();
        this.code = code;
    }

    public Language(int id, String code, String name, String smallName, boolean learn, boolean siteInterface, String icon) {

        super(id, name);
        this.code = code;
        this.smallName = smallName;
        this.learn = learn;
        this.siteInterface = siteInterface;
        this.icon = icon;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getSmallName() {

        return smallName;
    }

    public void setSmallName(String smallName) {

        this.smallName = smallName;
    }

    public boolean isLearn() {

        return learn;
    }

    public void setLearn(boolean learn) {

        this.learn = learn;
    }

    public boolean isSiteInterface() {

        return siteInterface;
    }

    public void setSiteInterface(boolean siteInterface) {

        this.siteInterface = siteInterface;
    }

    public String getIcon() {

        return icon;
    }

    public void setIcon(String icon) {

        this.icon = icon;
    }

    @JsonIgnore
    public List<Word> getWords() {

        return new WordDAOImpl().list(this);
    }
}