package org.sibza.model.language.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.language.Dialect;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.interfaces.DialectDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с диалектом языка
 */
public class DialectDAOImpl extends ModelDAOImpl implements DialectDAO {

    private static final String QUERY_LIST = "{ call dialects_list(?) }";

    @Override
    public List<Dialect> list(final Language language) {

        List<Dialect> dialects = jdbcTemplate.query(QUERY_LIST, new RowMapper<Dialect>() {

            @Override
            public Dialect mapRow(ResultSet resultSet, int i) throws SQLException {

                Dialect dialect = load(resultSet);
                dialect.setLanguage(language);

                return dialect;
            }
        });

        return dialects;
    }

    private Dialect load(ResultSet rs) throws SQLException {

        Language language = new Language(rs.getInt("lang_id"));
        return new Dialect(rs.getInt("id"), rs.getString("name"), language);
    }
}