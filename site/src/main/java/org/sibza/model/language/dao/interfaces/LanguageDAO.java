package org.sibza.model.language.dao.interfaces;

import org.sibza.model.language.Language;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с языком
 */
public interface LanguageDAO {

    /**
     * Возвращает язык по его мнемоническому коду
     *
     * @param code мнемонический код
     * @return
     */
    Language get(String code);

    /**
     * Возвращает список доступных языков
     *
     * @return
     */
    List<Language> list();

    /**
     * Возвращает общее количество доступных языков
     *
     * @return
     */
    int count();
}