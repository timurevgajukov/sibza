package org.sibza.model.language.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.interfaces.LanguageDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с языками
 */
public class LanguageDAOImpl extends ModelDAOImpl implements LanguageDAO {

    private static final String QUERY_GET_BY_CODE = "{ call languages_get_by_code(?) }";
    private static final String QUERY_LIST = "{ call languages_list() }";
    private static final String QUERY_STAT_COUNT = "{ call statistics_languages_count_all() }";

    @Override
    public Language get(String code) {

        List<Language> languages = jdbcTemplate.query(QUERY_GET_BY_CODE, new RowMapper<Language>() {

            @Override
            public Language mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, code);

        if (languages == null || languages.size() == 0) {
            return null;
        }

        return languages.get(0);
    }

    @Override
    public List<Language> list() {

        List<Language> languages = jdbcTemplate.query(QUERY_LIST, new RowMapper<Language>() {

            @Override
            public Language mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return languages;
    }

    @Override
    public int count() {

        return jdbcTemplate.queryForObject(QUERY_STAT_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }

    private Language load(ResultSet rs) throws SQLException {

        return new Language(rs.getInt("id"), rs.getString("code"), rs.getString("name"), rs.getString("small_name"),
                rs.getBoolean("learn"), rs.getBoolean("interface"), rs.getString("icon"));
    }
}