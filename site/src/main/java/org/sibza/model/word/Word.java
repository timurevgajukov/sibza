package org.sibza.model.word;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.sibza.model.language.Language;
import org.sibza.model.Model;
import org.sibza.cache.Categories;
import org.sibza.cache.Languages;
import org.sibza.model.category.Category;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.word.dao.WordDAOImpl;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 10.05.15.
 *
 * Модель слова
 */
public class Word extends Model {

    private String word;
    private Category category;
    private Language language;
    private String note; // используется в качестве комментария к переводу
    private List<Word> translates;
    private List<Resource> resources;

    public Word() {

        super();
    }

    public Word(int id) {

        super(id);
    }

    public Word(int id, String word, Category category, Language language) {

        super(id);
        this.word = word;
        this.category = category;
        this.language = language;
    }

    public String getWord() {

        return word;
    }

    public void setWord(String word) {

        this.word = word;
    }

    public Category getCategory() {

        return category;
    }

    public void setCategory(Category category) {

        this.category = category;
    }

    public void clearCategory() {

        category = null;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    @JsonIgnore
    public String getNote() {

        return note;
    }

    public void setNote(String note) {

        this.note = note;
    }

    public List<Word> getTranslates() {

        return translates;
    }

    public void loadTranslates() {

        if (translates != null) {
            translates.clear();
        }

        translates = new WordDAOImpl().translate(this);
    }

    public Word getTranslate(Language language) {

        return getTranslate(language, false);
    }

    public Word getTranslate(Language language, boolean clearOther) {

        loadTranslates();
        List<Word> translates = getTranslates();

        for (Word word : translates) {
            if (word.getLanguage().getId() == language.getId()) {
                if (clearOther) {
                    translates.clear();
                    translates.add(word);
                }
                return word;
            }
        }

        // не смогли найти прямого перевода, постараемся найти через цепочки переводов
        // пока поддерживается только один дополнительный шаг
        for (Word word : translates) {
            List<Word> newTranslates = word.getTranslates();
            if (newTranslates != null && newTranslates.size() != 0) {
                for (Word translate : newTranslates) {
                    if (translate.getLanguage().getId() == language.getId()) {
                        if (clearOther) {
                            translates.clear();
                            translates.add(translate);
                        }
                        return translate;
                    }
                }
            }
        }

        // не нашли перевод слова
        return null;
    }

    public List<Resource> getResources() {

        if (resources == null) {
            resources = new ResourceDAOImpl().list(this);
        }

        return resources;
    }

    @Override
    public String toString() {

        return word;
    }
}