package org.sibza.model.word.validate;

import org.sibza.model.Model;
import org.sibza.model.word.Word;
import org.sibza.validate.AValidateRule;

/**
 * Created by Тимур on 18.12.2015.
 *
 * Правило валидации: слово не должно быть пустым
 */
public class WordRule extends AValidateRule {

    public WordRule(String message) {

        super(message);
    }

    @Override
    public boolean validate(Model model) {

        if (model == null) {
            return false;
        }
        if (!(model instanceof Word)) {
            return false;
        }

        Word word = (Word) model;

        return (word.getWord() != null && word.getWord().trim().length() != 0);
    }
}