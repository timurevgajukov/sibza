package org.sibza.model.word.dao;

import org.sibza.cache.Categories;
import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.category.Category;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;
import org.sibza.model.word.dao.interfaces.WordDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 * <p/>
 * Реализация интерфейса для работы со словами
 */
public class WordDAOImpl extends ModelDAOImpl implements WordDAO {

    private static final String QUERY_GET_BY_ID = "{ call words_get(?) }";
    private static final String QUERY_LIST = "{ call words_list() }";
    private static final String QUERY_LIST_BY_LANGUAGE = "{ call words_list_by_language(?) }";
    private static final String QUERY_LIST_BY_LANG_AND_CATEGORY = "{ call words_list_by_language_and_category(?,?,?,?) }";
    private static final String QUERY_SEARCH = "{ call words_search(?) }";
    private static final String QUERY_SEARCH_BY_LANGUAGE = "{ call words_search_by_language(?,?) }";
    private static final String QUERY_TRANSLATES = "{ call words_translate_list(?) }";
    private static final String QUERY_SAVE = "{ call words_save(?,?,?,?,?) }";
    private static final String QUERY_SAVE_TRANSLATE = "{ call translates_save(?,?,?,?) }";
    private static final String QUERY_SAVE_TRANSLATE_BOTH = "{ call translates_both_save(?,?,?,?) }";
    private static final String QUERY_SAVE_RESOURCE_REF = "{ call resources_words_save(?,?,?) }";
    private static final String QUERY_DELETE = "{ call words_delete(?,?) }";
    private static final String QUERY_DELETE_TRANSLATE = "{ call translates_delete(?,?,?) }";
    private static final String QUERY_DELETE_RESOURCE_REF = "{ call resources_words_delete(?,?,?) }";
    private static final String QUERY_STAT_COUNT = "{ call statistics_words_count_all() }";

    @Override
    public Word get(int id) {

        List<Word> words = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Word>() {

            @Override
            public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        if (words == null || words.size() == 0) {
            return null;
        }

        Word word = words.get(0);

        Languages languages = Languages.getInstance();
        Categories categories = Categories.getInstance(null, word.getLanguage());

        word.setLanguage(languages.get(word.getLanguage().getId()));
        word.setCategory(categories.get(word.getCategory().getId()));

        return word;
    }

    @Override
    public Word get(String word) {

        List<Word> words = search(word);
        if (words == null || words.size() == 0) {
            return null;
        }

        for (Word item : words) {
            if (word.equalsIgnoreCase(item.getWord())) {
                return item;
            }
        }

        return null;
    }

    @Override
    public List<Word> list() {

        List<Word> words = jdbcTemplate.query(QUERY_LIST, new RowMapper<Word>() {

            @Override
            public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (words == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        Categories categories = Categories.getInstance(null, languages.get(Language.RU));
        for (Word item : words) {
            item.setLanguage(languages.get(item.getLanguage().getId()));
            item.setCategory(categories.get(item.getCategory().getId()));
        }

        return words;
    }

    @Override
    public List<Word> list(final Language language) {

        List<Word> words = jdbcTemplate.query(QUERY_LIST_BY_LANGUAGE, new RowMapper<Word>() {

            @Override
            public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                Word word = load(resultSet);
                word.setLanguage(language);

                return word;
            }
        }, language.getId());

        Categories categories = Categories.getInstance(null, language);
        for (Word item : words) {
            item.setCategory(categories.get(item.getCategory().getId()));
        }

        return words;
    }

    @Override
    public List<Word> list(final Language language, final Category category, int offset, int count) {

        List<Word> words = jdbcTemplate.query(QUERY_LIST_BY_LANG_AND_CATEGORY,
                new Object[]{
                        language.getId(),
                        category.getId(),
                        offset,
                        count
                }, new RowMapper<Word>() {

                    @Override
                    public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                        Word word = load(resultSet);
                        word.setLanguage(language);
                        word.setCategory(category);

                        return word;
                    }
                });

        return words;
    }

    @Override
    public List<Word> list(Letter letter) {

        List<Word> words = jdbcTemplate.query(QUERY_SEARCH_BY_LANGUAGE,
                new Object[]{
                        letter.getLetter() + "%",
                        letter.getLanguage().getId()
                }, new RowMapper<Word>() {

                    @Override
                    public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        if (words == null) {
            return new ArrayList<Word>();
        }

        Languages languages = Languages.getInstance();
        Categories categories = Categories.getInstance(null, letter.getLanguage());
        for (Word item : words) {
            item.setLanguage(languages.get(item.getLanguage().getId()));
            item.setCategory(categories.get(item.getCategory().getId()));
        }

        return words;
    }

    @Override
    public List<Word> search(String query) {

        List<Word> words = jdbcTemplate.query(QUERY_SEARCH, new RowMapper<Word>() {

            @Override
            public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, "%" + query + "%");

        if (words == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        Categories categories = Categories.getInstance(null, languages.get(Language.RU));
        for (Word item : words) {
            item.setLanguage(languages.get(item.getLanguage().getId()));
            item.setCategory(categories.get(item.getCategory().getId()));
        }

        return words;
    }

    @Override
    public List<Word> translate(Word word) {

        List<Word> words = jdbcTemplate.query(QUERY_TRANSLATES, new RowMapper<Word>() {

            @Override
            public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                Word word = load(resultSet);
                word.setNote(resultSet.getString("note"));

                return word;
            }
        }, word.getId());

        if (words == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        Categories categories = Categories.getInstance(null, languages.get(Language.RU));
        for (Word item : words) {
            item.setLanguage(languages.get(item.getLanguage().getId()));
            item.setCategory(categories.get(item.getCategory().getId()));
        }

        return words;
    }

    @Override
    public boolean save(Word word, User user) {

        Word newWord = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        word.getId() == 0 ? null : word.getId(),
                        word.getWord(),
                        word.getLanguage().getId(),
                        word.getCategory().getId(),
                        user.getId()
                }, new RowMapper<Word>() {

                    @Override
                    public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        word.setId(newWord.getId());

        return true;
    }

    @Override
    public boolean save(Word word1, Word word2, String note, boolean both, User user) {

        String query = QUERY_SAVE_TRANSLATE;
        if (both) {
            query = QUERY_SAVE_TRANSLATE_BOTH;
        }

        return jdbcTemplate.update(query,
                new Object[]{
                        word1.getId(),
                        word2.getId(),
                        note == null || note.trim().length() == 0 ? null : note.trim(),
                        user.getId()
                }) != 0;
    }

    @Override
    public boolean save(Word word, Resource resource, User user) {

        return jdbcTemplate.update(QUERY_SAVE_RESOURCE_REF,
                new Object[]{ resource.getId(), word.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean delete(Word word, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ word.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean delete(Word word1, Word word2, User user) {

        return jdbcTemplate.update(QUERY_DELETE_TRANSLATE,
                new Object[]{ word1.getId(), word2.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean delete(Word word, Resource resource, User user) {

        return jdbcTemplate.update(QUERY_DELETE_RESOURCE_REF,
                new Object[]{ resource.getId(), word.getId(), user.getId() }) != 0;
    }

    @Override
    public int count() {

        return jdbcTemplate.queryForObject(QUERY_STAT_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }

    private Word load(ResultSet rs) throws SQLException {

        Category category = new Category(rs.getInt("category_id"));
        Language language = new Language(rs.getInt("lang_id"));
        return new Word(rs.getInt("id"), rs.getString("word"), category, language);
    }
}