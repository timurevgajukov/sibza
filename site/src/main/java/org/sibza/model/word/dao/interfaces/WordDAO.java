package org.sibza.model.word.dao.interfaces;

import org.sibza.model.alphabet.Letter;
import org.sibza.model.category.Category;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы со словами
 */
public interface WordDAO {

    /**
     * Возвращает слово по его идентификатору
     *
     * @param id идентификатор слова
     * @return
     */
    Word get(int id);

    /**
     * Ищет слово и возвращает объект по нему
     *
     * @param word
     * @return
     */
    Word get(String word);

    /**
     * Возвращает список слов
     * @return
     */
    List<Word> list();

    /**
     * Возвращает список слов по определенному языку
     *
     * @param language
     * @return
     */
    List<Word> list(Language language);

    /**
     * Возвращает список слов по языку и категории
     *
     * @param language
     * @param category
     * @param offset
     * @param count
     * @return
     */
    List<Word> list(Language language, Category category, int offset, int count);

    /**
     * Возвращает список слов, которые начинаются на указанную букву
     *
     * @param letter
     * @return
     */
    List<Word> list(Letter letter);

    /**
     * Поиск слов
     *
     * @param query
     * @return
     */
    List<Word> search(String query);

    /**
     * Возвращает переводы для выбранного слова
     *
     * @param word
     * @return
     */
    List<Word> translate(Word word);

    /**
     * Сохраняет слово
     *
     * @param word
     * @param user
     * @return
     */
    boolean save(Word word, User user);

    /**
     * Сохраняет перевод для слова
     *
     * @param word1 слова
     * @param word2 перевод для слова
     * @param note комментарий к переводу
     * @param both признак двунаправленного перевода
     * @param user
     * @return
     */
    boolean save(Word word1, Word word2, String note, boolean both, User user);

    /**
     * Сохраняет связь слова с медиа ресурсом
     *
     * @param word слово
     * @param resource медиа ресурс
     * @param user
     * @return
     */
    boolean save(Word word, Resource resource, User user);

    /**
     * Удаляет слово
     *
     * @param word
     * @param user
     * @return
     */
    boolean delete(Word word, User user);

    /**
     * Удаляет связь перевода для слова
     *
     * @param word1
     * @param word2
     * @param user
     * @return
     */
    boolean delete(Word word1, Word word2, User user);

    /**
     * Удаляет связь слова с медиа ресурсом
     *
     * @param word слово
     * @param resource медиа ресурс
     * @param user
     * @return
     */
    boolean delete(Word word, Resource resource, User user);

    /**
     * Возвращает количество слов изучения в словаре
     *
     * @return
     */
    int count();
}
