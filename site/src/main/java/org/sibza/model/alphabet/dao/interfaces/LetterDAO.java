package org.sibza.model.alphabet.dao.interfaces;

import org.sibza.model.alphabet.Letter;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с буквами
 */
public interface LetterDAO {

    /**
     * Загружаем букву по его идентификатору
     *
     * @param id идентификатор буквы
     * @return
     */
    Letter get(int id);

    /**
     * Возвращает буквы для алфавита языка
     *
     * @param language язык, для которого возвращается алфавит
     * @return
     */
    List<Letter> list(Language language);

    /**
     * Сохраняет букву
     *
     * @param language к какому языку относится буква
     * @param letter буква
     * @param user
     * @return
     */
    boolean save(Language language, Letter letter, User user);

    /**
     * Сохраняет связку между буквой алфавита и медиа ресурсом
     *
     * @param letter
     * @param resource
     * @param user
     * @return
     */
    boolean save(Letter letter, Resource resource, User user);

    /**
     * Удаляет букву
     *
     * @param letter буква
     * @param user
     * @return
     */
    boolean delete(Letter letter, User user);

    /**
     * Удаляет все ресурсы, привязанные к букве алфавита
     *
     * @param letter
     * @param user
     * @return
     */
    boolean deleteResources(Letter letter, User user);
}
