package org.sibza.model.alphabet.dao.interfaces;

import org.sibza.model.alphabet.Letter;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;

import java.util.List;

/**
 * Created by timur on 17.02.16.
 *
 * Интерфейс для работы с связками букв со словами
 */
public interface LetterWordDAO {

    /**
     * Возвращает список слов для буквы
     *
     * @param letter
     * @return
     */
    List<Word> list(Letter letter);

    /**
     * Сохраняет связку слова с буквой
     *
     * @param letter
     * @param word
     * @param user
     * @return
     */
    boolean save(Letter letter, Word word, User user);

    /**
     * Удаляет все связки слов с буквой
     *
     * @param letter
     * @param user
     * @return
     */
    boolean delete(Letter letter, User user);

    /**
     * Удаляет связку буквы со словом
     *
     * @param letter
     * @param word
     * @param user
     * @return
     */
    boolean delete(Letter letter, Word word, User user);
}
