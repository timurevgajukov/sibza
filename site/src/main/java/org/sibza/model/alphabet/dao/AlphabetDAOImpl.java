package org.sibza.model.alphabet.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.alphabet.Alphabet;
import org.sibza.model.alphabet.dao.interfaces.AlphabetDAO;
import org.sibza.model.language.Language;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с алфавитом
 */
public class AlphabetDAOImpl extends ModelDAOImpl implements AlphabetDAO {

    private static final String QUERY_STAT_COUNT = "{ call statistics_alphabet_count_all() }";

    @Override
    public Alphabet get(Language language) {

        Alphabet alphabet = new Alphabet(language);
        alphabet.setLetters(new LetterDAOImpl().list(language));

        return alphabet;
    }

    @Override
    public int count() {

        return jdbcTemplate.queryForObject(QUERY_STAT_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }
}