package org.sibza.model.alphabet.dao.interfaces;

import org.sibza.model.alphabet.Alphabet;
import org.sibza.model.language.Language;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с алфавитом
 */
public interface AlphabetDAO {

    /**
     * Возвращает алфавит языка
     *
     * @param language язык, для которого возвращается алфавит
     * @return
     */
    Alphabet get(Language language);

    /**
     * Возвращает общее количество букв в алфавите
     *
     * @return
     */
    int count();
}
