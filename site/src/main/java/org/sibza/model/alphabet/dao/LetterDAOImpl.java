package org.sibza.model.alphabet.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.alphabet.dao.interfaces.LetterDAO;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с буквами
 */
public class LetterDAOImpl extends ModelDAOImpl implements LetterDAO {

    private static final String QUERY_GET_BY_ID = "{ call alphabet_letter_get(?) }";
    private static final String QUERY_LIST = "{ call alphabet_list(?) }";
    private static final String QUERY_SAVE = "{ call alphabet_letter_save(?,?,?,?,?) }";
    private static final String QUERY_SAVE_RESOURCE_REF = "{ call resources_alphabet_save(?,?,?) }";
    private static final String QUERY_DELETE = "{ call alphabet_letter_delete(?,?) }";
    private static final String QUERU_DELETE_RESOURCE_REF = "{ call resources_alphabet_delete(?,?) }";

    @Override
    public Letter get(int id) {

        Letter letter = jdbcTemplate.queryForObject(QUERY_GET_BY_ID, new RowMapper<Letter>() {

            @Override
            public Letter mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        return letter;
    }

    @Override
    public List<Letter> list(final Language language) {

        List<Letter> letters = jdbcTemplate.query(QUERY_LIST, new Object[]{language.getId()},
                new RowMapper<Letter>() {

                    @Override
                    public Letter mapRow(ResultSet resultSet, int i) throws SQLException {

                        Letter letter = load(resultSet);
                        letter.setLanguage(language);

                        return letter;
                    }
                });

        return letters;
    }

    @Override
    public boolean save(Language language, Letter letter, User user) {

        Letter newLetter = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        letter.getId() == 0 ? null : letter.getId(),
                        letter.getLetter(),
                        letter.getLowLetter(),
                        language.getId(),
                        user.getId()
                }, new RowMapper<Letter>() {

                    @Override
                    public Letter mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        letter.setId(newLetter.getId());

        return true;
    }

    @Override
    public boolean save(Letter letter, Resource resource, User user) {

        return jdbcTemplate.update(QUERY_SAVE_RESOURCE_REF, new Object[] { resource.getId(), letter.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean delete(Letter letter, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { letter.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean deleteResources(Letter letter, User user) {

        return jdbcTemplate.update(QUERU_DELETE_RESOURCE_REF, new Object[] { letter.getId(), user.getId() }) != 0;
    }

    private Letter load(ResultSet rs) throws SQLException {

        return new Letter(rs.getInt("id"), rs.getString("letter"), rs.getString("low_letter"));
    }
}