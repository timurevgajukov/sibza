package org.sibza.model.alphabet.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.alphabet.Letter;
import org.sibza.model.alphabet.dao.interfaces.LetterWordDAO;
import org.sibza.model.category.Category;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 17.02.16.
 *
 * Реализация интерфейса для работы со связками слов с буквами
 */
public class LetterWordDAOImpl extends ModelDAOImpl implements LetterWordDAO {

    private static final String QUERY_LIST_BY_LETTER = "{ call alphabet_words_list_by_letter(?) }";
    private static final String QUERY_SAVE = "{ call alphabet_words_save(?,?,?) }";
    private static final String QUERY_DELETE = "{ call alphabet_words_delete(?,?,?) }";
    private static final String QUERY_DELETE_BY_LETTER = "{ call alphabet_words_delete_by_letter(?,?) }";

    @Override
    public List<Word> list(Letter letter) {

        List<Word> list = jdbcTemplate.query(QUERY_LIST_BY_LETTER, new RowMapper<Word>() {

            @Override
            public Word mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, letter.getId());

        return list;
    }

    @Override
    public boolean save(Letter letter, Word word, User user) {

        return jdbcTemplate.update(QUERY_SAVE, new Object[] { letter.getId(), word.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean delete(Letter letter, User user) {

        return jdbcTemplate.update(QUERY_DELETE_BY_LETTER, new Object[] { letter.getId(), user.getId() }) != 0;
    }

    @Override
    public boolean delete(Letter letter, Word word, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { letter.getId(), word.getId(), user.getId() }) != 0;
    }

    private Word load(ResultSet rs) throws SQLException {

        Category category = new Category(rs.getInt("category_id"));
        Language language = new Language(rs.getInt("lang_id"));
        return new Word(rs.getInt("id"), rs.getString("word"), category, language);
    }
}