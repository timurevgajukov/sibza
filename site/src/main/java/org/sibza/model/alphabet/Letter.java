package org.sibza.model.alphabet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.sibza.model.alphabet.dao.LetterWordDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.Model;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.word.Word;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 10.05.15.
 *
 * Модель буквы алфавита
 */
public class Letter extends Model {

    private String letter;
    private String lowLetter;
    private Language language;
    private List<Resource> resources;
    private List<Word> words;

    public Letter() {

        super();
    }

    public Letter(int id) {

        super(id);
    }

    public Letter(int id, String letter, String lowLetter) {

        super(id);
        this.letter = letter;
        this.lowLetter = lowLetter;
    }

    public String getLetter() {

        return letter;
    }

    public void setLetter(String letter) {

        this.letter = letter;
    }

    public String getLowLetter() {

        return lowLetter;
    }

    public void setLowLetter(String lowLetter) {

        this.lowLetter = lowLetter;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public List<Resource> getResources() {

        return resources;
    }

    public void loadResources() {

        if (resources != null) {
            resources.clear();
        }

        resources = new ResourceDAOImpl().list(this, true);
    }

    public List<Word> getWords() {

        return words;
    }

    public void loadWords() {

        if (words != null) {
            words.clear();
        }

        words = new LetterWordDAOImpl().list(this);
    }

    @Override
    public String toString() {

        return letter;
    }
}