package org.sibza.model.alphabet;

import org.sibza.model.language.Language;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 10.05.15.
 *
 * Алфавит языка
 */
public class Alphabet {

    private List<Letter> letters;
    private Language language;

    {
        letters = new ArrayList<Letter>();
    }

    public Alphabet(Language language) {

        this.language = language;
    }

    public List<Letter> getLetters() {

        return letters;
    }

    public void setLetters(List<Letter> letters) {

        this.letters = letters;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }
}