package org.sibza.model.avatar.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.avatar.DefaultAvatar;
import org.sibza.model.avatar.dao.interfaces.DefaultAvatarDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Реализация интерфейса для работы со стандартными аватарками
 */
public class DefaultAvatarDAOImpl extends ModelDAOImpl implements DefaultAvatarDAO {

    private static final String QUERY_LIST = "{ call default_avatars_list() }";

    @Override
    public List<DefaultAvatar> list() {

        List<DefaultAvatar> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<DefaultAvatar>() {

            @Override
            public DefaultAvatar mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return list;
    }

    private DefaultAvatar load(ResultSet rs) throws SQLException {

        return new DefaultAvatar(rs.getInt("id"), rs.getString("file"));
    }
}