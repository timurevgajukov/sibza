package org.sibza.model.avatar;

import org.sibza.model.Model;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Стандартные аватарки
 */
public class DefaultAvatar extends Model {

    public static final String BASE_URL = "/resources/img/avatar/";

    private String file;

    public DefaultAvatar(int id) {

        super(id);
    }

    public DefaultAvatar(int id, String file) {

        super(id);
        this.file = file;
    }

    public String getFile() {

        return file;
    }

    public void setFile(String file) {

        this.file = file;
    }
    
    @Override
    public String toString() {

        return BASE_URL + file;
    }
}