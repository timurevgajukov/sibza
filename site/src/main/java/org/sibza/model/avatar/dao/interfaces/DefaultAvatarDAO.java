package org.sibza.model.avatar.dao.interfaces;

import org.sibza.model.avatar.DefaultAvatar;

import java.util.List;

/**
 * Created by Тимур on 13.10.2015.
 *
 * Интерфейс для работы со стандартными аватарками
 */
public interface DefaultAvatarDAO {

    List<DefaultAvatar> list();
}