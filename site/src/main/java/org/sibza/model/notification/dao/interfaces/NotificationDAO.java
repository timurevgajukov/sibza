package org.sibza.model.notification.dao.interfaces;

import org.sibza.model.notification.Notification;
import org.sibza.model.user.User;

/**
 * Created by Тимур on 12.01.2016.
 *
 * Интерфейс для работы с нотификациями на сайте
 */
public interface NotificationDAO {

    /**
     * Возвращает одну нотификация для пользователя
     *
     * @param user
     * @return
     */
    Notification get(User user);

    /**
     * Сохраняет новую нотификацию
     *
     * @param notification
     * @return
     */
    boolean save(Notification notification);

    /**
     * Удаляет нотификацию
     *
     * @param notification
     * @return
     */
    boolean delete(Notification notification);
}
