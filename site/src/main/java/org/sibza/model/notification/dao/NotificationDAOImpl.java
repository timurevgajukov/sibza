package org.sibza.model.notification.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.notification.Notification;
import org.sibza.model.notification.dao.interfaces.NotificationDAO;
import org.sibza.model.user.User;
import org.sibza.security.Message;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 12.01.2016.
 *
 * Реализация интерфейса для работы с нотификациями
 */
public class NotificationDAOImpl extends ModelDAOImpl implements NotificationDAO {

    private static final String QUERY_GET_BY_USER = "{ call notifications_get(?) }";
    private static final String QUERY_SAVE = "{ call notifications_save(?,?,?) }";
    private static final String QUERY_DELETE = "{ call notifications_delete(?) }";

    @Override
    public Notification get(User user) {

        List<Notification> notifications = jdbcTemplate.query(QUERY_GET_BY_USER, new RowMapper<Notification>() {

            @Override
            public Notification mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, user.getId());

        if (notifications == null || notifications.size() == 0) {
            return null;
        }

        return notifications.get(0);
    }

    @Override
    public boolean save(Notification notification) {

        return jdbcTemplate.update(QUERY_SAVE, new Object[] {
                notification.getUser().getId(),
                notification.getMessage().getBody(),
                notification.getMessage().getType()
        }) != 0;
    }

    @Override
    public boolean delete(Notification notification) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { notification.getId() }) != 0;
    }

    private Notification load(ResultSet rs) throws SQLException {

        User user = new User(rs.getInt("user_id"));
        Message message = new Message(rs.getString("body"), rs.getString("type"));
        return new Notification(rs.getInt("id"), rs.getTimestamp("create_dt"), user, message);
    }
}