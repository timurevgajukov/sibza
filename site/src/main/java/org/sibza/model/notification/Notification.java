package org.sibza.model.notification;

import org.sibza.model.Model;
import org.sibza.model.user.User;
import org.sibza.security.Message;

import java.util.Date;

/**
 * Created by Тимур on 12.01.2016.
 *
 * Нотификация для отображения на сайте
 */
public class Notification extends Model {

    public static final int ADMIN_ID = 2;

    private Date createDt;
    private User user;
    private Message message;
    private Channel channel;

    {
        channel = new Channel(Channel.SITE);
    }

    /**
     * Создает нотификацию для администратора
     *
     * @param body
     * @param type
     * @return
     */
    public static Notification create(String body, String type) {

        return new Notification(new User(ADMIN_ID), new Message(body, type));
    }

    public Notification(int id, Date createDt, User user, Message message) {

        super(id);
        this.createDt = createDt;
        this.user = user;
        this.message = message;
    }

    public Notification(User user, Message message) {

        this.user = user;
        this.message = message;
    }

    public Date getCreateDt() {

        return createDt;
    }

    public void setCreateDt(Date createDt) {

        this.createDt = createDt;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public Message getMessage() {

        return message;
    }

    public void setMessage(Message message) {

        this.message = message;
    }

    public Channel getChannel() {

        return channel;
    }

    public void setChannel(Channel channel) {

        this.channel = channel;
    }
}