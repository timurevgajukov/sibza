package org.sibza.model.notification;

import org.sibza.model.Model;

/**
 * Created by Тимур on 14.03.2016.
 *
 * Модель с типами каналов передачи нотификаций пользователю
 */
public class Channel extends Model {

    public static final int SITE  = 1;
    public static final int EMAIL = 2;
    public static final int SMS   = 3;
    public static final int PUSH  = 4;

    public Channel(int id) {

        super(id);
    }
}