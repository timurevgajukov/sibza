package org.sibza.model.game;

import org.sibza.model.Model;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;

/**
 * Created by Тимур on 22.01.2016.
 *
 * Информация по счету пользователя в игре
 */
public class Score extends Model {

    private Game game;
    private Competition competition;
    private User user;
    private int score;

    // технические поля
    private Word question; // текущий вопрос
    private Word answer; // правильный ответ

    public Score(int id, Game game, User user, int score) {

        super(id);

        this.game = game;
        this.user = user;
        this.score = score;
    }

    public Score(Game game, User user, int score) {

        super();

        this.game = game;
        this.user = user;
        this.score = score;
    }

    public Game getGame() {

        return game;
    }

    public void setGame(Game game) {

        this.game = game;
    }

    public Competition getCompetition() {

        return competition;
    }

    public void setCompetition(Competition competition) {

        this.competition = competition;
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public int getScore() {

        return score;
    }

    public void setScore(int score) {

        this.score = score;
    }

    public Word getQuestion() {

        return question;
    }

    public void setQuestion(Word question) {

        this.question = question;
    }

    public Word getAnswer() {

        return answer;
    }

    public void setAnswer(Word answer) {

        this.answer = answer;
    }
}