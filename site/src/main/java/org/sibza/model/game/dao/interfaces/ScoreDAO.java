package org.sibza.model.game.dao.interfaces;

import org.sibza.model.game.Competition;
import org.sibza.model.game.Game;
import org.sibza.model.game.Score;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 22.01.2016.
 *
 * Интерфейс для работы со счетом пользователя в игре
 */
public interface ScoreDAO {

    /**
     * Возвращает информацию по счету пользователя в игре
     *
     * @param game
     * @param competition
     * @param user
     * @return
     */
    Score get(Game game, Competition competition, User user);

    /**
     * Возвращает таблицу рейтингов пользователей в игре
     *
     * @param game
     * @param competition
     * @return
     */
    List<Score> list(Game game, Competition competition);

    /**
     * Сохраняет счет пользователя в игре
     *
     * @param scope
     * @param session
     * @return
     */
    boolean save(Score scope, String session);
}