package org.sibza.model.game.dao;

import org.sibza.cache.DefaultAvatars;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.avatar.DefaultAvatar;
import org.sibza.model.game.Competition;
import org.sibza.model.game.Game;
import org.sibza.model.game.Score;
import org.sibza.model.game.dao.interfaces.ScoreDAO;
import org.sibza.model.resource.Resource;
import org.sibza.model.user.User;
import org.sibza.model.user.UserInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 22.01.2016.
 *
 * Реализация интерфейса для работы со счетом пользователя в игре
 */
public class ScoreDAOImpl extends ModelDAOImpl implements ScoreDAO {

    private static final String QUERY_GET = "{ call games_users_score_get(?,?,?) }";
    private static final String QUERY_LIST_BY_GAME = "{ call games_users_score_list_by_game(?,?) }";
    private static final String QUERY_SAVE = "{ call games_users_score_save(?,?,?,?,?) }";
    private static final String QUERY_SAVE_WITH_SESSION = "{ call games_users_score_save_with_session(?,?,?,?,?,?) }";

    @Override
    public Score get(Game game, Competition competition, User user) {

        final DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();

        List<Score> scores = jdbcTemplate.query(QUERY_GET,
                new Object[] {
                        game.getId(),
                        competition != null ? competition.getId() : null,
                        user.getId()
                }, new RowMapper<Score>() {

                    @Override
                    public Score mapRow(ResultSet resultSet, int i) throws SQLException {

                        Score score = load(resultSet);
                        UserInfo userInfo = score.getUser().getInfo();
                        userInfo.setDefaultAvatar(defaultAvatars.get(userInfo.getDefaultAvatar().getId()));
                        return score;
                    }
                });

        if (scores == null || scores.size() == 0) {
            return null;
        }

        return scores.get(0);
    }

    @Override
    public List<Score> list(Game game, Competition competition) {

        final DefaultAvatars defaultAvatars = DefaultAvatars.getInstance();

        List<Score> scores = jdbcTemplate.query(QUERY_LIST_BY_GAME,
                new Object[] {
                        game.getId(),
                        competition != null ? competition.getId() : null
                }, new RowMapper<Score>() {

            @Override
            public Score mapRow(ResultSet resultSet, int i) throws SQLException {

                Score score = load(resultSet);
                UserInfo userInfo = score.getUser().getInfo();
                userInfo.setDefaultAvatar(defaultAvatars.get(userInfo.getDefaultAvatar().getId()));
                return score;
            }
        });

        return scores;
    }

    @Override
    public boolean save(Score score, String session) {

        Score newScore = jdbcTemplate.queryForObject(QUERY_SAVE_WITH_SESSION,
                new Object[]{
                        score.getId() == 0 ? null : score.getId(),
                        score.getGame().getId(),
                        score.getCompetition() != null ? score.getCompetition().getId() : null,
                        score.getUser().getId(),
                        session,
                        score.getScore()
                }, new RowMapper<Score>() {

                    @Override
                    public Score mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        score.setId(newScore.getId());

        return true;
    }

    private Score load(ResultSet rs) throws SQLException {

        Game game = new Game(rs.getInt("game_id"));
        int competitionId = rs.getInt("competition_id");
        Competition competition = null;
        if (competitionId != 0) {
            competition = new Competition(competitionId);
        }

        User user = new User(rs.getInt("user_id"), rs.getString("email"));
        user.setName(rs.getString("name"));
        user.setNick(rs.getString("nick"));
        user.getInfo().setDefaultAvatar(new DefaultAvatar(rs.getInt("default_avatar_id")));

        int avatarId = rs.getInt("avatar_id");
        if (avatarId != 0) {
            Resource avatar = new Resource(avatarId);
            String fileName = "img" + avatarId + "." + rs.getString("avatar_extension");
            avatar.setFileName(fileName);
            user.getInfo().setAvatar(avatar);
        }

        Score score = new Score(rs.getInt("id"), game, user, rs.getInt("score"));
        score.setCompetition(competition);

        return score;
    }
}