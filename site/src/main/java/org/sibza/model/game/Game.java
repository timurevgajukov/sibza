package org.sibza.model.game;

import org.sibza.model.Model;

/**
 * Created by Тимур on 22.01.2016.
 *
 * Модель с информацией по игре
 */
public class Game extends Model {

    public static final int GAME_WORDS_ID = 1;

    public Game(int id) {

        super(id);
    }

    public Game(int id, String name) {

        super(id, name);
    }
}