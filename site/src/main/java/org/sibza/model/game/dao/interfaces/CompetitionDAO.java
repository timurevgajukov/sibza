package org.sibza.model.game.dao.interfaces;

import org.sibza.model.game.Competition;

import java.util.List;

/**
 * Created by timur on 01.02.16.
 *
 * Интерфейс для работы с конкурсами
 */
public interface CompetitionDAO {

    /**
     * Возвращает текущие активные конкурсы
     *
     * @param active возвращать только активные конкурсы
     * @return
     */
    List<Competition> list(boolean active);

    /**
     * Возвращает список всех конкурсов
     *
     * @return
     */
    List<Competition> all();
}
