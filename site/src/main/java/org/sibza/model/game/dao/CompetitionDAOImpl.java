package org.sibza.model.game.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.game.Competition;
import org.sibza.model.game.dao.interfaces.CompetitionDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 01.02.16.
 *
 * Реализация интерфейса по работе с конкурсами
 */
public class CompetitionDAOImpl extends ModelDAOImpl implements CompetitionDAO {

    private static final String QUERY_LIST = "{ call competitions_list() }";
    private static final String QUERY_LIST_NEW = "{ call competitions_list_new() }";
    private static final String QUERY_LIST_ALL = "{ call competitions_list_all() }";

    @Override
    public List<Competition> list(boolean active) {

        String query = QUERY_LIST_NEW;
        if (active) {
            query = QUERY_LIST;
        }

        List<Competition> list = jdbcTemplate.query(query, new RowMapper<Competition>() {

            @Override
            public Competition mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return list;
    }

    @Override
    public List<Competition> all() {

        List<Competition> list = jdbcTemplate.query(QUERY_LIST_ALL, new RowMapper<Competition>() {

            @Override
            public Competition mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        return list;
    }

    private Competition load(ResultSet rs) throws SQLException {

        Competition competition = new Competition(rs.getInt("id"), rs.getTimestamp("start_dt"), rs.getTimestamp("end_dt"));
        competition.setTitle(rs.getString("title"));
        competition.setNote(rs.getString("note"));
        for (int i=1; i<=3; i++) {
            String prize = rs.getString("prize_" + i);
            if (prize != null && prize.length() != 0) {
                competition.addPrize(prize);
            }
        }

        return competition;
    }
}