package org.sibza.model.game;

import org.sibza.model.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by timur on 31.01.16.
 *
 * Модель для описания конкурсов
 */
public class Competition extends Model {

    private String title;
    private Date start;
    private Date end;
    private String note;
    private List<String> prizes;

    public Competition(int id) {

        super(id);
    }

    public Competition(int id, Date start, Date end) {

        super(id);

        this.start = start;
        this.end = end;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public Date getStart() {

        return start;
    }

    public void setStart(Date start) {

        this.start = start;
    }

    public Date getEnd() {

        return end;
    }

    public void setEnd(Date end) {

        this.end = end;
    }

    public String getNote() {

        return note;
    }

    public void setNote(String note) {

        this.note = note;
    }

    public List<String> getPrizes() {

        return prizes;
    }

    public Competition addPrize(String prize) {

        if (prizes == null) {
            prizes = new ArrayList<String>();
        }

        prizes.add(prize);

        return this;
    }

    @Override
    public String toString() {

        return title;
    }
}