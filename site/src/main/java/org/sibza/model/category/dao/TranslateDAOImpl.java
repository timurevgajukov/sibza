package org.sibza.model.category.dao;

import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.category.Category;
import org.sibza.model.category.Translate;
import org.sibza.model.category.dao.interfaces.TranslateDAO;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с переводами для категорий
 */
public class TranslateDAOImpl extends ModelDAOImpl implements TranslateDAO {

    private static final String QUERY_LIST = "{ call categories_languages_list(?) }";
    private static final String QUERY_SAVE = "{ call categories_languages_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call categories_languages_delete(?,?) }";

    @Override
    public List<Translate> list(final Category category) {

        final Languages languages = Languages.getInstance();

        List<Translate> translates = jdbcTemplate.query(QUERY_LIST, new Object[] { category.getId() },
                new RowMapper<Translate>() {

                    @Override
                    public Translate mapRow(ResultSet resultSet, int i) throws SQLException {

                        Translate translate = load(resultSet);
                        translate.setCategory(category);
                        translate.setLanguage(languages.get(translate.getLanguage().getId()));

                        return translate;
                    }
                });

        return translates;
    }

    @Override
    public boolean save(Translate translate, User user) {

        Translate newTranslate = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        translate.getCategory().getId(),
                        translate.getLanguage().getId(),
                        translate.getName(),
                        user.getId()
                }, new RowMapper<Translate>() {

                    @Override
                    public Translate mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        translate.setId(newTranslate.getId());

        return true;
    }

    @Override
    public boolean delete(Category category, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { category.getId(), user.getId() }) != 0;
    }

    private Translate load(ResultSet rs) throws SQLException {

        Category category = new Category(rs.getInt("category_id"));
        return new Translate(rs.getInt("id"), rs.getString("name"), category, new Language(rs.getInt("lang_id")));
    }
}