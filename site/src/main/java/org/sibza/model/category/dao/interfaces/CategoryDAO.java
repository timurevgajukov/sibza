package org.sibza.model.category.dao.interfaces;

import org.sibza.model.category.Category;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с категориями
 */
public interface CategoryDAO {

    /**
     * Возвращает категория по его идентификатору
     *
     * @param id идентификатор категории
     * @param language язык для расчета количества слов
     * @return
     */
    Category get(int id, Language language);

    /**
     * Возвращает подкатегории для родительской категории,
     * если родительская null, то возвращает категории верхнего уровня
     *
     * @param parent родительская категория
     * @param language язык для расчета количества слов
     * @return
     */
    List<Category> list(Category parent, Language language);

    /**
     * Поиск категорий
     *
     * @param query строка поиска
     * @return
     */
    List<Category> search(String query);

    /**
     * Сохраняет категорию
     *
     * @param category
     * @param user
     * @return
     */
    boolean save(Category category, User user);

    /**
     * Удаляет категорию
     *
     * @param category
     * @param user
     * @return
     */
    boolean delete(Category category, User user);
}
