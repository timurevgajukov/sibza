package org.sibza.model.category.dao.interfaces;

import org.sibza.model.category.Category;
import org.sibza.model.category.Translate;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с переводами наименований категорий
 */
public interface TranslateDAO {

    /**
     * Возвращает список переводов для категории
     *
     * @param category категория
     * @return
     */
    List<Translate> list(Category category);

    /**
     * Сохраняет перевод для категории
     *
     * @param translate
     * @param user
     * @return
     */
    boolean save(Translate translate, User user);

    /**
     * Удаляет перевод для категории
     *
     * @param category
     * @param user
     * @return
     */
    boolean delete(Category category, User user);
}
