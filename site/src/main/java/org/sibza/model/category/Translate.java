package org.sibza.model.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.sibza.model.language.Language;
import org.sibza.model.Model;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 11.05.15.
 *
 * Перевод наименования категории на другом языке
 */
public class Translate extends Model {

    private Category category;
    private Language language;

    public Translate() {

        super();
    }

    public Translate(int id, String name, Category category, Language language) {

        super(id, name);
        this.category = category;
        this.language = language;
    }

    @JsonIgnore
    public Category getCategory() {

        return category;
    }

    public void setCategory(Category category) {

        this.category = category;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }
}