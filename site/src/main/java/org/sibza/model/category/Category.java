package org.sibza.model.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.sibza.model.category.dao.TranslateDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.Model;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 09.05.15.
 *
 * Модель категории
 */
public class Category extends Model {

    private Category parent;
    private List<Translate> translates;
    private Integer wordsCount;

    public Category() {

        super();
    }

    public Category(int id) {

        super(id);
    }

    public Category(int id, String name, int parentId) {

        super(id, name);

        if (parentId != 0) {
            this.parent = new Category(parentId);
        }
    }

    @JsonIgnore
    public Category getParent() {

        return parent;
    }

    public void setParent(Category parent) {

        this.parent = parent;
    }

    public List<Translate> getTranslates() {

        if (translates == null) {
            translates = new TranslateDAOImpl().list(this);
        }

        return translates;
    }

    public Translate getTranslate(Language language) {

        List<Translate> translates = getTranslates();

        for (Translate translate : translates) {
            if (translate.getLanguage().getId() == language.getId()) {
                return translate;
            }
        }

        return null;
    }

    public Integer getWordsCount() {

        return wordsCount;
    }

    public void setWordsCount(Integer wordsCount) {

        this.wordsCount = wordsCount;
    }
}