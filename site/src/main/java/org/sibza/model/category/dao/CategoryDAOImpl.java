package org.sibza.model.category.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.category.Category;
import org.sibza.model.category.dao.interfaces.CategoryDAO;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 * <p/>
 * Реализация интерфейса для работы с категориями
 */
public class CategoryDAOImpl extends ModelDAOImpl implements CategoryDAO {

    private static final String QUERY_GET_BY_ID = "{ call categories_get(?,?) }";
    private static final String QUERY_LIST_BY_PARENT = "{ call categories_list(?,?) }";
    private static final String QUERY_SEARCH = "{ call categories_search(?) }";
    private static final String QUERY_SAVE = "{ call categories_save(?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call categories_delete(?,?) }";

    @Override
    public Category get(int id, Language language) {

        Category category = jdbcTemplate.queryForObject(QUERY_GET_BY_ID, new Object[] { id, language.getId() },
                new RowMapper<Category>() {

                    @Override
                    public Category mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        return category;
    }

    @Override
    public List<Category> list(final Category parent, Language language) {

        List<Category> categories = jdbcTemplate.query(QUERY_LIST_BY_PARENT,
                new Object[] {
                        parent == null ? null : parent.getId(),
                        language.getId()
                }, new RowMapper<Category>() {

                    @Override
                    public Category mapRow(ResultSet resultSet, int i) throws SQLException {

                        Category category = load(resultSet);
                        category.setParent(parent);

                        return category;
                    }
                });

        return categories;
    }

    @Override
    public List<Category> search(String query) {

        List<Category> categories = jdbcTemplate.query(QUERY_SEARCH,
                new Object[]{ "%" + query + "%" },
                new RowMapper<Category>() {

                    @Override
                    public Category mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        return categories;
    }

    @Override
    public boolean save(Category category, User user) {

        Category newCategory = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        category.getId() == 0 ? null : category.getId(),
                        category.getName(),
                        category.getParent() == null ? null : category.getParent().getId(),
                        user.getId()
                }, new RowMapper<Category>() {

                    @Override
                    public Category mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        category.setId(newCategory.getId());

        return true;
    }

    @Override
    public boolean delete(Category category, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ category.getId(), user.getId() }) != 0;
    }

    private Category load(ResultSet rs) throws SQLException {

        Category category = new Category(rs.getInt("id"), rs.getString("name"), rs.getInt("parent_id"));
        category.setWordsCount(rs.getInt("words_count"));

        return category;
    }
}