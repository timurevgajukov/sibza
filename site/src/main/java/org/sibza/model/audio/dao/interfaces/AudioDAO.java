package org.sibza.model.audio.dao.interfaces;

import org.sibza.model.audio.Audio;
import org.sibza.model.language.Language;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 28.09.2015.
 *
 * Интерфейс для работы с аудио данными
 */
public interface AudioDAO {

    /**
     * Возвращает информацию по аудио данным
     *
     * @param id
     * @return
     */
    Audio get(int id);

    /**
     * Возвращает список по всем аудио данным
     *
     * @return
     */
    List<Audio> list();

    /**
     * Возвращает список аудио данных по определенному языку
     *
     * @param language
     * @return
     */
    List<Audio> list(Language language);

    /**
     * Поиск аудио
     *
     * @param query
     * @return
     */
    List<Audio> search(String query);

    /**
     * Сохраняет аудио данные
     *
     * @param audio
     * @param user
     * @return
     */
    boolean save(Audio audio, User user);

    /**
     * Удаляет аудио данные
     *
     * @param audio
     * @param user
     * @return
     */
    boolean delete(Audio audio, User user);
}