package org.sibza.model.audio;

import org.sibza.model.Model;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;

/**
 * Created by Тимур on 28.09.2015.
 *
 * Модель для работы с музыкой
 */
public class Audio extends Model {

    private Language language;
    private Resource resource;
    private String title;
    private String note;

    public Audio() {

        super();
    }

    public Audio(int id) {

        super(id);
    }

    public Audio(int id, Language language, Resource resource, String title, String note) {

        super(id);
        this.language = language;
        this.resource = resource;
        this.title = title;
        this.note = note;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public Resource getResource() {

        return resource;
    }

    public void setResource(Resource resource) {

        this.resource = resource;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getNote() {

        return note;
    }

    public void setNote(String note) {

        this.note = note;
    }

    @Override
    public String toString() {

        return title;
    }
}