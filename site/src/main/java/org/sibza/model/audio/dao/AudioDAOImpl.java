package org.sibza.model.audio.dao;

import org.sibza.cache.Languages;
import org.sibza.cache.Resources;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.audio.Audio;
import org.sibza.model.audio.dao.interfaces.AudioDAO;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 28.09.2015.
 *
 * Реализация интерфейса для работы с аудио данными
 */
public class AudioDAOImpl extends ModelDAOImpl implements AudioDAO {

    private static final String QUERY_GET_BY_ID = "{ call audio_get(?) }";
    private static final String QUERY_LIST = "{ call audio_list() }";
    private static final String QUERY_LIST_BY_LANGUAGE = "{ call audio_list_by_language(?) }";
    private static final String QUERY_SEARCH = "{ call audio_search(?)}";
    private static final String QUERY_SAVE = "{ call audio_save(?,?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call audio_delete(?,?) }";

    @Override
    public Audio get(int id) {

        Audio audio = jdbcTemplate.queryForObject(QUERY_GET_BY_ID, new RowMapper<Audio>() {

            @Override
            public Audio mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        // добавление данных
        Languages languages = Languages.getInstance();
        audio.setLanguage(languages.get(audio.getLanguage().getId()));
        audio.setResource(new ResourceDAOImpl().get(audio.getResource().getId(), true));

        return audio;
    }

    @Override
    public List<Audio> list() {

        List<Audio> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Audio>() {

            @Override
            public Audio mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        Resources resources = new Resources();

        for (Audio audio : list) {
            // добавление данных
            audio.setLanguage(languages.get(audio.getLanguage().getId()));
            audio.setResource(resources.get(audio.getResource().getId()));
        }

        return list;
    }

    @Override
    public List<Audio> list(Language language) {

        List<Audio> list = jdbcTemplate.query(QUERY_LIST_BY_LANGUAGE, new RowMapper<Audio>() {

            @Override
            public Audio mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, language.getId());

        if (list == null) {
            return null;
        }

        Resources resources = new Resources();

        for (Audio audio : list) {
            // добавление данных
            audio.setLanguage(language);
            audio.setResource(resources.get(audio.getResource().getId()));
        }

        return list;
    }

    @Override
    public List<Audio> search(String query) {

        List<Audio> list = jdbcTemplate.query(QUERY_SEARCH, new RowMapper<Audio>() {

            @Override
            public Audio mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, "%" + query + "%");

        if (list == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        Resources resources = new Resources();

        for (Audio audio : list) {
            // добавление данных
            audio.setLanguage(languages.get(audio.getLanguage().getId()));
            audio.setResource(resources.get(audio.getResource().getId()));
        }

        return list;
    }

    @Override
    public boolean save(Audio audio, User user) {

        Audio newAudio = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        audio.getId() == 0 ? null : audio.getId(),
                        audio.getLanguage().getId(),
                        audio.getResource().getId(),
                        audio.getTitle(),
                        audio.getNote(),
                        user.getId()
                }, new RowMapper<Audio>() {

                    @Override
                    public Audio mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        audio.setId(newAudio.getId());

        return true;
    }

    @Override
    public boolean delete(Audio audio, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { audio.getId(), user.getId() }) != 0;
    }

    private Audio load(ResultSet rs) throws SQLException {

        Language language = new Language(rs.getInt("lang_id"));
        Resource resource = new Resource(rs.getInt("resource_id"));

        return new Audio(rs.getInt("id"), language, resource, rs.getString("title"), rs.getString("note"));
    }
}