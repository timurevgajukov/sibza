package org.sibza.model.proverb;

import org.sibza.model.Model;
import org.sibza.cache.Languages;
import org.sibza.model.language.Dialect;
import org.sibza.model.language.Language;
import org.sibza.utils.DBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 30.05.15.
 *
 * Модель пословицы
 */
public class Proverb extends Model {

    private Language language;
    private Dialect dialect;
    private String body;

    public Proverb() {

        super();
    }

    public Proverb(int id) {

        super(id);
    }

    public Proverb(int id, Language language, String body) {

        super(id);
        this.language = language;
        this.body = body;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public Dialect getDialect() {

        return dialect;
    }

    public void setDialect(Dialect dialect) {

        this.dialect = dialect;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    @Override
    public String toString() {

        return body;
    }
}