package org.sibza.model.proverb.dao.interfaces;

import org.sibza.model.language.Language;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Интерфейс для работы с пословицами
 */
public interface ProverbDAO {

    /**
     * Возвращает информацию по пословице
     *
     * @param id идентификатор пословицы
     * @return
     */
    Proverb get(int id);

    /**
     * Возвращает список пословиц для определенного языка
     *
     * @param language язык пословиц
     * @param offset смещение от начала списка
     * @param count количество элементов
     * @return
     */
    List<Proverb> list(Language language, int offset, int count);

    /**
     * Сохраняет пословицу
     *
     * @param proverb
     * @param user
     * @return
     */
    boolean save(Proverb proverb, User user);

    /**
     * Удаляет пословицу
     *
     * @param proverb
     * @param user
     * @return
     */
    boolean delete(Proverb proverb, User user);

    /**
     * Поиск пословицы
     *
     * @param query строка запроса
     * @return
     */
    List<Proverb> search(String query);

    /**
     * Возвращает количество пословиц
     *
     * @return
     */
    int count();
}
