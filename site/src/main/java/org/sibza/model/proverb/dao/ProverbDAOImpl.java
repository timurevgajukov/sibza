package org.sibza.model.proverb.dao;

import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.proverb.Proverb;
import org.sibza.model.proverb.dao.interfaces.ProverbDAO;
import org.sibza.model.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by timur on 24.09.15.
 *
 * Реализация интерфейса для работы с пословицами
 */
public class ProverbDAOImpl extends ModelDAOImpl implements ProverbDAO {

    private static final String QUERY_GET_BY_ID = "{ call proverbs_get(?) }";
    private static final String QUERY_LIST_BY_LANGUAGE = "{ call proverbs_list_by_language(?,?,?) }";
    private static final String QUERY_SAVE = "{ call proverbs_save(?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call proverbs_delete(?,?) }";
    private static final String QUERY_SEARCH = "{ call proverbs_search(?) }";
    private static final String QUERY_STAT_COUNT = "{ call statistics_proverbs_count_all() }";

    @Override
    public Proverb get(int id) {

        Proverb proverb = jdbcTemplate.queryForObject(QUERY_GET_BY_ID, new RowMapper<Proverb>() {

            @Override
            public Proverb mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, id);

        Languages languages = Languages.getInstance();
        proverb.setLanguage(languages.get(proverb.getLanguage().getId()));

        return proverb;
    }

    @Override
    public List<Proverb> list(final Language language, int offset, int count) {

        List<Proverb> proverbs = jdbcTemplate.query(QUERY_LIST_BY_LANGUAGE, new Object[] { language.getId(), offset, count },
                new RowMapper<Proverb>() {

                    @Override
                    public Proverb mapRow(ResultSet resultSet, int i) throws SQLException {

                        Proverb proverb = load(resultSet);
                        proverb.setLanguage(language);

                        return proverb;
                    }
                });

        return proverbs;
    }

    @Override
    public boolean save(Proverb proverb, User user) {

        Proverb newProverb = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        proverb.getId() == 0 ? null : proverb.getId(),
                        proverb.getLanguage().getId(),
                        proverb.getDialect() == null || proverb.getDialect().getId() == 0 ? null : proverb.getDialect().getId(),
                        proverb.getBody(),
                        user.getId()
                }, new RowMapper<Proverb>() {

                    @Override
                    public Proverb mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet);
                    }
                });

        proverb.setId(newProverb.getId());

        return true;
    }

    @Override
    public boolean delete(Proverb proverb, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[] { proverb.getId(), user.getId() }) != 0;
    }

    @Override
    public List<Proverb> search(String query) {

        List<Proverb> proverbs = jdbcTemplate.query(QUERY_SEARCH, new RowMapper<Proverb>() {

            @Override
            public Proverb mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        }, "%" + query + "%");

        if (proverbs == null) {
            return null;
        }

        Languages languages = Languages.getInstance();
        for (Proverb proverb : proverbs) {
            proverb.setLanguage(languages.get(proverb.getLanguage().getId()));
        }

        return proverbs;
    }

    @Override
    public int count() {

        return jdbcTemplate.queryForObject(QUERY_STAT_COUNT, new RowMapper<Integer>() {

            @Override
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {

                return resultSet.getInt("cnt");
            }
        });
    }

    private Proverb load(ResultSet rs) throws SQLException {

        Language language = new Language(rs.getInt("lang_id"));
        return new Proverb(rs.getInt("id"), language, rs.getString("body"));
    }
}