package org.sibza.model.book;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.sibza.model.Model;
import org.sibza.model.book.dao.BookDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.sibza.model.resource.dao.ResourceDAOImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Тимур on 15.10.2015.
 *
 * Модель информации по книге
 */
public class Book extends Model {

    @JsonIgnore
    private Book parent;
    private String title;
    private Language language;
    private String url;
    private String format;
    private double size;
    @JsonIgnore
    private Resource image;
    private List<Book> books;
    private String imageUrl;
    private BookType type;

    public Book(int id) {

        super(id);

        setImageUrl();
    }

    public Book(int id, String title, Language language) {

        super(id);
        this.title = title;
        this.language = language;

        setImageUrl();
    }

    public Book getParent() {

        return parent;
    }

    public void setParent(Book parent) {

        this.parent = parent;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getFormat() {

        return format;
    }

    public void setFormat(String format) {

        this.format = format;
    }

    public double getSize() {

        return size;
    }

    public void setSize(double size) {

        this.size = size;
    }

    public Resource getImage() {

        if (image.getFileName() == null) {
            // мы еще не загрузили ресурс с картинкой
            image = new ResourceDAOImpl().get(image.getId());
        }

        return image;
    }

    public void setImage(Resource image) {

        this.image = image;

        setImageUrl();
    }

    public String getImageUrl() {

        return imageUrl;
    }

    public void setImageUrl() {

        String fileName = "Book.png";
        int imgId = 659;
        if (image != null) {
            fileName = image.getFileName();
            imgId = image.getId();
        }

        this.imageUrl = String.format("/books/%d/%s", imgId, fileName);
    }

    public List<Book> getBooks() {

        if (books == null) {
            books = new BookDAOImpl().list(this);
            if (books == null) {
                // чтобы повторно каждый раз не запрашивать
                books = new ArrayList<Book>();
            }
        }

        return books;
    }

    public BookType getType() {

        return type;
    }

    public void setType(BookType type) {

        this.type = type;
    }

    @Override
    public String toString() {

        return title;
    }
}