package org.sibza.model.book.dao.interfaces;

import org.sibza.model.book.Book;
import org.sibza.model.book.BookType;

import java.util.List;

/**
 * Created by Тимур on 15.10.2015.
 *
 * Интерфейс для работы с информацией по книге
 */
public interface BookDAO {

    /**
     * Возвращает информацию по книге
     *
     * @param id
     * @return
     */
    Book get(int id);

    /**
     * Возвращает книги без томов
     *
     * @param offset
     * @param count
     * @param type
     *
     * @return
     */
    List<Book> list(int offset, int count, BookType type);

    /**
     * Возвращает список книг
     *
     * @param offset
     * @param count
     * @param type
     * @param full если true, то возвращает в том числе и список томов
     * @return
     */
    List<Book> list(int offset, int count, BookType type, boolean full);

    /**
     * Возвращает тома книги
     *
     * @param book книги чьи тома необходимо вернуть
     * @return
     */
    List<Book> list(Book book);

    /**
     * Поиск книги
     *
     * @param query
     * @return
     */
    List<Book> search(String query);
}