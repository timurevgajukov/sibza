package org.sibza.model.book.dao.interfaces;

import org.sibza.model.book.BookType;

import java.util.List;

/**
 * Created by Тимур on 11.05.2016.
 *
 * Интерфейс для работы с типами книг
 */
public interface BookTypeDAO {

    /**
     * Возвращает список типов книг
     *
     * @return
     */
    List<BookType> list();
}