package org.sibza.model.book.dao;

import org.sibza.cache.BookTypes;
import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.book.Book;
import org.sibza.model.book.BookType;
import org.sibza.model.book.dao.interfaces.BookDAO;
import org.sibza.model.language.Language;
import org.sibza.model.resource.Resource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 15.10.2015.
 *
 * Реализация интерфейса по работе с книгами
 */
public class BookDAOImpl extends ModelDAOImpl implements BookDAO {

    private static final String QUERY_GET_BY_ID = "{ call books_get_by_id(?) }";
    private static final String QUERY_LIST = "{ call books_list(?,?,?) }";
    private static final String QUERY_LIST_WITHOUT_PARENT = "{ call books_list_without_parent(?,?,?) }";
    private static final String QUERY_LIST_BY_PARENT = "{ call books_list_by_parent(?) }";
    private static final String QUERY_SEARCH = "{ call books_search(?) }";

    @Override
    public Book get(int id) {

        final BookTypes bookTypes = BookTypes.getInstance();

        List<Book> books = jdbcTemplate.query(QUERY_GET_BY_ID, new RowMapper<Book>() {

            @Override
            public Book mapRow(ResultSet resultSet, int i) throws SQLException {

                Book book = load(resultSet);
                book.setType(bookTypes.get(book.getType().getId()));

                return book;
            }
        }, id);

        if (books == null || books.size() == 0) {
            return null;
        }

        return books.get(0);
    }

    @Override
    public List<Book> list(int offset, int count, BookType type) {

        return list(offset, count, type, false);
    }

    @Override
    public List<Book> list(int offset, int count, final BookType type, boolean full) {

        String query = QUERY_LIST_WITHOUT_PARENT;
        if (full) {
            query = QUERY_LIST;
        }

        List<Book> books = jdbcTemplate.query(query, new Object[] { offset, count, type.getId() }, new RowMapper<Book>() {

            @Override
            public Book mapRow(ResultSet resultSet, int i) throws SQLException {

                Book book = load(resultSet);
                book.setType(type);

                return book;
            }
        });

        return books;
    }

    @Override
    public List<Book> list(final Book book) {

        final BookTypes bookTypes = BookTypes.getInstance();

        List<Book> books = jdbcTemplate.query(QUERY_LIST_BY_PARENT, new RowMapper<Book>() {

            @Override
            public Book mapRow(ResultSet resultSet, int i) throws SQLException {

                Book item = load(resultSet);
                item.setParent(book);
                item.setType(bookTypes.get(item.getType().getId()));

                return item;
            }
        }, book.getId());

        return books;
    }

    @Override
    public List<Book> search(String query) {

        final BookTypes bookTypes = BookTypes.getInstance();

        List<Book> books = jdbcTemplate.query(QUERY_SEARCH, new RowMapper<Book>() {

            @Override
            public Book mapRow(ResultSet resultSet, int i) throws SQLException {

                Book book = load(resultSet);
                book.setType(bookTypes.get(book.getType().getId()));

                return book;
            }
        }, "%" + query + "%");

        return books;
    }

    private Book load(ResultSet rs) throws SQLException {

        Languages languages = Languages.getInstance();
        Language language = languages.get(rs.getInt("language_id"));

        Book book = new Book(rs.getInt("id"), rs.getString("title"), language);
        book.setUrl(rs.getString("url"));
        book.setFormat(rs.getString("format"));
        book.setSize(rs.getDouble("size"));
        book.setType(new BookType(rs.getInt("type_id")));

        Resource image = new Resource(rs.getInt("img_id"));
        image.setFileName(rs.getString("file_name"));
        book.setImage(image);

        if (rs.getInt("parent_id") != 0) {
            book.setParent(new Book(rs.getInt("parent_id")));
        }

        return book;
    }
}