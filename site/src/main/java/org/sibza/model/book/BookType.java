package org.sibza.model.book;

import org.sibza.model.Model;

/**
 * Created by Тимур on 11.05.2016.
 *
 * Ти книги
 */
public class BookType extends Model {

    public static final int TYPE_BOOK = 1;
    public static final int TYPE_JOURNAL = 2;

    public BookType() {

        super();
    }

    public BookType(int id) {

        super(id);
    }

    public BookType(int id, String name) {

        super(id, name);
    }
}