package org.sibza.model.book.dao;

import org.sibza.model.ModelDAOImpl;
import org.sibza.model.book.BookType;
import org.sibza.model.book.dao.interfaces.BookTypeDAO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 11.05.2016.
 *
 * Реализация интерфейса для работы с типами книг
 */
public class BookTypeDAOImpl extends ModelDAOImpl implements BookTypeDAO {

    private static final String QUERY_LIST = "{ call book_types_list() }";

    @Override
    public List<BookType> list() {

        List<BookType> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<BookType>() {

            @Override
            public BookType mapRow(ResultSet resultSet, int i) throws SQLException {

                return load(resultSet);
            }
        });

        if (list == null || list.size() == 0) {
            return null;
        }

        return list;
    }

    private BookType load(ResultSet rs) throws SQLException {

        return new BookType(rs.getInt("id"), rs.getString("name"));
    }
}