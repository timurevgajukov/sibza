package org.sibza.model.numeral;

import org.sibza.model.Model;
import org.sibza.model.language.Language;
import org.sibza.model.word.Word;

/**
 * Created by Тимур on 26.01.2016.
 *
 * Модель числа
 */
public class Numeral extends Model {

    private int number;
    private Language language;
    private Word word;

    public Numeral(int id) {

        super(id);
    }

    public Numeral(int id, int number, Language language, Word word) {

        super(id);

        this.number = number;
        this.language = language;
        this.word = word;
    }

    public Numeral(int number, Language language, Word word) {

        super();

        this.number = number;
        this.language = language;
        this.word = word;
    }

    public int getNumber() {

        return number;
    }

    public void setNumber(int number) {

        this.number = number;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public Word getWord() {

        return word;
    }

    public void setWord(Word word) {

        this.word = word;
    }

    @Override
    public String toString() {

        return word.toString();
    }
}