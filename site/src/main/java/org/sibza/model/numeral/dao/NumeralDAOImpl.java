package org.sibza.model.numeral.dao;

import org.sibza.cache.Languages;
import org.sibza.model.ModelDAOImpl;
import org.sibza.model.language.Language;
import org.sibza.model.numeral.Numeral;
import org.sibza.model.numeral.dao.interfaces.NumeralDAO;
import org.sibza.model.user.User;
import org.sibza.model.word.Word;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Тимур on 26.01.2016.
 *
 * Реализация интерфейса для работы с числами
 */
public class NumeralDAOImpl extends ModelDAOImpl implements NumeralDAO {

    private static final String QUERY_LIST = "{ call numerals_list(?) }";
    private static final String QUERY_SAVE = "{ call numerals_save(?,?,?,?,?) }";
    private static final String QUERY_DELETE = "{ call numerals_delete(?,?) }";

    @Override
    public List<Numeral> list(Language language) {

        final Languages languages = Languages.getInstance();

        List<Numeral> list = jdbcTemplate.query(QUERY_LIST, new RowMapper<Numeral>() {

            @Override
            public Numeral mapRow(ResultSet resultSet, int i) throws SQLException {

                Numeral numeral = load(resultSet, true);
                numeral.setLanguage(languages.get(numeral.getLanguage().getId()));

                return numeral;
            }
        }, language.getId());

        return list;
    }

    @Override
    public boolean save(Numeral numeral, User user) {

        Numeral newNumeral = jdbcTemplate.queryForObject(QUERY_SAVE,
                new Object[]{
                        numeral.getId() == 0 ? null : numeral.getId(),
                        numeral.getNumber(),
                        numeral.getLanguage().getId(),
                        numeral.getWord().getId(),
                        user.getId()
                }, new RowMapper<Numeral>() {

                    @Override
                    public Numeral mapRow(ResultSet resultSet, int i) throws SQLException {

                        return load(resultSet, false);
                    }
                });

        numeral.setId(newNumeral.getId());

        return true;
    }

    @Override
    public boolean delete(Numeral numeral, User user) {

        return jdbcTemplate.update(QUERY_DELETE, new Object[]{ numeral.getId(), user.getId() }) != 0;
    }

    private Numeral load(ResultSet rs, boolean withWord) throws SQLException {

        Language language = new Language(rs.getInt("lang_id"));
        Word word = new Word(rs.getInt("word_id"));

        if (withWord) {
            word.setWord(rs.getString("word"));
        }

        return new Numeral(rs.getInt("id"), rs.getInt("number"), language, word);
    }
}