package org.sibza.model.numeral.dao.interfaces;

import org.sibza.model.language.Language;
import org.sibza.model.numeral.Numeral;
import org.sibza.model.user.User;

import java.util.List;

/**
 * Created by Тимур on 26.01.2016.
 *
 * Интерфейс для работы с числами
 */
public interface NumeralDAO {

    /**
     * Возвращает список чисел для определенного языка
     *
     * @param language
     * @return
     */
    List<Numeral> list(Language language);

    /**
     * Сохраняет число
     *
     * @param numeral
     * @param user
     * @return
     */
    boolean save(Numeral numeral, User user);

    /**
     * Удаляет число
     *
     * @param numeral
     * @param user
     * @return
     */
    boolean delete(Numeral numeral, User user);
}