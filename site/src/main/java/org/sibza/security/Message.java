package org.sibza.security;

import java.io.Serializable;

/**
 * Created by timur on 24.09.15.
 *
 * Сообщения нотификаций
 */
public class Message implements Serializable {

    public static final String TYPE_INFO = "info";
    public static final String TYPE_PRIMARY = "primary";
    public static final String TYPE_SUCCESS = "success";
    public static final String TYPE_WARNING = "warning";
    public static final String TYPE_DANGER = "danger";
    public static final String TYPE_MINT = "mint";
    public static final String TYPE_PURPLE = "purple";
    public static final String TYPE_PINK = "pink";
    public static final String TYPE_DARK = "dark";

    private String title;
    private String body;
    private String type;

    public Message(String body, String type) {

        this.body = body;
        this.type = type;
    }

    public Message(String title, String body, String type) {

        this.title = title;
        this.body = body;
        this.type = type;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    @Override
    public String toString() {

        return body;
    }
}