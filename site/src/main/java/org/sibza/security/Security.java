package org.sibza.security;

import org.sibza.model.game.Game;
import org.sibza.model.game.Score;
import org.sibza.model.language.Language;
import org.sibza.model.language.dao.LanguageDAOImpl;
import org.sibza.model.user.Role;
import org.sibza.model.user.User;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.translator.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 17.05.15.
 *
 * Основной класс безопасности
 */
@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Security implements Serializable {

    @Autowired
    private Translator t;

    public static final String DEFAULT_LANGUAGE = Language.RU;

    // сколько будет показано страниц анонимному пользователю до показа приглашения на регистрацию
    public static final int MAX_SHOW_PAGES = 3;

    // проварки на блокировку
    private static final int HOLD_COUNT = 20;
    private int holdCountCurrent = 0;

    private boolean demo;

    // используемый шаблон
    private String template;

    {
        template = "nifty";
    }

    private User user; // авторизованный пользователь
    private List<Message> messages; // список нотификаций
    private String sessionId; // Идентификатор сессии

    private Language language; // язык интерфейса
    private boolean userLanguageStatus; // признак, что пользователь самостоятельно выбрал язык

    private boolean showMobileAlert; // признак, что сообщение о мобильных проложениях уже показали

    private int showPagesCount; // количество просмотренных страниц анонимным посетителем
    private boolean showSocialModal; // признак, что необходимо отобразить окно с приглашением зарегистрироваться

    private Score score;

    {
        user = new User();
        messages = new ArrayList<Message>();
        language = new LanguageDAOImpl().get(DEFAULT_LANGUAGE);
        userLanguageStatus = false;
        showMobileAlert = false;
        demo = false;

        showPagesCount = 0;
        showSocialModal = false;

        score = new Score(new Game(Game.GAME_WORDS_ID), null, 10);
    }

    public User getUser() {

        return user;
    }

    public void setUser(User user) {

        this.user = user;

        if (user == null || user.getId() == 0) {
            // пустой пользователь
            return;
        }

        if (user.isHold()) {
            // пользователь заблокирован и нужно его выкнуть
            addMessage(t.g("Пользователь с таким адресом электронной почты заблокирован. Обратитесь к администратору."), Message.TYPE_DANGER);
            logout();
            return;
        }

        // проверяем верификацию почты
        if (!user.isConfirmed()) {
            addMessage(t.g("Электронная почта не проверена. Если вам не приходило письмо, " +
                    "то можете отправить его повторно. "
                    + "<a href='/security/email/sendConfirm'>" + t.g("Отправить") + "</a>"),
                    Message.TYPE_WARNING);
        }
    }

    public String getSessionId() {

        return sessionId;
    }

    public void setSessionId(String sessionId) {

        this.sessionId = sessionId;
    }

    public String getTemplate() {

        return template + "/";
    }

    public void setTemplate(String template) {

        this.template = template;
    }

    public Language getLanguage() {

        return language;
    }

    public void setLanguage(Language language) {

        this.language = language;
    }

    public boolean isUserLanguageStatus() {

        return userLanguageStatus;
    }

    public void setUserLanguageStatus(boolean userLanguageStatus) {

        this.userLanguageStatus = userLanguageStatus;
    }

    public boolean isShowMobileAlert() {

        return showMobileAlert;
    }

    public void setShowMobileAlert(boolean showMobileAlert) {

        this.showMobileAlert = showMobileAlert;
    }

    public void incShowPagesCount() {

        if (isAuth()) {
            showSocialModal = false;
            return;
        }

        showPagesCount++;

        if (MAX_SHOW_PAGES == showPagesCount) {
            showSocialModal = true;
        } else {
            showSocialModal = false;
        }
    }

    public void clearShowPageCount() {

        showPagesCount = 0;
        showSocialModal = false;
    }

    public boolean isShowSocialModal() {

        return showSocialModal;
    }

    public Score getScore() {

        return score;
    }

    public void setScore(Score score) {

        this.score = score;
    }

    public boolean isDemo() {

        return demo;
    }

    public void setDemo(boolean demo) {

        this.demo = demo;
    }

    /**
     * Возвращает копию списка сообщений
     *
     * @return
     */
    public List<Message> getMessages() {

        return new ArrayList<Message>(messages);
    }

    /**
     * Возвращает копию списка сообщений с очисткой
     *
     * @return
     */
    public List<Message> getMessagesWithClear() {

        List<Message> messages = new ArrayList<Message>(this.messages);
        clearMessages();

        return messages;
    }

    public boolean hasMessage() {

        return messages.size() != 0;
    }

    public void addMessage(Message message) {

        messages.add(message);
    }

    public void addMessage(String message, String type) {

        addMessage(new Message(message, type));
    }

    /**
     * Формирует список различных сообщений с одним типом
     *
     * @param messages
     * @param type
     */
    public void addMessages(List<String> messages, String type) {

        if (messages == null || messages.size() == 0) {
            return;
        }

        for (String message : messages) {
            addMessage(new Message(message, type));
        }
    }

    public void clearMessages() {

        messages.clear();
    }

    /**
     * Признак, что пользователь уже авторизован
     *
     * @return
     */
    public boolean isAuth() {

        boolean auth = (user != null && user.getId() != 0);

        if (!auth) {
            return auth;
        }

        // авторизованных периодически проверяем на блокировки
        if (holdCountCurrent <= HOLD_COUNT) {
            holdCountCurrent++;
        } else {
            holdCountCurrent = 0;
            user = new UserDAOImpl().get(user.getId());
            if (user.isHold()) {
                // пользователь заблокирован и нужно его выкнуть
                addMessage(t.g("Пользователь с таким адресом электронной почты заблокирован. Обратитесь к администратору."), Message.TYPE_DANGER);
                logout();
            }
        }

        return auth;
    }

    /**
     * Logout пользователя
     */
    public void logout() {

        user = null;
        score = new Score(new Game(Game.GAME_WORDS_ID), null, 10);
    }

    /**
     * Возвращает true, если пользователь входит в одну из ролей
     *
     * @param roles список идентификаторов ролей
     * @return
     */
    public boolean hasRole(int[] roles) {

        // в начале проверим, что пользователь авторизован
        if (!isAuth()) {
            return false;
        }

        for (int roleId : roles) {
            List<Role> userRoles = user.getRoles();
            if (userRoles != null) {
                for (Role userRole : userRoles) {
                    if (roleId == userRole.getId()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isAdminOrEditor() {

        return isAdmin() || isEditor();
    }

    public boolean isAdmin() {

        // в начале проверим, что пользователь авторизован
        if (!isAuth()) {
            return false;
        }

        return hasRole(new int[] { Role.ROLE_ADMIN });
    }

    public boolean isEditor() {

        if (!isAuth()) {
            return false;
        }

        return hasRole(new int[] { Role.ROLE_EDITOR });
    }
}