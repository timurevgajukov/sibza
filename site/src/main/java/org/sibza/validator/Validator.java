package org.sibza.validator;

import org.sibza.translator.Translator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by timur on 10.10.15.
 *
 * Общий класс для валидаторов
 */
public class Validator {

    private Translator t;
    private List<String> errors;

    public Validator(Translator t) {

        this.t = t;
    }

    public List<String> getErrors() {

        return errors;
    }

    protected void addError(String message) {

        if (errors == null) {
            errors = new ArrayList<String>();
        }

        errors.add(t.g(message));
    }

    public boolean hasErrors() {

        return errors != null && errors.size() != 0;
    }
}
