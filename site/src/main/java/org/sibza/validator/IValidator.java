package org.sibza.validator;

/**
 * Created by timur on 10.10.15.
 *
 * Интерфейс валидации моделей данных
 */
public interface IValidator<T> {

    /**
     * Валидация данных модели
     *
     * @param item
     * @return
     */
    boolean validate(T item);
}