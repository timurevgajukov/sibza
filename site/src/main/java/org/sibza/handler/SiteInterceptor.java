package org.sibza.handler;

import org.sibza.cache.Languages;
import org.sibza.model.language.Language;
import org.sibza.model.user.dao.UserDAOImpl;
import org.sibza.security.Security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Тимур on 19.11.2015.
 *
 * Для перехвата запросов к сайту
 */
@Component
public class SiteInterceptor extends HandlerInterceptorAdapter {

    public static final String COOKIE_LANGUAGE_NAME = "language";

    @Autowired
    protected Security security;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // для локального сайта работает отладочный режим
        debugMode(request);

        // запоминаем идентификатор сессии посетителя сайта
        security.setSessionId(request.getSession().getId());

        // определим языковые предпочтения посетителя
        acceptLanguage(request);

        // проверяем в куках настройки по языку интерфейса
//        Cookie cookie = getCookie(request, COOKIE_LANGUAGE_NAME);
//        Languages languages = Languages.getInstance();
//        if (cookie != null) {
//            security.setLanguage(languages.get(cookie.getValue()));
//        } else {
//            // загружаем в куки язык по-умолчанию
//            cookie = new Cookie(COOKIE_LANGUAGE_NAME, security.getLanguage().getCode());
//            // response.addCookie(cookie);
//        }

        return super.preHandle(request, response, handler);
    }

    /**
     * Автоматически авторизует администратора, если сайта запущен локально
     */
    private void debugMode(HttpServletRequest request) {

        if ("localhost".equalsIgnoreCase(request.getServerName())) {
            security.setDemo(true);
            if (!security.isAuth()) {
                security.setUser(new UserDAOImpl().auth("evgajukov@gmail.com", "Tck89mta7s3z"));
            }
        }
    }

    /**
     * Определяет языковые предпочтения посетителя сайта и делает соответствующие настройки
     *
     * @param request
     */
    private void acceptLanguage(HttpServletRequest request) {

        String userLocalesStr = request.getHeader("Accept-Language");
        if (userLocalesStr != null) {
            UserLocales locales = new UserLocales();

            String[] userLocales = userLocalesStr.split(",");
            if (userLocales.length != 0) {
                for (int i=0; i<userLocales.length; i++) {
                    locales.addUserLocale(new UserLocale(userLocales[i]));
                }
            }

            Language language = locales.getLanguage();
            if (language != null && security.getLanguage().getId() != language.getId() && !security.isUserLanguageStatus()) {
                security.setLanguage(language);
            }
        }
    }

    /**
     * Возвращает купи по его имени
     *
     * @param request
     * @param name
     * @return
     */
    private Cookie getCookie(HttpServletRequest request, String name) {

        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (name.equalsIgnoreCase(cookie.getName())) {
                return cookie;
            }
        }

        return null;
    }

    class UserLocales {

        private List<UserLocale> list;

        {
            list = new ArrayList<UserLocale>();
        }

        public Language getLanguage() {

            if (list == null || list.size() == 0) {
                Languages languages = Languages.getInstance();
                return languages.get(security.DEFAULT_LANGUAGE);
            }

            // получаем локаль с максимальным коэф.
            UserLocale locale = null;
            for (UserLocale userLocale : list) {
                if (locale == null || locale.getQ() < userLocale.getQ()) {
                    locale = userLocale;
                }
            }

            return locale.getLng();
        }

        public void addUserLocale(UserLocale locale) {

            if (locale.getLng() == null) {
                return;
            }

            list.add(locale);
        }
    }

    class UserLocale {

        private Language lng;
        private double q;

        public UserLocale(String userLocale) {

            String[] locale = userLocale.split(";");

            Languages languages = Languages.getInstance();
            String lngCode = locale[0].split("-")[0].toUpperCase();
            lng = languages.get(lngCode);

            if (locale.length > 1) {
                q = Double.parseDouble(locale[1].split("=")[1]);
            } else {
                q = 1;
            }
        }

        public Language getLng() {

            return lng;
        }

        public double getQ() {

            return q;
        }
    }
}
