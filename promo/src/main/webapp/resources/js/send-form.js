/**
 * Created by timur on 04.06.15.
 *
 * Обработка форм на landing page
 */
$(document).ready(function() {

    /**
     * Отправляем сообщение от посетителя сайта
     */
    $("#send-msg-form").click(function() {
        $.ajax({
            url: "/sendemail",
            type: "POST",
            data: $("#email-message-form").serialize(),
            success: function(response) {
                console.log("Send email message success!!!");

                $("#email-message-form").trigger("reset");
                $("#myModal").modal('show');
            },
            error: function(response) {
                console.log("Send email message errorXXX");
            }
        });

        return false;
    });

    /**
     * Отправляем запрос на добавление номера телефона в рассылку
     */
    $("#send-delivery-form").click(function() {
        $.ajax({
            url: "/senddelivery",
            type: "POST",
            data: $("#delivery-form").serialize(),
            success: function(response) {
                console.log("Send delivery request success!!!");

                $("#delivery-form").trigger("reset");
                $("#myModal").modal('show');
            },
            error: function(response) {
                console.log("Send delivery request errorXXX");
            }
        });

        return false;
    });
});