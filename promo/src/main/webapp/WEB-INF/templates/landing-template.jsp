<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<!-- HEAD SECTION -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Социально-образовательный проект призванный поддерживать и обучать всех желающих
        адыгскому языку и культуре">
    <meta name="author" content="Timur Kh. Evgazhukov">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>Социально-образовательный проект "Си бзэ" .::. обучение адыгскому языку и культуре</title>
    <link rel="shortcut icon" href="/resources/themes/nature/assets/img/favicon.jpg">
    <!--GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!--BOOTSTRAP MAIN STYLES -->
    <link href="/resources/themes/nature/assets/css/bootstrap.css" rel="stylesheet"/>
    <!--FONTAWESOME MAIN STYLE -->
    <link href="/resources/themes/nature/assets/css/font-awesome.min.css" rel="stylesheet"/>
    <!--CUSTOM STYLE -->
    <link href="/resources/themes/nature/assets/css/style.css" rel="stylesheet"/>

    <link href="/resources/themes/nature/assets/css/jquery.fancybox.css" rel="stylesheet"/>

    <link rel="stylesheet" href="/resources/themes/nature/assets/css/animate.min.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<!--END HEAD SECTION -->
<body>

<t:insertAttribute name="content"/>

<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
<!-- CORE JQUERY LIBRARY -->
<script src="/resources/themes/nature/assets/js/jquery.js"></script>
<!-- CORE BOOTSTRAP LIBRARY -->
<script src="/resources/themes/nature/assets/js/bootstrap.min.js"></script>
<!-- SCROLL REVEL LIBRARY FOR SCROLLING ANIMATIONS-->
<script src="/resources/themes/nature/assets/js/scrollReveal.js"></script>
<!-- CUSTOM SCRIPT-->
<script src="/resources/themes/nature/assets/js/custom.js"></script>

<script src="/resources/themes/nature/assets/js/wow.min.js"></script>

<script src="/resources/themes/nature/assets/js/jquery.fancybox.js"></script>

<script src="/resources/js/send-form.js"></script>

<script>
    new WOW().init();
</script>
</body>
</html>