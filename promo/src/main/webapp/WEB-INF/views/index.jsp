<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- NAV SECTION -->
<div class="navbar navbar-inverse navbar-fixed-top header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#header-section"><i class="fa fa-leaf"></i> Си бзэ</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#about-section">О НАС</a></li>
                <li><a href="#products-section">ПРОДУКЦИЯ</a></li>
                <li><a href="#applications-section">ПРИЛОЖЕНИЯ</a></li>
                <li><a href="#delivery-section">РАССЫЛКА</a></li>
                <li><a href="#donate-section">ПОДДЕРЖКА</a></li>
                <li><a href="#contact-section">КОНТАКТЫ</a></li>
            </ul>
        </div>
    </div>
</div>
<!--END NAV SECTION -->
<!-- HEADER SECTION -->
<div id="header-section">
    <div class="container">
        <div class="row centered">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                        <img style="max-width:100%" src="/resources/img/logo-512x324.png"
                             class="wow fadeInUp" data-wow-delay="1s" data-wow-duration="1s"/>
                    </div>
                </div>
                <h2 class="wow fadeInLeft" data-wow-delay="2s" data-wow-duration="1s">Социальный проект по изучению</h2>

                <h1 class="wow fadeInUp" data-wow-delay="3s" data-wow-duration="1s"><strong>АДЫГСКОГО ЯЗЫКА И
                    КУЛЬТУРЫ</strong></h1>
            </div>
        </div>
    </div>
</div>
<!--END HEADER SECTION -->
<!--ABOUT SECTION -->
<div id="about-section">
    <div class="container">
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1 class="wow bounceInDown">О нас</h1>
                <h4>
                    Мы команда энтузиастов, которые болеют за наш язык и культурное наследие адыгов. Каждый желающий
                    может
                    присоединиться к нам и помочь в развитии проекта.
                </h4>
            </div>
        </div>
        <hr>
        <!-- ./ Main Heading-->
        <div class="row main-low-margin text-center">
            <div class="col-md-3 col-sm-3">
                <!--<a class="img-thumbnail wow fadeInLeft fancybox" rel="gallery1" href="/resources/img/team/02-b.png" title="">
                    <img src="/resources/img/team/02.png" alt="" />
                </a>-->
                <h4>Анзор Хамдохов</h4>

                <p class="text-justify">
                    Наш идейный вдохновитель и координатор проекта.
                </p>
            </div>
            <div class="col-md-3 col-sm-3">
                <!--<a class="img-thumbnail wow fadeInUp fancybox" rel="gallery1" href="/resources/img/team/04-b.png" title="">
                    <img src="/resources/img/team/04.png" alt="" />
                </a>-->
                <h4>Тимур Евгажуков</h4>

                <p class="text-justify">
                    На нем держится вся техническая часть проекта.
                </p>
            </div>
            <div class="col-md-3 col-sm-3">
                <!--<a class="img-thumbnail wow fadeInLeft fancybox" rel="gallery1" href="/resources/img/team/03-b.png" title="">
                    <img src="/resources/img/team/03.png" alt="" />
                </a>-->
                <h4>Мусадин Карданов</h4>

                <p class="text-justify">
                    Преподаватель кабардинского языка и литературы, к.&nbsp;ф.&nbsp;н., старший научный сотрудник
                    Кабардино-Балкарского института гуманитарных исследований - наш консультант.
                </p>
            </div>
            <div class="col-md-3 col-sm-3">
                <!--<a class="img-thumbnail wow fadeInDown fancybox" rel="gallery1" href="/resources/img/team/01-b.png" title="">
                    <img src="/resources/img/team/01.png" alt="" />
                </a>-->
                <h4>Асланбек Гергов</h4>

                <p class="text-justify">
                    Занимается звуком и графикой.
                </p>
            </div>
        </div>
        <!-- ./ Row Content-->
    </div>
</div>
<!-- END ABOUT SECTION -->
<!-- PRODUCTS SECTION -->
<div id="products-section">
    <div class="container">
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1 class="wow bounceInDown">Наша продукция</h1>
                <h4>
                    На текущий момент мы работаем в нескольких направлениях
                </h4>
            </div>
        </div>
        <hr>
        <!-- ./ Main Heading-->
        <div class="row text-center">
            <div class="col-md-4 col-sm-4">
                <img src="/resources/themes/nature/assets/img/phone.png" data-wow-duration="2s" data-wow-delay="0.3s"
                     class="wow fadeIn"/>

                <h3>СЛОВАРЬ И РАЗГОВОРНИК</h3>

                <p>
                    Мы стараемся собрать наиболее полный словарь адыгского языка и удобный разговорник, который
                    поможет в повседневной жизни.<br/>
                    <a href="http://sibza.org/" target="_blank">перейти на основной сайт</a>
                </p>
            </div>
            <div class="col-md-4 col-sm-4">
                <img src="/resources/themes/nature/assets/img/clock.png" data-wow-duration="2s" data-wow-delay="0.3s"
                     class="wow fadeIn"/>

                <h3>ПОСЛОВИЦЫ И ПОГОВОРКИ</h3>

                <p>
                    Вся мудрость адыгского народа в виде пословиц и поговорок собрана в нашей обширной базе.<br/>
                    <a href="#delivery-section">подписаться на рассылку</a>
                </p>
            </div>
            <div class="col-md-4 col-sm-4">
                <img src="/resources/themes/nature/assets/img/monitor.png" data-wow-duration="2s" data-wow-delay="0.3s"
                     class="wow fadeIn"/>

                <h3>Мультфильмы</h3>

                <p>
                    Создаем обучающие и просто познавательные мультфильмы о нашей культуре и истории.<br/>
                    <a href="http://www.youtube.com/channel/UCPSNVRizRlpdqJYeq5-sAmA" target="_blank">перейти на наш
                        канал в youtube</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!--END PRODUCTS SECTION -->
<!--APPLICATIONS SECTION -->
<div id="applications-section">
    <div class="container">
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1 class="wow bounceInDown">Наши приложения</h1>
                <h4>
                    На текущий момент в Google Play доступно наше мобильное приложение.
                </h4>
            </div>
        </div>
        <!-- ./ Main Heading-->
        <hr/>
        <div class="row">
            <div class="col-md-10  col-md-offset-1 col-sm-12">
                <div class="col-md-4 col-sm-4">
                    <img src="/resources/themes/nature/assets/img/phone-2.jpg" alt="" class="wow flipInY"
                         data-wow-duration="2s">
                </div>
                <div class="col-md-8 col-sm-8">
                    <h4><i class="fa fa-tint"></i> <strong class="color-red">Мобильное приложение</strong></h4>

                    <p>
                        Для более удобного использования словаря и разговорника, а также прослушивания адыгской мызыки
                        мы создали мобильное приложение под android.
                    </p>

                    <p>
                        <i class="fa fa-check"></i> Словарь и разговорник по категориям<br/>
                        <i class="fa fa-check"></i> Адыгская народная музыка<br/>
                        <i class="fa fa-check"></i> Поддерживается 3 языка интерфейса (русский, английский и
                        турецкий)<br/>
                    </p>
                    <a href="https://play.google.com/store/apps/details?id=ru.evgajukov.mobile.circassian.activity"
                       class="btn btn-success">Загрузить сейчас</a>
                </div>
            </div>

        </div>
    </div>
</div>
<!--END APPLICATIONS SECTION -->
<!--DELIVERY SECTION -->
<div id="delivery-section">
    <div class="container">
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1>СМС-рассылка</h1>
                <h4>
                    Мы предлагаем уникальную возможность ежедневно получать в виде смс адыгские пословицы и поговорки.
                </h4>

                <p>
                    Уважаемые, посетители сайта, с 1 июля рассылка становится платной и будет стоить 100 руб. в месяц.
                </p>

                <form id="delivery-form" onsubmit="return false;">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" required="required"
                                       placeholder="Имя">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" required="required"
                                       placeholder="Адрес эл. почты">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" required="required"
                                       pattern="^\+7\d{10}$"
                                       placeholder="Номер телефона в формате +7XXXXXXXXXX">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                    <textarea name="message" name="message" required="required" class="form-control"
                                              rows="8" placeholder="Пару слов для нас"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="send-delivery-form">Подписаться на рассылку</button>
                            </div>
                        </div>
                    </div>
                </form>

                <h4>Оплатить подписку на рассылку</h4>
                <p>Стоимость рассылки составляет 100 руб. в месяц. Возможно оплатить сразу за несколько месяцев.</p>
                <p><strong>Внимание:</strong> при оплате указывайте номер, на который приходит рассылка.</p>
                <iframe frameborder="0" allowtransparency="true" scrolling="no"
                        src="https://money.yandex.ru/embed/shop.xml?account=41001219031090&quickpay=shop&payment-type-choice=on&writer=seller&targets=%D0%9E%D0%BF%D0%BB%D0%B0%D1%82%D0%B0+%D0%B7%D0%B0+%D1%81%D0%BC%D1%81+%D1%80%D0%B0%D1%81%D1%81%D1%8B%D0%BB%D0%BA%D1%83+%D0%B0%D0%B4%D1%8B%D0%B3%D1%81%D0%BA%D0%B8%D1%85+%D0%BF%D0%BE%D1%81%D0%BB%D0%BE%D0%B2%D0%B8%D1%86&default-sum=100&button-text=01&phone=on&successURL=http%3A%2F%2Fpromo.sibza.org"
                        width="450" height="213"></iframe>
            </div>
        </div>
        <hr>

    </div>
</div>
<!--END DELIVERY SECTION -->
<!-- DONATE SECTION -->
<div id="donate-section">
    <div class="container">
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1>Поддержите проект</h1>
                <h4>
                    Социальному проекту "Си бзэ" нужна ваша поддержка для того, чтобы мы могли радовать всех новыми,
                    интересными, а главное полезными сервисами для изучения адыгского языка и культуры.
                </h4>

                <iframe frameborder="0" allowtransparency="true" scrolling="no"
                        src="https://money.yandex.ru/embed/shop.xml?account=41001219031090&quickpay=shop&payment-type-choice=on&writer=seller&targets=%D0%9F%D0%BE%D0%BC%D0%BE%D1%89%D1%8C+%D1%81%D0%BE%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%BC%D1%83+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D1%83+%22%D0%A1%D0%B8+%D0%B1%D0%B7%D1%8D%22&default-sum=&button-text=03&comment=on&hint=%D0%92%D1%81%D0%B5+%D1%87%D1%82%D0%BE+%D0%BF%D0%BE%D1%81%D1%87%D0%B8%D1%82%D0%B0%D0%B5%D1%82%D0%B5+%D0%BD%D1%83%D0%B6%D0%BD%D1%8B%D0%BC+%D0%BC%D0%BE%D0%B6%D0%B5%D1%82%D0%B5+%D0%BD%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C+%D1%82%D1%83%D1%82&successURL=http%3A%2F%2Fpromo.sibza.org"
                        width="450" height="268"></iframe>
            </div>
        </div>
        <hr>
    </div>
</div>
<!-- END DONATE SECTION -->
<!--CONTACT SECTION -->
<div id="contact-section">
    <div class="container">
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <h1 class="wow bounceInDown">Контакты</h1>
                <h4>
                    Можете связаться с нами по любому указанному способу.
                </h4>
            </div>
        </div>
        <!-- ./ Main Heading-->
        <div class="row">
            <div class="col-md-12  col-sm-12 ">
                <div class="col-md-6  col-sm-12 wow fadeInLeftBig">
                    <h3><i class="fa fa-leaf"></i> Для связи</h3>
                    <hr/>
                    <p>
                        Эл. почта: info@sibza.org<br/>
                    </p>

                    <a href="https://www.facebook.com/pages/%D0%A1%D0%B8-%D0%B1%D0%B7%D1%8D/926917587328131"><img
                            src="/resources/img/ico/Facebook-icon.png" alt="Facebook"/></a>
                    <a href="https://vk.com/sibza"><img src="/resources/img/ico/VK-icon.png" alt="ВКонтакте"/></a>
                </div>
                <div class="col-md-6 col-sm-12 wow fadeInUpBig">
                    <h3>Остались вопросы? Спросите нас.</h3>
                    <hr/>
                    <form id="email-message-form" onsubmit="return false;">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" required="required"
                                           placeholder="Имя">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" required="required"
                                           placeholder="Адрес эл. почты">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <textarea name="message" name="message" required="required" class="form-control"
                                              rows="8" placeholder="Сообщение"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" id="send-msg-form">Отправить запрос</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- ./ Row Content-->
    </div>
</div>
<!--END CONTACT SECTION -->
<!--FOOTER SECTION -->
<div class="navbar navbar-inverse header footer margin-top-20">
    <div class="container">
        &copy; Социально-образовательный проект "Си бзэ", 2013 г.
        <!--
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#">Terms and Condition</a></li>
            <li><a href="#">Support</a></li>
            <li><a href="#">Career</a></li>
        </ul>
        -->
    </div>
</div>
<!--END FOOTER SECTION -->
<!-- MODAL SECTION -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Отправка формы</h4>
            </div>
            <div class="modal-body">
                Ваше сообщение успешно отправлено.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL SECTION -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter30758688 = new Ya.Metrika({
                    id: 30758688,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/30758688" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->