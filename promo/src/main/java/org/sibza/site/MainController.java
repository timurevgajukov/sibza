package org.sibza.site;

import org.sibza.utils.Email;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by timur on 08.05.15.
 *
 * Основной контроллер для обработки общих страниц сайта
 */
@Controller
public class MainController {

    private static final String EMAIL_ADDRESS = "info@sibza.org";
    private static final String EMAIL_PASSWORD = "Corvus2128506";

    @RequestMapping("/")
    public String indexAction() {

        return "index";
    }

    @RequestMapping(value = "/sendemail", method = RequestMethod.POST)
    public @ResponseBody String sendMessageAction(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "email") String emailAddress,
            @RequestParam(value = "message") String msg) {

        String message = String.format("Имя: %s<br />Обратный адрес: %s<br /><br />%s", name, emailAddress, msg);

        Email email = new Email(EMAIL_ADDRESS, EMAIL_PASSWORD);
        email.send("Сообщение от посетителя сайта sibza.org", message, EMAIL_ADDRESS);

        return "Ok";
    }

    @RequestMapping(value = "/senddelivery", method = RequestMethod.POST)
    public @ResponseBody String sendDelieryRequestAction(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "email") String emailAddress,
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "message") String msg) {

        Email email = new Email(EMAIL_ADDRESS, EMAIL_PASSWORD);

        String message = String.format("Имя: %s<br />Обратный адрес: %s<br />Номер телефона: %s<br /><br />%s",
                name, emailAddress, phone, msg);
        email.send("Заявка на рассылку от посетителя сайта sibza.org", message, EMAIL_ADDRESS);

        message = String.format("Здравствуйте, %s<br />Ваша заявка на добавление номера %s в рассылку принята.<br /><br />" +
                        "С уважением, команда проекта \"Си бзэ\"<br />Эл. почта для связи: info@sibza.org",
                name, phone);
        email.send("Заявка на рассылку адыгских пословиц", message, emailAddress);

        return "Ok";
    }
}
