package org.sibza.site.api.test.model.order;

/**
 * Created by timur on 28.07.15.
 */
public class Account {

    private String system;
    private String number;

    public String getSystem() {

        return system;
    }

    public void setSystem(String system) {

        this.system = system;
    }

    public String getNumber() {

        return number;
    }

    public void setNumber(String number) {

        this.number = number;
    }

    @Override
    public String toString() {

        return number;
    }
}
