package org.sibza.site.api.test.model.order;

/**
 * Created by timur on 28.07.15.
 */
public class Payment {

    private String content;
    private Double amount;
    private String currency;

    public String getContent() {

        return content;
    }

    public void setContent(String content) {

        this.content = content;
    }

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public String getCurrency() {

        return currency;
    }

    public void setCurrency(String currency) {

        this.currency = currency;
    }

    @Override
    public String toString() {

        return content;
    }
}
