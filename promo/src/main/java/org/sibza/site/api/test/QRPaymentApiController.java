package org.sibza.site.api.test;

import com.google.gson.Gson;
import org.sibza.site.api.test.model.order.Order;
import org.sibza.site.api.test.model.order.Seller;
import org.sibza.utils.Smsc;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by timur on 28.07.15.
 *
 * Тестовый api для демонстрации покупок через моб. приложение
 */
@Controller
public class QRPaymentApiController {

    private static final String MSG_OK = "Платеж № %s на сумму %.2f руб. успешно совершен";

    @RequestMapping(value = "/qrpayment/api/buy", method = RequestMethod.GET)
    public @ResponseBody String buyGetAction() {

        return "Необходимо POST запросом передать json с описанием платежа";
    }

    @RequestMapping(value = "/qrpayment/api/buy", method = RequestMethod.POST)
    public @ResponseBody String buyPostAction(@RequestBody String json) {

        Gson gson = new Gson();
        Order order = gson.fromJson(json, Order.class);
        if (order == null) {
            return "error";
        }

        // отправляем смс сообщение продавцу об успешности транзакции
        String msg = String.format(MSG_OK, order.getNumber(), order.getPayment().getAmount());
        sendSMS(order, msg);

        return "Ok";
    }

    private void sendSMS(Order order, String msg) {

        if (order.getSeller() != null) {
            String phone = order.getSeller().getPhone();
            if (phone != null && phone.trim().length() != 0) {
                Smsc sms = new Smsc();
                sms.send_sms(phone, msg, 0, "", "", 0, "SIBZA", "");
            }
        }
    }
}
