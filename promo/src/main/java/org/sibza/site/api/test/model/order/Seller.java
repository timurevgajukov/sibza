package org.sibza.site.api.test.model.order;

/**
 * Created by timur on 28.07.15.
 */
public class Seller {

    private String biztype;
    private String name;
    private String inn;
    private String address;
    private String phone;
    private Account account;

    public String getBiztype() {

        return biztype;
    }

    public void setBiztype(String biztype) {

        this.biztype = biztype;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getInn() {

        return inn;
    }

    public void setInn(String inn) {

        this.inn = inn;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public Account getAccount() {

        return account;
    }

    public void setAccount(Account account) {

        this.account = account;
    }

    @Override
    public String toString() {

        return name;
    }
}
