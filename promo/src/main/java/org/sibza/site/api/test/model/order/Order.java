package org.sibza.site.api.test.model.order;

/**
 * Created by timur on 28.07.15.
 */
public class Order {

    private String type;
    private String number;
    private String greeting;
    private String odate;
    private String otime;
    private Seller seller;
    private Payment payment;
    private String signature;

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getNumber() {

        return number;
    }

    public void setNumber(String number) {

        this.number = number;
    }

    public String getGreeting() {

        return greeting;
    }

    public void setGreeting(String greeting) {

        this.greeting = greeting;
    }

    public String getOdate() {

        return odate;
    }

    public void setOdate(String odate) {

        this.odate = odate;
    }

    public String getOtime() {

        return otime;
    }

    public void setOtime(String otime) {

        this.otime = otime;
    }

    public Seller getSeller() {

        return seller;
    }

    public void setSeller(Seller seller) {

        this.seller = seller;
    }

    public Payment getPayment() {

        return payment;
    }

    public void setPayment(Payment payment) {

        this.payment = payment;
    }

    public String getSignature() {

        return signature;
    }

    public void setSignature(String signature) {

        this.signature = signature;
    }

    @Override
    public String toString() {

        return number;
    }
}
