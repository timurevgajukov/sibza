package ru.evgajukov.mobile.circassian.cache;

import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.utils.Log;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для хранения избранных страниц
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Favorite {

    private static final String FLD_FAVOTITES = "favorites";

    private static Favorite instance;

    private SharedPreferences prefs;

    private List<Word> words;

    public static Favorite getInstance(Context context) {

        if (instance == null) {
            instance = new Favorite(context);
        }

        return instance;
    }

    private Favorite(Context context) {

        super();

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isFavorive(Word word) {

        if (words == null || words.size() == 0) {
            return false;
        }

        if (word == null) {
            return false;
        }

        for (Word wordFavorite : words) {
            try {
                if (wordFavorite.getWord().equalsIgnoreCase(word.getWord())) {
                    return true;
                }
            } catch (Exception e) {
            }
        }

        return false;
    }

    public List<Word> getFavorites() {

        return words;
    }

    public boolean addFavorite(Word word) {

        if (words == null) {
            words = new ArrayList<Word>();
        }

        words.add(word);

        return true;
    }

    public boolean delFavorite(Word word) {

        if (words == null || words.size() == 0) {
            return true;
        }

        for (Word wordFavorite : words) {
            if (word.getWord().equalsIgnoreCase(wordFavorite.getWord())) {
                return words.remove(wordFavorite);
            }
        }

        return true;
    }

    public boolean clear() {

        if (words != null) {
            words.clear();
            words = null;
        }

        return true;
    }

    /**
     * Загружает из внешнего источника список избранных статей
     */
    public void load() {

        Log.d("load");

        if (prefs.contains(FLD_FAVOTITES) && words == null) {
            String favoritesWordsJson = prefs.getString(FLD_FAVOTITES, null);
            if (favoritesWordsJson != null) {
                Gson gson = new Gson();
                words = gson.fromJson(favoritesWordsJson, new TypeToken<List<Word>>(){}.getType());
            }
        }
    }

    /**
     * Сохраняет списки избранных статей во внешнее хранилище
     */
    public void save() {

        Log.d("save");

        if (words == null || words.size() == 0) {
            return;
        }

        Gson gson = new Gson();
        String favoritesWordsJson = gson.toJson(words);
        if (favoritesWordsJson != null) {
            Editor editor = prefs.edit();
            editor.putString(FLD_FAVOTITES, favoritesWordsJson);
            editor.commit();
        }
    }
}