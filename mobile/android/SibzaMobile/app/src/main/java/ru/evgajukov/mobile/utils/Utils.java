package ru.evgajukov.mobile.utils;

import java.util.UUID;

import ru.evgajukov.mobile.circassian.GlobalConst;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

/**
 * Общие вспомогательное методы
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Utils {

    /**
     * RFC UUID generation
     */
    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        StringBuilder str = new StringBuilder(uuid.toString());
        int index = str.indexOf("-");
        while (index > 0) {
            str.deleteCharAt(index);
            index = str.indexOf("-");
        }
        return str.toString();
    }

    /**
     * Возвращает версию приложения из манифеста
     * 
     * @param context
     * @return
     */
    public static String getAppVersion(Context context) {

        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            Log.e("error get application version", e);
        }

        return GlobalConst.APP_VER;
    }
}
