package ru.evgajukov.mobile.circassian.activity;

import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.task.CategoriesListLoadTask;
import ru.evgajukov.mobile.circassian.task.NetTask;
import ru.evgajukov.mobile.circassian.view.CategoryItemView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

public class CategoriesActivity extends SlidingBaseActivity {

    private ImageView imgRefresh;
    private ProgressBar progressBar;
    private ListView listCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_categories);

        setSlidingMenu(this, R.id.categories_header_menu);

        imgRefresh = (ImageView) findViewById(R.id.categories_header_refresh);
        imgRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                getCache().clear();

                showCategories();
            }
        });

        listCategories = (ListView) findViewById(R.id.categories_list);
        listCategories.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Category category = ((CategoryItemView) view).getCategory();

                if (category != null) {
                    // перейти на список статей по выбранной категории
                    Intent intent = new Intent(CategoriesActivity.this, WordsActivity.class);
                    intent.putExtra("categoryId", category.getId());
                    startActivity(intent);
                }
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.categories_progress);
    }

    @Override
    protected void onResume() {

        super.onResume();

        showCategories();
    }

    private void showCategories() {

        progressBar.setVisibility(View.VISIBLE);

        new NetTask(getApplicationContext(),
                new CategoriesListLoadTask(this, progressBar, listCategories, listSliderCategories)).execute();
    }
}
