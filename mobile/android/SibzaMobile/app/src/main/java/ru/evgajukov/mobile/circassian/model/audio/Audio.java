package ru.evgajukov.mobile.circassian.model.audio;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.language.Language;
import ru.evgajukov.mobile.circassian.model.resource.Resource;

/**
 * Created by timur on 28.10.15.
 *
 * Модель для работы с музыкой
 */
public class Audio extends Model {

    private Language language;
    private Resource resource;
    private String title;
    private String note;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {

        return title;
    }
}