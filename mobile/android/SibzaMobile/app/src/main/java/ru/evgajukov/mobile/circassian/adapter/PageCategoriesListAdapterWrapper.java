package ru.evgajukov.mobile.circassian.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.utils.Log;
import android.content.Context;
import android.widget.SimpleExpandableListAdapter;

/**
 * Адаптер для отображения списка категорий для конкретной статьи
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class PageCategoriesListAdapterWrapper {

    private SimpleExpandableListAdapter adapter;
    private Context context;

    private List<Map<String, String>> groups;
    private List<List<Map<String, String>>> children;

    {
        groups = new ArrayList<Map<String, String>>();
        children = new ArrayList<List<Map<String, String>>>();
    }

    public PageCategoriesListAdapterWrapper(Context context, List<String> categories) {

        this.context = context;

        loadGroups();
        loadChildren(categories);

        adapter = new SimpleExpandableListAdapter(
                context,
                groups,
                R.layout.list_item_page_categories_group,
                new String[] {"categoryListTitle"},
                new int[] {R.id.page_list_item_categories_group},
                children,
                R.layout.list_item_page_categories,
                new String[] {"categoryName"},
                new int[] {R.id.page_list_item_categories});
    }

    public SimpleExpandableListAdapter getAdapter() {

        return adapter;
    }

    private void loadGroups() {

        HashMap<String, String> categoriesListTitle = new HashMap<String, String>();
        categoriesListTitle.put("categoryListTitle", context.getString(R.string.action_categories));
        groups.add(categoriesListTitle);
    }

    private void loadChildren(List<String> categories) {

        Map<String, String> item;
        List<Map<String, String>> itemList = new ArrayList<Map<String, String>>();
        try {
            if (categories != null) {
                for (String category : categories) {
                    item = new HashMap<String, String>();
                    item.put("categoryName", category);

                    itemList.add(item);
                }
            }
        } catch (Exception e) {
            Log.e(e.toString());
        }

        children.add(itemList);
    }
}
