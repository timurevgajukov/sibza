package ru.evgajukov.mobile.circassian.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.adapter.PageCategoriesListAdapterWrapper;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.model.resource.Resource;
import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.circassian.task.response.ResponseResources;
import ru.evgajukov.mobile.circassian.task.NetTask;
import ru.evgajukov.mobile.circassian.task.WordsListLoadTask;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

public class WordActivity extends SlidingBaseActivity {

    private Category category;
    private Word word;

    private ImageView imgFavorite;
    private ImageView imgRefresh;
    private TextView txtTitle;
    private TextView txtText;

    private RelativeLayout layoutAudioPlay;
    private ProgressBar progressBarAudio;
    private ImageView imgAudio;
    private ImageView imgPlayPause;
    private ImageView imgStop;
    private LinearLayout layoutAudioPanel;

    private ExpandableListView listCategories;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);

        setSlidingMenu(this, R.id.page_header_menu);

        int categoryId = getIntent().getExtras().getInt("categoryId");
        int wordId = getIntent().getExtras().getInt("wordId");
        category = getCache().getCategory(categoryId);
        if (category != null) {
            if (category.getWords() == null || category.getWords().size() == 0) {
                // загружаем список слов для категории с сервера
                new NetTask(getApplicationContext(), new WordsListLoadTask(category)).execute();
            }
            word = category.getWord(wordId);
        }

        txtTitle = (TextView) findViewById(R.id.page_header_title);

        imgRefresh = (ImageView) findViewById(R.id.page_header_refresh);
        imgRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                getCache().clearPage();

                showWord();
            }
        });

        imgFavorite = (ImageView) findViewById(R.id.page_header_favorite);
        imgFavorite.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (getFavoritesCache().isFavorive(word)) {
                    // удаляем страницу из списка изюранных и меняем отображение
                    // звездочки
                    getFavoritesCache().delFavorite(word);

                    imgFavorite.setImageResource(R.drawable.favorite_off);
                    imgFavorite.setContentDescription(getString(R.string.favorite_add));
                } else {
                    // добавляем страницу в список избранных и меняем вид
                    // звездочки
                    getFavoritesCache().addFavorite(word);

                    imgFavorite.setImageResource(R.drawable.favorite_on);
                    imgFavorite.setContentDescription(getString(R.string.favorite_del));
                }
            }
        });

        txtText = (TextView) findViewById(R.id.page_text);
        txtText.setLinksClickable(true);
        txtText.setMovementMethod(new LinkMovementMethod());

        listCategories = (ExpandableListView) findViewById(R.id.page_categories_list);
        listCategories.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                // переходим на список статей в выбранной категории
                if (category != null) {
                    Intent intent = new Intent(WordActivity.this, WordsActivity.class);
                    intent.putExtra("categoryId", category.getId());
                    startActivity(intent);
                }

                return false;
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.page_progress);
    }

    @Override
    protected void onStart() {

        super.onStart();

        initAudio();
    }

    @Override
    protected void onResume() {

        super.onResume();

        showWord();
    }

    private void showWord() {

        progressBar.setVisibility(View.VISIBLE);

        txtTitle.setText(word.getWord());

        int wordId = 0;
        String translatesList = "";
        for (Word translate : word.getTranslates()) {
            wordId = translate.getId();
            if (translate.getLanguage().getCode().equalsIgnoreCase("AD")) {
                translatesList += "<p>" + translate.getWord() + "</p>";
            }
        }
        txtText.setText(Html.fromHtml(translatesList));

        layoutAudioPlay.setVisibility(View.INVISIBLE);

        final int finalWordId = wordId;
        new AsyncTask<Void, Void, Word>() {

            @Override
            protected Word doInBackground(Void... params) {

                Cache cache = getCache();

                try {
                    Net net = Net.getInstance();
                    String result = net.send(GlobalConst.API_URL
                            + "resource/list?token=" + GlobalConst.APP_TOKEN + "&word=" + finalWordId);

                    if (result != null && result.length() != 0) {
                        Gson gson = new Gson();
                        ResponseResources response = gson.fromJson(result, ResponseResources.class);
                        if (response.getCode() == 0 && response.getBody() != null) {
                            word.setResources(response.getBody());
                        }
                    }
                } catch (Exception e) {
                    Log.e(e.toString(), e);
                }

                return word;
            }

            @Override
            protected void onPostExecute(Word w) {

                progressBar.setVisibility(View.INVISIBLE);

                word = w;

                if (word.getResources() == null || word.getResources().size() == 0) {
                    layoutAudioPlay.setVisibility(View.INVISIBLE);
                } else {
                    layoutAudioPlay.setVisibility(View.VISIBLE);
                }

                List<String> categories = null; //word.getCategories();
                if (categories != null) {
                    listCategories.setVisibility(View.VISIBLE);
                    listCategories.setAdapter(new PageCategoriesListAdapterWrapper(getApplicationContext(), categories).getAdapter());
                    listCategories.expandGroup(0);
                } else {
                    listCategories.setVisibility(View.INVISIBLE);
                }

                if (getFavoritesCache().isFavorive(word)) {
                    imgFavorite.setImageResource(R.drawable.favorite_on);
                    imgFavorite.setContentDescription(getString(R.string.favorite_del));
                } else {
                    imgFavorite.setImageResource(R.drawable.favorite_off);
                    imgFavorite.setContentDescription(getString(R.string.favorite_add));
                }

            }
        }.execute();
    }

    private void initAudio() {

        layoutAudioPlay = (RelativeLayout) findViewById(R.id.page_audio);

        layoutAudioPanel = (LinearLayout) findViewById(R.id.page_audio_panel);
        layoutAudioPanel.setVisibility(View.INVISIBLE);

        progressBarAudio = (ProgressBar) findViewById(R.id.page_progress_audio);
        progressBarAudio.setVisibility(View.INVISIBLE);

        imgAudio = (ImageView) findViewById(R.id.page_audio_img);
        imgAudio.setVisibility(View.VISIBLE);
        imgAudio.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Resource resource = word.getResources().get(0);
                download(GlobalConst.SITE_URL + "upload/" + resource.getId() + "/" + resource.getFileName());
            }
        });

        imgPlayPause = (ImageView) findViewById(R.id.page_audio_img_play_pause);
        imgPlayPause.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                        imgPlayPause.setImageResource(R.drawable.av_play);
                        imgPlayPause.setContentDescription(getString(R.string.play));
                    } else {
                        mediaPlayer.start();
                        imgPlayPause.setImageResource(R.drawable.av_pause);
                        imgPlayPause.setContentDescription(getString(R.string.pause));
                    }
                }
            }
        });

        imgStop = (ImageView) findViewById(R.id.page_audio_img_stop);
        imgStop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                stopAudio();
            }
        });
    }

    private void stopAudio() {

        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                imgPlayPause.setImageResource(R.drawable.av_play);
                imgPlayPause.setContentDescription(getString(R.string.play));
            }
            mediaPlayer.seekTo(0);
        }
    }

    private void play(String filePath) {

        releaseMP();
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            if (filePath != null) {
                mediaPlayer.setDataSource(filePath);
            } else {
                Resource resource = word.getResources().get(0);
                mediaPlayer.setDataSource(GlobalConst.SITE_URL
                        + "upload/" + resource.getId() + "/" + resource.getFileName());
            }
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer mp) {

                    Log.d("onPrepareg");

                    layoutAudioPanel.setVisibility(View.VISIBLE);

                    imgPlayPause.setImageResource(R.drawable.av_pause);
                    imgPlayPause.setContentDescription(getString(R.string.pause));

                    mp.start();
                }
            });
            mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {

                    Log.d("onCompletion");

                    imgPlayPause.setImageResource(R.drawable.av_play);
                    imgPlayPause.setContentDescription(getString(R.string.play));
                }
            });
            // mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void download(String urlFile) {

        Log.d("download file: " + urlFile);

        new AsyncTask<String, Integer, String>() {

            @Override
            protected void onPreExecute() {

                imgAudio.setVisibility(View.INVISIBLE);

                progressBarAudio.setClickable(false);
                progressBarAudio.setMax(100);
                progressBarAudio.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {

                try {
                    URL url = new URL(params[0].replaceAll(" ", "%20"));
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoOutput(true);
                    conn.connect();

                    // выделить из url имя файла
                    String[] filePath = params[0].split("/");
                    String fileName = filePath[filePath.length - 1];
                    Log.d("file name = " + fileName);

                    // получаем размер файла
                    int totalSize = conn.getContentLength();

                    if (Environment.getExternalStorageDirectory().equals(Environment.MEDIA_MOUNTED)) {
                        // в мобильном устройстве доступна SD_карта
                        // формируем все необходимые на папки ссылки
                        String appFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                                + GlobalConst.APP_NAME;

                        // папка для временных файлов
                        String tmpFolder = appFolder + File.separator + GlobalConst.TEMP_FOLDER;
                        File fTmpFolder = new File(tmpFolder);
                        if (!fTmpFolder.exists()) {
                            // если папка не существует, то необходимо создать
                            if (!fTmpFolder.mkdirs()) {
                                Log.e("Cannot create " + fTmpFolder.getAbsolutePath());
                                return null;
                            }
                        }

                        // папка для аудио файлов
                        String audioCacheFolder = appFolder + File.separator + GlobalConst.AUDIO_FOLDER;
                        File fAudioCacheFolder = new File(audioCacheFolder);
                        if (!fAudioCacheFolder.exists()) {
                            // если папка еще не существует, то необходимо создать
                            if (!fAudioCacheFolder.mkdirs()) {
                                Log.e("Cannot create " + fAudioCacheFolder.getAbsolutePath());
                                return null;
                            }
                        }

                        // если файл с таким именем найден, то возвращаем ссылку на него
                        File audioFile = new File(fAudioCacheFolder, fileName);
                        if (audioFile.exists()) {
                            // файл уже ранее был загружен в телефон
                            // сверим размеры локального и сетевого файла
                            if (audioFile.length() == totalSize) {
                                return audioFile.getAbsolutePath();
                            }
                        }

                        File tmpAudioFile = new File(fTmpFolder, fileName);
                        tmpAudioFile.createNewFile();

                        FileOutputStream fos = new FileOutputStream(tmpAudioFile);
                        InputStream is = conn.getInputStream();

                        int downloadedSize = 0;

                        byte[] buffer = new byte[1024];
                        int len = 0;
                        while ((len = is.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                            downloadedSize += len;
                            publishProgress(downloadedSize, totalSize);
                        }
                        fos.close();
                        is.close();

                        // после успешной загрузки во временную папке, файл нужно
                        // перенести в основную папку хранения аудио-файлов
                        tmpAudioFile.renameTo(audioFile);

                        return audioFile.getAbsolutePath();
                    } else {
                        String audioFile = fileName;
                        FileOutputStream fos = openFileOutput(audioFile, Context.MODE_PRIVATE);
                        InputStream is = conn.getInputStream();

                        int downloadedSize = 0;

                        byte[] buffer = new byte[1024];
                        int len = 0;
                        while ((len = is.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                            downloadedSize += len;
                            publishProgress(downloadedSize, totalSize);
                        }
                        fos.close();
                        is.close();

                        String absolutePath = getFilesDir() + File.separator + audioFile;

                        return absolutePath;
                    }
                } catch (Exception e) {
                    Log.e("error to download file", e);
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {

                progressBarAudio.setProgress((int) ((values[0] / (float) values[1]) * 100));
            }

            @Override
            protected void onPostExecute(String filePath) {

                progressBarAudio.setVisibility(View.INVISIBLE);

                play(filePath);
            }
        }.execute(urlFile);
    }
}