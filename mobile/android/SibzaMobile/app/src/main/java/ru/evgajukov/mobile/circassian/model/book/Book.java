package ru.evgajukov.mobile.circassian.model.book;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.language.Language;

/**
 * Created by timur on 28.10.15.
 *
 * Модель книги
 */
public class Book extends Model {

    private Book parent;
    private String title;
    private Language language;
    private String url;
    private String format;
    private double size;
    private List<Book> books;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Book getParent() {
        return parent;
    }

    public void setParent(Book parent) {
        this.parent = parent;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {

        return title;
    }
}