package ru.evgajukov.mobile.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.UnknownHostException;

import javax.net.ssl.HttpsURLConnection;

import ru.evgajukov.mobile.circassian.GlobalConst;

/**
 * Класс для отправик и получения сообщений по сети
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Net {

    private static Net instance;

    {
        instance = null;
    }

    private int errorno;
    private String errormsg;
    private String result;

    {
        errorno = 0;
        errormsg = "";
        result = "";
    }

    public static Net getInstance() {
        if (instance == null) {
            instance = new Net();
        }

        return instance;
    }

    private Net() {
        super();
    }

    public synchronized String send(String urlRequest) {
        Log.d(urlRequest);

        HttpsURLConnection conn = null;
        try {
            Proxy proxy = Proxy.NO_PROXY;

            URL url = new URL(urlRequest);

            conn = (HttpsURLConnection) url.openConnection(proxy);

            conn.setConnectTimeout(GlobalConst.CONN_TIMEOUT);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                result = builder.toString();

                if (result.length() == 0) {
                    Log.w("result len = 0");
                } else {
                    Log.d(result);
                    return result;
                }

            } else {
                // Получить сообщение из потока ошибок
                InputStream is = conn.getErrorStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                errorno = code;
                errormsg = builder.toString();
                if (errormsg.length() == 0) {
                    Log.e("error result len = 0");
                } else {
                    Log.e(errormsg);
                }
            }
        } catch (UnknownHostException e) {
            Log.e(e.toString(), e);
        } catch (Exception e) {
            Log.e(e.toString(), e);
        } finally {
            try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                Log.e(e.toString(), e);
            }
        }

        return null;
    }
}
