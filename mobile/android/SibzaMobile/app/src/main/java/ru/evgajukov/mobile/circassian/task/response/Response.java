package ru.evgajukov.mobile.circassian.task.response;

/**
 * Created by timur on 25.10.15.
 *
 * Общий класс ответа от сервера
 */
public class Response extends ResponseBase {

    private Object body;

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}