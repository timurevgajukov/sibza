package ru.evgajukov.mobile.circassian.activity;

import android.os.Bundle;

/**
 * Created by timur on 28.10.15.
 *
 * Страница с книгами
 */
public class BooksActivity extends SlidingBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        setSlidingMenu(this, R.id.books_header_menu);
    }
}