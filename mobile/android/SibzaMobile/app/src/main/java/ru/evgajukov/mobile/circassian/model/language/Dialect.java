package ru.evgajukov.mobile.circassian.model.language;

import ru.evgajukov.mobile.circassian.model.Model;

/**
 * Created by timur on 28.10.15.
 *
 * Модель диалекта
 */
public class Dialect extends Model {

    private Language language;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}