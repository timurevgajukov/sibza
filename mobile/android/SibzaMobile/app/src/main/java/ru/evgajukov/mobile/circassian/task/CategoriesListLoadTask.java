package ru.evgajukov.mobile.circassian.task;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.circassian.adapter.CategoriesListAdapter;
import ru.evgajukov.mobile.circassian.adapter.SliderCategoriesListAdapter;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.task.response.ResponseCategories;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;

/**
 * Created by timur on 26.10.15.
 *
 * Загрузка списка категорий
 */
public class CategoriesListLoadTask extends AsyncTask<Void, Void, List<Category>> {

    public static final String API_LIST = "category/list";

    private Context context;
    private ProgressBar progressBar;
    private ListView categoriesList;
    private ListView categoriesSliderList;
    private Category parent;

    public CategoriesListLoadTask(Context context,
                                  ProgressBar progressBar,
                                  ListView categoriesList, ListView categoriesSliderList) {

        this(context, progressBar, categoriesList, categoriesSliderList, null);
    }

    public CategoriesListLoadTask(Context context,
                                  ProgressBar progressBar,
                                  ListView categoriesList, ListView categoriesSliderList,
                                  Category parent) {

        this.context = context;
        this.progressBar = progressBar;
        this.categoriesList = categoriesList;
        this.categoriesSliderList = categoriesSliderList;
        this.parent = parent;
    }

    @Override
    protected List<Category> doInBackground(Void... params) {

        Cache cache = Cache.getInstance(context);

        if (cache.getCategories() != null && cache.getCategories().size() > 0) {
            return cache.getCategories();
        }

        List<Category> categories = new ArrayList<Category>();

        try {
            Net net = Net.getInstance();
            String result = net.send(GlobalConst.API_URL
                    + "category/list?token=" + GlobalConst.APP_TOKEN);

            if (result != null && result.length() != 0) {
                Gson gson = new Gson();
                ResponseCategories response = gson.fromJson(result, ResponseCategories.class);
                if (response.getCode() == 0 && response.getBody() != null) {
                    categories = response.getBody();
                }

            }
        } catch (Exception e) {
            Log.e(e.toString(), e);
        }

        if (categories != null && categories.size() != 0) {
            cache.setCategories(categories);
        } else {
            cache.setCategories(null);
        }

        return categories;
    }

    @Override
    protected void onPostExecute(List<Category> categories) {

        progressBar.setVisibility(View.INVISIBLE);

        categoriesList.setAdapter(new CategoriesListAdapter(context, R.layout.list_item_category, categories));

        // заодно еще и в боковом меню проинициализируем список категорий
        categoriesSliderList.setAdapter(new SliderCategoriesListAdapter(context, R.layout.slider_list_item_category, categories));
    }
}