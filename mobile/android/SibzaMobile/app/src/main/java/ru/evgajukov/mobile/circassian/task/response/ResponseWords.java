package ru.evgajukov.mobile.circassian.task.response;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.word.Word;

/**
 * Created by timur on 25.10.15.
 *
 * Класс ответа от сервера со списком слов по категории и языку
 */
public class ResponseWords extends ResponseBase {

    private List<Word> body;

    public List<Word> getBody() {
        return body;
    }

    public void setBody(List<Word> body) {
        this.body = body;
    }
}