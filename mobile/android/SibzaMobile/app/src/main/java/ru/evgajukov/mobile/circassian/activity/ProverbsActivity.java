package ru.evgajukov.mobile.circassian.activity;

import android.os.Bundle;

/**
 * Created by timur on 28.10.15.
 *
 * Страница с пословицами
 */
public class ProverbsActivity extends SlidingBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proverbs);

        setSlidingMenu(this, R.id.proverbs_header_menu);
    }
}