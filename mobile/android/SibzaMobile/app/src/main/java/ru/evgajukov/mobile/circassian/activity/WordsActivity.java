package ru.evgajukov.mobile.circassian.activity;

import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.circassian.task.NetTask;
import ru.evgajukov.mobile.circassian.task.WordsListLoadTask;
import ru.evgajukov.mobile.circassian.view.WordItemView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

public class WordsActivity extends SlidingBaseActivity {

    private Category category;

    private ImageView imgRefresh;
    private ListView listWords;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pages);

        setSlidingMenu(this, R.id.pages_header_menu);

        int categoryId = getIntent().getExtras().getInt("categoryId");
        category = getCache().getCategory(categoryId);

        imgRefresh = (ImageView) findViewById(R.id.pages_header_refresh);
        imgRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                showWords();
            }
        });

        listWords = (ListView) findViewById(R.id.pages_list);
        listWords.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Word word = ((WordItemView) view).getWord();

                if (word != null && word.toString().length() > 0) {
                    // перейдем на страницу статьи
                    Intent intent = new Intent(WordsActivity.this, WordActivity.class);
                    intent.putExtra("categoryId", category.getId());
                    intent.putExtra("wordId", word.getId());
                    startActivity(intent);
                }
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.pages_progress);
    }

    @Override
    protected void onResume() {

        super.onResume();

        showWords();
    }

    private void showWords() {

        progressBar.setVisibility(View.VISIBLE);

        new NetTask(getApplicationContext(),
                new WordsListLoadTask(this, category, progressBar, listWords)).execute();
    }
}