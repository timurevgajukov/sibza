package ru.evgajukov.mobile.circassian.task;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.model.alphabet.Alphabet;
import ru.evgajukov.mobile.circassian.task.response.ResponseAlphabet;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;

/**
 * Created by timur on 28.10.15.
 *
 * Загрузка алфавита
 */
public class AlphabetLoadTask extends AsyncTask<Void, Void, Alphabet> {

    public static final String API_LIST = "alphabet/letter/list";

    private Context context;
    private ProgressBar progressBar;
    private ListView lettersList;

    public AlphabetLoadTask(Context context, ProgressBar progressBar, ListView lettersList) {

        this.context = context;
        this.progressBar = progressBar;
        this.lettersList = lettersList;
    }

    @Override
    protected Alphabet doInBackground(Void... params) {

        try {
            Net net = Net.getInstance();
            String result = net.send(GlobalConst.API_URL
                    + API_LIST + "?token=" + GlobalConst.APP_TOKEN
                    + "&lng=" + GlobalConst.language);

            if (result != null && result.length() != 0) {
                Gson gson = new Gson();
                ResponseAlphabet response = gson.fromJson(result, ResponseAlphabet.class);
                if (response.getCode() == 0 && response.getBody() != null) {
                    return response.getBody();
                }
            }
        } catch (Exception e) {
            Log.e(e.toString(), e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Alphabet alphabet) {

        progressBar.setVisibility(View.INVISIBLE);
    }
}