package ru.evgajukov.mobile.circassian.activity;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.adapter.SliderCategoriesListAdapter;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.task.response.Response;
import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.view.SliderCategoryItemView;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Базовый класс для активностей с боковым меню
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class SlidingBaseActivity extends BaseActivity implements SlidingMenu.OnOpenedListener {

    protected SlidingMenu menu;

    protected TextView txtSliderHome;
    protected TextView txtSliderAlphabet;
    protected TextView txtSliderProverbs;
    protected TextView txtSliderAudio;
    protected TextView txtSliderVideo;
    protected TextView txtSliderBooks;
    protected TextView txtSliderSearch;
    protected TextView txtFavorites;
    protected TextView txtSliderAboutApp;
    protected TextView txtSliderSettings;
    protected ListView listSliderCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // сконфигурируем SlidingMenu
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
    }

    protected void setSlidingMenu(final Activity activity, int imgOpenSlidingMenuResId) {

        menu.attachToActivity(activity, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.slider_menu);

        menu.setOnOpenedListener(this);

        ImageView imgOpenSlidingMenu = (ImageView) findViewById(imgOpenSlidingMenuResId);
        if (imgOpenSlidingMenu != null) {
            imgOpenSlidingMenu.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    showSlidingMenu();
                }
            });
        }

        txtSliderHome = (TextView) findViewById(R.id.slider_menu_home);
        txtSliderHome.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, CategoriesActivity.class);
                startActivity(intent);
            }
        });

//        txtSliderAlphabet = (TextView) findViewById(R.id.slider_menu_alphabet);
//        txtSliderAlphabet.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(activity, AlphabetActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        txtSliderProverbs = (TextView) findViewById(R.id.slider_menu_proverbs);
//        txtSliderProverbs.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(activity, ProverbsActivity.class);
//                startActivity(intent);
//            }
//        });
//
        txtSliderAudio = (TextView) findViewById(R.id.slider_menu_audio);
        txtSliderAudio.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AudioActivity.class);
                startActivity(intent);
            }
        });
//
//        txtSliderVideo = (TextView) findViewById(R.id.slider_menu_video);
//        txtSliderVideo.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(activity, VideoActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        txtSliderBooks = (TextView) findViewById(R.id.slider_menu_books);
//        txtSliderBooks.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(activity, BooksActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        txtSliderSearch = (TextView) findViewById(R.id.slider_menu_search);
//        txtSliderSearch.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(activity, SearchActivity.class);
//                startActivity(intent);
//            }
//        });

        txtFavorites = (TextView) findViewById(R.id.slider_menu_favorites);
        txtFavorites.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, FavoritesActivity.class);
                startActivity(intent);
            }
        });

        txtSliderAboutApp = (TextView) findViewById(R.id.slider_menu_about_app);
        txtSliderAboutApp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AboutAppActivity.class);
                startActivity(intent);
            }
        });

        txtSliderSettings = (TextView) findViewById(R.id.slider_menu_settings);
        txtSliderSettings.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, SettingsActivity.class);
                startActivity(intent);
            }
        });

        listSliderCategories = (ListView) findViewById(R.id.slider_menu_categories_list);
        listSliderCategories.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Category category = ((SliderCategoryItemView) view).getCategory();

                if (category != null) {
                    // перейти на список статей по выбранной категории
                    Intent intent = new Intent(activity, WordsActivity.class);
                    intent.putExtra("categoryId", category.getId());
                    startActivity(intent);
                }
            }
        });

        Cache cache = getCache();
        if (cache.getCategories() != null) {
            listSliderCategories.setAdapter(new SliderCategoriesListAdapter(getApplicationContext(), R.layout.slider_list_item_category, cache.getCategories()));
        }
    }

    protected void showSlidingMenu() {

        menu.showMenu();
    }

    @Override
    public void onOpened() {

        final Cache cache = getCache();
        if (cache.getCategories() == null) {
            new AsyncTask<Void, Void, List<Category>>() {

                @Override
                protected List<Category> doInBackground(Void... params) {

                    List<Category> categories = new ArrayList<Category>();

                    try {
                        Net net = Net.getInstance();
                        String result = net.send(GlobalConst.API_URL + "category/list");

                        if (result != null && result.length() != 0) {
                            Gson gson = new Gson();
                            Response response = gson.fromJson(result, Response.class);
                            if (response.getCode() == 0 && response.getBody() != null) {
                                categories = (List<Category>) response.getBody();
                            }

                        }
                    } catch (Exception e) {
                        Log.e(e.toString(), e);
                    }

                    cache.setCategories(categories);

                    return categories;
                }

                @Override
                protected void onPostExecute(List<Category> categories) {

                    listSliderCategories.setAdapter(new SliderCategoriesListAdapter(getApplicationContext(), R.layout.slider_list_item_category, categories));
                }
            }.execute();
        }
    }
}
