package ru.evgajukov.mobile.circassian.view;

import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.circassian.model.word.Word;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WordItemView extends RelativeLayout {

    private TextView txtWord;

    private Word word;

    public WordItemView(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {

        super.onFinishInflate();

        txtWord = (TextView) findViewById(R.id.list_item_page_title);
    }

    public Word getWord() {

        return word;
    }

    public void setWord(Word word) {

        this.word = word;

        txtWord.setText(word.toString());
    }
}
