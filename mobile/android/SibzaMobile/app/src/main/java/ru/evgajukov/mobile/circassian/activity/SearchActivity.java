package ru.evgajukov.mobile.circassian.activity;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.adapter.WordsListAdapter;
import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.circassian.view.WordItemView;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends SlidingBaseActivity {

    private String text;

    private EditText editText;
    private ImageView imgSearch;
    private ListView listResult;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setSlidingMenu(this, R.id.search_header_menu);

        editText = (EditText) findViewById(R.id.search_text);

        imgSearch = (ImageView) findViewById(R.id.search_btn);
        imgSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                text = editText.getText().toString();
                if (text != null && text.trim().length() > 0) {
                    text = text.trim();
                    showPages();
                }
            }
        });

        listResult = (ListView) findViewById(R.id.search_result_list);
        listResult.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Word word = ((WordItemView) view).getWord();

                if (word != null && word.toString().length() > 0) {
                    // перейдем на страницу статьи
                    Intent intent = new Intent(SearchActivity.this, WordActivity.class);
                    intent.putExtra("word", word.toString());
                    startActivity(intent);
                }
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.search_progress);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showPages() {

        progressBar.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, List<Word>>() {

            @Override
            protected List<Word> doInBackground(Void... params) {

                List<Word> pages = new ArrayList<Word>();

                try {
                    Net net = Net.getInstance();
                    String result = net.send(GlobalConst.API_URL + "");
                } catch (Exception e) {
                    Log.e(e.toString(), e);
                }

                return pages;
            }

            @Override
            protected void onPostExecute(List<Word> words) {

                progressBar.setVisibility(View.INVISIBLE);

                listResult.setAdapter(new WordsListAdapter(getApplicationContext(), R.layout.list_item_page, words));
            }
        }.execute();
    }
}
