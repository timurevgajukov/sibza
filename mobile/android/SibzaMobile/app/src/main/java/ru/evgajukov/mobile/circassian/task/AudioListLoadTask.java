package ru.evgajukov.mobile.circassian.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.model.audio.Audio;

/**
 * Created by timur on 27.12.15.
 *
 * Загрузка музыкального плейлиста
 */
public class AudioListLoadTask extends AsyncTask<Void, Void, List<Audio>> {

    public static final String API_LIST = "audio/list";

    private Context context;
    private ProgressBar progressBar;
    private ListView audioList;

    public AudioListLoadTask(Context context, ProgressBar progressBar, ListView audioList) {

        this.context = context;
        this.progressBar = progressBar;
        this.audioList = audioList;
    }

    @Override
    protected List<Audio> doInBackground(Void... params) {

        Cache cache = Cache.getInstance(context);

        return null;
    }

    @Override
    protected void onPostExecute(List<Audio> audios) {

        super.onPostExecute(audios);
    }
}
