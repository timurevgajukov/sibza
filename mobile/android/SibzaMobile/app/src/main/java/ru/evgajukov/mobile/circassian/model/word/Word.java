package ru.evgajukov.mobile.circassian.model.word;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.model.language.Language;
import ru.evgajukov.mobile.circassian.model.resource.Resource;

/**
 * Created by timur on 25.10.15.
 *
 * Модель слова
 */
public class Word extends Model {

    private String word;
    private Category category;
    private Language language;
    private List<Word> translates;
    private List<Resource> resources;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    public List<Word> getTranslates() {
        return translates;
    }

    public void setTranslates(List<Word> translates) {
        this.translates = translates;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toString() {

        return word;
    }
}