package ru.evgajukov.mobile.circassian.activity;

import ru.evgajukov.mobile.circassian.adapter.WordsListAdapter;
import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.circassian.view.WordItemView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.util.List;

/**
 * Страница со списком избранных статей
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class FavoritesActivity extends SlidingBaseActivity {

    private ListView listWords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        setSlidingMenu(this, R.id.favorites_header_menu);

        listWords = (ListView) findViewById(R.id.favorites_list);
        listWords.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Word word = ((WordItemView) view).getWord();

                if (word != null && word.toString().length() > 0) {
                    // перейдем на страницу статьи
                    Intent intent = new Intent(FavoritesActivity.this, WordActivity.class);
                    intent.putExtra("categoryId", word.getCategory().getId());
                    intent.putExtra("wordId", word.getId());
                    startActivity(intent);
                }
            }
        });

        showWords();
    }

    private void showWords() {

        List<Word> words = getFavoritesCache().getFavorites();
        if (words == null || words.size() == 0) {
            return;
        }

        listWords.setAdapter(new WordsListAdapter(getApplicationContext(), R.layout.list_item_page, words));
    }
}