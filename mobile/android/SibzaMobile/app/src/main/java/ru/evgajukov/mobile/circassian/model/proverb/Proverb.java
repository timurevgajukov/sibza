package ru.evgajukov.mobile.circassian.model.proverb;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.language.Dialect;
import ru.evgajukov.mobile.circassian.model.language.Language;

/**
 * Created by timur on 28.10.15.
 *
 * Модель пословицы
 */
public class Proverb extends Model {

    private Language language;
    private Dialect dialect;
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Dialect getDialect() {
        return dialect;
    }

    public void setDialect(Dialect dialect) {
        this.dialect = dialect;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    @Override
    public String toString() {

        return body;
    }
}