package ru.evgajukov.mobile.circassian.task.response;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.resource.Resource;

/**
 * Created by timur on 25.10.15.
 *
 * Класс ответа от сервера по получению списка ресурсов
 */
public class ResponseResources extends ResponseBase {

    private List<Resource> body;

    public List<Resource> getBody() {
        return body;
    }

    public void setBody(List<Resource> body) {
        this.body = body;
    }
}
