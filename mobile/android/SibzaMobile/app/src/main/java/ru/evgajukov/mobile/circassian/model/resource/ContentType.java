package ru.evgajukov.mobile.circassian.model.resource;

import ru.evgajukov.mobile.circassian.model.Model;

/**
 * Created by timur on 25.10.15.
 *
 * Модель типа контента
 */
public class ContentType extends Model {

    private Type type;
    private String contentType;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
