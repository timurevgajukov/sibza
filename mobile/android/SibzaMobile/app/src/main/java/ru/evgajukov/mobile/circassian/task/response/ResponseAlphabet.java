package ru.evgajukov.mobile.circassian.task.response;

import ru.evgajukov.mobile.circassian.model.alphabet.Alphabet;

/**
 * Created by timur on 28.10.15.
 *
 * Класс ответа от сервера с алфавитом
 */
public class ResponseAlphabet extends ResponseBase {

    private Alphabet body;

    public Alphabet getBody() {
        return body;
    }

    public void setBody(Alphabet body) {
        this.body = body;
    }
}