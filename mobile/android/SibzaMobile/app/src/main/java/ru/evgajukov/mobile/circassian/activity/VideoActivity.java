package ru.evgajukov.mobile.circassian.activity;

import android.os.Bundle;

/**
 * Created by timur on 28.10.15.
 *
 * Страница с видео
 */
public class VideoActivity extends SlidingBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        setSlidingMenu(this, R.id.video_header_menu);
    }
}