package ru.evgajukov.mobile.circassian.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import ru.evgajukov.mobile.circassian.task.AudioListLoadTask;
import ru.evgajukov.mobile.circassian.task.NetTask;

/**
 * Created by timur on 28.10.15.
 *
 * Страница с аудио
 */
public class AudioActivity extends SlidingBaseActivity {

    private ImageView imgRefresh;
    private ProgressBar progressBar;
    private ListView listAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        setSlidingMenu(this, R.id.audio_header_menu);

        imgRefresh = (ImageView) findViewById(R.id.categories_header_refresh);
        imgRefresh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                getCache().clear();

                showAudioList();
            }
        });

        listAudio = (ListView) findViewById(R.id.audio_list);
        listAudio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        progressBar = (ProgressBar) findViewById(R.id.audio_progress);
    }

    @Override
    protected void onResume() {

        super.onResume();

        showAudioList();
    }

    private void showAudioList() {

        progressBar.setVisibility(View.VISIBLE);

        new NetTask(getApplicationContext(),
                new AudioListLoadTask(this, progressBar, listAudio)).execute();
    }
}