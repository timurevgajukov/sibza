package ru.evgajukov.mobile.circassian.model;

/**
 * Created by timur on 25.10.15.
 *
 * Базовый класс моделей данных
 */
public class Model {

    protected int id;
    protected String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {

        return name;
    }
}
