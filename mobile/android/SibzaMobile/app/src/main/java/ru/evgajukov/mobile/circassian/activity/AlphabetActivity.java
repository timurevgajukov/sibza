package ru.evgajukov.mobile.circassian.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import ru.evgajukov.mobile.circassian.task.AlphabetLoadTask;
import ru.evgajukov.mobile.circassian.task.NetTask;

/**
 * Created by timur on 28.10.15.
 *
 * Страница с алфавитом
 */
public class AlphabetActivity extends SlidingBaseActivity {

    private ProgressBar progressBar;
    private ListView listAlphabet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alphabet);

        setSlidingMenu(this, R.id.alphabet_header_menu);

        listAlphabet = (ListView) findViewById(R.id.alphabet_list);

        progressBar = (ProgressBar) findViewById(R.id.alphabet_progress);
    }

    @Override
    protected void onResume() {

        super.onResume();

        showAlphabet();
    }

    private void showAlphabet() {

        progressBar.setVisibility(View.VISIBLE);

        new NetTask(getApplicationContext(),
                new AlphabetLoadTask(this, progressBar, listAlphabet)).execute();
    }
}