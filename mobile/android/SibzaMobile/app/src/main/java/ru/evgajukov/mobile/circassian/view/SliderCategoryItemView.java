package ru.evgajukov.mobile.circassian.view;

import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.circassian.model.category.Category;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SliderCategoryItemView extends RelativeLayout {

    private TextView txtCategoryName;

    private Category category;

    public SliderCategoryItemView(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {

        super.onFinishInflate();

        txtCategoryName = (TextView) findViewById(R.id.slider_list_item_category_name);
    }

    public Category getCategory() {

        return category;
    }

    public void setCategory(Category category) {

        this.category = category;

        txtCategoryName.setText(category.toString());
    }
}
