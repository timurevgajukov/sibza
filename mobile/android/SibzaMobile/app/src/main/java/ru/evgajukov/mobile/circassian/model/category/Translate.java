package ru.evgajukov.mobile.circassian.model.category;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.language.Language;

/**
 * Created by timur on 25.10.15.
 *
 * Модель перевода для категории
 */
public class Translate extends Model {

    private Category category;
    private Language language;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}