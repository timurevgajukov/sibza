package ru.evgajukov.mobile.circassian.model.language;

import ru.evgajukov.mobile.circassian.model.Model;

/**
 * Created by timur on 25.10.15.
 *
 * Модель языка
 */
public class Language extends Model {

    private String code;
    private String smallName;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSmallName() {
        return smallName;
    }

    public void setSmallName(String smallName) {
        this.smallName = smallName;
    }
}