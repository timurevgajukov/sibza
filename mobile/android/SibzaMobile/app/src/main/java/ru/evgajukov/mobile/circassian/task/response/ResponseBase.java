package ru.evgajukov.mobile.circassian.task.response;

/**
 * Created by timur on 25.10.15.
 *
 * Базовый класс ответа от сервера
 */
public class ResponseBase {

    private int code;
    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}