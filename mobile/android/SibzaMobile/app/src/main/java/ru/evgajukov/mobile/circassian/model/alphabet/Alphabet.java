package ru.evgajukov.mobile.circassian.model.alphabet;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.language.Language;

/**
 * Created by timur on 28.10.15.
 *
 * Алфавит
 */
public class Alphabet extends Model {

    private List<Letter> letters;
    private Language language;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<Letter> getLetters() {
        return letters;
    }

    public void setLetters(List<Letter> letters) {
        this.letters = letters;
    }
}