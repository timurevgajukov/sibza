package ru.evgajukov.mobile.circassian.model.alphabet;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.resource.Resource;

/**
 * Created by timur on 28.10.15.
 *
 * Буква алфавита
 */
public class Letter extends Model {

    private String letter;
    private String lowLetter;
    private List<Resource> resources;

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getLowLetter() {
        return lowLetter;
    }

    public void setLowLetter(String lowLetter) {
        this.lowLetter = lowLetter;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    @Override
    public String toString() {

        return letter;
    }
}