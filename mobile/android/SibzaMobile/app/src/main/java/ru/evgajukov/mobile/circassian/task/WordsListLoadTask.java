package ru.evgajukov.mobile.circassian.task;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import java.util.List;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.circassian.adapter.WordsListAdapter;
import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.circassian.task.response.ResponseWords;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;

/**
 * Created by timur on 26.10.15.
 *
 * Загрузка списка слов
 */
public class WordsListLoadTask extends AsyncTask<Void, Void, List<Word>> {

    public static final String API_LIST = "word/list";

    private Context context;
    private Category category;
    private ProgressBar progressBar;
    private ListView wordsList;

    private boolean showWordsList;

    public WordsListLoadTask(Category category) {

        this.category = category;
        this.showWordsList = false;
    }

    public WordsListLoadTask(Context context, Category category, ProgressBar progressBar, ListView wordsList) {

        this.context = context;
        this.category = category;
        this.progressBar = progressBar;
        this.wordsList = wordsList;
        this.showWordsList = true;
    }

    @Override
    protected List<Word> doInBackground(Void... params) {

        try {
            Net net = Net.getInstance();
            String result = net.send(GlobalConst.API_URL
                    + API_LIST + "?token=" + GlobalConst.APP_TOKEN
                    + "&category=" + category.getId()
                    + "&lng=" + GlobalConst.language);

            if (result != null && result.length() != 0) {
                Gson gson = new Gson();
                ResponseWords response = gson.fromJson(result, ResponseWords.class);
                if (response.getCode() == 0 && response.getBody() != null) {
                    category.setWords(response.getBody());

                    return category.getWords();
                }
            }
        } catch (Exception e) {
            Log.e(e.toString(), e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Word> words) {

        if (!showWordsList) {
            return;
        }

        progressBar.setVisibility(View.INVISIBLE);

        if (words == null || words.size() == 0) {
            return;
        }

        wordsList.setAdapter(new WordsListAdapter(context, R.layout.list_item_page, words));
    }
}