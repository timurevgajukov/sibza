package ru.evgajukov.mobile.circassian.task;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by timur on 26.10.15.
 *
 * Общий класс для работы с сетью
 */
public class NetTask {

    private Context context;
    private AsyncTask<Void, Void, ?> task;

    public NetTask(Context context, AsyncTask<Void, Void, ?> task) {

        this.context = context;
        this.task = task;
    }

    public void execute() {

        task.execute();
    }
}