package ru.evgajukov.mobile.circassian.cache;

import ru.evgajukov.mobile.circassian.model.audio.Audio;
import ru.evgajukov.mobile.circassian.model.category.Category;
import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.utils.Log;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import java.util.List;

/**
 * Кэширование данных, полученных из сети на время работы приложения
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Cache {

    private static final String FLD_CACHE = "cache";

    private static Cache instance;

    private SharedPreferences prefs;

    private List<Category> categories;
    private List<Audio> audioList;
    private Word word;

    public static Cache getInstance(Context context) {

        if (instance == null) {
            instance = new Cache(context);
        }

        return instance;
    }

    private Cache(Context context) {

        super();

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public List<Category> getCategories() {

        return categories;
    }

    public Category getCategory(String name) {

        if (categories == null) {
            return null;
        }

        for (Category category : categories) {
            if (category.getName().equalsIgnoreCase(name)) {
                return category;
            }
        }

        return null;
    }

    public Category getCategory(int id) {

        if (categories == null) {
            return null;
        }

        for (Category category : categories) {
            if (category.getId() == id) {
                return category;
            }
        }

        return null;
    }

    public void setCategories(List<Category> categories) {

        this.categories = categories;
    }

    public void clear() {

        if (categories != null) {
            categories.clear();
            categories = null;
        }
    }

    public Word getWord() {

        return word;
    }

    public void setWord(Word word) {

        this.word = word;
    }

    public void clearPage() {

        word = null;
    }

    /**
     * загружает из внешнего хранилища данные в кеш
     */
    public void load() {

        Log.d("load");

        if (prefs.contains(FLD_CACHE) && categories == null) {
            String cacheXML = prefs.getString(FLD_CACHE, null);
            if (cacheXML != null) {

            }
        }
    }

    /**
     * сохраняет кеш во внешнее хранилище
     */
    public void save() {

        Log.d("save");

        if (categories == null) {
            return;
        }

        String cacheXML = null;
        if (cacheXML != null) {
            Editor editor = prefs.edit();
            editor.putString(FLD_CACHE, cacheXML);
            editor.commit();
        }
    }
}
