package ru.evgajukov.mobile.circassian.model.resource;

import ru.evgajukov.mobile.circassian.model.Model;

/**
 * Created by timur on 25.10.15.
 *
 * Модель медия ресурса
 */
public class Resource extends Model {

    private ContentType type;
    private String fileName;
    private byte[] resource;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getResource() {
        return resource;
    }

    public void setResource(byte[] resource) {
        this.resource = resource;
    }

    public ContentType getType() {
        return type;
    }

    public void setType(ContentType type) {
        this.type = type;
    }
}