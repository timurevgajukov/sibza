package ru.evgajukov.mobile.circassian.adapter;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.word.Word;
import ru.evgajukov.mobile.circassian.view.WordItemView;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class WordsListAdapter extends ArrayAdapter<Word> {

    private List<Word> items;
    private int viewResourceId;

    public WordsListAdapter(Context context, int textViewResourceId, List<Word> items) {

        super(context, textViewResourceId, items);

        this.items = items;
        this.viewResourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WordItemView itemView = null;
        if (convertView != null) {
            itemView = (WordItemView) convertView;
        } else {
            itemView = (WordItemView) View.inflate(getContext(), viewResourceId, null);
        }
        try {
            itemView.setWord(items.get(position));
        } catch (Exception e) {
        }

        return itemView;
    }
}
