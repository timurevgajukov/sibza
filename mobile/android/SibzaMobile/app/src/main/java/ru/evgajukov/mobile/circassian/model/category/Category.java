package ru.evgajukov.mobile.circassian.model.category;

import java.util.List;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.model.Model;
import ru.evgajukov.mobile.circassian.model.word.Word;

/**
 * Created by timur on 25.10.15.
 *
 * Модель категории
 */
public class Category extends Model {

    private Category parent;
    private List<Translate> translates;
    private List<Word> words;

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public List<Translate> getTranslates() {
        return translates;
    }

    public Translate getTranslate(String language) {

        if (translates == null || translates.size() == 0) {
            return null;
        }

        for (Translate translate : translates) {
            if (language.equalsIgnoreCase(translate.getLanguage().getCode())) {
                return translate;
            }
        }

        return null;
    }

    public void setTranslates(List<Translate> translates) {
        this.translates = translates;
    }

    public List<Word> getWords() {
        return words;
    }

    public Word getWord(int id) {

        if (words == null || words.size() == 0) {
            return null;
        }

        for (Word word : words) {
            if (word.getId() == id) {
                return word;
            }
        }

        return null;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    @Override
    public String toString() {

        Translate translate = getTranslate(GlobalConst.language);
        if (translate != null) {
            return translate.getName();
        } else {
            return name;
        }
    }
}