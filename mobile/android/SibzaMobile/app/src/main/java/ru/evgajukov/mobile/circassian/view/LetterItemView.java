package ru.evgajukov.mobile.circassian.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.circassian.model.alphabet.Letter;

/**
 * Created by timur on 28.10.15.
 */
public class LetterItemView extends RelativeLayout {

    private TextView txtLetter;

    private Letter letter;


    public LetterItemView(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {

        super.onFinishInflate();

        txtLetter = (TextView) findViewById(R.id.list_item_letter);
    }

    public Letter getLetter() {

        return letter;
    }

    public void setLetter(Letter letter) {

        this.letter = letter;

        txtLetter.setText(letter.getLetter());
    }
}