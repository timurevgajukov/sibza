package ru.evgajukov.mobile.circassian.task.response;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.category.Category;

/**
 * Created by timur on 25.10.15.
 *
 * Класс ответа от сервера по получению списка категорий
 */
public class ResponseCategories extends ResponseBase {

    private List<Category> body;

    public List<Category> getBody() {
        return body;
    }

    public void setBody(List<Category> body) {
        this.body = body;
    }
}
