package ru.evgajukov.backend.circassian.wiki.request;

import ru.evgajukov.backend.circassian.GlobalConsts;

/**
 * Получение информации по странице
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class PageInfoWikiRequest extends WikiRequest {

    public PageInfoWikiRequest(String url) {

        super(url);

        action("query").format("xml").addParam("prop", "revisions")
                .addParam("rvprop", "content").addParam("redirects", "1");
    }

    public PageInfoWikiRequest titles(String value) {

        addParam("titles", value);

        return this;
    }

    public static void main(String[] args) {

        try {
            PageInfoWikiRequest req = new PageInfoWikiRequest(GlobalConsts.API_URL);
            req.titles("Здравствуй(те)!");

            String result = req.get();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
