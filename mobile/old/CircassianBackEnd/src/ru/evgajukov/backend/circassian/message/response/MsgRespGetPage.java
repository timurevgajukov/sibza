package ru.evgajukov.backend.circassian.message.response;

import ru.alfabank.server.backend.message.MessageWrapper;
import ru.alfabank.server.xml.message.BEMessage;
import ru.alfabank.server.xml.message.BodyElement;
import ru.evgajukov.backend.circassian.wiki.model.Page;

/**
 * Класс-обертка вокруг сообщения Response для method=GetPage
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgRespGetPage extends MessageWrapper {

    public MsgRespGetPage(BEMessage msg) {

        super(msg);
    }

    public void render(Page page) {

        BodyElement pageElement = new BodyElement("page");

        pageElement.addChild(new BodyElement("title", page.getTitle()));
        pageElement.addChild(new BodyElement("text", "<![CDATA[" + page.getText() + "]]>"));
        pageElement.addChild(new BodyElement("audio", page.getAudioUrl()));

        if (page.getCategories() != null) {
            BodyElement categories = new BodyElement("categories");
            for (String category : page.getCategories()) {
                categories.addChild(new BodyElement("category", category));
            }
            pageElement.addChild(categories);
        }

        getBody().addChild(pageElement);
    }
}
