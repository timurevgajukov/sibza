package ru.evgajukov.backend.circassian.message.response;

import java.util.List;

import ru.alfabank.server.backend.message.MessageWrapper;
import ru.alfabank.server.xml.message.BEMessage;
import ru.alfabank.server.xml.message.BodyElement;
import ru.evgajukov.backend.circassian.wiki.model.Page;

/**
 * Класс-обертка вокруг сообщения Response для method=GetPagesList
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgRespGetPagesList extends MessageWrapper {

    public MsgRespGetPagesList(BEMessage msg) {

        super(msg);
    }

    public void render(List<Page> list) {

        BodyElement pages = new BodyElement("pages");

        if (list != null) {
            for (Page page : list) {
                BodyElement pageElement = new BodyElement("page");

                pageElement.addChild(new BodyElement("id", String.valueOf(page.getId())));
                pageElement.addChild(new BodyElement("title", page.getTitle()));

                pages.addChild(pageElement);
            }
        }

        getBody().addChild(pages);
    }
}
