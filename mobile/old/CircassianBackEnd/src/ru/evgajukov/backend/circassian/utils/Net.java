package ru.evgajukov.backend.circassian.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

/**
 * Класс для работы с сетью
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Net {

    private static final int TIMEOUT = 5000;

    /**
     * Запрос по методу GET
     * 
     * @param request
     *            ссылка для запроса
     * @return
     */
    public static String get(String request) {

        try {
            Proxy proxy = Proxy.NO_PROXY;
            URL url = new URL(request);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
            conn.setConnectTimeout(TIMEOUT);

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                String result = builder.toString();
                if (result != null && result.length() > 0) {
                    return result;
                }
            }
        } catch (Exception e) {

        }

        return null;
    }
}