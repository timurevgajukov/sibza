package ru.evgajukov.backend.circassian.wiki.model.factory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ru.alfabank.server.backend.errors.BackEndException;
import ru.evgajukov.backend.circassian.GlobalConsts;
import ru.evgajukov.backend.circassian.utils.Utils;
import ru.evgajukov.backend.circassian.wiki.model.Category;
import ru.evgajukov.backend.circassian.wiki.request.AllCategoriesWikiRequest;
import ru.evgajukov.backend.circassian.wiki.request.CategorymembersWikiRequest;

/**
 * Фабрика для создания объектов категорий
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Categories {

    private static Categories instance;

    private String urlApi;

    public static Categories getInstance(String urlApi) {

        if (instance == null) {
            instance = new Categories(urlApi);
        }

        return instance;
    }

    private Categories(String urlApi) {

        this.urlApi = urlApi;
    }

    /**
     * Возвращает все категории
     * 
     * @return
     */
    public List<Category> list() {

        AllCategoriesWikiRequest req = new AllCategoriesWikiRequest(urlApi);
        req.aclimit("500");
        req.acprop("size");
        try {
            String result = req.get();
            List<Category> list = parse(result);

            return list;
        } catch (BackEndException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Возвращает подкатегории, входящие в языковую техническую категорию
     * 
     * @param language
     * @return
     */
    public List<Category> list(String language) {

        CategorymembersWikiRequest req = new CategorymembersWikiRequest(urlApi);
        req.cmtitle(language.toUpperCase());
        req.cmlimit("500");
        try {
            String result = req.get();
            List<Category> list = parseCategorymembers(result, "cm");

            // теперь нужно добавить количество статей в категориях
            List<Category> allCategories = list();
            for (Category category : list) {
                category.setPagesCount(allCategories);
            }

            return list;
        } catch (BackEndException e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<Category> parse(String xml) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            Document doc = dBuilder.parse(is);

            NodeList nodeList = doc.getElementsByTagName("c");
            if (nodeList.getLength() > 0) {
                List<Category> list = new ArrayList<Category>();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);

                    int pagesCount = Integer.parseInt(Utils.getAttribute(node, "pages"));
                    String name = node.getFirstChild().getNodeValue();

                    if (pagesCount > 0) {
                        // добавлять в список только категории, в которых
                        // содержаться статьи
                        list.add(new Category(name, pagesCount));
                    }
                }

                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<Category> parseCategorymembers(String xml, String elementName) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            Document doc = dBuilder.parse(is);

            NodeList nodeList = doc.getElementsByTagName(elementName);
            if (nodeList.getLength() > 0) {
                List<Category> list = new ArrayList<Category>();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);

                    String title = Utils.getAttribute(node, "title").replaceAll("Категория:", "");

                    list.add(new Category(title, 0));
                }

                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] arg) {

        try {
            Categories categories = new Categories(GlobalConsts.API_URL);
            List<Category> list = categories.list("tr");
            for (Category category : list) {
                System.out.println(category + "[" + category.getPagesCount() + "]");
            }
            System.out.println(list.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
