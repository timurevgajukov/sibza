package ru.evgajukov.backend.circassian.message.request;

import ru.alfabank.server.backend.message.MessageWrapper;
import ru.alfabank.server.xml.message.BEMessage;

/**
 * Базовый класс для все классов-оберток объектов Request
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class BaseMsgRequest extends MessageWrapper {

    private static final String H_FLD_LANGUAGE = "language";
    private static final String DEFAULT_LANGUAGE = "en";

    public BaseMsgRequest(BEMessage msg) {

        super(msg);
    }

    public String getLanguage() throws Exception {

        try {
            return getHeader().getElement(H_FLD_LANGUAGE);
        } catch (Exception e) {
            throw new Exception(String.format("BaseMsgRequest: failed to get '%s' value.", DEFAULT_LANGUAGE));
        }
    }
}
