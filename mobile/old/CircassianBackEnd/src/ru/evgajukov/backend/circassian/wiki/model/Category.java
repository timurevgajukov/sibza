package ru.evgajukov.backend.circassian.wiki.model;

import java.util.List;

/**
 * Класс для описания категории статей
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Category {

    private String name;
    private int pagesCount;

    {
        pagesCount = 0;
    }

    public Category(String name) {

        this.name = name;
    }

    public Category(String name, int pagesCount) {

        this(name);
        this.pagesCount = pagesCount;
    }

    public int getPagesCount() {

        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {

        this.pagesCount = pagesCount;
    }

    /**
     * Ищет первую соответствующую категорию в предложенном списке и берет
     * оттуда pagesCount
     * 
     * @param list
     */
    public void setPagesCount(List<Category> list) {

        for (Category category : list) {
            if (name.equalsIgnoreCase(category.name)) {
                pagesCount = category.pagesCount;
                return;
            }
        }
    }

    @Override
    public String toString() {

        return name;
    }
}
