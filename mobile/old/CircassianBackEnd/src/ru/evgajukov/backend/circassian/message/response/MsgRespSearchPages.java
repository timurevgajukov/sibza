package ru.evgajukov.backend.circassian.message.response;

import ru.alfabank.server.xml.message.BEMessage;

/**
 * Класс-обертка вокруг сообщения Response для method=SearchPages
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgRespSearchPages extends MsgRespGetPagesList {

    public MsgRespSearchPages(BEMessage msg) {

        super(msg);
    }
}
