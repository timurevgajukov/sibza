package ru.evgajukov.backend.circassian.message.request;

import ru.alfabank.server.xml.message.BEMessage;

/**
 * Класс-обертка вокруг сообщения Request для method=GetCategoryList
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgReqGetCategoryList extends BaseMsgRequest {

    public static final String COMMAND = "GetCategoryList";

    public MsgReqGetCategoryList(BEMessage msg) throws Exception {

        super(msg);

        if (!getHeader().getMethod().equalsIgnoreCase(COMMAND)) {
            throw new Exception(String.format("Wrong message type. Expecting %s.", COMMAND));
        }
    }
}
