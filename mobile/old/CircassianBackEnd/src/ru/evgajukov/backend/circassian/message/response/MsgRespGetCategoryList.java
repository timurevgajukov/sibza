package ru.evgajukov.backend.circassian.message.response;

import java.util.List;

import ru.alfabank.server.backend.message.MessageWrapper;
import ru.alfabank.server.xml.message.BEMessage;
import ru.alfabank.server.xml.message.BodyElement;
import ru.evgajukov.backend.circassian.wiki.model.Category;

/**
 * Класс-обертка вокруг сообщения Response для method=GetCategoryList
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgRespGetCategoryList extends MessageWrapper {

    public MsgRespGetCategoryList(BEMessage msg) {

        super(msg);
    }

    public void render(List<Category> list) {

        BodyElement categories = new BodyElement("categories");

        if (list != null) {
            for (Category category : list) {
                BodyElement element = new BodyElement("category");
                element.addChild(new BodyElement("name", category.toString()));
                element.addChild(new BodyElement("pages_count", String.valueOf(category.getPagesCount())));

                categories.addChild(element);
            }
        }

        getBody().addChild(categories);
    }
}
