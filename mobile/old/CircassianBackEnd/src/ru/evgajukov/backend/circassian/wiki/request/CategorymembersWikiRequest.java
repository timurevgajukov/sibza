package ru.evgajukov.backend.circassian.wiki.request;

import ru.evgajukov.backend.circassian.GlobalConsts;

/**
 * Получение списка страниц для конкретной категории
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class CategorymembersWikiRequest extends WikiRequest {

    public CategorymembersWikiRequest(String url) {

        super(url);

        action("query").format("xml").list("categorymembers");
    }

    public CategorymembersWikiRequest cmtitle(String value) {

        addParam("cmtitle", "Категория:" + value);

        return this;
    }

    public CategorymembersWikiRequest cmlimit(String value) {

        addParam("cmlimit", value);

        return this;
    }

    public static void main(String[] args) {

        try {
            CategorymembersWikiRequest req = new CategorymembersWikiRequest(GlobalConsts.API_URL);
            req.cmtitle("Приветствие");

            String result = req.get();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
