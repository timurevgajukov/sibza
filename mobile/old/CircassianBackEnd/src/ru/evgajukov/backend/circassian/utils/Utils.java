package ru.evgajukov.backend.circassian.utils;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Полузные методы
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Utils {

    /**
     * Возвращает атрибут xml-узла по его имени
     * 
     * @param node
     * @param name
     * @return
     */
    public static String getAttribute(Node node, String name) {

        NamedNodeMap attr = node.getAttributes();
        for (int i = 0; i < attr.getLength(); i++) {
            if (attr.item(i).getNodeName().equalsIgnoreCase(name)) {
                return attr.item(i).getNodeValue();
            }
        }

        return null;
    }
}
