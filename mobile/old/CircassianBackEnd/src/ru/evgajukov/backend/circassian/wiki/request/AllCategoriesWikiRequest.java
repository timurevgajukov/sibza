package ru.evgajukov.backend.circassian.wiki.request;

import ru.evgajukov.backend.circassian.GlobalConsts;

/**
 * Запрос на получение списка всех категорий
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class AllCategoriesWikiRequest extends WikiRequest {

    public AllCategoriesWikiRequest(String url) {

        super(url);

        action("query").format("xml").list("allcategories");
    }

    public AllCategoriesWikiRequest acprefix(String value) {

        addParam("acprefix", value);

        return this;
    }

    public AllCategoriesWikiRequest aclimit(String value) {

        addParam("aclimit", value);

        return this;
    }

    public AllCategoriesWikiRequest acprop(String value) {

        addParam("acprop", value);

        return this;
    }

    public static void main(String[] args) {

        try {
            AllCategoriesWikiRequest req = new AllCategoriesWikiRequest(GlobalConsts.API_URL);

            String result = req.get();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
