package ru.evgajukov.backend.circassian.wiki.request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import ru.alfabank.server.backend.errors.BackEndException;
import ru.evgajukov.backend.circassian.utils.Net;

/**
 * Общий класс для запросов к MediaWiki
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class WikiRequest {

    private String urlApi; // ссылка на api
    private Map<String, String> params; // параметры запроса

    public WikiRequest(String url) {

        urlApi = url;
    }

    /**
     * Добавить новый параметр к запросу
     * 
     * @param key
     * @param value
     * @return
     */
    public WikiRequest addParam(String key, String value) {

        if (params == null) {
            params = new HashMap<String, String>();
        }

        params.put(key, value);

        return this;
    }

    public WikiRequest action(String value) {

        addParam("action", value);

        return this;
    }

    public WikiRequest format(String value) {

        addParam("format", value);

        return this;
    }

    public WikiRequest list(String value) {

        addParam("list", value);

        return this;
    }

    /**
     * Отправляет запрос на сервер и вернуть строку
     * 
     * @return
     * @throws BackEndException
     */
    public String get() throws BackEndException {

        System.out.println("WikiRequest.get()");

        String url = render();
        System.out.println(url);
        String result = Net.get(url);

        System.out.println(result);

        return result;
    }

    /**
     * По списку параметров и ссылке на api возвращает полностью сформированную
     * ссылку
     * 
     * @return
     * @throws BackEndException
     */
    private String render() throws BackEndException {

        if (params == null) {
            throw new BackEndException(-1, "Отсутствуют параметры запроса");
        }

        StringBuilder renderParams = new StringBuilder(urlApi);
        renderParams.append("?");
        for (String key : params.keySet()) {
            // необходимо преобразовать, чтобы убрать неподдерживаемые символы
            try {
                renderParams.append(key).append("=").append(URLEncoder.encode(params.get(key), "UTF-8")).append("&");
            } catch (UnsupportedEncodingException e) {
                throw new BackEndException(-2, "Не удалось сформировать ссылку с параметрами");
            }
        }
        // удаляем последний символ "&"
        renderParams.deleteCharAt(renderParams.length() - 1);

        return renderParams.toString();
    }
}
