package ru.evgajukov.backend.circassian;

import ru.alfabank.server.backend.DockServlet;
import ru.alfabank.server.backend.common.DockLogic;

public class CircassianDock extends DockServlet {

    private static final long serialVersionUID = -6276540603443959967L;

    @Override
    public DockLogic getDockLogic() {

        return new CircassianDockLogic(this);
    }
}
