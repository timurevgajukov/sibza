package ru.evgajukov.backend.circassian.wiki.request;

import ru.evgajukov.backend.circassian.GlobalConsts;

/**
 * Запрос на получение списка загруженных в wiki файлов
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class AllImagesWikiRequest extends WikiRequest {

    public AllImagesWikiRequest(String url) {

        super(url);

        action("query").format("xml").list("allimages");
    }

    public AllImagesWikiRequest ailimit(String value) {

        addParam("ailimit", value);

        return this;
    }

    public AllImagesWikiRequest aiprefix(String value) {

        addParam("aiprefix", value);

        return this;
    }

    public static void main(String[] args) {

        try {
            AllImagesWikiRequest req = new AllImagesWikiRequest(GlobalConsts.API_URL);
            req.ailimit("500");
            req.aiprefix("21.mp3");

            String result = req.get();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
