package ru.evgajukov.backend.circassian.wiki.request;

import ru.evgajukov.backend.circassian.GlobalConsts;

/**
 * Получение списка страниц в поиске
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class AllPagesWikiRequest extends WikiRequest {

    public AllPagesWikiRequest(String url) {

        super(url);

        action("query").format("xml").list("allpages");
    }

    public AllPagesWikiRequest aplimit(String value) {

        addParam("aplimit", value);

        return this;
    }

    public AllPagesWikiRequest apprefix(String value) {

        addParam("apprefix", value);

        return this;
    }

    public static void main(String[] args) {

        try {
            AllPagesWikiRequest req = new AllPagesWikiRequest(GlobalConsts.API_URL);
            req.aplimit("500");
            req.apprefix("Как");

            String result = req.get();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
