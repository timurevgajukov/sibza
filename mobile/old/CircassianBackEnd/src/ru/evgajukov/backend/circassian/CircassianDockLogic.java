package ru.evgajukov.backend.circassian;

import java.lang.Exception;import java.lang.Override;import java.lang.String;import java.util.List;

import ru.alfabank.server.backend.DockServlet;
import ru.alfabank.server.backend.common.DockLogic;
import ru.alfabank.server.backend.errors.BackEndException;
import ru.alfabank.server.xml.message.Request;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.backend.circassian.message.request.MsgReqGetCategoryList;
import ru.evgajukov.backend.circassian.message.request.MsgReqGetPage;
import ru.evgajukov.backend.circassian.message.request.MsgReqGetPagesList;
import ru.evgajukov.backend.circassian.message.request.MsgReqSearchPages;
import ru.evgajukov.backend.circassian.message.response.MsgRespGetCategoryList;
import ru.evgajukov.backend.circassian.message.response.MsgRespGetPage;
import ru.evgajukov.backend.circassian.message.response.MsgRespGetPagesList;
import ru.evgajukov.backend.circassian.message.response.MsgRespSearchPages;
import ru.evgajukov.backend.circassian.wiki.model.Category;
import ru.evgajukov.backend.circassian.wiki.model.Page;
import ru.evgajukov.backend.circassian.wiki.model.factory.Categories;
import ru.evgajukov.backend.circassian.wiki.model.factory.Pages;

public class CircassianDockLogic extends DockLogic {

    public CircassianDockLogic(DockServlet servlet) {

        super(servlet);
    }

    @Override
    public String getInstanceName() {

        return "Circassian BackEnd business logic ver 1.0.3";
    }

    @Override
    public String getSystem() {

        return "Circassian";
    }

    @Override
    public void onRequest(Request req, Response resp) throws BackEndException {

        log.info("onRequest");

        String method = req.getHeader().getMethod();
        if (method.equalsIgnoreCase(MsgReqGetCategoryList.COMMAND)) {
            // получить список категорий
            onGetCategoryList(req, resp);
        } else if (method.equalsIgnoreCase(MsgReqGetPagesList.COMMAND)) {
            // получить список страниц в категории
            onGetPagesList(req, resp);
        } else if (method.equalsIgnoreCase(MsgReqGetPage.COMMAND)) {
            // получить полную информацию по странице
            onGetPage(req, resp);
        } else if (method.equalsIgnoreCase(MsgReqSearchPages.COMMAND)) {
            // поиск статей по фильтру
            onSearchPages(req, resp);
        } else {
            BackEndException beEx = new BackEndException(-1, "");
            beEx.setDescription("Неподдерживаемая команда");
            throw beEx;
        }
    }

    @Override
    public void onResponse(Response resp) throws BackEndException {
    }

    /**
     * получить список категорий
     * 
     * @param req
     * @param resp
     * @throws BackEndException
     */
    private void onGetCategoryList(Request req, Response resp) throws BackEndException {

        log.info("onGetCategoryList");

        MsgReqGetCategoryList msg;
        try {
            msg = new MsgReqGetCategoryList(req);
        } catch (Exception e) {
            BackEndException beEx = new BackEndException(-1, "");
            beEx.setDescription("Неподдерживаемый формат команды");
            throw beEx;
        }

        String language = null;
        try {
            language = msg.getLanguage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Categories categories = Categories.getInstance(GlobalConsts.API_URL);
        List<Category> list;
        if (language != null && language.length() > 0) {
            list = categories.list(language);
        } else {
            list = categories.list();
        }

        MsgRespGetCategoryList respGetCategoryList = new MsgRespGetCategoryList(resp);
        respGetCategoryList.getHeader().setResult("0", "OK");
        respGetCategoryList.render(list);
    }

    /**
     * получить список страниц в категории
     * 
     * @param req
     * @param resp
     * @throws BackEndException
     */
    private void onGetPagesList(Request req, Response resp) throws BackEndException {

        log.info("onGetPagesList");

        String categoryName = null;
        try {
            MsgReqGetPagesList msg = new MsgReqGetPagesList(req);
            categoryName = msg.getCategory();
        } catch (Exception e) {
            BackEndException beEx = new BackEndException(-1, "");
            beEx.setDescription("Неподдерживаемый формат команды");
            throw beEx;
        }

        Pages pages = Pages.getInstance(GlobalConsts.API_URL);
        List<Page> list = pages.list(new Category(categoryName));

        MsgRespGetPagesList respGetPagesList = new MsgRespGetPagesList(resp);
        respGetPagesList.getHeader().setResult("0", "OK");
        respGetPagesList.render(list);
    }

    /**
     * получить полную информацию по странице
     * 
     * @param req
     * @param resp
     * @throws BackEndException
     */
    private void onGetPage(Request req, Response resp) throws BackEndException {

        log.info("onGetPage");

        String title = null;
        try {
            MsgReqGetPage msg = new MsgReqGetPage(req);
            title = msg.getTitle();
        } catch (Exception e) {
            BackEndException beEx = new BackEndException(-1, "");
            beEx.setDescription("Неподдерживаемый формат команды");
            throw beEx;
        }

        Page page = new Page(0, title);
        Pages pages = Pages.getInstance(GlobalConsts.API_URL);
        page = pages.info(page);

        MsgRespGetPage respGetPage = new MsgRespGetPage(resp);
        respGetPage.getHeader().setResult("0", "OK");
        respGetPage.render(page);
    }

    /**
     * поиск статей по фильтру
     * 
     * @param req
     * @param resp
     * @throws BackEndException
     */
    private void onSearchPages(Request req, Response resp) throws BackEndException {

        log.info("onSearchPages");

        String search = null;
        try {
            MsgReqSearchPages msg = new MsgReqSearchPages(req);
            search = msg.getSearch();
        } catch (Exception e) {
            BackEndException beEx = new BackEndException(-1, "");
            beEx.setDescription("Неподдерживаемый формат команды");
            throw beEx;
        }

        Pages pages = Pages.getInstance(GlobalConsts.API_URL);
        List<Page> list = pages.search(search);

        MsgRespSearchPages respSearchPages = new MsgRespSearchPages(resp);
        respSearchPages.getHeader().setResult("0", "OK");
        respSearchPages.render(list);
    }
}
