package ru.evgajukov.backend.circassian.message.request;

import ru.alfabank.server.xml.message.BEMessage;

/**
 * Класс-обертка вокруг сообщения Response для method=SearchPages
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgReqSearchPages extends BaseMsgRequest {

    public static final String COMMAND = "SearchPages";

    public static final String FLD_SEARCH = "search";

    public MsgReqSearchPages(BEMessage msg) throws Exception {

        super(msg);

        if (!getHeader().getMethod().equalsIgnoreCase(COMMAND)) {
            throw new Exception(String.format("Wrong message type. Expecting %s.", COMMAND));
        }
    }

    public String getSearch() throws Exception {

        try {
            return getBody().getChildValue(FLD_SEARCH);
        } catch (Exception e) {
            throw new Exception(String.format("MsgReqSearchPages: failed to get '%s' value.", FLD_SEARCH));
        }
    }
}
