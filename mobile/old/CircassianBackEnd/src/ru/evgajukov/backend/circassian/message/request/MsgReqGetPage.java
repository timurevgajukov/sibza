package ru.evgajukov.backend.circassian.message.request;

import ru.alfabank.server.xml.message.BEMessage;

/**
 * Класс-обертка вокруг сообщения Response для method=GetPage
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgReqGetPage extends BaseMsgRequest {

    public static final String COMMAND = "GetPage";

    public static final String FLD_TITLE = "title";

    public MsgReqGetPage(BEMessage msg) throws Exception {

        super(msg);

        if (!getHeader().getMethod().equalsIgnoreCase(COMMAND)) {
            throw new Exception(String.format("Wrong message type. Expecting %s.", COMMAND));
        }
    }

    public String getTitle() throws Exception {

        try {
            return getBody().getChildValue(FLD_TITLE);
        } catch (Exception e) {
            throw new Exception(String.format("MsgReqGetPage: failed to get '%s' value.", FLD_TITLE));
        }
    }
}
