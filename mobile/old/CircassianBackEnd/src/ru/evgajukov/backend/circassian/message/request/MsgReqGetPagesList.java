package ru.evgajukov.backend.circassian.message.request;

import ru.alfabank.server.xml.message.BEMessage;

/**
 * Класс-обертка вокруг сообщения Response для method=GetPagesList
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MsgReqGetPagesList extends BaseMsgRequest {

    public static final String COMMAND = "GetPagesList";

    public static final String FLD_CATEGORY = "category";

    public MsgReqGetPagesList(BEMessage msg) throws Exception {

        super(msg);

        if (!getHeader().getMethod().equalsIgnoreCase(COMMAND)) {
            throw new Exception(String.format("Wrong message type. Expecting %s.", COMMAND));
        }
    }

    public String getCategory() throws Exception {

        try {
            return getBody().getChildValue(FLD_CATEGORY);
        } catch (Exception e) {
            throw new Exception(String.format("MsgReqGetPagesList: failed to get '%s' value.", FLD_CATEGORY));
        }
    }
}
