package ru.evgajukov.backend.circassian.test;

import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.backend.circassian.test.message.MblReqGetCategoryList;
import ru.evgajukov.backend.circassian.test.utils.Net;

/**
 * Created by Тимур on 18.10.2015.
 */
public class Test {

    public static void main(String[] args) {

        MblReqGetCategoryList req = new MblReqGetCategoryList("RU");

        Response resp = null;
        try {
            Net net = Net.getInstance();
            resp = net.send(req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
