package ru.evgajukov.backend.circassian.wiki.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для описания статьи
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Page {

    private int id;
    private String title;
    private String text;
    private String audioUrl;
    private List<String> categories;

    public Page() {

        this(0, null);
    }

    public Page(int id, String title) {

        this.id = id;
        this.title = title;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }

    public String getAudioUrl() {

        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {

        this.audioUrl = audioUrl;
    }

    public List<String> getCategories() {

        return categories;
    }

    public void addCategory(String category) {

        if (categories == null) {
            categories = new ArrayList<String>();
        }

        categories.add(category);
    }

    @Override
    public String toString() {

        return title;
    }
}
