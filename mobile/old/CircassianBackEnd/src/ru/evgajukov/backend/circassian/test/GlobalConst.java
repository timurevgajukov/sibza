package ru.evgajukov.backend.circassian.test;

import java.util.HashMap;

public class GlobalConst {

    public static final String SYSTEM = "Circassian";
    public static final String APP_NAME = "Circassian";
    public static final String APP_OS = "Android";
    public static String APP_VER = "0.0.0";

    public static final String BE_URL = "http://api.sibza.org";
    public static final int CONN_TIMEOUT = 30000;

    public static final String TEMP_FOLDER = "tmp";
    public static final String AUDIO_FOLDER = "audio";

    public static String language;
    public static String oldLanguage;
    public static HashMap<String, String> LANGUAGE_LOCALE;

    public static String getLanguageLocale(String languageCode) {

        if (LANGUAGE_LOCALE == null) {
            LANGUAGE_LOCALE = new HashMap<String, String>();
            LANGUAGE_LOCALE.put("en", "en");
            LANGUAGE_LOCALE.put("ru", "ru");
            LANGUAGE_LOCALE.put("tr", "tr");
        }

        return LANGUAGE_LOCALE.get(languageCode);
    }
}