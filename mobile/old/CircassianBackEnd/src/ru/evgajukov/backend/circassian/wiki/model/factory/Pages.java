package ru.evgajukov.backend.circassian.wiki.model.factory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ru.alfabank.server.backend.errors.BackEndException;
import ru.evgajukov.backend.circassian.GlobalConsts;
import ru.evgajukov.backend.circassian.utils.Utils;
import ru.evgajukov.backend.circassian.wiki.model.Category;
import ru.evgajukov.backend.circassian.wiki.model.Page;
import ru.evgajukov.backend.circassian.wiki.request.AllImagesWikiRequest;
import ru.evgajukov.backend.circassian.wiki.request.AllPagesWikiRequest;
import ru.evgajukov.backend.circassian.wiki.request.CategorymembersWikiRequest;
import ru.evgajukov.backend.circassian.wiki.request.PageInfoWikiRequest;

/**
 * Фабрика для создания объектов статей
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Pages {

    private static Pages instance;

    private String urlApi;

    public static Pages getInstance(String urlApi) {

        if (instance == null) {
            instance = new Pages(urlApi);
        }

        return instance;
    }

    private Pages(String urlApi) {

        this.urlApi = urlApi;
    }

    public List<Page> list(Category category) {

        CategorymembersWikiRequest req = new CategorymembersWikiRequest(urlApi);
        req.cmtitle(category.toString());
        req.cmlimit("500");
        try {
            String result = req.get();

            List<Page> list = parseList(result, "cm");

            return list;
        } catch (BackEndException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<Page> search(String search) {

        AllPagesWikiRequest req = new AllPagesWikiRequest(urlApi);
        req.apprefix(search);
        req.aplimit("500");
        try {
            String result = req.get();

            List<Page> list = parseList(result, "p");

            return list;
        } catch (BackEndException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Page info(Page page) {

        PageInfoWikiRequest req = new PageInfoWikiRequest(urlApi);
        req.titles(page.getTitle());
        try {
            String result = req.get();

            return parseInfo(result);
        } catch (BackEndException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String audioUrl(String fileName) {

        AllImagesWikiRequest req = new AllImagesWikiRequest(urlApi);
        req.aiprefix(fileName);
        try {
            String result = req.get();

            return parseAudio(result);
        } catch (BackEndException e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<Page> parseList(String xml, String elementName) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            Document doc = dBuilder.parse(is);

            NodeList nodeList = doc.getElementsByTagName(elementName);
            if (nodeList.getLength() > 0) {
                List<Page> list = new ArrayList<Page>();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node node = nodeList.item(i);

                    int id = Integer.parseInt(Utils.getAttribute(node, "pageid"));
                    String title = Utils.getAttribute(node, "title");

                    list.add(new Page(id, title));
                }

                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Page parseInfo(String xml) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            Document doc = dBuilder.parse(is);

            NodeList nodeList = doc.getElementsByTagName("rev");
            if (nodeList.getLength() > 0) {
                Page page = new Page();
                Node node = nodeList.item(0);

                String text = node.getFirstChild().getNodeValue();

                // если имеется ссылка на аудио-файл, то выделим его и получим
                // ссылку
                final String beginTag = "<html5media>";
                final String endTag = "</html5media>";
                int indBegin = text.indexOf(beginTag);
                if (indBegin != -1) {
                    int indEnd = text.indexOf(endTag);
                    String audio = text.substring(indBegin + beginTag.length(), indEnd);
                    String[] audioFileNameArray = audio.split(":");
                    String audioFileName = audioFileNameArray[0];
                    if (audioFileNameArray.length > 1) {
                        audioFileName = audioFileNameArray[1];
                    }

                    // по имени аудио-файла получим на него ссылку
                    page.setAudioUrl(audioUrl(audioFileName));

                    // удаляем из текста информацию по аудио-файлу
                    text = text.replaceAll(beginTag, "").replaceAll(audio, "").replaceAll(endTag, "");
                }

                // распарсить список категорий для статьи
                try {
                    Pattern p = Pattern.compile("\\[\\[Категория:(.+?)\\]\\]");
                    Matcher m = p.matcher(text);
                    while (m.find()) {
                        page.addCategory(m.group(1));
                    }
                    // после того как выявили все категории, можно убрать их из
                    // основного текста
                    text = m.replaceAll("");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // распарсим свойства языка
                try {
                    Pattern pLanguage = Pattern.compile("\\{\\{(.+?)\\}\\}");
                    Matcher mLanguage = pLanguage.matcher(text);
                    while (mLanguage.find()) {
                        // TODO: что-то сделать со свойствами
                    }
                    // удаляем свойства из текста
                    text = mLanguage.replaceAll("");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                // убираем все лишние пустые строки
                // TODO: со временем нужно сделать, чтобы удалялись пока они
                // имеются
                for (int i = 0; i < 3; i++) {
                    text = text.replaceAll("\n\n", "\n");
                }
                text = text.trim();

                // сохранаем обработанный текст
                page.setText(text);

                return page;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String parseAudio(String xml) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            Document doc = dBuilder.parse(is);

            NodeList nodeList = doc.getElementsByTagName("img");
            if (nodeList.getLength() > 0) {
                Node node = nodeList.item(0);
                String audioUrl = Utils.getAttribute(node, "url");

                return audioUrl;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {

        try {
            Pages pages = Pages.getInstance(GlobalConsts.API_URL);
            Page page = pages.info(new Page(0, "Здравствуй(те)!"));
            System.out.println(page.getText());
            System.out.println(page.getCategories().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
