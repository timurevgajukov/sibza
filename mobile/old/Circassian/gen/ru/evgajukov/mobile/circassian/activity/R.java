/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package ru.evgajukov.mobile.circassian.activity;

public final class R {
    public static final class array {
        public static final int languages_title=0x7f070000;
        public static final int languages_value=0x7f070001;
    }
    public static final class attr {
    }
    public static final class bool {
        /** Enable automatic activity tracking
         */
        public static final int ga_autoActivityTracking=0x7f060000;
        /** Enable automatic exception tracking
         */
        public static final int ga_reportUncaughtExceptions=0x7f060001;
    }
    public static final class color {
        public static final int activity_textcolor=0x7f0a0002;
        public static final int bottom_background=0x7f0a0005;
        public static final int header_background=0x7f0a0000;
        public static final int header_textcolor=0x7f0a0001;
        public static final int slider_background=0x7f0a0003;
        public static final int slider_textcolor=0x7f0a0004;
    }
    public static final class dimen {
        public static final int list_padding=0x7f080001;
        public static final int shadow_width=0x7f080002;
        public static final int slidingmenu_offset=0x7f080000;
    }
    public static final class drawable {
        public static final int action_search=0x7f020000;
        public static final int av_pause=0x7f020001;
        public static final int av_play=0x7f020002;
        public static final int av_stop=0x7f020003;
        public static final int device_access_volume_on=0x7f020004;
        public static final int favorite_off=0x7f020005;
        public static final int favorite_on=0x7f020006;
        public static final int ic_launcher=0x7f020007;
        public static final int navigation_previous_item=0x7f020008;
        public static final int navigation_refresh=0x7f020009;
        public static final int wiki=0x7f02000a;
    }
    public static final class id {
        public static final int about_app_copyright=0x7f0c0010;
        public static final int about_app_header_menu=0x7f0c0001;
        public static final int about_app_header_title=0x7f0c0002;
        public static final int about_app_logo=0x7f0c0003;
        public static final int about_app_name=0x7f0c0004;
        public static final int about_app_name_sub=0x7f0c0005;
        public static final int about_app_project_contact=0x7f0c0008;
        public static final int about_app_project_email=0x7f0c000a;
        public static final int about_app_project_site=0x7f0c0009;
        public static final int about_app_project_worker=0x7f0c000b;
        public static final int about_app_project_worker_list_1=0x7f0c000c;
        public static final int about_app_project_worker_list_2=0x7f0c000d;
        public static final int about_app_project_worker_list_3=0x7f0c000e;
        public static final int about_app_project_worker_list_4=0x7f0c000f;
        public static final int about_app_version=0x7f0c0006;
        public static final int about_app_welcome=0x7f0c0007;
        public static final int categories_header=0x7f0c0011;
        public static final int categories_header_menu=0x7f0c0012;
        public static final int categories_header_refresh=0x7f0c0014;
        public static final int categories_header_title=0x7f0c0013;
        public static final int categories_list=0x7f0c0015;
        public static final int categories_progress=0x7f0c0016;
        public static final int favorites_header=0x7f0c0017;
        public static final int favorites_header_menu=0x7f0c0018;
        public static final int favorites_header_title=0x7f0c0019;
        public static final int favorites_list=0x7f0c001a;
        public static final int list_item_category_name=0x7f0c0036;
        public static final int list_item_page_title=0x7f0c0037;
        public static final int page_audio=0x7f0c0022;
        public static final int page_audio_img=0x7f0c0023;
        public static final int page_audio_img_play_pause=0x7f0c0025;
        public static final int page_audio_img_stop=0x7f0c0026;
        public static final int page_audio_panel=0x7f0c0024;
        public static final int page_categories_list=0x7f0c0021;
        public static final int page_header=0x7f0c001b;
        public static final int page_header_favorite=0x7f0c001f;
        public static final int page_header_menu=0x7f0c001c;
        public static final int page_header_refresh=0x7f0c001e;
        public static final int page_header_title=0x7f0c001d;
        public static final int page_list_item_categories=0x7f0c0038;
        public static final int page_list_item_categories_group=0x7f0c0039;
        public static final int page_progress=0x7f0c0028;
        public static final int page_progress_audio=0x7f0c0027;
        public static final int page_text=0x7f0c0020;
        public static final int pages_header=0x7f0c0000;
        public static final int pages_header_menu=0x7f0c0029;
        public static final int pages_header_refresh=0x7f0c002b;
        public static final int pages_header_title=0x7f0c002a;
        public static final int pages_list=0x7f0c002c;
        public static final int pages_progress=0x7f0c002d;
        public static final int search_btn=0x7f0c0033;
        public static final int search_header=0x7f0c002e;
        public static final int search_header_menu=0x7f0c002f;
        public static final int search_header_refresh=0x7f0c0031;
        public static final int search_header_title=0x7f0c0030;
        public static final int search_progress=0x7f0c0035;
        public static final int search_result_list=0x7f0c0034;
        public static final int search_text=0x7f0c0032;
        public static final int slider_list_item_category_name=0x7f0c003a;
        public static final int slider_menu_about_app=0x7f0c003e;
        public static final int slider_menu_categories_header=0x7f0c0040;
        public static final int slider_menu_categories_list=0x7f0c0041;
        public static final int slider_menu_favorites=0x7f0c003d;
        public static final int slider_menu_home=0x7f0c003b;
        public static final int slider_menu_search=0x7f0c003c;
        public static final int slider_menu_settings=0x7f0c003f;
    }
    public static final class integer {
        public static final int num_cols=0x7f090000;
    }
    public static final class layout {
        public static final int activity_about_app=0x7f030000;
        public static final int activity_categories=0x7f030001;
        public static final int activity_favorites=0x7f030002;
        public static final int activity_page=0x7f030003;
        public static final int activity_pages=0x7f030004;
        public static final int activity_search=0x7f030005;
        public static final int list_item_category=0x7f030006;
        public static final int list_item_page=0x7f030007;
        public static final int list_item_page_categories=0x7f030008;
        public static final int list_item_page_categories_group=0x7f030009;
        public static final int slider_list_item_category=0x7f03000a;
        public static final int slider_menu=0x7f03000b;
    }
    public static final class string {
        public static final int action_about_app=0x7f05000c;
        public static final int action_categories=0x7f050003;
        public static final int action_favorites=0x7f050025;
        public static final int action_home=0x7f05000d;
        public static final int action_page=0x7f050017;
        public static final int action_pages=0x7f050004;
        public static final int action_search=0x7f050011;
        public static final int action_settings=0x7f050002;
        public static final int app_name=0x7f050001;
        public static final int app_name_header=0x7f050009;
        public static final int app_name_subheader=0x7f05000a;
        public static final int article_load_failed=0x7f05001d;
        public static final int choose_language=0x7f050020;
        public static final int choose_language_2=0x7f050021;
        public static final int contact_header=0x7f05001c;
        public static final int copyright=0x7f05000b;
        public static final int email=0x7f05001b;
        public static final int favorite_add=0x7f050026;
        public static final int favorite_del=0x7f050027;
        /** Replace placeholder ID with your tracking ID
         */
        public static final int ga_trackingId=0x7f050000;
        public static final int pause=0x7f050006;
        public static final int play=0x7f050005;
        public static final int project_worker=0x7f05000e;
        public static final int project_worker_list_1=0x7f05000f;
        public static final int project_worker_list_2=0x7f050010;
        public static final int project_worker_list_3=0x7f050012;
        public static final int project_worker_list_4=0x7f050013;
        public static final int refresh=0x7f050018;
        public static final int search_btn=0x7f050015;
        public static final int search_text=0x7f050014;
        public static final int settings_clear=0x7f050022;
        public static final int settings_clear_message=0x7f050024;
        public static final int settings_clear_summary=0x7f050023;
        public static final int site=0x7f05001a;
        public static final int slider_menu=0x7f050016;
        public static final int stop=0x7f050007;
        public static final int target_language=0x7f05001f;
        public static final int version=0x7f05001e;
        public static final int wait=0x7f050008;
        public static final int welcome=0x7f050019;
    }
    public static final class style {
        public static final int Bottom=0x7f0b0005;
        public static final int Bottom_Audio=0x7f0b0006;
        public static final int Bottom_Audio_Pause=0x7f0b0008;
        public static final int Bottom_Audio_Play=0x7f0b0007;
        public static final int Bottom_Audio_Stop=0x7f0b0009;
        public static final int Header=0x7f0b0000;
        public static final int Header_Favorite=0x7f0b0004;
        public static final int Header_Img=0x7f0b0001;
        public static final int Header_Refresh=0x7f0b0003;
        public static final int Header_Title=0x7f0b0002;
        public static final int ListItem=0x7f0b000a;
        public static final int Slider=0x7f0b000b;
        public static final int SliderMenuItem=0x7f0b000c;
        public static final int SliderMenuItem_Header=0x7f0b000d;
        public static final int SliderMenuItem_ListItem=0x7f0b000e;
    }
    public static final class xml {
        public static final int settings=0x7f040000;
    }
}
