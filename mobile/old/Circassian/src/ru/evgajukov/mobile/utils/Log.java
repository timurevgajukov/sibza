package ru.evgajukov.mobile.utils;

import android.text.TextUtils;

/**
 * Класс логирования в мобильном приложении
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public final class Log {

    private static boolean _debugMode = true;

    private static String _tag = "";

    public static boolean isDebugMode() {

        return _debugMode;
    }

    public static void setDebugMode(boolean mode) {

        _debugMode = mode;
    }

    public static String getTag() {

        return _tag;
    }

    public static void setTag(String tag) {

        _tag = tag;
    }

    // debug
    public static void d(String msg) {

        if (_debugMode) {
            android.util.Log.d(_tag, getLocation() + msg);
        }
    }

    // error
    public static void e(String msg) {

        if (_debugMode) {
            android.util.Log.e(_tag, getLocation() + msg);
        }
    }

    // error
    public static void e(String msg, Throwable exception) {

        if (_debugMode) {
            android.util.Log.e(_tag, msg, exception);
        }
    }

    // warn
    public static void w(String msg) {

        if (_debugMode) {
            android.util.Log.w(_tag, getLocation() + msg);
        }
    }

    // info
    public static void i(String msg) {

        if (_debugMode) {
            android.util.Log.i(_tag, getLocation() + msg);
        }
    }

    private static String getLocation() {

        String className = Log.class.getName();
        StackTraceElement[] traces = Thread.currentThread().getStackTrace();
        boolean found = false;

        for (int i = 0; i < traces.length; i++) {
            StackTraceElement trace = traces[i];

            try {
                if (found) {
                    if (!trace.getClassName().startsWith(className)) {
                        Class<?> clazz = Class.forName(trace.getClassName());
                        return "[" + getClassName(clazz) + ":" + trace.getMethodName() + ":" +
                                trace.getLineNumber() + "]: ";
                    }
                } else if (trace.getClassName().startsWith(className)) {
                    found = true;
                    continue;
                }
            } catch (ClassNotFoundException e) {
            }
        }

        return "[]: ";
    }

    private static String getClassName(Class<?> clazz) {

        if (clazz != null) {
            if (!TextUtils.isEmpty(clazz.getSimpleName())) {
                return clazz.getSimpleName();
            }

            return getClassName(clazz.getEnclosingClass());
        }

        return "";
    }
}
