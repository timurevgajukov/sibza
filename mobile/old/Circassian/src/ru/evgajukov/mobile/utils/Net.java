package ru.evgajukov.mobile.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.UnknownHostException;

import ru.alfabank.server.xml.message.Request;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.errors.BackEndException;

/**
 * Класс для отправик и получения сообщений по сети
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Net {

    private static Net instance;

    {
        instance = null;
    }

    private int errorno;
    private String errormsg;
    private String result;

    {
        errorno = 0;
        errormsg = "";
        result = "";
    }

    public static Net getInstance() {
        if (instance == null) {
            instance = new Net();
        }

        return instance;
    }

    private Net() {
        super();

        // необходим, чтобы в андройде работал нормально разборщик xml от
        // библиотеки OAVDO
        System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");
    }

    public synchronized Response send(Request request) throws BackEndException {
        String requestRender = request.renderSafe();

        Log.d(requestRender);

        HttpURLConnection conn = null;
        try {
            Proxy proxy = Proxy.NO_PROXY;

            URL url = new URL(GlobalConst.BE_URL);

            conn = (HttpURLConnection) url.openConnection(proxy);

            conn.setConnectTimeout(GlobalConst.CONN_TIMEOUT);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/xml;charset=UTF-8");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            osw.write(requestRender);
            osw.flush();
            osw.close();

            int code = conn.getResponseCode();
            if (code == HttpURLConnection.HTTP_OK) {
                InputStream is = conn.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                result = builder.toString();
                result = result.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&quot;", "\"");

                if (result.length() == 0) {
                    Log.w("result len = 0");
                    throw new BackEndException("Result length is null");
                } else {
                    Log.d(result);

                    Response resp = new Response();
                    resp.parse(result);

                    // проверим на наличие ошибки бизнес-логики
                    if (Integer.parseInt(resp.getHeader().getResult()) != 0) {
                        String msg = resp.getHeader().getDescription();
                        String errorCode = resp.getHeader().getResult();
                        if (errorCode == null) {
                            errorCode = "-1";
                        }
                        Log.e("Error code: " + errorCode + " Description: " + msg);
                        throw new BackEndException(Integer.parseInt(errorCode), msg);
                    }

                    return resp;
                }

            } else {
                // Получить сообщение из потока ошибок
                InputStream is = conn.getErrorStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder builder = new StringBuilder("");
                String line;
                while ((line = br.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                is.close();

                errorno = code;
                errormsg = builder.toString();
                if (errormsg.length() == 0) {
                    Log.e("error result len = 0");
                } else {
                    Log.e(errormsg);
                }

                throw new BackEndException("Server error. ErrorNo: " + errorno + ", Msg: " + errormsg);
            }
        } catch (UnknownHostException e) {
            Log.e(e.toString(), e);
            throw new BackEndException(e);
        } catch (Exception e) {
            Log.e(e.toString(), e);
            throw new BackEndException(e);
        } finally {
            try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                Log.e(e.toString(), e);
            }
        }
    }
}
