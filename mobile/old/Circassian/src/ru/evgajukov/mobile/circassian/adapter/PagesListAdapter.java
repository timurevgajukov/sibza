package ru.evgajukov.mobile.circassian.adapter;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.circassian.view.PageItemView;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class PagesListAdapter extends ArrayAdapter<Page> {

    private List<Page> items;
    private int viewResourceId;

    public PagesListAdapter(Context context, int textViewResourceId, List<Page> items) {

        super(context, textViewResourceId, items);

        this.items = items;
        this.viewResourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PageItemView itemView = null;
        if (convertView != null) {
            itemView = (PageItemView) convertView;
        } else {
            itemView = (PageItemView) View.inflate(getContext(), viewResourceId, null);
        }
        try {
            itemView.setPage(items.get(position));
        } catch (Exception e) {
        }

        return itemView;
    }
}
