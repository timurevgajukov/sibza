package ru.evgajukov.mobile.circassian.message.request;

import ru.alfabank.server.xml.message.BodyElement;

public class MblReqSearchPages extends RequestWrapper {

    public static final String COMMAND = "SearchPages";

    public static final String FLD_SEARCH = "search";

    public MblReqSearchPages(String language) {

        super(language);

        getHeader().setMethod(COMMAND);
    }

    public void setSearch(String text) {

        getBody().addChild(new BodyElement(FLD_SEARCH, text));
    }
}
