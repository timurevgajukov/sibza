package ru.evgajukov.mobile.circassian.message.request;

/**
 * Запрос на получение списка категорий
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class MblReqGetCategoryList extends RequestWrapper {

    public static final String COMMAND = "GetCategoryList";

    public MblReqGetCategoryList(String language) {

        super(language);

        getHeader().setMethod(COMMAND);
    }
}
