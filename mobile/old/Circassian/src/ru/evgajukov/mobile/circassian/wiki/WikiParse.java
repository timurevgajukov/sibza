package ru.evgajukov.mobile.circassian.wiki;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.evgajukov.mobile.circassian.integface.IWikiParse;
import ru.evgajukov.mobile.utils.Log;

/**
 * Wiki-парсер
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class WikiParse implements IWikiParse {

    private final String ACTIVITY_URL = "activity-run://PageActivityHost?page=";

    @Override
    public String parse(String wiki) {

        String parseStr = wiki;

        try {
            Pattern p = Pattern.compile("\\[\\[(.+?)\\]\\]");
            Matcher m = p.matcher(parseStr);
            while (m.find()) {
                String[] wikiUrlArr = m.group(1).split("\\|");
                if (wikiUrlArr.length > 1) {
                    parseStr = parseStr.replace(String.format("[[%s|%s]]", wikiUrlArr[0], wikiUrlArr[1]),
                            String.format("<a href=\"%s%s\">%s</a>", ACTIVITY_URL, wikiUrlArr[0], wikiUrlArr[1]));
                } else {
                    parseStr = parseStr.replace(String.format("[[%s]]", wikiUrlArr[0]),
                            String.format("<a href=\"%s%s\">%s</a>", ACTIVITY_URL, wikiUrlArr[0], wikiUrlArr[0]));
                }
            }
        } catch (Exception e) {
            Log.e("ERROR", e);
        }

        parseStr = parseStr.replaceAll("\n", "<br />\n").trim();

        Log.d(parseStr);

        return parseStr;
    }
}
