package ru.evgajukov.mobile.circassian.activity;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.adapter.PagesListAdapter;
import ru.evgajukov.mobile.circassian.message.request.MblReqSearchPages;
import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.circassian.view.PageItemView;
import ru.evgajukov.mobile.errors.BackEndException;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

public class SearchActivity extends SlidingBaseActivity {

    private String text;

    private EditText editText;
    private ImageView imgSearch;
    private ListView listResult;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setSlidingMenu(this, R.id.search_header_menu);

        editText = (EditText) findViewById(R.id.search_text);

        imgSearch = (ImageView) findViewById(R.id.search_btn);
        imgSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                text = editText.getText().toString();
                if (text != null && text.trim().length() > 0) {
                    text = text.trim();
                    showPages();
                }
            }
        });

        listResult = (ListView) findViewById(R.id.search_result_list);
        listResult.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Page page = ((PageItemView) view).getPage();

                if (page != null && page.toString().length() > 0) {
                    // перейдем на страницу статьи
                    Intent intent = new Intent(SearchActivity.this, PageActivity.class);
                    intent.putExtra("page", page.toString());
                    startActivity(intent);
                }
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.search_progress);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void showPages() {

        progressBar.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, List<Page>>() {

            @Override
            protected List<Page> doInBackground(Void... params) {

                List<Page> pages = new ArrayList<Page>();

                MblReqSearchPages req = new MblReqSearchPages(getLanguage());
                req.setSearch(text);

                try {
                    Net net = Net.getInstance();
                    Response resp = net.send(req);

                    for (BodyElement element : resp.getBody().getChild("pages").getChilds("page")) {
                        pages.add(new Page(element.getChildValue("title")));
                    }
                } catch (BackEndException e) {
                    Log.e(e.toString(), e);
                }

                return pages;
            }

            @Override
            protected void onPostExecute(List<Page> pages) {

                progressBar.setVisibility(View.INVISIBLE);

                listResult.setAdapter(new PagesListAdapter(getApplicationContext(), R.layout.list_item_page, pages));
            }
        }.execute();
    }
}
