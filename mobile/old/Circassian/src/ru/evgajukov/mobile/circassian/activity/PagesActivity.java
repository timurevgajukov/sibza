package ru.evgajukov.mobile.circassian.activity;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.adapter.PagesListAdapter;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.message.request.MblReqGetPagesList;
import ru.evgajukov.mobile.circassian.model.Category;
import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.circassian.view.PageItemView;
import ru.evgajukov.mobile.errors.BackEndException;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

public class PagesActivity extends SlidingBaseActivity {

    private String category;

    private ImageView imgRefresh;
    private ListView listPages;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pages);

        setSlidingMenu(this, R.id.pages_header_menu);

        category = getIntent().getExtras().getString("category");

        Log.d("Category name = " + category);

        imgRefresh = (ImageView) findViewById(R.id.pages_header_refresh);
        imgRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Category currentCategory = getCache().getCategory(category);
                if (currentCategory != null) {
                    currentCategory.clearPages();
                }

                showPages();
            }
        });

        listPages = (ListView) findViewById(R.id.pages_list);
        listPages.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Page page = ((PageItemView) view).getPage();

                if (page != null && page.toString().length() > 0) {
                    // перейдем на страницу статьи
                    Intent intent = new Intent(PagesActivity.this, PageActivity.class);
                    intent.putExtra("page", page.toString());
                    startActivity(intent);
                }
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.pages_progress);
    }

    @Override
    protected void onResume() {

        super.onResume();

        showPages();
    }

    private void showPages() {

        progressBar.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, List<Page>>() {

            @Override
            protected List<Page> doInBackground(Void... params) {

                Cache cache = getCache();

                Category currentCategory = cache.getCategory(category);
                if (currentCategory != null && currentCategory.getPages() != null && currentCategory.getPages().size() > 0) {
                    return currentCategory.getPages();
                }

                List<Page> pages = new ArrayList<Page>();

                MblReqGetPagesList req = new MblReqGetPagesList(getLanguage());
                req.setCategory(category);

                try {
                    Net net = Net.getInstance();
                    Response resp = net.send(req);

                    for (BodyElement element : resp.getBody().getChild("pages").getChilds("page")) {
                        pages.add(new Page(element.getChildValue("title")));
                    }
                } catch (BackEndException e) {
                    Log.e(e.toString(), e);
                }

                if (currentCategory != null) {
                    currentCategory.setPages(pages);
                }

                return pages;
            }

            @Override
            protected void onPostExecute(List<Page> pages) {

                progressBar.setVisibility(View.INVISIBLE);

                listPages.setAdapter(new PagesListAdapter(getApplicationContext(), R.layout.list_item_page, pages));
            }
        }.execute();
    }
}
