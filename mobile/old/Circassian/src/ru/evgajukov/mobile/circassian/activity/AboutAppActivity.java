package ru.evgajukov.mobile.circassian.activity;

import ru.evgajukov.mobile.utils.Utils;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class AboutAppActivity extends SlidingBaseActivity {

    private TextView txtSite;
    private TextView txtEmail;
    private TextView txtVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);

        setSlidingMenu(this, R.id.about_app_header_menu);

        txtVersion = (TextView) findViewById(R.id.about_app_version);
        txtSite = (TextView) findViewById(R.id.about_app_project_site);
        txtEmail = (TextView) findViewById(R.id.about_app_project_email);

        txtVersion.setText(Html.fromHtml(String.format("<b>%s</b> %s", getString(R.string.version), Utils.getAppVersion(this))));
        txtSite.setText(Html.fromHtml(getString(R.string.site)));
        txtEmail.setText(Html.fromHtml(getString(R.string.email)));
    }

}
