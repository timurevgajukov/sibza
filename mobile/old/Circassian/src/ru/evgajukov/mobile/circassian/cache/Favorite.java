package ru.evgajukov.mobile.circassian.cache;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.utils.Log;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Класс для хранения избранных страниц
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Favorite {

    private static final String FLD_FAVOTITES = "favorites";

    private static Favorite instance;

    private SharedPreferences prefs;

    private List<Page> pages;

    public static Favorite getInstance(Context context) {

        if (instance == null) {
            instance = new Favorite(context);
        }

        return instance;
    }

    private Favorite(Context context) {

        super();

        System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isFavorive(Page page) {

        if (pages == null || pages.size() == 0) {
            return false;
        }

        if (page == null) {
            return false;
        }

        for (Page pageFavorite : pages) {
            try {
                if (pageFavorite.getTitle().equalsIgnoreCase(page.getTitle())) {
                    return true;
                }
            } catch (Exception e) {
            }
        }

        return false;
    }

    public List<Page> getFavorites() {

        return pages;
    }

    public boolean addFavorite(Page page) {

        if (pages == null) {
            pages = new ArrayList<Page>();
        }

        pages.add(page);

        return true;
    }

    public boolean delFavorite(Page page) {

        if (pages == null || pages.size() == 0) {
            return true;
        }

        for (Page pageFavorite : pages) {
            if (page.getTitle().equalsIgnoreCase(pageFavorite.getTitle())) {
                return pages.remove(pageFavorite);
            }
        }

        return true;
    }

    public boolean clear() {

        if (pages != null) {
            pages.clear();
            pages = null;
        }

        return true;
    }

    /**
     * Загружает из внешнего источника список избранных статей
     */
    public void load() {

        Log.d("load");

        if (prefs.contains(FLD_FAVOTITES) && pages == null) {
            String favoritesXML = prefs.getString(FLD_FAVOTITES, null);
            if (favoritesXML != null) {
                parse(favoritesXML);
            }
        }
    }

    /**
     * Сохраняет списки избранных статей во внешнее хранилище
     */
    public void save() {

        Log.d("save");

        if (pages == null || pages.size() == 0) {
            return;
        }

        String favoritesXML = render();
        if (favoritesXML != null) {
            Editor editor = prefs.edit();
            editor.putString(FLD_FAVOTITES, favoritesXML);
            editor.commit();
        }
    }

    private void parse(String xml) {

        Log.d("parse");

        Response msg = new Response();
        try {
            this.pages = new ArrayList<Page>();

            msg.parse(xml);
            BodyElement pages = msg.getBody().getChild("pages");
            for (BodyElement pageElement : pages.getChilds("page")) {
                Page page = new Page(null);
                page.parse(pageElement);

                this.pages.add(page);
            }
        } catch (Exception e) {
            Log.e("Error favorites parse", e);
        }
    }

    public String render() {

        Log.d("render");

        BodyElement pages = new BodyElement("pages");
        for (Page page : this.pages) {
            pages.addChild(page.render());
        }

        Response msg = new Response();
        msg.getBody().addChild(pages);

        return msg.renderSafe();
    }
}
