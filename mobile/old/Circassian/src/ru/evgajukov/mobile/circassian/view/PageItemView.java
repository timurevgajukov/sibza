package ru.evgajukov.mobile.circassian.view;

import ru.evgajukov.mobile.circassian.activity.R;
import ru.evgajukov.mobile.circassian.model.Page;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PageItemView extends RelativeLayout {

    private TextView txtPageTitle;

    private Page page;

    public PageItemView(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {

        super.onFinishInflate();

        txtPageTitle = (TextView) findViewById(R.id.list_item_page_title);
    }

    public Page getPage() {

        return page;
    }

    public void setPage(Page page) {

        this.page = page;

        txtPageTitle.setText(page.toString());
    }
}
