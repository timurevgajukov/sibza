package ru.evgajukov.mobile.circassian.activity;

import java.util.Locale;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.cache.Favorite;
import ru.evgajukov.mobile.utils.Log;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.analytics.tracking.android.EasyTracker;
import com.yandex.metrica.Counter;

/**
 * Базовый класс для всех активностей приложения
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class BaseActivity extends Activity {

    private Cache cache;
    private Favorite favorite;

    protected MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        cache = Cache.getInstance(this);

        favorite = Favorite.getInstance(this);
        favorite.load();

        // приведем в порядок локаль приложения
        changeAppLocale();
    }

    @Override
    protected void onStart() {

        super.onStart();

        // запускаем google аналитику
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onResume() {

        super.onResume();

        Counter.sharedInstance().onResumeActivity(this);
    }

    @Override
    protected void onPause() {

        super.onPause();

        Counter.sharedInstance().onPauseActivity(this);
    }

    @Override
    protected void onStop() {

        super.onStop();

        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }

        // сохраним кеш в файле
        cache.save();

        // сохраняем списки избранных в файл
        favorite.save();

        // останавливаем google аналитику
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        releaseMP();
    }

    /**
     * Очищает ресурсы выделенные под MediaPlayer
     */
    protected void releaseMP() {

        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                Log.e("Error in releaseMP", e);
            }
        }
    }

    protected Cache getCache() {

        return cache;
    }

    protected Favorite getFavoritesCache() {

        return favorite;
    }

    protected String getLanguage() {

        if (GlobalConst.language == null || GlobalConst.language.length() == 0) {
            GlobalConst.language = Locale.getDefault().getLanguage();
        }

        return GlobalConst.language;
    }

    protected void changeAppLocale() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String language = prefs.getString("language", "");
        if (language != null && language.length() > 1) {
            // проверим чтобы язык локали совпадал с языком в настройках
            String currentLanguage = Locale.getDefault().getLanguage();
            if (!language.equalsIgnoreCase(currentLanguage)) {
                GlobalConst.language = language;

                // меняем локаль приложения
                String languageLocale = GlobalConst.getLanguageLocale(GlobalConst.language);
                if (languageLocale == null || languageLocale.length() == 0) {
                    languageLocale = "en";
                }
                Locale locale = new Locale(languageLocale);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config, null);

                // очищаем кеш
                Cache cache = Cache.getInstance(getApplicationContext());
                cache.clear();
            }
        }
    }
}
