package ru.evgajukov.mobile.circassian;

import android.app.Application;

import com.yandex.metrica.Counter;

public class CircassianApplication extends Application {

    @Override
    public void onCreate() {

        super.onCreate();

        Counter.initialize(getApplicationContext());
    }
}
