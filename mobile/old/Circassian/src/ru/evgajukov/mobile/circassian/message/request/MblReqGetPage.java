package ru.evgajukov.mobile.circassian.message.request;

import ru.alfabank.server.xml.message.BodyElement;

public class MblReqGetPage extends RequestWrapper {

    public static String COMMAND = "GetPage";

    public static final String FLD_TITLE = "title";

    public MblReqGetPage(String language) {

        super(language);

        getHeader().setMethod(COMMAND);
    }

    public void setTitle(String title) {

        getBody().addChild(new BodyElement(FLD_TITLE, title));
    }
}
