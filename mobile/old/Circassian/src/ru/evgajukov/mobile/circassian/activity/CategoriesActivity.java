package ru.evgajukov.mobile.circassian.activity;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.adapter.CategoriesListAdapter;
import ru.evgajukov.mobile.circassian.adapter.SliderCategoriesListAdapter;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.message.request.MblReqGetCategoryList;
import ru.evgajukov.mobile.circassian.model.Category;
import ru.evgajukov.mobile.circassian.view.CategoryItemView;
import ru.evgajukov.mobile.errors.BackEndException;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.crittercism.app.Crittercism;

public class CategoriesActivity extends SlidingBaseActivity {

    private ImageView imgRefresh;
    private ProgressBar progressBar;
    private ListView listCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Crittercism.initialize(getApplicationContext(), "5243de29558d6a6914000003");

        setContentView(R.layout.activity_categories);

        setSlidingMenu(this, R.id.categories_header_menu);

        imgRefresh = (ImageView) findViewById(R.id.categories_header_refresh);
        imgRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                getCache().clear();

                showCategories();
            }
        });

        listCategories = (ListView) findViewById(R.id.categories_list);
        listCategories.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                String category = ((CategoryItemView) view).getCategory().getName();

                if (category != null && category.length() > 0) {
                    // перейти на список статей по выбранной категории
                    Intent intent = new Intent(CategoriesActivity.this, PagesActivity.class);
                    intent.putExtra("category", category);
                    startActivity(intent);
                }
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.categories_progress);
    }

    @Override
    protected void onResume() {

        super.onResume();

        showCategories();
    }

    private void showCategories() {

        progressBar.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, List<Category>>() {

            @Override
            protected List<Category> doInBackground(Void... params) {

                Cache cache = getCache();

                if (cache.getCategories() != null && cache.getCategories().size() > 0) {
                    return cache.getCategories();
                }

                List<Category> categories = new ArrayList<Category>();

                MblReqGetCategoryList req = new MblReqGetCategoryList(getLanguage());

                try {
                    Net net = Net.getInstance();
                    Response resp = net.send(req);

                    for (BodyElement element : resp.getBody().getChild("categories").getChilds("category")) {
                        String name = element.getChildValue("name");
                        int pagesCount = Integer.parseInt(element.getChildValue("pages_count"));
                        Category category = new Category(name, pagesCount);
                        categories.add(category);
                    }
                } catch (BackEndException e) {
                    Log.e(e.toString(), e);
                }

                cache.setCategories(categories);

                return categories;
            }

            @Override
            protected void onPostExecute(List<Category> categories) {

                progressBar.setVisibility(View.INVISIBLE);

                listCategories.setAdapter(new CategoriesListAdapter(getApplicationContext(), R.layout.list_item_category, categories));

                // заодно еще и в боковом меню проинициализируем список
                // категорий
                listSliderCategories.setAdapter(new SliderCategoriesListAdapter(getApplicationContext(), R.layout.slider_list_item_category, categories));
            }
        }.execute();
    }
}
