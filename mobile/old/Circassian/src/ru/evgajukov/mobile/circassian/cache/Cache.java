package ru.evgajukov.mobile.circassian.cache;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.model.Category;
import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.utils.Log;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Кэширование данных, полученных из сети на время работы приложения
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class Cache {

    private static final String FLD_CACHE = "cache";

    private static Cache instance;

    private SharedPreferences prefs;

    private List<Category> categories;
    private Page page;

    public static Cache getInstance(Context context) {

        if (instance == null) {
            instance = new Cache(context);
        }

        return instance;
    }

    private Cache(Context context) {

        super();

        System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public List<Category> getCategories() {

        return categories;
    }

    public Category getCategory(String name) {

        if (categories == null) {
            return null;
        }

        for (Category category : categories) {
            if (category.getName().equalsIgnoreCase(name)) {
                return category;
            }
        }

        return null;
    }

    public void setCategories(List<Category> categories) {

        this.categories = categories;
    }

    public void clear() {

        if (categories != null) {
            categories.clear();
            categories = null;
        }
    }

    public Page getPage() {

        return page;
    }

    public void setPage(Page page) {

        this.page = page;
    }

    public void clearPage() {

        page = null;
    }

    /**
     * загружает из внешнего хранилища данные в кеш
     */
    public void load() {

        Log.d("load");

        if (prefs.contains(FLD_CACHE) && categories == null) {
            String cacheXML = prefs.getString(FLD_CACHE, null);
            if (cacheXML != null) {
                parse(cacheXML);
            }
        }
    }

    /**
     * сохраняет кеш во внешнее хранилище
     */
    public void save() {

        Log.d("save");

        if (categories == null) {
            return;
        }

        String cacheXML = render();
        if (cacheXML != null) {
            Editor editor = prefs.edit();
            editor.putString(FLD_CACHE, cacheXML);
            editor.commit();
        }
    }

    private void parse(String xml) {

        Log.d("parse");

        Response msg = new Response();
        try {
            this.categories = new ArrayList<Category>();

            msg.parse(xml);
            BodyElement categories = msg.getBody().getChild("categories");
            for (BodyElement categoryElement : categories.getChilds("category")) {
                Category category = new Category(null, 0);
                category.parse(categoryElement);

                this.categories.add(category);
            }
        } catch (Exception e) {
            Log.e("Error cache parse", e);
        }
    }

    public String render() {

        Log.d("render");

        BodyElement categories = new BodyElement("categories");
        for (Category category : this.categories) {
            categories.addChild(category.render());
        }

        Response msg = new Response();
        msg.getBody().addChild(categories);

        return msg.renderSafe();
    }
}
