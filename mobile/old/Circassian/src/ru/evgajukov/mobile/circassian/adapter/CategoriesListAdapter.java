package ru.evgajukov.mobile.circassian.adapter;

import java.util.List;

import ru.evgajukov.mobile.circassian.model.Category;
import ru.evgajukov.mobile.circassian.view.CategoryItemView;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class CategoriesListAdapter extends ArrayAdapter<Category> {

    private List<Category> items;
    private int viewResourceId;

    public CategoriesListAdapter(Context context, int textViewResourceId, List<Category> items) {

        super(context, textViewResourceId, items);

        this.items = items;
        this.viewResourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CategoryItemView itemView = null;
        if (convertView != null) {
            itemView = (CategoryItemView) convertView;
        } else {
            itemView = (CategoryItemView) View.inflate(getContext(), viewResourceId, null);
        }
        itemView.setCategory(items.get(position));

        return itemView;
    }
}
