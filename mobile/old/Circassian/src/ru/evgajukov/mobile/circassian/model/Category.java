package ru.evgajukov.mobile.circassian.model;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import android.annotation.SuppressLint;

public class Category {

    private String name;
    private int pagesCount;
    private List<Page> pages;

    public Category(String name, int pagesCount) {

        this.name = name;
        this.pagesCount = pagesCount;
    }

    public String getName() {

        return name;
    }

    public int getPagesCount() {

        return pagesCount;
    }

    public List<Page> getPages() {

        return pages;
    }

    public void setPages(List<Page> pages) {

        this.pages = pages;
    }

    public void clearPages() {

        if (pages != null) {
            pages.clear();
            pages = null;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public String toString() {

        return String.format("%s (%d)", name, pagesCount);
    }

    public void parse(BodyElement category) {

        name = category.getChildValue("name");
        pagesCount = Integer.parseInt(category.getChildValue("pages_count"));

        pages = new ArrayList<Page>();
        for (BodyElement pageElement : category.getChild("pages").getChilds("page")) {
            Page page = new Page(null);
            page.parse(pageElement);

            pages.add(page);
        }
    }

    public BodyElement render() {

        BodyElement category = new BodyElement("category");
        category.addChild(new BodyElement("name", name));
        category.addChild(new BodyElement("pages_count", String.valueOf(pagesCount)));

        BodyElement pages = new BodyElement("pages");
        category.addChild(pages);

        if (this.pages != null) {
            for (Page page : this.pages) {
                pages.addChild(page.render());
            }
        }

        return category;
    }
}
