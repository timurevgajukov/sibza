package ru.evgajukov.mobile.circassian.model;

import java.util.ArrayList;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.evgajukov.mobile.circassian.cache.Cache;

public class Page {

    private String title;
    private String text;
    private String audioUrl;
    private List<String> categories;

    public Page(String title) {

        this.title = title;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {

        this.text = text;
    }

    public String getAudioUrl() {

        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {

        this.audioUrl = audioUrl;
    }

    public List<String> getCategories() {

        return categories;
    }

    public void addCategory(String category) {

        if (categories == null) {
            categories = new ArrayList<String>();
        }

        categories.add(category);
    }

    @Override
    public String toString() {

        return title;
    }

    /**
     * распарсить ответ из сети
     * 
     * @param element
     * @param cache
     */
    public void parse(BodyElement element, Cache cache) {

        this.text = element.getChildValue("text");
        this.audioUrl = element.getChildValue("audio");

        BodyElement categories = element.getChild("categories");
        if (categories != null) {
            for (BodyElement category : categories.getChilds("category")) {
                // добавляем только категории языка телефона
                if (cache.getCategories() == null || cache.getCategory(category.getValue()) != null) {
                    addCategory(category.getValue());
                }
            }
        }
    }

    /**
     * распарсить xml из кеша
     * 
     * @param xml
     */
    public void parse(BodyElement page) {

        title = page.getChildValue("title");
        text = page.getChildValue("text");
        audioUrl = page.getChildValue("audio_url");

        categories = new ArrayList<String>();
        for (BodyElement category : page.getChild("page_categories").getChilds("page_category")) {
            categories.add(category.getValue());
        }
    }

    /**
     * Сформировать xml для хранения в кеше
     * 
     * @return
     */
    public BodyElement render() {

        BodyElement page = new BodyElement("page");
        page.addChild(new BodyElement("title", title));
        page.addChild(new BodyElement("text", text));
        page.addChild(new BodyElement("audio_url", audioUrl));

        BodyElement categories = new BodyElement("page_categories");
        page.addChild(categories);

        if (this.categories != null) {
            for (String category : this.categories) {
                categories.addChild(new BodyElement("page_category", category));
            }
        }

        return page;
    }
}
