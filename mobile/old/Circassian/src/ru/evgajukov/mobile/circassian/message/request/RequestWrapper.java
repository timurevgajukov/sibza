package ru.evgajukov.mobile.circassian.message.request;

import ru.alfabank.server.xml.message.Request;
import ru.evgajukov.mobile.circassian.GlobalConst;

/**
 * Общий класс-оболочка для отправки запросов
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class RequestWrapper extends Request {

    public RequestWrapper(String language) {
        super();

        initHeader(language);
    }

    private void initHeader(String language) {
        getHeader().setSystem(GlobalConst.SYSTEM);
        getHeader().setOS(String.format("%s %s", GlobalConst.APP_OS, android.os.Build.VERSION.RELEASE));
        getHeader().setAppClient(String.format("%s %s", GlobalConst.APP_NAME, GlobalConst.APP_VER));

        // установим языковые настройки приложения
        getHeader().setElement("language", language);
    }
}
