package ru.evgajukov.mobile.circassian.activity;

import java.io.File;
import java.util.Locale;

import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.cache.Cache;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.widget.Toast;

/**
 * Активность для работы с настройками приложения
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        changeAppLocale();

        addPreferencesFromResource(R.xml.settings);

        final Preference pref = findPreference("clear_audio_cache");
        pref.setSummary(getString(R.string.settings_clear_summary) + " [" + getAudioCacheSize() + "]");
        pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {

                String appFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                        + GlobalConst.APP_NAME;
                String audioCacheFolderPath = appFolder + File.separator + GlobalConst.AUDIO_FOLDER;
                File audioCacheFolder = new File(audioCacheFolderPath);
                if (audioCacheFolder.exists()) {
                    if (audioCacheFolder.isDirectory()) {
                        // необходимо очистить папку с аудио файлами
                        for (File audioFile : audioCacheFolder.listFiles()) {
                            if (!audioFile.isDirectory()) {
                                audioFile.delete();
                            }
                        }
                    }
                }

                pref.setSummary(getString(R.string.settings_clear_summary) + " [" + getAudioCacheSize() + "]");

                Toast.makeText(getApplicationContext(), R.string.settings_clear_message, Toast.LENGTH_LONG).show();

                return true;
            }
        });

        ListPreference listPref = (ListPreference) findPreference("language");
        listPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                GlobalConst.oldLanguage = GlobalConst.language;

                GlobalConst.language = (String) newValue;

                if (!GlobalConst.language.equalsIgnoreCase(GlobalConst.oldLanguage)) {
                    // меняем локаль приложения
                    String languageLocale = GlobalConst.getLanguageLocale(GlobalConst.language);
                    if (languageLocale == null || languageLocale.length() == 0) {
                        languageLocale = "en_US";
                    }
                    Locale locale = new Locale(languageLocale);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getResources().updateConfiguration(config, null);

                    // очищаем кеш
                    Cache cache = Cache.getInstance(getApplicationContext());
                    cache.clear();

                    // нужно перерисовать экран
                    Intent intent = getIntent();
                    overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }

                return true;
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // перехватили аппаратную кнопку возврата
            Intent intent = new Intent(SettingsActivity.this, CategoriesActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private String getAudioCacheSize() {

        long totalSize = 0;

        String appFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                + GlobalConst.APP_NAME;
        String audioCacheFolderPath = appFolder + File.separator + GlobalConst.AUDIO_FOLDER;
        File audioCacheFolder = new File(audioCacheFolderPath);
        if (audioCacheFolder.exists()) {
            if (audioCacheFolder.isDirectory()) {
                // необходимо очистить папку с аудио файлами
                for (File audioFile : audioCacheFolder.listFiles()) {
                    if (!audioFile.isDirectory()) {
                        totalSize += audioFile.length();
                    }
                }
            }
        }

        return String.format("%.1fMb", totalSize / 1048576.0);
    }

    private void changeAppLocale() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String language = prefs.getString("language", "");
        if (language != null && language.length() > 1) {
            // проверим чтобы язык локали совпадал с языком в настройках
            String currentLanguage = Locale.getDefault().getLanguage();
            if (!language.equalsIgnoreCase(currentLanguage)) {
                GlobalConst.language = language;

                // меняем локаль приложения
                String languageLocale = GlobalConst.getLanguageLocale(GlobalConst.language);
                if (languageLocale == null || languageLocale.length() == 0) {
                    languageLocale = "en";
                }
                Locale locale = new Locale(languageLocale);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getResources().updateConfiguration(config, null);

                // очищаем кеш
                Cache cache = Cache.getInstance(getApplicationContext());
                cache.clear();
            }
        }
    }
}
