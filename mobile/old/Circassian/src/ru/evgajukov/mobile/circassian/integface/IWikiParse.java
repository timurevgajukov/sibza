package ru.evgajukov.mobile.circassian.integface;

/**
 * Интерфейс для wiki парсеров
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public interface IWikiParse {

    public String parse(String wiki);
}
