package ru.evgajukov.mobile.circassian.activity;

import java.util.List;

import ru.evgajukov.mobile.circassian.adapter.PagesListAdapter;
import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.circassian.view.PageItemView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * Страница со списком избранных статей
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class FavoritesActivity extends SlidingBaseActivity {

    private ListView listPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        setSlidingMenu(this, R.id.favorites_header_menu);

        listPages = (ListView) findViewById(R.id.favorites_list);
        listPages.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {

                Page page = ((PageItemView) view).getPage();

                if (page != null && page.toString().length() > 0) {
                    // перейдем на страницу статьи
                    Intent intent = new Intent(FavoritesActivity.this, PageActivity.class);
                    intent.putExtra("page", page.toString());
                    startActivity(intent);
                }
            }
        });

        showPages();
    }

    private void showPages() {

        List<Page> pages = getFavoritesCache().getFavorites();
        if (pages == null || pages.size() == 0) {
            return;
        }

        listPages.setAdapter(new PagesListAdapter(getApplicationContext(), R.layout.list_item_page, pages));
    }
}
