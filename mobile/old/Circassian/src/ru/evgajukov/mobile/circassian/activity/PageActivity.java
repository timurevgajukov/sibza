package ru.evgajukov.mobile.circassian.activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import ru.alfabank.server.xml.message.BodyElement;
import ru.alfabank.server.xml.message.Response;
import ru.evgajukov.mobile.circassian.GlobalConst;
import ru.evgajukov.mobile.circassian.adapter.PageCategoriesListAdapterWrapper;
import ru.evgajukov.mobile.circassian.cache.Cache;
import ru.evgajukov.mobile.circassian.message.request.MblReqGetPage;
import ru.evgajukov.mobile.circassian.model.Page;
import ru.evgajukov.mobile.circassian.wiki.WikiParse;
import ru.evgajukov.mobile.errors.BackEndException;
import ru.evgajukov.mobile.utils.Log;
import ru.evgajukov.mobile.utils.Net;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PageActivity extends SlidingBaseActivity {

    private String title;
    private Page page;

    private ImageView imgFavorite;
    private ImageView imgRefresh;
    private TextView txtTitle;
    private TextView txtText;

    private RelativeLayout layoutAudioPlay;
    private ProgressBar progressBarAudio;
    private ImageView imgAudio;
    private ImageView imgPlayPause;
    private ImageView imgStop;
    private LinearLayout layoutAudioPanel;

    private ExpandableListView listCategories;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);

        setSlidingMenu(this, R.id.page_header_menu);

        title = getIntent().getExtras().getString("page");
        if (title == null) {
            title = getIntent().getData().getQueryParameter("page");
        }

        txtTitle = (TextView) findViewById(R.id.page_header_title);

        imgRefresh = (ImageView) findViewById(R.id.page_header_refresh);
        imgRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                getCache().clearPage();

                showPage();
            }
        });

        imgFavorite = (ImageView) findViewById(R.id.page_header_favorite);
        imgFavorite.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (getFavoritesCache().isFavorive(page)) {
                    // удаляем страницу из списка изюранных и меняем отображение
                    // звездочки
                    getFavoritesCache().delFavorite(page);

                    imgFavorite.setImageResource(R.drawable.favorite_off);
                    imgFavorite.setContentDescription(getString(R.string.favorite_add));
                } else {
                    // добавляем страницу в список избранных и меняем вид
                    // звездочки
                    getFavoritesCache().addFavorite(page);

                    imgFavorite.setImageResource(R.drawable.favorite_on);
                    imgFavorite.setContentDescription(getString(R.string.favorite_del));
                }
            }
        });

        txtText = (TextView) findViewById(R.id.page_text);
        txtText.setLinksClickable(true);
        txtText.setMovementMethod(new LinkMovementMethod());

        listCategories = (ExpandableListView) findViewById(R.id.page_categories_list);
        listCategories.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                // переходим на список статей в выбранной категории
                String category = page.getCategories().get(childPosition);

                if (category != null && category.length() > 0) {
                    Intent intent = new Intent(PageActivity.this, PagesActivity.class);
                    intent.putExtra("category", category);
                    startActivity(intent);
                }

                return false;
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.page_progress);
    }

    @Override
    protected void onStart() {

        super.onStart();

        initAudio();
    }

    @Override
    protected void onResume() {

        super.onResume();

        showPage();
    }

    private void showPage() {

        progressBar.setVisibility(View.VISIBLE);

        txtTitle.setText(title);
        layoutAudioPlay.setVisibility(View.INVISIBLE);

        new AsyncTask<Void, Void, Page>() {

            @Override
            protected Page doInBackground(Void... params) {

                Cache cache = getCache();

                Page current = cache.getPage();
                if (current != null && current.getTitle().equalsIgnoreCase(title)) {
                    return current;
                }

                Page page = new Page(title);
                page.setText(getString(R.string.article_load_failed));

                MblReqGetPage req = new MblReqGetPage(getLanguage());
                req.setTitle(title);

                try {
                    Net net = Net.getInstance();
                    Response resp = net.send(req);

                    BodyElement pageElement = resp.getBody().getChild("page");

                    page.parse(pageElement, cache);
                } catch (BackEndException e) {
                    Log.e(e.toString(), e);
                }

                cache.setPage(page);

                return page;
            }

            @Override
            protected void onPostExecute(Page p) {

                progressBar.setVisibility(View.INVISIBLE);

                page = p;

                WikiParse wiki = new WikiParse();

                txtText.setText(Html.fromHtml(wiki.parse(page.getText())));
                if (page.getAudioUrl() == null || page.getAudioUrl().length() == 0) {
                    layoutAudioPlay.setVisibility(View.INVISIBLE);
                } else {
                    layoutAudioPlay.setVisibility(View.VISIBLE);
                }

                List<String> categories = page.getCategories();
                if (categories != null) {
                    listCategories.setVisibility(View.VISIBLE);
                    listCategories.setAdapter(new PageCategoriesListAdapterWrapper(getApplicationContext(), categories).getAdapter());
                    listCategories.expandGroup(0);
                } else {
                    listCategories.setVisibility(View.INVISIBLE);
                }

                if (getFavoritesCache().isFavorive(page)) {
                    imgFavorite.setImageResource(R.drawable.favorite_on);
                    imgFavorite.setContentDescription(getString(R.string.favorite_del));
                } else {
                    imgFavorite.setImageResource(R.drawable.favorite_off);
                    imgFavorite.setContentDescription(getString(R.string.favorite_add));
                }

            }
        }.execute();
    }

    private void initAudio() {

        layoutAudioPlay = (RelativeLayout) findViewById(R.id.page_audio);

        layoutAudioPanel = (LinearLayout) findViewById(R.id.page_audio_panel);
        layoutAudioPanel.setVisibility(View.INVISIBLE);

        progressBarAudio = (ProgressBar) findViewById(R.id.page_progress_audio);
        progressBarAudio.setVisibility(View.INVISIBLE);

        imgAudio = (ImageView) findViewById(R.id.page_audio_img);
        imgAudio.setVisibility(View.VISIBLE);
        imgAudio.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                download(page.getAudioUrl());
            }
        });

        imgPlayPause = (ImageView) findViewById(R.id.page_audio_img_play_pause);
        imgPlayPause.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                        imgPlayPause.setImageResource(R.drawable.av_play);
                        imgPlayPause.setContentDescription(getString(R.string.play));
                    } else {
                        mediaPlayer.start();
                        imgPlayPause.setImageResource(R.drawable.av_pause);
                        imgPlayPause.setContentDescription(getString(R.string.pause));
                    }
                }
            }
        });

        imgStop = (ImageView) findViewById(R.id.page_audio_img_stop);
        imgStop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                stopAudio();
            }
        });
    }

    private void stopAudio() {

        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                imgPlayPause.setImageResource(R.drawable.av_play);
                imgPlayPause.setContentDescription(getString(R.string.play));
            }
            mediaPlayer.seekTo(0);
        }
    }

    private void play(String filePath) {

        releaseMP();
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            if (filePath != null) {
                mediaPlayer.setDataSource(filePath);

            } else {
                mediaPlayer.setDataSource(page.getAudioUrl());
            }
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer mp) {

                    Log.d("onPrepareg");

                    layoutAudioPanel.setVisibility(View.VISIBLE);

                    imgPlayPause.setImageResource(R.drawable.av_pause);
                    imgPlayPause.setContentDescription(getString(R.string.pause));

                    mp.start();
                }
            });
            mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {

                    Log.d("onCompletion");

                    imgPlayPause.setImageResource(R.drawable.av_play);
                    imgPlayPause.setContentDescription(getString(R.string.play));
                }
            });
            // mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void download(String urlFile) {

        Log.d("download file: " + urlFile);

        new AsyncTask<String, Integer, String>() {

            @Override
            protected void onPreExecute() {

                imgAudio.setVisibility(View.INVISIBLE);

                progressBarAudio.setClickable(false);
                progressBarAudio.setMax(100);
                progressBarAudio.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {

                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoOutput(true);
                    conn.connect();

                    // формируем все необходимые на папки ссылки
                    String appFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                            + GlobalConst.APP_NAME;

                    // папка для временных файлов
                    String tmpFolder = appFolder + File.separator + GlobalConst.TEMP_FOLDER;
                    File fTmpFolder = new File(tmpFolder);
                    if (!fTmpFolder.exists()) {
                        // если папка не существует, то необходимо создать
                        if (!fTmpFolder.mkdirs()) {
                            Log.e("Cannot create " + fTmpFolder.getAbsolutePath());
                            return null;
                        }
                    }

                    // папка для аудио файлов
                    String audioCacheFolder = appFolder + File.separator + GlobalConst.AUDIO_FOLDER;
                    File fAudioCacheFolder = new File(audioCacheFolder);
                    if (!fAudioCacheFolder.exists()) {
                        // если папка еще не существует, то необходимо создать
                        if (!fAudioCacheFolder.mkdirs()) {
                            Log.e("Cannot create " + fAudioCacheFolder.getAbsolutePath());
                            return null;
                        }
                    }

                    // выделить из url имя файла
                    String[] filePath = params[0].split("/");
                    String fileName = filePath[filePath.length - 1];
                    Log.d("file name = " + fileName);

                    // получаем размер файла
                    int totalSize = conn.getContentLength();

                    // если файл с таким именем найден, то возвращаем ссылку на
                    // него
                    File audioFile = new File(fAudioCacheFolder, fileName);
                    if (audioFile.exists()) {
                        // файл уже ранее был загружен в телефон
                        // сверим размеры локального и сетевого файла
                        if (audioFile.length() == totalSize) {
                            return audioFile.getAbsolutePath();
                        }
                    }

                    File tmpAudioFile = new File(fTmpFolder, fileName);
                    tmpAudioFile.createNewFile();

                    FileOutputStream fos = new FileOutputStream(tmpAudioFile);
                    InputStream is = conn.getInputStream();

                    int downloadedSize = 0;

                    byte[] buffer = new byte[1024];
                    int len = 0;
                    while ((len = is.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                        downloadedSize += len;
                        publishProgress(downloadedSize, totalSize);
                    }
                    fos.close();
                    is.close();

                    // после успешной загрузки во временную папке, файл нужно
                    // перенести в основную папку хранения аудио-файлов
                    tmpAudioFile.renameTo(audioFile);

                    return audioFile.getAbsolutePath();
                } catch (Exception e) {
                    Log.e("error to download file", e);
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {

                progressBarAudio.setProgress((int) ((values[0] / (float) values[1]) * 100));
            }

            @Override
            protected void onPostExecute(String filePath) {

                progressBarAudio.setVisibility(View.INVISIBLE);

                play(filePath);
            }
        }.execute(urlFile);
    }
}
