package ru.evgajukov.mobile.circassian.message.request;

import ru.alfabank.server.xml.message.BodyElement;

public class MblReqGetPagesList extends RequestWrapper {

    public static final String COMMAND = "GetPagesList";

    public static final String FLD_CATEGORY = "category";

    public MblReqGetPagesList(String language) {

        super(language);

        getHeader().setMethod(COMMAND);
    }

    public void setCategory(String category) {

        getBody().addChild(new BodyElement(FLD_CATEGORY, category));
    }
}
