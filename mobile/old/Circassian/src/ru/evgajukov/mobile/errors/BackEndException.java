package ru.evgajukov.mobile.errors;


/**
 * Исключение при ошибке работы с BE
 * 
 * @author Евгажуков Т.Х.
 * 
 */
public class BackEndException extends Throwable {

    private static final long serialVersionUID = 1L;

    private int errorCode = -100;

    public BackEndException() {

        super();
    }

    public BackEndException(Throwable e) {

        super(e);
    }

    public BackEndException(String msg) {

        super(msg);
    }

    public BackEndException(int errorCode, String msg) {

        super(msg);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {

        return errorCode;
    }
}
